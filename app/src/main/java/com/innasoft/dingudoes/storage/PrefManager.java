package com.innasoft.dingudoes.storage;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

import java.util.HashMap;

/**
 * Created by Ravi on 08/07/15.
 */
public class PrefManager {
    // Shared Preferences
    SharedPreferences pref;

    // Editor for Shared preferences
    Editor editor;

    // Context
    Context _context;

    // Shared pref mode
    int PRIVATE_MODE = 0;

    // Shared preferences file name
    private static final String PREF_NAME = "DinguDoes";

    // All Shared Preferences Keys
    private static final String KEY_IS_WAITING_FOR_SMS = "IsWaitingForSms";
    private static final String KEY_MOBILE_NUMBER = "mobile_number";
    private static final String KEY_IS_LOGGED_IN = "isLoggedIn";
    private static final String KEY_ID = "id";
    private static final String KEY_NAME = "name";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_MOBILE = "mobile";
    private static final String KEY_PROFILE_PIC = "profilepic";
    private static final String KEY_DEVICEID="deviceId";
    private static final String KEY_TOKEN="Token";
    private static final String KEY_GENDER="gender";
    private static final String IS_FIRST_TIME_LAUNCH = "IsFirstTimeLaunch";
    private static final String KEY_USERID = "userId";
    private static final String KEY_MOBILEVERIFIED = "mobileVerified";
    private static final String KEY_DATEOFBIRTH = "dateofbirth";
    private static final String KEY_ADDRESS = "address";
    private static final String KEY_REFERALCODE = "referalcode";



    public PrefManager(Context context) {
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void setIsWaitingForSms(boolean isWaiting) {
        editor.putBoolean(KEY_IS_WAITING_FOR_SMS, isWaiting);
        editor.commit();
    }

    public boolean isWaitingForSms() {
        return pref.getBoolean(KEY_IS_WAITING_FOR_SMS, false);
    }

    public void setMobileNumber(String mobileNumber) {
        editor.putString(KEY_MOBILE_NUMBER, mobileNumber);
        editor.commit();
    }

    public String getMobileNumber() {
        return pref.getString(KEY_MOBILE_NUMBER, null);
    }

    public void createLogin(String id, String name, String email, String mobile, String profilepic, String device_id, String token, String gender,String rcode) {
        editor.putString(KEY_ID, id);
        editor.putString(KEY_NAME, name);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_MOBILE, mobile);
        editor.putString(KEY_PROFILE_PIC, profilepic);
        editor.putString(KEY_DEVICEID,device_id);
        editor.putString(KEY_TOKEN,token);
        editor.putString(KEY_GENDER,gender);
        editor.putString(KEY_REFERALCODE,rcode);
        editor.putBoolean(KEY_IS_LOGGED_IN, true);

        editor.commit();
    }

    public void Login(String token,String f_name,String email,String mobile,String userid,String rcode){
        editor.putString(KEY_TOKEN, token);
        editor.putString(KEY_NAME, f_name);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_MOBILE, mobile);
        editor.putString(KEY_USERID, userid);
        editor.putString(KEY_REFERALCODE, rcode);

        editor.putBoolean(KEY_IS_LOGGED_IN, true);
        editor.commit();
    }

    public void profile(String mobile,String date,String address){
        editor.putString(KEY_MOBILEVERIFIED, mobile);
        editor.putString(KEY_DATEOFBIRTH, date);
        editor.putString(KEY_ADDRESS, address);

    }

    public boolean isLoggedIn() {
        return pref.getBoolean(KEY_IS_LOGGED_IN, false);
    }

    public void clearSession() {
        editor.clear();
        editor.commit();
    }

    public HashMap<String, String> getUserDetails() {
        HashMap<String, String> profile = new HashMap<>();
        profile.put("id", pref.getString(KEY_ID, null));
        profile.put("name", pref.getString(KEY_NAME, null));
        profile.put("email", pref.getString(KEY_EMAIL, null));
        profile.put("mobile", pref.getString(KEY_MOBILE, null));
        profile.put("profilepic", pref.getString(KEY_PROFILE_PIC, null));
        profile.put("deviceId",pref.getString(KEY_DEVICEID,null));
        profile.put("Token",pref.getString(KEY_TOKEN,null));
        profile.put("gender",pref.getString(KEY_GENDER,null));
        profile.put("userid",pref.getString(KEY_USERID,null));
        profile.put("mobileVerified",pref.getString(KEY_MOBILEVERIFIED,null));
        profile.put("dateofbirth",pref.getString(KEY_DATEOFBIRTH,null));
        profile.put("address",pref.getString(KEY_ADDRESS,null));
        profile.put("referalcode",pref.getString(KEY_REFERALCODE,null));


        return profile;
    }

   /* public void setFirstTimeLaunch(boolean isFirstTime) {
        editor.putBoolean(IS_FIRST_TIME_LAUNCH, isFirstTime);
        editor.commit();
    }



    public boolean isFirstTimeLaunch() {
        return pref.getBoolean(IS_FIRST_TIME_LAUNCH, true);
    }
*/
}
