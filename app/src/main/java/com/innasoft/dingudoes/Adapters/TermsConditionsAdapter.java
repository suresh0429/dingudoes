package com.innasoft.dingudoes.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.dingudoes.Activities.PaymentHistoryActivity;
import com.innasoft.dingudoes.Activities.TermsConditionsActivity;
import com.innasoft.dingudoes.Models.HistoryModel;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.TermsConditionsResponse;

import java.util.List;

public class TermsConditionsAdapter extends RecyclerView.Adapter<TermsConditionsAdapter.Holder> {
    List<TermsConditionsResponse.DataBean> dataBeanList;
    Context context;


    public TermsConditionsAdapter(List<TermsConditionsResponse.DataBean> dataBeanList, TermsConditionsActivity termsConditionsActivity) {
        this.dataBeanList=dataBeanList;
        this.context=termsConditionsActivity;
    }

    @NonNull
    @Override
    public TermsConditionsAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_terms, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TermsConditionsAdapter.Holder holder, int position) {
        holder.txt_data.setText(dataBeanList.get(position).getLookupValue());
      //  holder.txt_payment.setText(historyModels[position].getPayment());

    }

    @Override
    public int getItemCount() {
        return dataBeanList.size();
    }
    class Holder extends RecyclerView.ViewHolder{
        TextView txt_payment,txt_data,txt_hours,txt_name;

        public Holder(@NonNull View itemView) {
            super(itemView);

            txt_data=itemView.findViewById(R.id.txt_data);
//            txt_hours=itemView.findViewById(R.id.txt_hours);
//            txt_amount=itemView.findViewById(R.id.txt_amount);
//            txt_payment=itemView.findViewById(R.id.txt_payment);
        }
    }
}
