package com.innasoft.dingudoes.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.dingudoes.Models.MessageModel;
import com.innasoft.dingudoes.R;

public class MessagesRecyclerAdapter extends RecyclerView.Adapter<MessagesRecyclerAdapter.Holder> {
    private MessageModel[] messageModels;
    Context context;

    public MessagesRecyclerAdapter(MessageModel[] data, FragmentActivity activity) {
        this.messageModels=data;
        this.context=activity;
    }

    @NonNull
    @Override
    public MessagesRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_messages, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MessagesRecyclerAdapter.Holder holder, int position) {

        holder.txt_name.setText(messageModels[position].getTxt_name());
        holder.txt_area.setText(messageModels[position].getTxt_area());
        holder.txt_desc.setText(messageModels[position].getTxt_desc());

    }

    @Override
    public int getItemCount() {
        return messageModels.length;
    }
    public class Holder extends RecyclerView.ViewHolder{
        TextView txt_desc,txt_area,txt_name;

        public Holder(@NonNull View itemView) {
            super(itemView);

            txt_name=itemView.findViewById(R.id.txt_title);
            txt_area=itemView.findViewById(R.id.txt_area);
            txt_desc=itemView.findViewById(R.id.txt_desc);
        }
    }
}
