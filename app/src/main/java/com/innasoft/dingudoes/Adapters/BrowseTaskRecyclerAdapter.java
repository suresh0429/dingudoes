package com.innasoft.dingudoes.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.innasoft.dingudoes.Activities.PostTaskActivity;
import com.innasoft.dingudoes.Activities.TaskDetailsActivity;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.SearchTaskResponse;
import com.innasoft.dingudoes.storage.PrefManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.innasoft.dingudoes.Singleton.AppController.TAG;
import static com.innasoft.dingudoes.Utilities.utility.capitalize;

public class BrowseTaskRecyclerAdapter extends RecyclerView.Adapter<BrowseTaskRecyclerAdapter.Holder>{


    List<SearchTaskResponse.DataBean> dataBeanList;
    Context context;
    PrefManager prefManager;
    String token,taskid;

    public BrowseTaskRecyclerAdapter(List<SearchTaskResponse.DataBean> dataBeanList, FragmentActivity activity) {
        this.dataBeanList=dataBeanList;
        this.context=activity;
    }

    public List<SearchTaskResponse.DataBean> getMovies() {
        return dataBeanList;
    }

    public void setMovies(List<SearchTaskResponse.DataBean> movies) {
        this.dataBeanList = movies;
    }

    @NonNull
    @Override
    public BrowseTaskRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_browsetasks, parent, false);

        return new Holder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull BrowseTaskRecyclerAdapter.Holder holder, int position) {

        String firstLetter = " ";
        ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
        // generate random color
        int color = generator.getRandomColor();
        if (dataBeanList.get(position).getTitle() != null) {
            firstLetter = capitalize(dataBeanList.get(position).getTitle()).substring(0, 1);
            holder.txt_name.setText(capitalize(dataBeanList.get(position).getTitle()));
        }
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .width(40)  // width in px
                .height(40) // height in px
                .endConfig()
                .buildRoundRect(firstLetter, color,5);

        holder.image_thumbnail.setImageDrawable(drawable);
        holder.txt_name.setText(capitalize(dataBeanList.get(position).getTitle()));
        holder.txt_amount.setText("\u20B9"+dataBeanList.get(position).getBudget());
        holder.txt_area.setText(dataBeanList.get(position).getAddress());
        holder.txt_date.setText(dataBeanList.get(position).getFinishDate());
        holder.txt_time.setText(dataBeanList.get(position).getFinishTime());
        holder.txt_category.setText(capitalize(dataBeanList.get(position).getCategory()));
        if (dataBeanList.get(position).getStatus().equals("assigned")){
            holder.txt_status.setTextColor(Color.RED);

        }
        if (dataBeanList.get(position).getStatus().equals("open")){
            holder.txt_status.setTextColor(Color.parseColor("#43A047"));

        }

        holder.txt_status.setText(dataBeanList.get(position).getStatus());
        holder.txt_status.setAnimation(AnimationUtils.loadAnimation(context, R.anim.leave));

        holder.itemView.setOnClickListener(view -> context.startActivity(new Intent(context, TaskDetailsActivity.class)));

        prefManager = new PrefManager(context);
        HashMap<String, String> profile = prefManager.getUserDetails();
        token = profile.get("Token");

        Log.e(TAG, "onBindViewHolder:"+token );




        holder.parentCard.setOnClickListener(v -> {
            taskid=String.valueOf(dataBeanList.get(position).getId());

            Intent intent=new Intent(context,TaskDetailsActivity.class);
            intent.putExtra("Task_id",dataBeanList.get(position).getId());
            context.startActivity(intent);

        });

        if (dataBeanList.get(position).getStatus().equalsIgnoreCase("assigned")){
            holder.btnQoute.setEnabled(false);
            holder.btnQoute.setAlpha(0.5f);
        }else {
            holder.btnQoute.setEnabled(true);
            holder.btnQoute.setVisibility(View.VISIBLE);
        }
        holder.btnQoute.setOnClickListener(v -> {
            taskid=String.valueOf(dataBeanList.get(position).getId());

            Intent intent=new Intent(context,TaskDetailsActivity.class);
            intent.putExtra("Task_id",dataBeanList.get(position).getId());
            context.startActivity(intent);

        });

       /* holder.btnpostSimilar.setOnClickListener(view -> {

        });*/
    }

    @Override
    public int getItemCount() {
        return dataBeanList.size();
    }

    public class Holder extends RecyclerView.ViewHolder{
        TextView txt_area,txt_date,txt_time,txt_amount,txt_status,txt_name,txt_category;
        ImageView image_thumbnail;
        Button btnpostSimilar,btnQoute;
        CardView parentCard;

        public Holder(@NonNull View itemView) {
            super(itemView);
            txt_amount=itemView.findViewById(R.id.txt_amount);
            txt_area=itemView.findViewById(R.id.txt_area);
            txt_date=itemView.findViewById(R.id.txt_date);
            txt_time=itemView.findViewById(R.id.txt_time);
            txt_status=itemView.findViewById(R.id.txtStatus);
            txt_name=itemView.findViewById(R.id.txt_title);
            txt_category=itemView.findViewById(R.id.txt_category);

            image_thumbnail=itemView.findViewById(R.id.image_thumbnail);
           // btnpostSimilar=itemView.findViewById(R.id.btnpostSimilar);
            btnQoute=itemView.findViewById(R.id.btnQoute);
            parentCard=itemView.findViewById(R.id.parentCard);


        }
    }

    public void updateList(List<SearchTaskResponse.DataBean> list) {
        dataBeanList = list;
        notifyDataSetChanged();
    }
}
