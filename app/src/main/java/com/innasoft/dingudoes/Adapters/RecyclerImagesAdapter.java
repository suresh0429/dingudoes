package com.innasoft.dingudoes.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.dingudoes.Activities.TaskAlertActivity;
import com.innasoft.dingudoes.Activities.TaskDetailsActivity;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.GetTaskAlertResponse;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RecyclerImagesAdapter extends RecyclerView.Adapter<RecyclerImagesAdapter.Holder> {
    String[] dataBeanList;
    Context context;


    public RecyclerImagesAdapter(String[] dataBeanList, TaskDetailsActivity termsConditionsActivity) {
        this.dataBeanList=dataBeanList;
        this.context=termsConditionsActivity;
    }

    @NonNull
    @Override
    public RecyclerImagesAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_images, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerImagesAdapter.Holder holder, int position) {

        Picasso.get().load("http://35.154.13.209:8080/"+dataBeanList[position]).error(R.mipmap.ic_launcher_round).into(holder.img_categroy);




    }

    @Override
    public int getItemCount() {
        return dataBeanList.length;
    }
    class Holder extends RecyclerView.ViewHolder{
        TextView txt_title,txt_distance;
        ImageView img_categroy;

        public Holder(@NonNull View itemView) {
            super(itemView);
            img_categroy=itemView.findViewById(R.id.img_categroy);
        }
    }
}
