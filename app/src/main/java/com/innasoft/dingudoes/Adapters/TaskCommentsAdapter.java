package com.innasoft.dingudoes.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.dingudoes.Activities.ProfileActivity;
import com.innasoft.dingudoes.Activities.TaskDetailsActivity;
import com.innasoft.dingudoes.Fragments.CustomBottomSheetDialogFragment;
import com.innasoft.dingudoes.Models.CommentsModel;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.GetTaskCommentResponse;
import com.innasoft.dingudoes.storage.PrefManager;

import java.util.HashMap;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static androidx.constraintlayout.widget.Constraints.TAG;
import static com.facebook.FacebookSdk.getApplicationContext;

public class TaskCommentsAdapter extends RecyclerView.Adapter<TaskCommentsAdapter.Holder> {
    List<CommentsModel> commentsBeanList;
    Context context;

    public TaskCommentsAdapter(List<CommentsModel> commentsBeanList, FragmentActivity activity) {
        this.commentsBeanList=commentsBeanList;
        this.context=activity;
    }

    @NonNull
    @Override
    public TaskCommentsAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_comment, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TaskCommentsAdapter.Holder holder, int position) {

      //  holder.txt_name.setText(capitalize(dataBeanList.get(position).getTitle()));
        holder.txt_desc.setText(commentsBeanList.get(position).getCommentText());
        holder.txt_name.setText(commentsBeanList.get(position).getFullName());
        holder.circleImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                PrefManager prefManager = new PrefManager(getApplicationContext());
                HashMap<String, String> profile = prefManager.getUserDetails();
                String token = profile.get("Token");
                String email = profile.get("email");
                //String userId = profile.get("userid");

                Intent intent=new Intent(context, ProfileActivity.class);
                intent.putExtra("user_id",String.valueOf(commentsBeanList.get(position).getUserId()));
                intent.putExtra("email",email);
                intent.putExtra("token",token);
                intent.putExtra("fromTask",true);
                context.startActivity(intent);
            }
        });

        holder.linear_reply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "onClick: "+commentsBeanList.get(position).getParentId());

                    Bundle bundle = new Bundle();
                    bundle.putString("id", String.valueOf(commentsBeanList.get(position).getId()));
                   // bundle.putString("parent_id", String.valueOf(commentsBeanList.get(position).getParentId()));
                    bundle.putInt("task_id", commentsBeanList.get(position).getTaskId());

                    CustomBottomSheetDialogFragment newFragment = new CustomBottomSheetDialogFragment();
                    newFragment.show(((AppCompatActivity)context).getSupportFragmentManager(), "Dialog");
                    newFragment.setArguments(bundle);



            }
        });
    }

    @Override
    public int getItemCount() {
        return commentsBeanList.size();
    }

    public class Holder extends RecyclerView.ViewHolder{
        TextView txt_name,txt_desc;
        CircleImageView circleImageView;
        Button btn_quote;
        LinearLayout linear_reply;

        public Holder(@NonNull View itemView) {
            super(itemView);
            txt_name=itemView.findViewById(R.id.txt_name);
            txt_desc=itemView.findViewById(R.id.txt_desc);
            circleImageView=itemView.findViewById(R.id.image);
            linear_reply=itemView.findViewById(R.id.linear_reply);

        }
    }
}
