package com.innasoft.dingudoes.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.innasoft.dingudoes.Activities.MyTaskDetailsActivity;
import com.innasoft.dingudoes.Activities.PostTaskActivity;
import com.innasoft.dingudoes.Activities.TaskDetailsActivity;
import com.innasoft.dingudoes.Db.SqliteDatabase;
import com.innasoft.dingudoes.Models.TaskDraftModel;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.GetPostTaskResponse;

import java.util.ArrayList;
import java.util.List;

import static com.innasoft.dingudoes.Utilities.utility.capitalize;

public class TaskDraftAdapter extends RecyclerView.Adapter<TaskDraftAdapter.Holder>{

    List<TaskDraftModel> dataBeanList;
    Context context;
    private SqliteDatabase mDatabase;

    public TaskDraftAdapter(List<TaskDraftModel> dataBeanList, FragmentActivity activity) {
        this.dataBeanList=dataBeanList;
        this.context=activity;
        mDatabase = new SqliteDatabase(context);
    }

    @NonNull
    @Override
    public TaskDraftAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_drafts, parent, false);
        return new TaskDraftAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TaskDraftAdapter.Holder holder, int position) {
        String firstLetter = " ";
        ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
        // generate random color
        int color = generator.getRandomColor();
        if (dataBeanList.get(position).getTitle() != null && !dataBeanList.get(position).getTitle().equalsIgnoreCase("")) {
            firstLetter = capitalize(dataBeanList.get(position).getTitle()).substring(0, 1);
            holder.txt_name.setText(capitalize(dataBeanList.get(position).getTitle()));
        }
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .width(40)  // width in px
                .height(40) // height in px
                .endConfig()
                .buildRoundRect(firstLetter, color,5);

        holder.image_thumbnail.setImageDrawable(drawable);

        holder.txt_amount.setText("\u20B9"+dataBeanList.get(position).getPrice());
        holder.txt_area.setText(dataBeanList.get(position).getLocation());
        holder.txt_date.setText(dataBeanList.get(position).getDateString());
        holder.txt_time.setText(dataBeanList.get(position).getTime());
        holder.txt_status.setText("Draft");
        holder.txt_status.setTextColor(Color.parseColor("#FF7308"));
        holder.txt_status.setAnimation(AnimationUtils.loadAnimation(context, R.anim.leave));


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, PostTaskActivity.class);
                intent.putExtra("title", dataBeanList.get(position).getTitle());
                intent.putExtra("description", dataBeanList.get(position).getDesc());
                intent.putExtra("category", dataBeanList.get(position).getCategory());
                intent.putExtra("isRemote", String.valueOf(dataBeanList.get(position).getIsRemote()));
                intent.putExtra("address", dataBeanList.get(position).getLocation());
                intent.putExtra("budgetType", dataBeanList.get(position).getBudgetType());
                intent.putExtra("budget", dataBeanList.get(position).getPrice());
                intent.putExtra("taskHours", dataBeanList.get(position).getHours());
                intent.putExtra("taskers", String.valueOf(dataBeanList.get(position).getTaskers()));
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);


            }
        });

        holder.imgDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mDatabase.deleteContact(dataBeanList.get(position).getId());

                // These two lines added for remove data from adapter also.
                dataBeanList.remove(position);
                notifyDataSetChanged();

                //refresh the activity page.
               // ((Activity)context).finish();
              //  context.startActivity(((Activity) context).getIntent());
            }
        });

    }

    @Override
    public int getItemCount() {
        return dataBeanList.size();
    }

    public class Holder extends RecyclerView.ViewHolder{
        TextView txt_area,txt_date,txt_time,txt_amount,txt_status,txt_name;
        ImageView image_thumbnail,imgDelete;

        public Holder(@NonNull View itemView) {
            super(itemView);
            txt_amount=itemView.findViewById(R.id.txt_amount);
            txt_area=itemView.findViewById(R.id.txt_area);
            txt_date=itemView.findViewById(R.id.txt_date);
            txt_time=itemView.findViewById(R.id.txt_time);
            txt_status=itemView.findViewById(R.id.txtStatus);
            txt_name=itemView.findViewById(R.id.txt_title);
            image_thumbnail=itemView.findViewById(R.id.image_thumbnail);
            imgDelete=itemView.findViewById(R.id.imgDelete);

            //   btn_quote=itemView.findViewById(R.id.btn_quote);


        }
    }
}
