package com.innasoft.dingudoes.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.dingudoes.Activities.PendingReviewsActivity;
import com.innasoft.dingudoes.Activities.ReviewActity;
import com.innasoft.dingudoes.Activities.TaskAlertActivity;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.GetTaskAlertResponse;
import com.innasoft.dingudoes.Response.PendingReviewsResponse;

import java.util.List;

public class PendingReviewsAdapter extends RecyclerView.Adapter<PendingReviewsAdapter.Holder> {
    List<PendingReviewsResponse.DataBean> dataBeanList;
    Context context;


    public PendingReviewsAdapter(List<PendingReviewsResponse.DataBean> dataBeanList, PendingReviewsActivity termsConditionsActivity) {
        this.dataBeanList=dataBeanList;
        this.context=termsConditionsActivity;
    }

    @NonNull
    @Override
    public PendingReviewsAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_pendingreview, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PendingReviewsAdapter.Holder holder, int position) {
        holder.txt_title.setText(dataBeanList.get(position).getTitle());
        holder.txt_amount.setText("\u20B9"+dataBeanList.get(position).getBudget()+"/-");
        holder.txt_category.setText(dataBeanList.get(position).getCategory());
        holder.txt_area.setText(dataBeanList.get(position).getAddress());
        holder.txt_date.setText(dataBeanList.get(position).getFinishDate());
        holder.txt_time.setText(dataBeanList.get(position).getFinishTime());
        holder.txtStatus.setText(dataBeanList.get(position).getStatus());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String id=String.valueOf(dataBeanList.get(position).getId());

                Intent intent=new Intent(context, ReviewActity.class);
                intent.putExtra("TaskId",id);
                intent.putExtra("Activity","PendingReviewsActivity");
               // intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataBeanList.size();
    }
    class Holder extends RecyclerView.ViewHolder{
        TextView txtStatus,txt_title,txt_category,txt_area,txt_date,txt_time,txt_amount;

        public Holder(@NonNull View itemView) {
            super(itemView);

            txt_title=itemView.findViewById(R.id.txt_title);
            txt_amount=itemView.findViewById(R.id.txt_amount);
            txt_category=itemView.findViewById(R.id.txt_category);
            txtStatus=itemView.findViewById(R.id.txtStatus);
            txt_area=itemView.findViewById(R.id.txt_area);
            txt_date=itemView.findViewById(R.id.txt_date);
            txt_time=itemView.findViewById(R.id.txt_time);
        }
    }
}
