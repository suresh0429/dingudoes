package com.innasoft.dingudoes.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.dingudoes.Activities.PaymentHistoryActivity;
import com.innasoft.dingudoes.Models.HistoryModel;
import com.innasoft.dingudoes.R;

public class PaymentOutAdapter extends RecyclerView.Adapter<PaymentOutAdapter.Holder> {
    HistoryModel[] historyModels;
    Context context;

    public PaymentOutAdapter(HistoryModel[] historyModels, PaymentHistoryActivity paymentHistoryActivity) {
        this.historyModels=historyModels;
        this.context=paymentHistoryActivity;
    }

    @NonNull
    @Override
    public PaymentOutAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_paid, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull PaymentOutAdapter.Holder holder, int position) {
        holder.txt_amount.setText(historyModels[position].getAmount());
        holder.txt_payment.setText(historyModels[position].getPayment());

    }

    @Override
    public int getItemCount() {
        return historyModels.length;
    }
    class Holder extends RecyclerView.ViewHolder{
        TextView txt_payment,txt_amount,txt_hours,txt_name;

        public Holder(@NonNull View itemView) {
            super(itemView);

            txt_name=itemView.findViewById(R.id.txt_title);
            txt_hours=itemView.findViewById(R.id.txt_hours);
            txt_amount=itemView.findViewById(R.id.txt_amount);
            txt_payment=itemView.findViewById(R.id.txt_payment);
        }
    }
}
