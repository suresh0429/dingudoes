package com.innasoft.dingudoes.Adapters;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.dingudoes.Activities.MyTaskDetailsActivity;
import com.innasoft.dingudoes.Activities.PostTaskActivity;
import com.innasoft.dingudoes.Fragments.HomeFragment;

import com.innasoft.dingudoes.Models.HomeCategoryModel;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.HomeCategoriesResponse;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.innasoft.dingudoes.Apis.RetrofitClient.IMAGE_BASE_URL;

public class CategoriesRecyclerAdapter extends RecyclerView.Adapter<CategoriesRecyclerAdapter.Holder> {
    private static final String TAG = "TAG";
    List<HomeCategoriesResponse.DataBean> dataBeans;
  //  List<HomeCategoryModel> dataBeans;
    Context context;

    public CategoriesRecyclerAdapter(List<HomeCategoriesResponse.DataBean> dataBeans, Context homeFragment) {
        this.dataBeans = dataBeans;
        this.context = homeFragment;

    }

    /*public CategoriesRecyclerAdapter(List<HomeCategoryModel> dataBeans, Context context) {
        this.dataBeans = dataBeans;
        this.context = context;
    }*/

    @NonNull
    @Override
    public CategoriesRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_home_category, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoriesRecyclerAdapter.Holder holder, int position) {

        holder.category_title.setText(dataBeans.get(position).getName());

        Picasso.get().load(IMAGE_BASE_URL + dataBeans.get(position).getImageUrl()).into(holder.img_categroy);
        Log.d(TAG, "onBindViewHolder: "+IMAGE_BASE_URL + dataBeans.get(position).getImageUrl());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(context,PostTaskActivity.class);
                intent.putExtra("category",dataBeans.get(position).getName());
                context.startActivity(intent);

            }
        });

        /*holder.category_title.setText(dataBeans.get(position).getCateg_name());

        Picasso.get().load(IMAGE_BASE_URL + "/resources/painting.png").into(holder.img_categroy);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(context,PostTaskActivity.class);
                intent.putExtra("category",dataBeans.get(position).getCateg_name());
                context.startActivity(intent);

            }
        });*/

    }

    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        public ImageView img_categroy;
        public CardView frame_week;
        public TextView category_title;

        public Holder(@NonNull View itemView) {
            super(itemView);
            img_categroy = itemView.findViewById(R.id.img_categroy);
            category_title = itemView.findViewById(R.id.category_title);
            frame_week = itemView.findViewById(R.id.frame_week);

        }
    }
}
