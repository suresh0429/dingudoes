package com.innasoft.dingudoes.Adapters;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.dingudoes.Fragments.CustomBottomSheetDialogFragment;
import com.innasoft.dingudoes.Models.CommentsModel;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.CommentsParentResponse;

import java.util.ArrayList;

public class TaskParentCommentsAdapter extends RecyclerView.Adapter<TaskParentCommentsAdapter.Holder> {
    ArrayList<CommentsModel> dataBeanList;
    Context context;

    public TaskParentCommentsAdapter(ArrayList<CommentsModel> dataBeanList,Context context) {
        this.dataBeanList=dataBeanList;
        this.context=context;
    }

    @NonNull
    @Override
    public TaskParentCommentsAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_parentcomment, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TaskParentCommentsAdapter.Holder holder, int position) {

      //  holder.txt_name.setText(capitalize(dataBeanList.get(position).getTitle()));
        holder.txt_desc.setText(dataBeanList.get(position).getCommentText());
        holder.txt_name.setText(dataBeanList.get(position).getFullName());


        String task_id=String.valueOf(dataBeanList.get(position).getId());


//        holder.linear_reply.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                Bundle bundle=new Bundle();
//                bundle.putString("parent_id",String.valueOf(dataBeanList.get(position).getParentid()));
//                bundle.putString("task_id",task_id);
//
//
//                CustomBottomSheetDialogFragment newFragment = new CustomBottomSheetDialogFragment();
//                newFragment.show(((AppCompatActivity)context).getSupportFragmentManager(), "Dialog");
//                newFragment.setArguments(bundle);
//
//            }
//        });
    }

    @Override
    public int getItemCount() {
        return dataBeanList.size();
    }

    public class Holder extends RecyclerView.ViewHolder{
        TextView txt_name,txt_desc;

        public Holder(@NonNull View itemView) {
            super(itemView);
            txt_name=itemView.findViewById(R.id.txt_name);
            txt_desc=itemView.findViewById(R.id.txt_desc);


        }
    }
}
