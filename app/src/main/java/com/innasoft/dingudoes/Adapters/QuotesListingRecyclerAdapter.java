package com.innasoft.dingudoes.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.dingudoes.Activities.MyTaskDetailsActivity;
import com.innasoft.dingudoes.Interface.AcceptListner;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.MyTaskDetailsResponse;

import java.util.List;

public class QuotesListingRecyclerAdapter extends RecyclerView.Adapter<QuotesListingRecyclerAdapter.Holder> {
    List<MyTaskDetailsResponse.DataBean.QuotesBean> quotesBeanList;
    Context context;
    String Token,UserId;
    String budget;
    private AcceptListner acceptListner;
    public QuotesListingRecyclerAdapter(List<MyTaskDetailsResponse.DataBean.QuotesBean> quotesBeanList, Context myTaskDetailsActivity, String token, String userId, String budget, AcceptListner acceptListner) {
        this.quotesBeanList=quotesBeanList;
        this.context=myTaskDetailsActivity;
        this.Token=token;
        this.UserId=userId;
        this.budget=budget;
        this.acceptListner=acceptListner;
    }

    @NonNull
    @Override
    public QuotesListingRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_quoteslist, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull QuotesListingRecyclerAdapter.Holder holder, int position) {

        holder.txt_name.setText(quotesBeanList.get(position).getCreatedAt());
        holder.txt_price.setText(budget);

        String bt=String.valueOf(quotesBeanList.get(position).getIsAccepted());

        if (bt.equals("1")){
            holder.btn_accept.setAlpha(.5f);
            holder.btn_accept.setEnabled(false);
            /*holder.btn_accept.setBackgroundColor(Color.parseColor("#fffff"));*/
        }
        else {
            holder.btn_accept.setVisibility(View.VISIBLE);
        }

        holder.btn_accept.setOnClickListener(v ->
                acceptListner.onClick(position,quotesBeanList,holder.btn_accept));

    }

    @Override
    public int getItemCount() {
        return quotesBeanList.size();
    }
    public class Holder extends RecyclerView.ViewHolder{

        TextView txt_price,txt_name;
        Button btn_accept;

        public Holder(@NonNull View itemView) {
            super(itemView);
            txt_name=itemView.findViewById(R.id.txt_title);
            txt_price=itemView.findViewById(R.id.txt_price);

            btn_accept=itemView.findViewById(R.id.btn_accept);

        }
    }
}
