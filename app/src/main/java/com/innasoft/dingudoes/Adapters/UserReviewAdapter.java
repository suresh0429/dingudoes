package com.innasoft.dingudoes.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.widget.AppCompatRatingBar;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.dingudoes.Activities.TaskAlertActivity;
import com.innasoft.dingudoes.Models.UserReviewModel;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.GetTaskAlertResponse;
import com.innasoft.dingudoes.Response.GetUserReviewResponse;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.innasoft.dingudoes.Utilities.utility.capitalize;

public class UserReviewAdapter extends RecyclerView.Adapter<UserReviewAdapter.Holder> {
    List<UserReviewModel> dataBeanList;
    Context context;


    public UserReviewAdapter(List<UserReviewModel> dataBeanList, Context context) {
        this.dataBeanList = dataBeanList;
        this.context = context;
    }

    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_user_reviews, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {
        if (dataBeanList.get(position).getReviewText() != null) {
            holder.txtReview.setText(capitalize(dataBeanList.get(position).getReviewText()));
        }
        holder.ratingbar.setRating((float) dataBeanList.get(position).getRating());

    }

    @Override
    public int getItemCount() {
        return dataBeanList.size();
    }

   public class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.txtReview)
        TextView txtReview;
        @BindView(R.id.ratingbar)
        AppCompatRatingBar ratingbar;


        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);
        }
    }
}
