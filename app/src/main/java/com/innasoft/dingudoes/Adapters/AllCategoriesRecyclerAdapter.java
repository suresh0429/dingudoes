package com.innasoft.dingudoes.Adapters;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.dingudoes.Activities.PostTaskActivity;
import com.innasoft.dingudoes.Models.HomeCategoryModel;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.HomeCategoriesResponse;
import com.squareup.picasso.Picasso;

import java.util.List;

import static com.innasoft.dingudoes.Apis.RetrofitClient.IMAGE_BASE_URL;

public class AllCategoriesRecyclerAdapter extends RecyclerView.Adapter<AllCategoriesRecyclerAdapter.Holder> {
    List<HomeCategoryModel> dataBeans;
    Context context;

    public AllCategoriesRecyclerAdapter(List<HomeCategoryModel> dataBeans, Context homeFragment) {
        this.dataBeans = dataBeans;
        this.context = homeFragment;

    }

    @NonNull
    @Override
    public AllCategoriesRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_home_allcategory, parent, false);
        return new AllCategoriesRecyclerAdapter.Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull AllCategoriesRecyclerAdapter.Holder holder, int position) {

        holder.category_title.setText(dataBeans.get(position).getCateg_name());

       // Picasso.get().load(IMAGE_BASE_URL + dataBeans.get(position).getImageUrl()).into(holder.img_categroy);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent=new Intent(context,PostTaskActivity.class);
                intent.putExtra("category",dataBeans.get(position).getCateg_name());
                context.startActivity(intent);

            }
        });

    }

    @Override
    public int getItemCount() {
        return dataBeans.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        public TextView category_title;

        public Holder(@NonNull View itemView) {
            super(itemView);
            category_title = itemView.findViewById(R.id.category_title);


        }
    }
}
