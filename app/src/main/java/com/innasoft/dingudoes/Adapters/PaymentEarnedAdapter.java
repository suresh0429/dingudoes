package com.innasoft.dingudoes.Adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.dingudoes.Activities.PaymentHistoryActivity;
import com.innasoft.dingudoes.Models.HistoryModel;

public class PaymentEarnedAdapter extends RecyclerView.Adapter<PaymentEarnedAdapter.Holder> {
    HistoryModel[] historyModels;
    Context context;

    public PaymentEarnedAdapter(HistoryModel[] historyModels, PaymentHistoryActivity paymentHistoryActivity) {
        this.historyModels=historyModels;
        this.context=paymentHistoryActivity;
    }

    @NonNull
    @Override
    public PaymentEarnedAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return null;
    }

    @Override
    public void onBindViewHolder(@NonNull PaymentEarnedAdapter.Holder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return historyModels.length;
    }
    class Holder extends RecyclerView.ViewHolder{

        public Holder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
