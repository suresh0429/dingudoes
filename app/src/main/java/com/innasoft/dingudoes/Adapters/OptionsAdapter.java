package com.innasoft.dingudoes.Adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.dingudoes.Activities.ChattingActivity;
import com.innasoft.dingudoes.Activities.FeedBackActivity;
import com.innasoft.dingudoes.Activities.ProfileActivity;
import com.innasoft.dingudoes.Activities.DashBoardActivity;
import com.innasoft.dingudoes.Activities.HelpActivity;
import com.innasoft.dingudoes.Activities.PaymentHistoryActivity;
import com.innasoft.dingudoes.Activities.PendingReviewsActivity;
import com.innasoft.dingudoes.Activities.ReferAndEarnActivity;
import com.innasoft.dingudoes.Activities.SettingsActivity;
import com.innasoft.dingudoes.Activities.WalletActivity;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.OptionsModel;
import com.innasoft.dingudoes.storage.PrefManager;

import java.util.HashMap;

import static com.facebook.FacebookSdk.getApplicationContext;

public class OptionsAdapter extends RecyclerView.Adapter<OptionsAdapter.Holder> {
    OptionsModel[] optionsModels;
    Context context;


    public OptionsAdapter(OptionsModel[] optionsModels, FragmentActivity termsConditionsActivity) {
        this.optionsModels=optionsModels;
        this.context=termsConditionsActivity;
    }

    @NonNull
    @Override
    public OptionsAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_options, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OptionsAdapter.Holder holder, int position) {
        holder.category_title.setText(optionsModels[position].getCateg_name());

        holder.img_categroy.setImageResource(optionsModels[position].getCateg_image());

        holder.itemView.setOnClickListener(v -> {

            if(position==0)
            {
                context.startActivity(new Intent(context, DashBoardActivity.class));
            }
            else if(position==1)
            {

                PrefManager prefManager = new PrefManager(getApplicationContext());
                HashMap<String, String> profile = prefManager.getUserDetails();
                String token = profile.get("Token");
                String email = profile.get("email");
                String userId = profile.get("userid");

                Intent intent=new Intent(context, ProfileActivity.class);
                intent.putExtra("user_id",userId);
                intent.putExtra("email",email);
                intent.putExtra("token",token);
                context.startActivity(intent);

            }
            else if(position==2)
            {
                context.startActivity(new Intent(context, PaymentHistoryActivity.class));
            }
            else if(position==3)
            {
                context.startActivity(new Intent(context, PendingReviewsActivity.class));
            }
            else if (position==4)
            {
                context.startActivity(new Intent(context, SettingsActivity.class));
            }
            else if (position==5)
            {
                context.startActivity(new Intent(context, HelpActivity.class));
            }
            else if (position==6)
            {
                context.startActivity(new Intent(context, ReferAndEarnActivity.class));
            }

            else if (position==7)
            {
               context.startActivity(new Intent(context, FeedBackActivity.class));
            }
            else if (position==8)
            {
                  context.startActivity(new Intent(context, WalletActivity.class));
            }
            else if (position==9)
            {
                context.startActivity(new Intent(context, ChattingActivity.class));
            }

        });

    }

    @Override
    public int getItemCount() {
        return optionsModels.length;
    }
    class Holder extends RecyclerView.ViewHolder{
        TextView category_title,txt_distance;
        ImageView img_categroy;
        public Holder(@NonNull View itemView) {
            super(itemView);

            category_title=itemView.findViewById(R.id.category_title);
            img_categroy=itemView.findViewById(R.id.img_categroy);
        }

    }
}
