package com.innasoft.dingudoes.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.innasoft.dingudoes.Models.WalletModel;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.SearchTaskResponse;
import com.innasoft.dingudoes.storage.PrefManager;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import static com.innasoft.dingudoes.Utilities.utility.capitalize;

public class WalletAdapter extends RecyclerView.Adapter<WalletAdapter.Holder> {


    List<WalletModel> dataBeanList;
    Context context;


    public WalletAdapter(List<WalletModel> dataBeanList, FragmentActivity activity) {
        this.dataBeanList = dataBeanList;
        this.context = activity;
    }



    @NonNull
    @Override
    public Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_wallet, parent, false);

        return new Holder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull Holder holder, int position) {

        String firstLetter = " ";
        ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
        // generate random color
        int color = generator.getRandomColor();
        if (dataBeanList.get(position).getName() != null) {
            firstLetter = capitalize(dataBeanList.get(position).getName()).substring(0, 1);
            holder.txtName.setText(capitalize(dataBeanList.get(position).getName()));
        }
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .width(40)  // width in px
                .height(40) // height in px
                .endConfig()
                .buildRoundRect(firstLetter, color, 5);

        holder.imageThumbnail.setImageDrawable(drawable);
        holder.txtDate.setText(dataBeanList.get(position).getDate());


    }

    @Override
    public int getItemCount() {
        return dataBeanList.size();
    }

    public class Holder extends RecyclerView.ViewHolder {
        @BindView(R.id.image_thumbnail)
        ImageView imageThumbnail;
        @BindView(R.id.txtName)
        TextView txtName;
        @BindView(R.id.txt_date)
        TextView txtDate;
        public Holder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this,itemView);


        }
    }

}
