package com.innasoft.dingudoes.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.dingudoes.Activities.NotificationsActivity;
import com.innasoft.dingudoes.Activities.TaskAlertActivity;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.GetNotificationsResponse;
import com.innasoft.dingudoes.Response.GetTaskAlertResponse;

import java.util.List;

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.Holder> {
    List<GetNotificationsResponse.DataBean> dataBeanList;
    Context context;



    public NotificationsAdapter(List<GetNotificationsResponse.DataBean> getNotificationsResponseList, FragmentActivity activity) {
        this.dataBeanList=getNotificationsResponseList;
        this.context=activity;
    }

    @NonNull
    @Override
    public NotificationsAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_notifications, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationsAdapter.Holder holder, int position) {

        for (int i=0;i<dataBeanList.size();i++){
            GetNotificationsResponse.DataBean.TaskBean taskBean=dataBeanList.get(i).getTask();

            holder.txt_title.setText(taskBean.getTitle());
            holder.txt_decs.setText(taskBean.getDescription());
        }



    }

    @Override
    public int getItemCount() {
        return dataBeanList.size();
    }
    class Holder extends RecyclerView.ViewHolder{
        TextView txt_title,txt_decs;

        public Holder(@NonNull View itemView) {
            super(itemView);

            txt_title=itemView.findViewById(R.id.txt_title);
            txt_decs=itemView.findViewById(R.id.txt_decs);
        }
    }
}
