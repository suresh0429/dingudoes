package com.innasoft.dingudoes.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.dingudoes.Activities.PrivacyPolicyActivity;
import com.innasoft.dingudoes.Activities.TaskAlertActivity;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.GetTaskAlertResponse;
import com.innasoft.dingudoes.Response.PrivacyPolicyResponse;

import java.util.List;

public class TaskAlertsAdapter extends RecyclerView.Adapter<TaskAlertsAdapter.Holder> {
    List<GetTaskAlertResponse.DataBean.TasksAlertsBean> dataBeanList;
    Context context;


    public TaskAlertsAdapter(List<GetTaskAlertResponse.DataBean.TasksAlertsBean> dataBeanList, TaskAlertActivity termsConditionsActivity) {
        this.dataBeanList=dataBeanList;
        this.context=termsConditionsActivity;
    }

    @NonNull
    @Override
    public TaskAlertsAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_alerts, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TaskAlertsAdapter.Holder holder, int position) {
        holder.txt_title.setText(dataBeanList.get(position).getKeywords());
        holder.txt_distance.setText("in "+dataBeanList.get(position).getLocation()+","+dataBeanList.get(position).getDistance()+"Km"+" radius");

    }

    @Override
    public int getItemCount() {
        return dataBeanList.size();
    }
    class Holder extends RecyclerView.ViewHolder{
        TextView txt_title,txt_distance;

        public Holder(@NonNull View itemView) {
            super(itemView);

            txt_title=itemView.findViewById(R.id.txt_title);
            txt_distance=itemView.findViewById(R.id.txt_distance);
        }
    }
}
