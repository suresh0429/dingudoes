package com.innasoft.dingudoes.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.amulyakhare.textdrawable.TextDrawable;
import com.amulyakhare.textdrawable.util.ColorGenerator;
import com.innasoft.dingudoes.Activities.MyTaskDetailsActivity;
import com.innasoft.dingudoes.Activities.OngoingMyTaskDetailsActivity;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.OnGoingTaskResponse;

import java.util.List;

import static com.innasoft.dingudoes.Utilities.utility.capitalize;

public class OngoingTaskRecyclerAdapter extends RecyclerView.Adapter<OngoingTaskRecyclerAdapter.Holder> {
    List<OnGoingTaskResponse.DataBean> dataBeanList;
    Context context;

    public OngoingTaskRecyclerAdapter(List<OnGoingTaskResponse.DataBean> dataBeanList, FragmentActivity activity) {
        this.dataBeanList=dataBeanList;
        this.context=activity;
    }

    @NonNull
    @Override
    public OngoingTaskRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_ongoing, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull OngoingTaskRecyclerAdapter.Holder holder, int position) {

        String firstLetter = " ";
        ColorGenerator generator = ColorGenerator.MATERIAL; // or use DEFAULT
        // generate random color
        int color = generator.getRandomColor();
        if (dataBeanList.get(position).getTitle() != null) {
            firstLetter = capitalize(dataBeanList.get(position).getTitle()).substring(0, 1);
            holder.txt_title.setText(capitalize(dataBeanList.get(position).getTitle()));
        }
        TextDrawable drawable = TextDrawable.builder()
                .beginConfig()
                .width(40)  // width in px
                .height(40) // height in px
                .endConfig()
                .buildRoundRect(firstLetter, color,5);

        holder.image_thumbnail.setImageDrawable(drawable);

        holder.txt_amount.setText("\u20B9"+dataBeanList.get(position).getBudget());
        holder.txt_area.setText(dataBeanList.get(position).getAddress());
        holder.txt_date.setText(dataBeanList.get(position).getFinishDate());
        holder.txt_time.setText(dataBeanList.get(position).getFinishTime());

        if (dataBeanList.get(position).getStatus().equals("assigned")){
            holder.txt_status.setTextColor(Color.RED);

        }
        if (dataBeanList.get(position).getStatus().equals("open")){
            holder.txt_status.setTextColor(Color.parseColor("#43A047"));

        }


        holder.txt_status.setText(dataBeanList.get(position).getStatus());

        holder.txt_status.setAnimation(AnimationUtils.loadAnimation(context, R.anim.leave));

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String t_id=String.valueOf(dataBeanList.get(position).getId());

                Intent intent=new Intent(context, OngoingMyTaskDetailsActivity.class);
                intent.putExtra("TaskId",t_id);
                intent.putExtra("Activity","Ongoing");
                intent.putExtra("status",dataBeanList.get(position).getStatus());


                context.startActivity(intent
                );


            }
        });
    }

    @Override
    public int getItemCount() {
        return dataBeanList.size();
    }

    public class Holder extends RecyclerView.ViewHolder{
        TextView txt_area,txt_date,txt_time,txt_amount,txt_status,txt_title;
        ImageView image_thumbnail;
        public Holder(@NonNull View itemView) {
            super(itemView);

            txt_amount=itemView.findViewById(R.id.txt_amount);
            txt_area=itemView.findViewById(R.id.txt_area);
            txt_date=itemView.findViewById(R.id.txt_date);
            txt_time=itemView.findViewById(R.id.txt_time);
            txt_status=itemView.findViewById(R.id.txtStatus);
            txt_title=itemView.findViewById(R.id.txt_title);
            image_thumbnail=itemView.findViewById(R.id.image_thumbnail);
        }
    }
}
