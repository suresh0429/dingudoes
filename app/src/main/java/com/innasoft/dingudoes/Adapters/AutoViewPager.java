package com.innasoft.dingudoes.Adapters;

import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.viewpager.widget.PagerAdapter;
import androidx.viewpager.widget.ViewPager;

import com.innasoft.dingudoes.R;
import com.squareup.picasso.Picasso;

import java.util.List;


public class AutoViewPager extends PagerAdapter {

    private Context context;
    private LayoutInflater layoutInflater;
    //List<SliderResponse> upSliderResponse;
    Integer[] upSliderResponse;

    public AutoViewPager(Integer[] upSliderResponse, Context context) {
        this.context = context;
        this.upSliderResponse = upSliderResponse;
    }

    @Override
    public int getCount() {
        return upSliderResponse.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public Object instantiateItem(ViewGroup container, final int position) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.banner_card, container, false);

        ImageView imageView = (ImageView) view.findViewById(R.id.imageView);

        imageView.setImageResource(upSliderResponse[position]);
      //  Picasso.get().load("https://www.testocar.in/boloadmin/" + upSliderResponse.get(position).image).placeholder(R.drawable.default_loading).into(imageView);

        ViewPager vp = (ViewPager) container;
        vp.addView(view, 0);


        return view;

    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {

        ViewPager vp = (ViewPager) container;
        View view = (View) object;
        vp.removeView(view);

    }
}
