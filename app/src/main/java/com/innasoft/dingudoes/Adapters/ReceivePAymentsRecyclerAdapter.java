package com.innasoft.dingudoes.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.dingudoes.Activities.PaymentMethodsActivity;
import com.innasoft.dingudoes.Models.PaymentMethodsModel;
import com.innasoft.dingudoes.Models.ReceivePaymentModel;
import com.innasoft.dingudoes.R;

public class ReceivePAymentsRecyclerAdapter extends RecyclerView.Adapter<ReceivePAymentsRecyclerAdapter.Holder> {
    ReceivePaymentModel[] paymentMethodsModels;
    Context context;

    public ReceivePAymentsRecyclerAdapter(ReceivePaymentModel[] paymentMethodsModels, PaymentMethodsActivity paymentMethodsActivity) {
        this.paymentMethodsModels=paymentMethodsModels;
        this.context=paymentMethodsActivity;
    }

    @NonNull
    @Override
    public ReceivePAymentsRecyclerAdapter.Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_pmethods, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ReceivePAymentsRecyclerAdapter.Holder holder, int position) {

        holder.txt_ano.setText(paymentMethodsModels[position].getAc_no());
        holder.txt_edate.setText(paymentMethodsModels[position].getExp_date());

    }

    @Override
    public int getItemCount() {
        return paymentMethodsModels.length;
    }

    public class Holder extends RecyclerView.ViewHolder{
        TextView txt_ano,txt_edate;

        public Holder(@NonNull View itemView) {
            super(itemView);

            txt_edate=itemView.findViewById(R.id.txt_edate);
            txt_ano=itemView.findViewById(R.id.txt_ano);
        }
    }
}
