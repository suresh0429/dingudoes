package com.innasoft.dingudoes.Apis;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitClient {

   // public static final String BASE_URL = "http://35.154.13.209:8080/";
    public static final String BASE_URL = "http://35.154.117.45:8080/";
    //public static final String IMAGE_BASE_URL = "http://35.154.13.209:8080/";
    public static final String IMAGE_BASE_URL = "http://35.154.117.45:8080/";

    public static final String ABOUT_US_URL = BASE_URL+"urls/about-us";
    public static final String PRIVACY_POLICY_URL = BASE_URL+"getMetaData?category=privacyPolicy";
    public static final String TERMS_CONDITIONS_URL = BASE_URL+"getMetaData?category=termsAndConditions";
    public static final String CONTACT_US_URL = BASE_URL+"getMetaData?category=contactUs";
    public static final String INSURENCE_URL = BASE_URL+"getMetaData?category=insurence";

    private static RetrofitClient mInstance;
    private Retrofit retrofit;

    private RetrofitClient() {

        OkHttpClient okHttpClient = new OkHttpClient.Builder()
                .addInterceptor(
                        chain -> {
                            Request request = chain.request().newBuilder().build();
                            return chain.proceed(request);
                        }).build();

        retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(okHttpClient)
                .build();
    }

    public static synchronized RetrofitClient getInstance() {
        if (mInstance == null) {
            mInstance = new RetrofitClient();
        }
        return mInstance;
    }

    public Api getApi() {
        return retrofit.create(Api.class);
    }
}
