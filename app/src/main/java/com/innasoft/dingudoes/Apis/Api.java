package com.innasoft.dingudoes.Apis;
import com.innasoft.dingudoes.Response.AddBankAccountResponse;
import com.innasoft.dingudoes.Response.ChangeMobilenumberResponse;
import com.innasoft.dingudoes.Response.CompletedTaskResponse;
import com.innasoft.dingudoes.Response.ContactUsResponse;
import com.innasoft.dingudoes.Response.CouponResponse;
import com.innasoft.dingudoes.Response.CreateCommentResponse;
import com.innasoft.dingudoes.Response.CreateQuoteResponse;
import com.innasoft.dingudoes.Response.CreateReportTaskResponse;
import com.innasoft.dingudoes.Response.CreateReviewResponse;
import com.innasoft.dingudoes.Response.CreateTaskAlertsResponse;
import com.innasoft.dingudoes.Response.DashboardResponse;
import com.innasoft.dingudoes.Response.EditProfileResponse;
import com.innasoft.dingudoes.Response.EnableTaskAlertResponse;
import com.innasoft.dingudoes.Response.FCmTokenResponse;
import com.innasoft.dingudoes.Response.ForgotPasswordResponse;
import com.innasoft.dingudoes.Response.GeneratePayoutTokenResponse;
import com.innasoft.dingudoes.Response.GetNotificationsResponse;
import com.innasoft.dingudoes.Response.GetPostTaskResponse;
import com.innasoft.dingudoes.Response.GetProfileResponse;
import com.innasoft.dingudoes.Response.GetTaskAlertResponse;
import com.innasoft.dingudoes.Response.GetTaskCommentResponse;
import com.innasoft.dingudoes.Response.GetTaskDetailsResponse;
import com.innasoft.dingudoes.Response.GetUserReviewResponse;
import com.innasoft.dingudoes.Response.HomeCategoriesResponse;
import com.innasoft.dingudoes.Response.LoginResponse;
import com.innasoft.dingudoes.Response.MyTaskDetailsResponse;
import com.innasoft.dingudoes.Response.OnGoingTaskResponse;
import com.innasoft.dingudoes.Response.PaymentPaidHistoryResponse;
import com.innasoft.dingudoes.Response.PaymentRecievedHistoryResponse;
import com.innasoft.dingudoes.Response.PendingReviewsResponse;
import com.innasoft.dingudoes.Response.PosATaskResponse;
import com.innasoft.dingudoes.Response.PrivacyPolicyResponse;
import com.innasoft.dingudoes.Response.ProfileResponse;
import com.innasoft.dingudoes.Response.QuoteAcceptResponse;
import com.innasoft.dingudoes.Response.RegisterResponse;
import com.innasoft.dingudoes.Response.ReplyCommentResponse;
import com.innasoft.dingudoes.Response.SearchTaskResponse;
import com.innasoft.dingudoes.Response.SetPasswordResponse;
import com.innasoft.dingudoes.Response.TermsConditionsResponse;
import com.innasoft.dingudoes.Response.TokenResponse;
import com.innasoft.dingudoes.Response.TransactioSuccessResponse;
import com.innasoft.dingudoes.Response.UpdatetaskSattusResponse;
import com.innasoft.dingudoes.Response.VerifyMobileOtpResponse;
import com.innasoft.dingudoes.Response.VerifyMobileResponse;
import com.innasoft.dingudoes.Response.VerifyOtpResponse;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;

import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;

import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.HeaderMap;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface Api {

    @FormUrlEncoded
    @POST("userLogin")
    Call<LoginResponse> userLogin(@Field("username") String username,
                                  @Field("password") String password);

    @FormUrlEncoded
    @POST("updateFcmToken")
    Call<FCmTokenResponse> updateFcmToken(@Header("token") String token,@Field("fcm_token") String fcm_token);



    @FormUrlEncoded
    @POST("externalAuth")
    Call<LoginResponse> userSocialLogin(@Field("provider") String provider,
                                  @Field("identifier") String identifier,
                                  @Field("name") String name,
                                  @Field("email") String email
    );


    @FormUrlEncoded
    @POST("createUser")
    Call<RegisterResponse> Registration(@Field("email") String email, @Field("full_name") String full_name,
                                                   @Field("mobile") String mobile, @Field("password") String password,
                                        @Field("post_task") String post_task,@Field("complete_task") String complete_task,
                                        @Field("referer_code") String referer_code);



    @FormUrlEncoded
    @POST("verifyMobileNumber")
    Call<VerifyMobileResponse> VerifyMobile(@Field("mobile") String mobile, @Field("user_id") String user_id);

    @FormUrlEncoded
    @Headers("Content-Type:application/x-www-form-urlencoded")
    @POST("verifyMobileOtp")
    Call<VerifyOtpResponse> VerifyOtp(@Field("oneTimePassword") String oneTimePassword, @Field("user_id") String userid);


//    @FormUrlEncoded
//    @Headers("Content-Type:application/x-www-form-urlencoded")
//    @POST("verifyMobileOtp")
//    Call<VerifyMobileOtpResponse> VerifyMobileOtp(@Field("oneTimePassword") String oneTimePassword, @Field("user_id") String userid);


    @FormUrlEncoded
    @POST("forgotPassword")
    Call<ForgotPasswordResponse> ForgotPassword(@Field("username") String username);

    @FormUrlEncoded
    @POST("setPassword")
    Call<SetPasswordResponse> SetPassword(@Field("token") String token, @Field("password") String password);


    @FormUrlEncoded
    @POST("changePassword")
    Call<SetPasswordResponse> ChangePassword(@Header("token") String token, @Field("password") String password,@Field("newpaasword") String newpaasword);


    @FormUrlEncoded
    @POST("searchTasks")
    Call<SearchTaskResponse> SearchTask(@Header("Authorization-Basic") String tokenValue, @Field("to_be_done") String to_be_done,
                                        @Field("min_price") String min_price,@Field("max_price") String max_price,@Field("location") String location,
                                        @Field("distance") String distance);



    @FormUrlEncoded
    @POST("createquote")
    Call<CreateQuoteResponse> CreateQuote(@Header("Authorization") String Authorization, @Field("taskId") int taskId);

    @GET("getTaskDetails")
    Call<GetTaskDetailsResponse> GetTeskDetails(@Header("Authorization") String Authorization, @Query("taskId") int taskId);

    @GET("/getCategoriesList")
    Call<HomeCategoriesResponse> HomeCategories(@Header("Authorization") String Authorization);


    @Multipart
    @POST("createtask")
    Call<PosATaskResponse> PostTask(@Header("Authorization") String auth, @Part List<MultipartBody.Part> documentImages,
                                @Part("title") RequestBody title, @Part("description") RequestBody description,
                                @Part("category") RequestBody category, @Part("is_remote") RequestBody is_remote,
                                @Part("address") RequestBody address, @Part("task_date") RequestBody task_date,
                                @Part("budget_type") RequestBody budget_type, @Part("budget") RequestBody budget,
                                @Part("taskers") RequestBody taskers, @Part("must_haves") RequestBody must_haves,
                                @Part("task_hours") RequestBody task_hours, @Part("task_time") RequestBody task_time,
                                @Part("latitude") RequestBody latitude, @Part("longitude") RequestBody longitude) ;

    @Multipart
    @POST("uploadProfilePic")
    Call<ProfileResponse> uploadProfilepic(@Header("Authorization") String auth, @Part MultipartBody.Part image) ;

    @Headers("Content-Type:application/json")
    @GET("getUserDetails/{id}")
    Call<GetProfileResponse> Profile(@Path("id") String userId);

    @GET("getTasksList/{id}")
    Call<CompletedTaskResponse> CompletedTask(@Path("id") String userId, @Query("status") String status);

    @GET("getTasksList/{id}")
    Call<OnGoingTaskResponse> OnGoingTask(@Path("id") String userId, @Query("status") String status);

    @FormUrlEncoded
    @POST("getTaskComments ")
    Call<GetTaskCommentResponse> GetTaskComments(@Header("Authorization") String Authorization, @Field("taskId") int taskId);

    @FormUrlEncoded
    @POST("createcomment")
    Call<CreateCommentResponse> CreateComment(@Header("Authorization") String Authorization, @Field("parentId") Integer parentId, @Field("taskId") int taskId, @Field("commentText") String commentText);



    @FormUrlEncoded
    @POST("updateUserDetails")
    Call<EditProfileResponse> EditProfile(@Header("Authorization") String Authorization,
                                          @Field("billingAddress") String billingAddress,
                                          @Field("email") String email,
                                          @Field("name") String name,
                                          @Field("skills") String skills,
                                          @Field("about") String about,
                                          @Field("transport") String transport,
                                          @Field("languages") String languages,
                                          @Field("date_of_birth") String date_of_birth);



    @GET("paymentsHistory")
    Call<PaymentPaidHistoryResponse> PaymentHistoryPaid(@Header("Authorization") String Authorization,@Query("type") String type);


    @GET("paymentsHistory")
    Call<PaymentRecievedHistoryResponse> PaymentHistoryRecieved(@Header("Authorization") String Authorization, @Query("type") String type);

    @GET("getMetaData")
    Call<PrivacyPolicyResponse> PrivacyPolicy(@Header("Authorization") String Authorization, @Query("category") String category);
    @GET("getMetaData")
    Call<TermsConditionsResponse> TermsConditions(@Header("Authorization") String Authorization, @Query("category") String category);


    @FormUrlEncoded
    @POST("createReportTask")
    Call<CreateReportTaskResponse> CreateReportTask(@Header("Authorization") String Authorization,@Field("Content-Type") String Content, @Field("taskId") int taskId, @Field("reportText") String reportText);


    @FormUrlEncoded
    @POST("createcomment")
    Call<ReplyCommentResponse> ReplyComment(@Header("Authorization") String Authorization, @Field("parentId") int parentId, @Field("taskId") String taskId, @Field("commentText") String commentText);

    @GET("getPostedTasks")
    Call<GetPostTaskResponse> GetPostTasks(@Header("Authorization") String Authorization);

    @GET("getTaskDetails")
    Call<MyTaskDetailsResponse> GetMyTeskDetails(@Header("Authorization") String Authorization, @Query("taskId") String taskId);

    @FormUrlEncoded
    @POST("acceptQuoteTask")
    Call<QuoteAcceptResponse> AcceptQuote(@Header("Authorization") String Authorization, @Field("userId") String userId, @Field("taskId") String taskId);


    @FormUrlEncoded
    @POST("verifyMobileNumber")
    Call<ChangeMobilenumberResponse> ChangeMobilenumber(@Header("Authorization") String Authorization, @Field("userId") String userId, @Field("mobile") String mobile);

    @GET("getMetaData")
    Call<ContactUsResponse> ContactUs(@Header("Authorization") String Authorization, @Query("category") String category);


    @GET("generatePayoutToken")
    Call<GeneratePayoutTokenResponse> GenerateToken(@Header("Authorization") String Authorization, @Header("Content-Type") String content);

    @FormUrlEncoded
    @Headers("Content-Type:application/x-www-form-urlencoded")
    @POST("payoutAddBeneficiary")
    Call<AddBankAccountResponse> AddBankAccount(@Header("Authorization") String Authorization,@Header("token") String token,
                                                @Field("name") String name,@Field("email") String email,@Field("phoneNumber") String phoneNumber,
                                                @Field("bankAccountNumber") String bankAccountNumber,@Field("ifscCode") String ifscCode,
                                                @Field("address") String address,@Field("city") String city,@Field("state") String state,
                                                @Field("pinCode") String pinCode);


    @FormUrlEncoded
    @Headers("Content-Type:application/x-www-form-urlencoded")
    @POST("createOrder")
    Call<TokenResponse> generateToken(@Header("Authorization") String Authorization,
                                      @Field("orderId") String orderId, @Field("orderAmount") String orderAmount);


    @FormUrlEncoded
    @Headers("Content-Type:application/x-www-form-urlencoded")
    @POST("createTransaction")
    Call<TransactioSuccessResponse> orderTransaction(@Header("Authorization") String Authorization, @Field("paidBy") int paidBy,
                                                     @Field("paidTo") int paidTo, @Field("taskId") int taskId,
                                                     @Field("quoteId") int quoteId, @Field("amount") double amount,
                                                     @Field("status") String status, @Field("transactionNumber") String transactionNumber,
                                                     @Field("transactionComments") String transactionComments, @Field("orderId") String orderId);


    @GET("getuserDashboardDetails/{id}")
    Call<DashboardResponse> DashBoard(@Header("Authorization") String Authorization, @Header("Content-Type") String Content,
                                      @Path("id") String userId);


    @FormUrlEncoded
    @Headers("Content-Type:application/x-www-form-urlencoded")
    @POST("createTasksAlert")
    Call<CreateTaskAlertsResponse> CreateTaskAlert(@Header("Authorization") String Authorization,
                                                   @Field("userId") String userId, @Field("isRemote") String isRemote,
                                                   @Field("keywords") String keywords,@Field("location") String location,
                                                   @Field("distance") String distance);
    @GET("getTasksAlertForUser")
    Call<GetTaskAlertResponse> GetTaskAlerts(@Header("Authorization") String Authorization, @Header("Content-Type") String Content);


    @GET("getUserReviews")
    Call<GetUserReviewResponse> GetUserReviews(@Header("Authorization") String Authorization);

    @GET("pendingReviews")
    Call<PendingReviewsResponse> PendingReviews(@Header("Authorization") String Authorization);


    @FormUrlEncoded
    @POST("createReview")
    Call<CreateReviewResponse> CreateReview(@Header("Authorization") String Authorization,
                                            @Field("userId") String userId, @Field("taskId") String taskId,
                                            @Field("reviewText") String reviewText,
                                            @Field("rating") String rating);

    @GET("enableTaskAlert")
    Call<EnableTaskAlertResponse> EnableTaskAlert(@Header("Authorization") String Authorization,@Header("Content-Type") String Content);


    @FormUrlEncoded
    @POST("updateTaskStatus")
    Call<UpdatetaskSattusResponse> updateTaskStatus(@Header("token") String token, @Field("status") String status,@Field("task_id") String task_id);

    @FormUrlEncoded
    @POST("/userNotifications")
    Call<GetNotificationsResponse> GetNotifications(@Field("user_id") String user_id);

    @FormUrlEncoded
    @POST("coupon/validate")
    Call<CouponResponse> CouponData(@Field("userId") String user_id,@Field("OrderAmount") String OrderAmount,@Field("coupon") String coupon);

}
