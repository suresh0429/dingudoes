package com.innasoft.dingudoes.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.dingudoes.Activities.LoginActivity;
import com.innasoft.dingudoes.Adapters.OptionsAdapter;

import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.OptionsModel;
import com.innasoft.dingudoes.storage.PrefManager;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OptionsFragment extends Fragment {

    View view;

    @BindView(R.id.btn_logout)
    Button btnLogout;
    @BindView(R.id.recycler_options)
    RecyclerView recyclerOptions;
    private PrefManager pref;
    OptionsAdapter optionsAdapter;
    GridLayoutManager gridLayoutManager;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_options, container, false);

        ButterKnife.bind(this, view);

        pref = new PrefManager(getContext());
        HashMap<String, String> profile = pref.getUserDetails();
        //token = profile.get("Token");

        OptionsModel[] data = new OptionsModel[] {
                new OptionsModel("Activity", R.drawable.ic_activity),
                new OptionsModel("Profile", R.drawable.ic_avatar),
                new OptionsModel("Payment History",R.drawable.ic_history),
                new OptionsModel("Pending Reviews", R.drawable.ic_rating),
                new OptionsModel("Settings",R.drawable.ic_settings),
                new OptionsModel("Help",R.drawable.ic_information),
                new OptionsModel("Refer & Earn",R.drawable.ic_refer),
                new OptionsModel("Feedback",R.drawable.ic_survey),
                new OptionsModel("DD Rewards",R.drawable.ic_rewards),
                new OptionsModel("Messages",R.drawable.ic_chat1)

        };

        optionsAdapter = new OptionsAdapter(data,getActivity());
        recyclerOptions.setNestedScrollingEnabled(false);
        gridLayoutManager = new GridLayoutManager(getActivity(), 2);
        recyclerOptions.setLayoutManager(gridLayoutManager);
        recyclerOptions.setAdapter(optionsAdapter);


        btnLogout.setOnClickListener(view -> {

            final AlertDialog dialogBuilder = new AlertDialog.Builder(getActivity()).create();
            LayoutInflater inflater1 = getLayoutInflater();
            View dialogView = inflater1.inflate(R.layout.logout, null);

            RelativeLayout relative_logout = (RelativeLayout) dialogView.findViewById(R.id.relative_logout);
            RelativeLayout relative_cancel = (RelativeLayout) dialogView.findViewById(R.id.relative_cancel);


            relative_cancel.setOnClickListener(view1 -> {
                // DO SOMETHINGS
                dialogBuilder.dismiss();
            });

            relative_logout.setOnClickListener(view12 -> {
                pref.clearSession();
                Intent intent = new Intent(getActivity(), LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                getActivity().overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                getActivity().finish();
            });

            dialogBuilder.setView(dialogView);
            dialogBuilder.show();


        });

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        AppCompatActivity activity = (AppCompatActivity) getActivity();
        ActionBar actionBar = activity.getSupportActionBar();
        actionBar.setTitle(getResources().getString(R.string.options));

    }

}
