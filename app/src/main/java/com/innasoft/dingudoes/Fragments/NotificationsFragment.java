package com.innasoft.dingudoes.Fragments;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.dingudoes.Adapters.MessagesRecyclerAdapter;
import com.innasoft.dingudoes.Adapters.NotificationsAdapter;
import com.innasoft.dingudoes.Apis.RetrofitClient;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.GetNotificationsResponse;
import com.innasoft.dingudoes.Singleton.AppController;
import com.innasoft.dingudoes.storage.PrefManager;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class NotificationsFragment extends Fragment {
    RecyclerView recycler_notifications;
    MessagesRecyclerAdapter messagesRecyclerAdapter;
    AppController appController;
    PrefManager prefManager;
    String userId;

    View view;
    @BindView(R.id.progressLayout)
    LinearLayout progressLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_messages, container, false);
        ButterKnife.bind(this,view);

        recycler_notifications = view.findViewById(R.id.recycler_notifications);

        prefManager = new PrefManager(getContext());
        HashMap<String, String> profile = prefManager.getUserDetails();
        userId = profile.get("userid");
        setHasOptionsMenu(true);

        appController = (AppController) getActivity().getApplication();
        if (appController.isConnection()) {

            getData(userId);

        } else {
            String message = "Sorry! Not connected to internet";
            int color = Color.RED;

            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
        }


        return view;
    }

    private void getData(String userId) {

        progressLayout.setVisibility(View.VISIBLE);

        Call<GetNotificationsResponse> call = RetrofitClient.getInstance().getApi().GetNotifications(userId);
        call.enqueue(new Callback<GetNotificationsResponse>() {
            @Override
            public void onResponse(Call<GetNotificationsResponse> call, Response<GetNotificationsResponse> response) {
                if (response.isSuccessful()) ;
                GetNotificationsResponse getNotificationsResponse = response.body();
                if (getNotificationsResponse.isSuccess()) {
                    progressLayout.setVisibility(View.GONE);

                    List<GetNotificationsResponse.DataBean> getNotificationsResponseList = getNotificationsResponse.getData();
                    NotificationsAdapter notificationsAdapter = new NotificationsAdapter(getNotificationsResponseList, getActivity());
                    final LinearLayoutManager layoutManager1 = new LinearLayoutManager(getContext());
                    layoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
                    recycler_notifications.setLayoutManager(layoutManager1);
                    recycler_notifications.setNestedScrollingEnabled(false);
                    recycler_notifications.setAdapter(notificationsAdapter);

                } else {
                    progressLayout.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<GetNotificationsResponse> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();

        AppCompatActivity activity = (AppCompatActivity) getActivity();
        ActionBar actionBar = activity.getSupportActionBar();
        actionBar.setTitle(getResources().getString(R.string.notifications));
    }
}
