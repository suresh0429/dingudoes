package com.innasoft.dingudoes.Fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.innasoft.dingudoes.Activities.FilterActivity;
import com.innasoft.dingudoes.Activities.MapSearchActivity;
import com.innasoft.dingudoes.Activities.TaskDetailsActivity;
import com.innasoft.dingudoes.Apis.RetrofitClient;
import com.innasoft.dingudoes.Singleton.AppController;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.SearchTaskResponse;
import com.innasoft.dingudoes.storage.PrefManager;

import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.innasoft.dingudoes.Singleton.AppController.TAG;

public class
BrowseTaskFragmnet extends Fragment implements OnMapReadyCallback {
    View view;
    private GoogleMap mMap;
    PrefManager prefManager;
    String token,id,min,max,loc,dist;
    LatLng latLng;
    AppController appController;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.browsetask_fragmrnt, container, false);
        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        appController=(AppController) getActivity().getApplication();

        setHasOptionsMenu(true);

        prefManager = new PrefManager(getActivity());
        HashMap<String, String> profile = prefManager.getUserDetails();
        token = profile.get("Token");

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu,inflater);
        inflater.inflate(R.menu.menu, menu);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id){
            case R.id.map:
                return true;
            case R.id.filter:
                startActivity(new Intent(getContext(), FilterActivity.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        // Add a marker in Sydney and move the camera
       /* LatLng latLng = new LatLng(17.4435, 78.3772);
        mMap.addMarker(new MarkerOptions().position(latLng));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));*/

       if (appController.isConnection()){
           searchData(token,"1","100","1000","hyderabad","50");

       }
       else {
           String message = "Sorry! Not connected to internet";
           int color = Color.RED;

           Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
       }



    }

    private void searchData(String token, String id, String min, String max, String loc, String dist){

        ProgressDialog progressDialog=new ProgressDialog(getActivity());
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        Call<SearchTaskResponse> call= RetrofitClient.getInstance().getApi().SearchTask(token, id, min, max, loc,dist);
        call.enqueue(new Callback<SearchTaskResponse>() {
            @Override
            public void onResponse(Call<SearchTaskResponse> call, Response<SearchTaskResponse> response) {
                if (response.isSuccessful());
                SearchTaskResponse searchTaskResponse=response.body();
                //  Log.d(TAG, "onResponse: "+searchTaskResponse.getData());
                if (searchTaskResponse.isSuccess()){
                    progressDialog.dismiss();

                    List<SearchTaskResponse.DataBean> dataBeanList = searchTaskResponse.getData();

                    for (SearchTaskResponse.DataBean dataBean : dataBeanList) {
                        Log.d(TAG, "onResponse: "+dataBean.toString());
                        if (dataBean.getLatitude() != null && dataBean.getLongitude() != null) {

                            try {
                                latLng = new LatLng(Double.parseDouble(dataBean.getLatitude()), Double.parseDouble(dataBean.getLongitude()));
                            } catch (NumberFormatException e) {
                                e.printStackTrace();
                            }
                            mMap.addMarker(new MarkerOptions().position(latLng).title(dataBean.getTitle()).snippet(dataBean.getAddress()+"\n"+dataBean.getBudget()+"\n"+dataBean.getId()).icon(BitmapDescriptorFactory.fromResource(R.drawable.dingu)));
                            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                            // mMap.animateCamera(latLng, 14));

                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 6));

                             Log.d(TAG, "onResponse: "+dataBean.getLongitude().length());

                            mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                                @Override
                                public View getInfoWindow(Marker marker) {

                                    return null;
                                }

                                @Override
                                public View getInfoContents(Marker marker) {
                                    View v = getLayoutInflater().inflate(R.layout.info_title, null);
                                    v.setLayoutParams(new LinearLayout.LayoutParams(700,270));

                                    final TextView info1 = (TextView) v.findViewById(R.id.txt_title);
                                    TextView info2 = (TextView) v.findViewById(R.id.txtAddress);
                                    TextView info3 = (TextView) v.findViewById(R.id.txtPrice);

                                    String data = marker.getSnippet();
                                    StringTokenizer stringTokenizer = new StringTokenizer(data);
                                    String address = stringTokenizer.nextToken("\n");
                                    String budget = stringTokenizer.nextToken("\n");
                                    String taskId = stringTokenizer.nextToken("\n");

                                    info1.setText(marker.getTitle());
                                    info3.setText("\u20B9"+budget);

                                    info2.setText(address);

                                    mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                                        public void onInfoWindowClick(Marker marker)
                                        {
                                            Intent intent=new Intent(getActivity(), TaskDetailsActivity.class);
                                            intent.putExtra("Task_id",Integer.valueOf(taskId));
                                            startActivity(intent);

                                        }
                                    });

                                    return v;
                                }
                            });
                        }
                    }





                }

            }

            @Override
            public void onFailure(Call<SearchTaskResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });



    }




}
