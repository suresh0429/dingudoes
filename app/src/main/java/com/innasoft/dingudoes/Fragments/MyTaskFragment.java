package com.innasoft.dingudoes.Fragments;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.dingudoes.Adapters.CompletedTaskRecycerAdapter;
import com.innasoft.dingudoes.Adapters.OngoingTaskRecyclerAdapter;
import com.innasoft.dingudoes.Adapters.PostsTaskRecycerAdapter;
import com.innasoft.dingudoes.Adapters.TaskDraftAdapter;
import com.innasoft.dingudoes.Apis.RetrofitClient;
import com.innasoft.dingudoes.Db.SqliteDatabase;
import com.innasoft.dingudoes.Models.TaskDraftModel;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.CompletedTaskResponse;
import com.innasoft.dingudoes.Response.GetPostTaskResponse;
import com.innasoft.dingudoes.Response.OnGoingTaskResponse;
import com.innasoft.dingudoes.Singleton.AppController;
import com.innasoft.dingudoes.storage.PrefManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.ceryle.segmentedbutton.SegmentedButtonGroup;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MyTaskFragment extends Fragment {
    View view;
    @BindView(R.id.segmentedButtonGroup)
    SegmentedButtonGroup segmentedButtonGroup;
    @BindView(R.id.recycler_ongoing)
    RecyclerView recyclerOngoing;
    @BindView(R.id.txt_nodata)
    TextView txtNodata;
    @BindView(R.id.nodataLayout)
    LinearLayout nodataLayout;
    PrefManager prefManager;
    String userId, token;
    OngoingTaskRecyclerAdapter ongoingTaskRecyclerAdapter;
    CompletedTaskRecycerAdapter completedTaskRecycerAdapter;
    PostsTaskRecycerAdapter postsTaskRecycerAdapter;
    AppController appController;
    @BindView(R.id.progressLayout)
    LinearLayout progressLayout;


    private SqliteDatabase mDatabase;
    private ArrayList<TaskDraftModel> taskDraftModels = new ArrayList<>();
    private TaskDraftAdapter mAdapter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_tasks, container, false);
        ButterKnife.bind(this, view);

        appController = (AppController) getActivity().getApplication();
        setHasOptionsMenu(true);

        prefManager = new PrefManager(getActivity());
        HashMap<String, String> profile = prefManager.getUserDetails();
        userId = profile.get("userid");
        token = profile.get("Token");


        if (appController.isConnection()) {

            draftsTasks();
            segmentedButtonGroup.setOnClickedButtonListener(new SegmentedButtonGroup.OnClickedButtonListener() {
                @Override
                public void onClickedButton(int position) {
                    if (position == 0) {
                        draftsTasks();

                    } else if (position == 1) {
                        PostTasks();
                    } else if (position == 2) {
                        OngoingTasks();
                    } else if (position == 3) {
                      //  completedTask();
                    }
                }
            });


        } else {
            String message = "Sorry! Not connected to internet";
            int color = Color.RED;

            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
        }

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        AppCompatActivity activity = (AppCompatActivity) getActivity();
        ActionBar actionBar = activity.getSupportActionBar();
        actionBar.setTitle(getResources().getString(R.string.title_mytasks));
    }

    private void OngoingTasks() {

        progressLayout.setVisibility(View.VISIBLE);

        String ass = "assigned";

        Call<OnGoingTaskResponse> call = RetrofitClient.getInstance().getApi().OnGoingTask(userId, ass);
        call.enqueue(new Callback<OnGoingTaskResponse>() {
            @Override
            public void onResponse(Call<OnGoingTaskResponse> call, Response<OnGoingTaskResponse> response) {
                if (response.isSuccessful()) ;
                OnGoingTaskResponse onGoingTaskResponse = response.body();
                if (onGoingTaskResponse.isSuccess()) {
                    progressLayout.setVisibility(View.GONE);
                    List<OnGoingTaskResponse.DataBean> dataBeanList = onGoingTaskResponse.getData();

                    recyclerOngoing.setVisibility(View.VISIBLE);
                    nodataLayout.setVisibility(View.GONE);
                    ongoingTaskRecyclerAdapter = new OngoingTaskRecyclerAdapter(dataBeanList, getActivity());
                    LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
                    layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                    recyclerOngoing.setLayoutManager(layoutManager);
                    recyclerOngoing.setNestedScrollingEnabled(false);
                    recyclerOngoing.setAdapter(ongoingTaskRecyclerAdapter);
                    ongoingTaskRecyclerAdapter.notifyDataSetChanged();

                } else {
                    progressLayout.setVisibility(View.GONE);
                    recyclerOngoing.setVisibility(View.GONE);
                    nodataLayout.setVisibility(View.VISIBLE);
                    txtNodata.setText("Ongoing Tasks Not Found !");

                }
            }

            @Override
            public void onFailure(Call<OnGoingTaskResponse> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


    }


    private void PostTasks() {

        progressLayout.setVisibility(View.VISIBLE);

        Call<GetPostTaskResponse> call = RetrofitClient.getInstance().getApi().GetPostTasks(token);
        call.enqueue(new Callback<GetPostTaskResponse>() {
            @Override
            public void onResponse(Call<GetPostTaskResponse> call, Response<GetPostTaskResponse> response) {
                if (response.isSuccessful()) ;
                GetPostTaskResponse getPostTaskResponse = response.body();
                if (getPostTaskResponse.isSuccess()) {
                    progressLayout.setVisibility(View.GONE);
                    recyclerOngoing.setVisibility(View.VISIBLE);
                    nodataLayout.setVisibility(View.GONE);
                    List<GetPostTaskResponse.DataBean> dataBeanList = getPostTaskResponse.getData();

                    postsTaskRecycerAdapter = new PostsTaskRecycerAdapter(dataBeanList, getActivity());
                    LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
                    layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                    recyclerOngoing.setLayoutManager(layoutManager);
                    recyclerOngoing.setNestedScrollingEnabled(false);
                    recyclerOngoing.setAdapter(postsTaskRecycerAdapter);
                    postsTaskRecycerAdapter.notifyDataSetChanged();

                } else {
                    progressLayout.setVisibility(View.GONE);
                    recyclerOngoing.setVisibility(View.GONE);
                    nodataLayout.setVisibility(View.VISIBLE);
                    txtNodata.setText("Posted Tasks Not Found !");
                }
            }

            @Override
            public void onFailure(Call<GetPostTaskResponse> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void draftsTasks() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        recyclerOngoing.setLayoutManager(linearLayoutManager);
        recyclerOngoing.setHasFixedSize(true);
        mDatabase = new SqliteDatabase(getActivity());
        taskDraftModels = mDatabase.listContacts();

        if (taskDraftModels.size() > 0) {
            recyclerOngoing.setVisibility(View.VISIBLE);
            nodataLayout.setVisibility(View.GONE);
            mAdapter = new TaskDraftAdapter(taskDraftModels, getActivity());
            recyclerOngoing.setAdapter(mAdapter);

        } else {
            recyclerOngoing.setVisibility(View.GONE);
            nodataLayout.setVisibility(View.VISIBLE);
            txtNodata.setText("Drafts  Not Found !");
        }



    }


//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        super.onCreateOptionsMenu(menu, inflater);
//        inflater.inflate(R.menu.home_menu, menu);
//
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
//        int id = item.getItemId();
//        switch (id) {
//            case R.id.notification:
//                startActivity(new Intent(getContext(), NotificationsActivity.class));
//                return true;
//
//        }
//
//        return super.onOptionsItemSelected(item);
//    }


}
