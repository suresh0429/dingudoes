package com.innasoft.dingudoes.Fragments;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;


import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.innasoft.dingudoes.Adapters.TaskParentCommentsAdapter;
import com.innasoft.dingudoes.Apis.RetrofitClient;
import com.innasoft.dingudoes.Models.CommentsModel;
import com.innasoft.dingudoes.Response.CreateCommentResponse;
import com.innasoft.dingudoes.Singleton.AppController;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.CommentsParentResponse;
import com.innasoft.dingudoes.Response.GetTaskCommentResponse;
import com.innasoft.dingudoes.Response.ReplyCommentResponse;
import com.innasoft.dingudoes.storage.PrefManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.constraintlayout.widget.Constraints.TAG;

public class CustomBottomSheetDialogFragment extends BottomSheetDialogFragment {

    private BottomSheetBehavior mBehavior;
    ImageView img_cancel,img_send;
    String token,Task_id;
    EditText edt_comment;
    PrefManager prefManager;
    AppController appController;
    RecyclerView recycler_bottom;
    TaskParentCommentsAdapter taskParentCommentsAdapter;
    int taskid;
    String id,parentId;
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);

        View view = View.inflate(getContext(), R.layout.bottomsheet, null);
        appController=(AppController) getActivity().getApplication();

        img_cancel=view.findViewById(R.id.img_cancel);
        img_send=view.findViewById(R.id.img_send);
        edt_comment=view.findViewById(R.id.edt_comment);
        recycler_bottom=view.findViewById(R.id.recycler_bottom);


        prefManager = new PrefManager(getContext());
        HashMap<String, String> profile = prefManager.getUserDetails();
        token = profile.get("Token");



        img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });

        Bundle bundle = getArguments();

        if (bundle != null){

            id=bundle.getString("id");
           // parentId=bundle.getString("parent_id");
            taskid=bundle.getInt("task_id",0);
            //taskid=Integer.parseInt(Task_id);

        }

        if (appController.isConnection()){
            getComments();

            img_send.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    getData();
                }
            });

        }
        else {

            String message = "Sorry! Not connected to internet";
            int color = Color.RED;

            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

        }





        dialog.setContentView(view);
        mBehavior = BottomSheetBehavior.from((View) view.getParent());
        return dialog;
    }

    private void getComments() {
        ArrayList<CommentsModel> commentsModels = new ArrayList<>();
        Call<GetTaskCommentResponse> call = RetrofitClient.getInstance().getApi().GetTaskComments(token, taskid);
        call.enqueue(new Callback<GetTaskCommentResponse>() {
            @Override
            public void onResponse(Call<GetTaskCommentResponse> call, Response<GetTaskCommentResponse> response) {
                if (response.isSuccessful()) ;
                GetTaskCommentResponse getTaskCommentResponse = response.body();
                if (getTaskCommentResponse.isSuccess()) {
                    GetTaskCommentResponse.DataBean dataBean = getTaskCommentResponse.getData();

                    List<GetTaskCommentResponse.DataBean.CommentsBean> commentsBeanList = dataBean.getComments();
                    commentsModels.clear();
                    for (GetTaskCommentResponse.DataBean.CommentsBean commentsBean : commentsBeanList) {
                        if (commentsBean.getParentId() != 0 && commentsBean.getParentId()==Integer.parseInt(id)) {
                            Log.d(TAG, "onResponse: "+commentsBean.getParentId() +"---"+id);
                            commentsModels.add(new CommentsModel(commentsBean.getId(),commentsBean.getParentId(), commentsBean.getTaskId(), commentsBean.getCommentText(),
                                    commentsBean.getIsDeleted(), commentsBean.getCreatedBy(), commentsBean.getCreatedAt(), commentsBean.getUpdatedBy(),
                                    commentsBean.getUpdatedAt(), commentsBean.getCreated_by(), commentsBean.getUser().getFull_name(),commentsBean.getUser().getUser_id()));
                        }
                    }

                    taskParentCommentsAdapter = new TaskParentCommentsAdapter(commentsModels,getActivity());
                    LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
                    layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                    recycler_bottom.setLayoutManager(layoutManager);
                    recycler_bottom.setNestedScrollingEnabled(false);
                    recycler_bottom.setAdapter(taskParentCommentsAdapter);
                    taskParentCommentsAdapter.notifyDataSetChanged();

                }

            }

            @Override
            public void onFailure(Call<GetTaskCommentResponse> call, Throwable t) {
                Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void getData() {

        String comment=edt_comment.getText().toString().trim();

        if (comment.equals("")){
            Toast.makeText(getContext(), "Please enter comments..", Toast.LENGTH_SHORT).show();

        }
        else {
            ProgressDialog progressDialog = new ProgressDialog(getContext());
            progressDialog.setMessage("Loading.....");
            progressDialog.show();

            Call<CreateCommentResponse> call= RetrofitClient.getInstance().getApi().CreateComment(token, Integer.parseInt(id),taskid,comment);
            call.enqueue(new Callback<CreateCommentResponse>() {
                @Override
                public void onResponse(Call<CreateCommentResponse> call, Response<CreateCommentResponse> response) {
                    if (response.isSuccessful());
                    CreateCommentResponse createCommentResponse=response.body();
                    if (createCommentResponse.isSuccess()){
                        progressDialog.dismiss();
                        Toast.makeText(getContext(), createCommentResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        edt_comment.setText("");
                        getComments();

                    }
                    else {
                        progressDialog.dismiss();
                        Toast.makeText(getContext(), createCommentResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<CreateCommentResponse> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(getContext(), t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        mBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
    }

}