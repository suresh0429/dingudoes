package com.innasoft.dingudoes.Fragments;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.flexbox.FlexDirection;
import com.google.android.flexbox.FlexboxLayoutManager;
import com.google.android.flexbox.JustifyContent;
import com.innasoft.dingudoes.Adapters.AllCategoriesRecyclerAdapter;
import com.innasoft.dingudoes.Adapters.CategoriesRecyclerAdapter;
import com.innasoft.dingudoes.Adapters.SliderAdapter;
import com.innasoft.dingudoes.Apis.RetrofitClient;
import com.innasoft.dingudoes.Models.HomeCategoryModel;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.HomeCategoriesResponse;
import com.innasoft.dingudoes.Singleton.AppController;
import com.innasoft.dingudoes.storage.PrefManager;
import com.smarteist.autoimageslider.IndicatorAnimations;
import com.smarteist.autoimageslider.SliderAnimations;
import com.smarteist.autoimageslider.SliderView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;

public class HomeFragment extends Fragment {
    View view;
    CategoriesRecyclerAdapter categoriesRecyclerAdapter;
    AllCategoriesRecyclerAdapter allCategoriesRecyclerAdapter;
    PrefManager prefManager;
    String token;
    AppController appController;
    //AutoScrollViewPager viewPager;
    Integer[] imageId = {R.drawable.banner, R.drawable.service, R.drawable.banner, R.drawable.service};
    SliderView sliderView;

    RecyclerView recyclerViewCategory, recyclerViewotherCategory;

    List<HomeCategoryModel> homeCategoryModelList = new ArrayList<>();
    @BindView(R.id.progressLayout)
    LinearLayout progressLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this,view);

        sliderView = view.findViewById(R.id.imageSlider);
        recyclerViewCategory = view.findViewById(R.id.recyclerViewCategory);
        recyclerViewotherCategory = view.findViewById(R.id.recyclerViewotherCategory);

        final GridLayoutManager gridLayoutManager = new GridLayoutManager(getContext(), 3);
        recyclerViewCategory.setNestedScrollingEnabled(false);
        recyclerViewCategory.setLayoutManager(gridLayoutManager);

        SliderAdapter adapter = new SliderAdapter(getContext(), imageId);
        sliderView.setSliderAdapter(adapter);

        sliderView.setIndicatorAnimation(IndicatorAnimations.SLIDE); //set indicator animation by using SliderLayout.IndicatorAnimations. :WORM or THIN_WORM or COLOR or DROP or FILL or NONE or SCALE or SCALE_DOWN or SLIDE and SWAP!!
        sliderView.setSliderTransformAnimation(SliderAnimations.CUBEINROTATIONTRANSFORMATION);
        sliderView.setAutoCycleDirection(SliderView.AUTO_CYCLE_DIRECTION_RIGHT);
        sliderView.setIndicatorSelectedColor(Color.WHITE);
        sliderView.setIndicatorUnselectedColor(Color.GRAY);
        sliderView.startAutoCycle();


        appController = (AppController) getActivity().getApplication();
        setHasOptionsMenu(true);

        prefManager = new PrefManager(getActivity());
        HashMap<String, String> profile = prefManager.getUserDetails();
        token = profile.get("Token");

        if (appController.isConnection()) {
            getData();

        } else {
            String message = "Sorry! Not connected to internet";
            int color = Color.RED;

            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();

        }
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        AppCompatActivity activity = (AppCompatActivity) getActivity();
        ActionBar actionBar = activity.getSupportActionBar();
        actionBar.setTitle(getResources().getString(R.string.title_postatask));

    }

    private void getData() {


       progressLayout.setVisibility(View.VISIBLE);

        Call<HomeCategoriesResponse> categoriesResponseCall = RetrofitClient.getInstance().getApi().HomeCategories(token);
        categoriesResponseCall.enqueue(new Callback<HomeCategoriesResponse>() {
            @Override
            public void onResponse(Call<HomeCategoriesResponse> call, Response<HomeCategoriesResponse> response) {
                if (response.isSuccessful()) ;
                HomeCategoriesResponse homeCategoriesResponse = response.body();
                if (homeCategoriesResponse.isSuccess()) {
                    progressLayout.setVisibility(View.GONE);

                    List<HomeCategoriesResponse.DataBean> dataBeans = homeCategoriesResponse.getData();
                    categoriesRecyclerAdapter = new CategoriesRecyclerAdapter(dataBeans, getActivity());
                   // categoriesRecyclerAdapter = new CategoriesRecyclerAdapter(getPopularCategerios(), getActivity());
                   // final LinearLayoutManager gridLayoutManager = new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false);

                    recyclerViewCategory.setAdapter(categoriesRecyclerAdapter);

                    getAllCatageroies();

                } else {
                    progressLayout.setVisibility(View.GONE);
                    Toast.makeText(getActivity(), "No Data Found..", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<HomeCategoriesResponse> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private ArrayList<HomeCategoryModel> getPopularCategerios(){

        ArrayList<HomeCategoryModel> homeCategoryModels = new ArrayList<>();
        homeCategoryModels.add(new HomeCategoryModel("Painting",1));
        homeCategoryModels.add(new HomeCategoryModel("Cleaning",2));
        homeCategoryModels.add(new HomeCategoryModel("Gardening",3));
        homeCategoryModels.add(new HomeCategoryModel("Computer",4));
        homeCategoryModels.add(new HomeCategoryModel("Photography",5));
        homeCategoryModels.add(new HomeCategoryModel("Delivery",6));
        homeCategoryModels.add(new HomeCategoryModel("Beauty Services",7));
        homeCategoryModels.add(new HomeCategoryModel("Furniture Assembly",8));
        homeCategoryModels.add(new HomeCategoryModel("Home Repairs",9));

        return homeCategoryModels;
    }

    private void getAllCatageroies() {

        HomeCategoryModel homeCategoryModel = new HomeCategoryModel("Help moving", 9);
        homeCategoryModelList.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel("Carpentry", 10);
        homeCategoryModelList.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel("Plumbing", 11);
        homeCategoryModelList.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel("Electronic setup", 12);
        homeCategoryModelList.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel("Electrical work", 13);
        homeCategoryModelList.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel("Pest removal", 14);
        homeCategoryModelList.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel("House organising assistance", 15);
        homeCategoryModelList.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel("Help in packing", 16);
        homeCategoryModelList.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel("Heavy lifting", 17);
        homeCategoryModelList.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel("Groceries delivery", 18);
        homeCategoryModelList.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel("Collect and drop", 19);
        homeCategoryModelList.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel("Cook/ bring me a meal", 20);
        homeCategoryModelList.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel("Child care assistance", 21);
        homeCategoryModelList.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel("Aged care assistance", 22);
        homeCategoryModelList.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel("Alter my dress", 23);
        homeCategoryModelList.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel("Wash my clothes", 24);
        homeCategoryModelList.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel("I need a mechanic", 25);
        homeCategoryModelList.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel("Help me with my assignment", 26);
        homeCategoryModelList.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel("Wash my car", 27);
        homeCategoryModelList.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel("Get me medicines", 28);
        homeCategoryModelList.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel("Give me a lift", 29);
        homeCategoryModelList.add(homeCategoryModel);

        homeCategoryModel = new HomeCategoryModel("Help me decorate my house", 30);
        homeCategoryModelList.add(homeCategoryModel);


        // Create the FlexboxLayoutMananger, only flexbox library version 0.3.0 or higher support.
        FlexboxLayoutManager flexboxLayoutManager = new FlexboxLayoutManager(getApplicationContext());
        // Set flex direction.
        flexboxLayoutManager.setFlexDirection(FlexDirection.ROW);
        // Set JustifyContent.
        flexboxLayoutManager.setJustifyContent(JustifyContent.FLEX_START);
        recyclerViewotherCategory.setLayoutManager(flexboxLayoutManager);
        allCategoriesRecyclerAdapter = new AllCategoriesRecyclerAdapter(homeCategoryModelList, getContext());
        recyclerViewotherCategory.setAdapter(allCategoriesRecyclerAdapter);


    }

//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        super.onCreateOptionsMenu(menu, inflater);
//        inflater.inflate(R.menu.home_menu, menu);
//
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
//        int id = item.getItemId();
//        switch (id) {
//            case R.id.notification:
//                startActivity(new Intent(getContext(), NotificationsActivity.class));
//                return true;
//
//        }
//
//        return super.onOptionsItemSelected(item);
//    }


}
