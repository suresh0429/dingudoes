package com.innasoft.dingudoes.Fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.core.view.MenuItemCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.dingudoes.Activities.FilterActivity;
import com.innasoft.dingudoes.Activities.MapSearchActivity;
import com.innasoft.dingudoes.Adapters.BrowseTaskRecyclerAdapter;
import com.innasoft.dingudoes.Apis.RetrofitClient;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.SearchTaskResponse;
import com.innasoft.dingudoes.Singleton.AppController;
import com.innasoft.dingudoes.storage.PrefManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.innasoft.dingudoes.Singleton.AppController.TAG;

public class SearchFragment extends Fragment {
    RecyclerView recycler_Tasks;
    LinearLayout nodataLayout;
    @BindView(R.id.progressLayout)
    LinearLayout progressLayout;
    private List<SearchTaskResponse.DataBean> dataBeanList;
    BrowseTaskRecyclerAdapter browseTaskRecyclerAdapter;
    PrefManager prefManager;
    String token, id, min, max, loc, dist;

    View view;
    AppController appController;

    public static SearchFragment newInstance() {
        SearchFragment fragment = new SearchFragment();

        // do some initial setup if needed, for example Listener etc

        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        view = inflater.inflate(R.layout.fragment_search, container, false);
        ButterKnife.bind(this,view);
        appController = (AppController) getActivity().getApplication();

        setHasOptionsMenu(true);

        recycler_Tasks = view.findViewById(R.id.recycler_Tasks);
        nodataLayout = view.findViewById(R.id.nodataLayout);

        prefManager = new PrefManager(getActivity());
        HashMap<String, String> profile = prefManager.getUserDetails();
        token = profile.get("Token");


        Bundle bundle = this.getArguments();
        if (bundle != null) {
            loc = bundle.getString("Location");
            min = bundle.getString("Minimum");
            max = bundle.getString("Maximum");
            dist = bundle.getString("Distance");
            id = bundle.getString("Tobe");
        }

        if (appController.isConnection()) {
            searchData(token, id, min, max, loc, dist);
        } else {
            String message = "Sorry! Not connected to internet";
            int color = Color.RED;

            Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
        }


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        AppCompatActivity activity = (AppCompatActivity) getActivity();
        ActionBar actionBar = activity.getSupportActionBar();
        actionBar.setTitle(getResources().getString(R.string.title_browse));
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && data != null) {

            loc = data.getStringExtra("Location");
            min = data.getStringExtra("Minimum");
            max = data.getStringExtra("Maximum");
            dist = data.getStringExtra("Distance");
            id = data.getStringExtra("Tobe");
            Log.d(TAG, "onActivityResult: " + id);

            searchData(token, id, min, max, loc, dist);

        }
    }


    private void searchData(String token, String id, String min, String max, String loc, String dist) {

       progressLayout.setVisibility(View.VISIBLE);

        Call<SearchTaskResponse> call = RetrofitClient.getInstance().getApi().SearchTask(token, id, min, max, loc, dist);
        call.enqueue(new Callback<SearchTaskResponse>() {
            @Override
            public void onResponse(Call<SearchTaskResponse> call, Response<SearchTaskResponse> response) {
                if (response.isSuccessful()) ;
                SearchTaskResponse searchTaskResponse = response.body();
                //  Log.d(TAG, "onResponse: "+searchTaskResponse.getData());
                if (searchTaskResponse.isSuccess()) {
                    progressLayout.setVisibility(View.GONE);

                    dataBeanList = searchTaskResponse.getData();
                    Log.d(TAG, "onResponse: " + dataBeanList.size());
                    if (dataBeanList.size() > 0) {

                        browseTaskRecyclerAdapter = new BrowseTaskRecyclerAdapter(dataBeanList, getActivity());
                        final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
                        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                        recycler_Tasks.setLayoutManager(layoutManager);
                        recycler_Tasks.setNestedScrollingEnabled(false);
                        recycler_Tasks.setAdapter(browseTaskRecyclerAdapter);

                        browseTaskRecyclerAdapter.notifyDataSetChanged();

                    } else {

                        Log.d(TAG, "onResponse: " + "No Data Found !");
                        final LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
                        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                        recycler_Tasks.setLayoutManager(layoutManager);
                        recycler_Tasks.setNestedScrollingEnabled(false);
                        recycler_Tasks.setAdapter(null);

                        //  Toast.makeText(getActivity(),"No Data Found !",Toast.LENGTH_SHORT).show();
                    }


                }

            }

            @Override
            public void onFailure(Call<SearchTaskResponse> call, Throwable t) {
                progressLayout.setVisibility(View.GONE);
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu, menu);
        MenuItem search = menu.findItem(R.id.search);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(search);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                return false;
            }

            @Override
            public boolean onQueryTextChange(String query) {
                if (!query.isEmpty()) {
                    filter(query.toString());
                    browseTaskRecyclerAdapter.notifyDataSetChanged();
                }
                return false;
            }          });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.map:
                startActivity(new Intent(getContext(), MapSearchActivity.class));
                return true;
            case R.id.filter:
                Intent intent = new Intent(getContext(), FilterActivity.class);
                startActivityForResult(intent, 100);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    // search filter
    private void filter(String text) {
        ArrayList temp = new ArrayList();
        for (SearchTaskResponse.DataBean d : dataBeanList) {
//or use .equal(text) with you want equal match
//use .toLowerCase() for better matches
            if (d.getTitle().toLowerCase().contains(text.toLowerCase())
                    || d.getCategory().toLowerCase().contains(text.toLowerCase())) {
                temp.add(d);
            }
        }
        //update recyclerview

        if (temp.size() != 0) {
            browseTaskRecyclerAdapter.updateList(temp);
            recycler_Tasks.setVisibility(View.VISIBLE);
            nodataLayout.setVisibility(View.GONE);
        } else {
            browseTaskRecyclerAdapter.notifyDataSetChanged();
            Toast.makeText(getActivity(), "No Data ", Toast.LENGTH_SHORT).show();
            recycler_Tasks.setVisibility(View.GONE);
            nodataLayout.setVisibility(View.VISIBLE);
        }
    }
}
