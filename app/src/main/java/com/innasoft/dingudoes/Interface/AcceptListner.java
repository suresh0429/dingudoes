package com.innasoft.dingudoes.Interface;

import android.widget.Button;

import com.innasoft.dingudoes.Response.MyTaskDetailsResponse;

import java.util.List;

public interface AcceptListner {

    void onClick(int position, List<MyTaskDetailsResponse.DataBean.QuotesBean> quotesBeanList, Button btn_accept);
}
