package com.innasoft.dingudoes.Db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.innasoft.dingudoes.Models.TaskDraftModel;

import java.util.ArrayList;

public class SqliteDatabase extends SQLiteOpenHelper {

    private	static final int DATABASE_VERSION =	5;
    public static final String TABLE_NAME = "DRAFTTASKS";
    private	static final String TABLE_CONTACTS = "drafts";

    public static final String _ID = "_id";
    public static final String TITLE = "title";
    public static final String DESC = "description";
    public static final String LOCATION = "location";
    public static final String DATE = "date";
    public static final String MUST_HAVE = "mustHave";
    public static final String HOUR = "hour";
    public static final String TIME = "time";
    public static final String PRICE = "price";
    public static final String CATEGORY = "category";
    public static final String ISREMOTE = "isremote";
    public static final String BUDGETTYPE = "budgetType";
    public static final String TASKERS = "taskers";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";

    public SqliteDatabase(Context context) {
        super(context, TABLE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String	CREATE_CONTACTS_TABLE = "CREATE	TABLE " + TABLE_CONTACTS + "(" + _ID + " INTEGER PRIMARY KEY," + TITLE + " TEXT NOT NULL, " + DESC + " TEXT, " + LOCATION + " TEXT, " + DATE + " TEXT" +
                "," + MUST_HAVE + " TEXT, " + HOUR + " TEXT, "+ TIME + " TEXT, "+ PRICE + " TEXT, "+ CATEGORY + " TEXT, "+ ISREMOTE + " TEXT, " +BUDGETTYPE + " TEXT, " +TASKERS + " TEXT, " + LATITUDE + " TEXT, " + LONGITUDE + " TEXT);";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_CONTACTS);
        onCreate(db);
    }

    public ArrayList<TaskDraftModel> listContacts(){
        String sql = "select * from " + TABLE_CONTACTS;
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<TaskDraftModel> storeContacts = new ArrayList<>();
        Cursor cursor = db.rawQuery(sql, null);
        if(cursor.moveToFirst()){
            do{
                int id = Integer.parseInt(cursor.getString(0));
                String title = cursor.getString(1);
                String desc = cursor.getString(2);
                String location = cursor.getString(3);
                String date = cursor.getString(4);
                String showvalue = cursor.getString(5);
                String hours = cursor.getString(6);
                String time = cursor.getString(7);
                String price = cursor.getString(8);
                String category = cursor.getString(9);
                String isRemote = cursor.getString(10);
                String budgetType = cursor.getString(11);
                String taskers = cursor.getString(12);
                Double latitude = cursor.getDouble(13);
                Double longitude = cursor.getDouble(14);
                storeContacts.add(new TaskDraftModel(id,title, desc, location,  date, showvalue, hours, time,price,category,isRemote,budgetType,taskers, latitude, longitude));
            }while (cursor.moveToNext());
        }
        cursor.close();
        return storeContacts;
    }

    public void addContacts(TaskDraftModel taskDraftModel){
        ContentValues contentValue = new ContentValues();
        contentValue.put(TITLE,taskDraftModel.getTitle() );
        contentValue.put(DESC, taskDraftModel.getDesc());
        contentValue.put(LOCATION, taskDraftModel.getLocation());
        contentValue.put(DATE, taskDraftModel.getDateString());
        contentValue.put(MUST_HAVE, taskDraftModel.getMust_haves());
        contentValue.put(HOUR, taskDraftModel.getHours());
        contentValue.put(TIME, taskDraftModel.getTime());
        contentValue.put(PRICE, taskDraftModel.getPrice());
        contentValue.put(CATEGORY, taskDraftModel.getCategory());
        contentValue.put(ISREMOTE, taskDraftModel.getIsRemote());
        contentValue.put(BUDGETTYPE, taskDraftModel.getBudgetType());
        contentValue.put(TASKERS, taskDraftModel.getTaskers());
        contentValue.put(LATITUDE, taskDraftModel.getLatitude());
        contentValue.put(LONGITUDE, taskDraftModel.getLongitude());
        SQLiteDatabase db = this.getWritableDatabase();
        db.insert(TABLE_CONTACTS, null, contentValue);
    }

    public void updateContacts(TaskDraftModel taskDraftModel){
        ContentValues contentValue = new ContentValues();
        contentValue.put(TITLE,taskDraftModel.getTitle() );
        contentValue.put(DESC, taskDraftModel.getDesc());
        contentValue.put(LOCATION, taskDraftModel.getLocation());
        contentValue.put(DATE, taskDraftModel.getDateString());
        contentValue.put(MUST_HAVE, taskDraftModel.getMust_haves());
        contentValue.put(HOUR, taskDraftModel.getHours());
        contentValue.put(TIME, taskDraftModel.getTime());
        contentValue.put(PRICE, taskDraftModel.getPrice());
        contentValue.put(CATEGORY, taskDraftModel.getCategory());
        contentValue.put(ISREMOTE, taskDraftModel.getIsRemote());
        contentValue.put(BUDGETTYPE, taskDraftModel.getBudgetType());
        contentValue.put(TASKERS, taskDraftModel.getTaskers());
        contentValue.put(LATITUDE, taskDraftModel.getLatitude());
        contentValue.put(LONGITUDE, taskDraftModel.getLongitude());
        SQLiteDatabase db = this.getWritableDatabase();
        db.update(TABLE_CONTACTS, contentValue, _ID	+ "	= ?", new String[] { String.valueOf(taskDraftModel.getId())});
    }

   /* public TaskDraftModel findContacts(String name){
        String query = "Select * FROM "	+ TABLE_CONTACTS + " WHERE " + TITLE + " = " + "name";
        SQLiteDatabase db = this.getWritableDatabase();
        TaskDraftModel contacts = null;
        Cursor cursor = db.rawQuery(query,	null);
        if	(cursor.moveToFirst()){
            int id = Integer.parseInt(cursor.getString(0));
            String contactsName = cursor.getString(1);
            String contactsNo = cursor.getString(2);
            contacts = new TaskDraftModel(id, contactsName, contactsNo);
        }
        cursor.close();
        return contacts;
    }*/

    public void deleteContact(int id){
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_CONTACTS, _ID	+ "	= ?", new String[] { String.valueOf(id)});
    }
}
