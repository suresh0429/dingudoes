package com.innasoft.dingudoes.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.innasoft.dingudoes.Apis.RetrofitClient;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.CreateTaskAlertsResponse;
import com.innasoft.dingudoes.Singleton.AppController;
import com.innasoft.dingudoes.storage.PrefManager;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.ceryle.segmentedbutton.SegmentedButtonGroup;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewTaskAlertActivity extends AppCompatActivity {

    @BindView(R.id.segmentedButtonGroup)
    SegmentedButtonGroup segmentedButtonGroup;
    @BindView(R.id.linear_inperson)
    LinearLayout linearInperson;
    @BindView(R.id.linear_remote)
    LinearLayout linearRemote;
    @BindView(R.id.btn_login)
    Button btnLogin;
    AppController appController;
    @BindView(R.id.txt_dstnce)
    TextView txtDstnce;
    @BindView(R.id.seekbar_distance)
    SeekBar seekbarDistance;
    String Inperson, Remote;
    @BindView(R.id.edt_text)
    EditText edtText;
    @BindView(R.id.edt_location)
    EditText edtLocation;
    String location,desc,distance,token;
    PrefManager prefManager;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.newtaskalert_activity);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("New Task Alert");
        prefManager = new PrefManager(getApplicationContext());
        HashMap<String, String> profile = prefManager.getUserDetails();
        token = profile.get("Token");



        appController = (AppController) getApplication();

        if (appController.isConnection()) {

            getData();
        } else {
            String message = "Sorry! Not connected to internet";
            int color = Color.RED;

            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }


    }

    private void getData() {

        Remote = "0";
        segmentedButtonGroup.setOnClickedButtonListener(position -> {
            if (position == 0) {
                Remote = "0";

            } else if (position == 1) {
                Remote = "1";

            }
        });

        seekbarDistance.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                txtDstnce.setText(" " + i);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }


            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                desc=edtText.getText().toString().trim();
                location=edtLocation.getText().toString().trim();
                distance=txtDstnce.getText().toString().trim();

                if (desc.equals("")){
                    Toast.makeText(NewTaskAlertActivity.this, "Enter Task Title..", Toast.LENGTH_SHORT).show();
                }
                else if (location.equals("")){
                    Toast.makeText(NewTaskAlertActivity.this, "Enter Location..", Toast.LENGTH_SHORT).show();

                }
                else if (distance.isEmpty()){
                    Toast.makeText(NewTaskAlertActivity.this, "select distance..", Toast.LENGTH_SHORT).show();
                }
                else {
                    getCall(token,desc,location,distance,Remote);
                }



            }
        });

    }

    private void getCall(String token, String desc, String location, String distance, String remote) {

        ProgressDialog progressDialog=new ProgressDialog(NewTaskAlertActivity.this);
        progressDialog.setMessage("Loading....");
        progressDialog.show();

        Call<CreateTaskAlertsResponse> createTaskAlertsResponseCall= RetrofitClient.getInstance().getApi().CreateTaskAlert(token,"",remote,desc,location,distance);
        createTaskAlertsResponseCall.enqueue(new Callback<CreateTaskAlertsResponse>() {
            @Override
            public void onResponse(Call<CreateTaskAlertsResponse> call, Response<CreateTaskAlertsResponse> response) {
                if (response.isSuccessful());
                CreateTaskAlertsResponse createTaskAlertsResponse=response.body();
                if (createTaskAlertsResponse.isSuccess()){
                    progressDialog.dismiss();
                    Toast.makeText(NewTaskAlertActivity.this, createTaskAlertsResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    Intent intent=new Intent(NewTaskAlertActivity.this,TaskAlertActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
                else {
                    progressDialog.dismiss();
                    Toast.makeText(NewTaskAlertActivity.this, createTaskAlertsResponse.getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<CreateTaskAlertsResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(NewTaskAlertActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);

    }

}
