package com.innasoft.dingudoes.Activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.loader.content.CursorLoader;

import com.innasoft.dingudoes.Apis.RetrofitClient;
import com.innasoft.dingudoes.Singleton.AppController;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.GetProfileResponse;
import com.innasoft.dingudoes.Response.ProfileResponse;
import com.innasoft.dingudoes.storage.PrefManager;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static com.innasoft.dingudoes.Activities.PostTaskActivity.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE;
import static com.innasoft.dingudoes.Singleton.AppController.TAG;

public class ConfirmDetailsActivity extends AppCompatActivity {

    @BindView(R.id.relative_uprofile)
    RelativeLayout relativeUprofile;
    @BindView(R.id.relative_baccount)
    RelativeLayout relativeBaccount;
    @BindView(R.id.relative_baddress)
    RelativeLayout relativeBaddress;
    @BindView(R.id.relative_dateofbirth)
    RelativeLayout relativeDateofbirth;
    @BindView(R.id.relative_mobile)
    RelativeLayout relativeMobile;
    PrefManager prefManager;
    String token, DateofBirth, Address;
    @BindView(R.id.img_uploadpicture)
    ImageView imgUploadpicture;
    @BindView(R.id.img_banaccount)
    ImageView imgBanaccount;
    @BindView(R.id.img_address)
    ImageView imgAddress;
    @BindView(R.id.img_dateof)
    ImageView imgDateof;
    @BindView(R.id.img_mobile)
    ImageView imgMobile;
    String activity;
    String dateofBirth, address, userId, bankAccount, photo, name, email, about, skill, transportt, language;
    int mobile_verify;
    public static final int RESULT_GALLERY = 0;
    @BindView(R.id.buttonOk)
    Button buttonOk;

    private ArrayList permissionsToRequest;
    private ArrayList permissionsRejected = new ArrayList();
    private ArrayList permissions = new ArrayList();

    private final static int ALL_PERMISSIONS_RESULT = 101;
    AppController appController;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.confirm_details);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Confirm Details");
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_clear_black);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        prefManager = new PrefManager(ConfirmDetailsActivity.this);
        HashMap<String, String> profile = prefManager.getUserDetails();
        token = profile.get("Token");
        userId = profile.get("userid");

        appController = (AppController) getApplication();

        if (appController.isConnection()) {

            getUserDetails(userId);


        } else {

            String message = "Sorry! Not connected to internet";
            int color = Color.RED;

            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }


        checkPermissionREAD_EXTERNAL_STORAGE(ConfirmDetailsActivity.this);
        permissions.add(ACCESS_FINE_LOCATION);
        permissions.add(ACCESS_COARSE_LOCATION);

        permissionsToRequest = findUnAskedPermissions(permissions);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            if (permissionsToRequest.size() > 0)
                requestPermissions((String[]) permissionsToRequest.toArray(new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
        }

        if (getIntent() != null) {
            activity = getIntent().getStringExtra("Activity");
        }

    }

    private void getUserDetails(String userId) {

        Call<GetProfileResponse> call = RetrofitClient.getInstance().getApi().Profile(userId);
        call.enqueue(new Callback<GetProfileResponse>() {
            @Override
            public void onResponse(Call<GetProfileResponse> call, Response<GetProfileResponse> response) {
                if (response.isSuccessful()) ;
                GetProfileResponse getProfileResponse = response.body();
                if (getProfileResponse.isSuccess()) {
                    GetProfileResponse.DataBean dataBean = getProfileResponse.getData();
                    GetProfileResponse.DataBean.UserDetailBean userDetailBean = dataBean.getUserDetail();

                    mobile_verify = dataBean.getMobileVerified();
                    // dateofBirth = userDetailBean.getDateOfBirth();
                    address = userDetailBean.getBillingAddress();
                    bankAccount = dataBean.getBeneficaryId();
                    photo = dataBean.getProfileImage();
                    name = dataBean.getFullName();
                    email = dataBean.getEmail();
                    about = dataBean.getUserDetail().getAbout();
                    skill = dataBean.getUserDetail().getSkills();
                    transportt = dataBean.getUserDetail().getTransport();
                    language = dataBean.getUserDetail().getLanguages();
                    dateofBirth = dataBean.getUserDetail().getDateOfBirth();


                    if (photo != null && !photo.isEmpty()) {

                        imgUploadpicture.setImageResource(R.drawable.ic_confirn);
                        relativeUprofile.setEnabled(false);

                    } else {
                        imgUploadpicture.setImageResource(R.drawable.ic_chevron_right_black_24dp);
                        relativeUprofile.setEnabled(true);
                    }
                    //bankaccount
                    if (bankAccount != null && !bankAccount.isEmpty()) {

                        imgBanaccount.setImageResource(R.drawable.ic_confirn);
                        relativeBaccount.setEnabled(false);

                    } else {
                        imgBanaccount.setImageResource(R.drawable.ic_chevron_right_black_24dp);
                        relativeBaccount.setEnabled(true);
                    }

                    //address
                    if (address != null && !address.isEmpty()) {
                        imgAddress.setImageResource(R.drawable.ic_confirn);
                        relativeBaddress.setEnabled(false);
                    } else {
                        imgAddress.setImageResource(R.drawable.ic_chevron_right_black_24dp);
                        relativeBaddress.setEnabled(true);
                    }

                    //dateofbirth
                    if (dateofBirth != null && !dateofBirth.isEmpty()) {

                        imgDateof.setImageResource(R.drawable.ic_confirn);
                        relativeDateofbirth.setEnabled(false);
                    } else {

                        imgDateof.setImageResource(R.drawable.ic_chevron_right_black_24dp);
                        relativeDateofbirth.setEnabled(true);

                    }
                    //mobile number
                    if (mobile_verify == 0) {

                        imgMobile.setImageResource(R.drawable.ic_chevron_right_black_24dp);
                        relativeMobile.setEnabled(true);

                    } else {
                        imgMobile.setImageResource(R.drawable.ic_confirn);
                        relativeMobile.setEnabled(false);
                    }


                }

            }

            @Override
            public void onFailure(Call<GetProfileResponse> call, Throwable t) {
                Toast.makeText(ConfirmDetailsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);

    }

    @OnClick({R.id.relative_uprofile, R.id.relative_baccount, R.id.relative_baddress, R.id.relative_dateofbirth, R.id.relative_mobile, R.id.buttonOk})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.relative_uprofile:
                Intent galleryIntent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                startActivityForResult(galleryIntent, RESULT_GALLERY);

                break;
            case R.id.relative_baccount:
                Intent intent3 = new Intent(ConfirmDetailsActivity.this, AddBankAccountActivity.class);
                startActivityForResult(intent3, 100);

                break;
            case R.id.relative_baddress:
                Intent intent = new Intent(ConfirmDetailsActivity.this, EditProfileActivity.class);
                intent.putExtra("Fullname", name);
                intent.putExtra("Email", email);
                intent.putExtra("address", address);
                intent.putExtra("About", about);
                intent.putExtra("Skills", skill);
                intent.putExtra("Transport", transportt);
                intent.putExtra("Language", language);
                intent.putExtra("dob", dateofBirth);
                startActivityForResult(intent, 102);

                break;
            case R.id.relative_dateofbirth:
                Intent intent1 = new Intent(ConfirmDetailsActivity.this, EditProfileActivity.class);
                intent1.putExtra("Fullname", name);
                intent1.putExtra("Email", email);
                intent1.putExtra("address", address);
                intent1.putExtra("About", about);
                intent1.putExtra("Skills", skill);
                intent1.putExtra("Transport", transportt);
                intent1.putExtra("Language", language);
                intent1.putExtra("dob", dateofBirth);
                startActivityForResult(intent1, 103);

                break;
            case R.id.relative_mobile:
                Intent intent2 = new Intent(ConfirmDetailsActivity.this, VerifyMobileNumberActivity.class);
                startActivityForResult(intent2, 101);
                break;
            case R.id.buttonOk:

                if (photo == null || photo.isEmpty()) {
                    Toast.makeText(ConfirmDetailsActivity.this, "Please Upload Profile Picture..", Toast.LENGTH_SHORT).show();

                } else if (bankAccount == null || bankAccount.isEmpty()) {
                    Toast.makeText(ConfirmDetailsActivity.this, "Please Verify bankAccount..", Toast.LENGTH_SHORT).show();

                } else if (address == null || address.isEmpty()) {

                    Toast.makeText(ConfirmDetailsActivity.this, "Please Verify address..", Toast.LENGTH_SHORT).show();
                } else if (dateofBirth == null || dateofBirth.isEmpty()) {
                    Toast.makeText(ConfirmDetailsActivity.this, "Please Verify Date of Birth..", Toast.LENGTH_SHORT).show();

                } else if (mobile_verify == 0) {
                    Toast.makeText(ConfirmDetailsActivity.this, "Please Verify Mobile Number..", Toast.LENGTH_SHORT).show();

                } else {

                    Intent intent5 = getIntent();
                    intent5.putExtra("Activity",activity);
                    intent5.putExtra("userId",userId);
                    setResult(Activity.RESULT_OK, intent5);
                    finish();
                }

                break;
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == RESULT_GALLERY && data != null) {

            Uri imageUri = data.getData();
            Log.d(TAG, "onActivityResult: " + imageUri);

            uploadProfile(imageUri);
            getUserDetails(userId);

        } else if (requestCode == 101) {

            getUserDetails(userId);
            Log.d(TAG, "onActivityResult:" + "result cancelled1");

            if (resultCode == Activity.RESULT_CANCELED) {


                Log.d(TAG, "onActivityResult:" + "result cancelled");
            }

        } else if (requestCode == 100) {
            if (resultCode == Activity.RESULT_CANCELED) {
                getUserDetails(userId);

            }
        }

    }

    private void uploadProfile(Uri imageUri) {
        ProgressDialog progressDialog = new ProgressDialog(ConfirmDetailsActivity.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();

        //pass it like this
        File file = new File(getRealPathFromURI(imageUri));
        RequestBody requestFile = RequestBody.create(MediaType.parse("multipart/form-data"), file);

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part body = MultipartBody.Part.createFormData("profilePic", file.getName(), requestFile);


        Call<ProfileResponse> call = RetrofitClient.getInstance().getApi().uploadProfilepic(token, body);
        call.enqueue(new Callback<ProfileResponse>() {
            @Override
            public void onResponse(Call<ProfileResponse> call, Response<ProfileResponse> response) {
                if (response.isSuccessful()) ;
                ProfileResponse createReportTaskResponse = response.body();
                if (createReportTaskResponse.isSuccess()) {
                    progressDialog.dismiss();
                    // finish();
                    Toast.makeText(ConfirmDetailsActivity.this, createReportTaskResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    getUserDetails(userId);

                } else {
                    progressDialog.dismiss();
                    Toast.makeText(ConfirmDetailsActivity.this, createReportTaskResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<ProfileResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(ConfirmDetailsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(this, contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }


    public boolean checkPermissionREAD_EXTERNAL_STORAGE(
            final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context,
                    Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        (Activity) context,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    showDialog("External storage", context, Manifest.permission.READ_EXTERNAL_STORAGE);

                } else {
                    ActivityCompat
                            .requestPermissions(
                                    (Activity) context,
                                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }

        } else {
            return true;
        }
    }

    private void showDialog(String external_storage, Context context, String readExternalStorage) {

        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setCancelable(true);
        alertBuilder.setTitle("Permission necessary");
        alertBuilder.setMessage(external_storage + " permission is necessary");
        alertBuilder.setPositiveButton(android.R.string.yes,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions((Activity) context,
                                new String[]{readExternalStorage},
                                MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                    }
                });
        AlertDialog alert = alertBuilder.create();
        alert.show();
    }

    private ArrayList findUnAskedPermissions(ArrayList wanted) {
        ArrayList result = new ArrayList();

        for (Object perm : wanted) {
            if (!hasPermission(String.valueOf(perm))) {
                result.add(perm);
            }
        }

        return result;
    }

    private boolean hasPermission(Object permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission((String) permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }


    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case ALL_PERMISSIONS_RESULT:
                for (Object perms : permissionsToRequest) {
                    if (!hasPermission(perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale((String) permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions((String[]) permissionsRejected.toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }

                }

                break;
        }

    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(ConfirmDetailsActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

}
