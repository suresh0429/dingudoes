package com.innasoft.dingudoes.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.innasoft.dingudoes.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MakeanOfferActivity1 extends AppCompatActivity {

    @BindView(R.id.layoutTwo)
    LinearLayout layoutTwo;
    @BindView(R.id.txtFinal)
    TextView txtFinal;
    @BindView(R.id.txtFinalPrice)
    TextView txtFinalPrice;
    @BindView(R.id.btnContinue)
    Button btnContinue;

    double reciveAmount, servicePrice, gstPrice;
    int taskid;
    @BindView(R.id.etDescription)
    EditText etDescription;
    @BindView(R.id.txtCount)
    TextView txtCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_makean_offer1);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Make an offer");

        if (getIntent() != null) {
            reciveAmount = getIntent().getDoubleExtra("receiveAmount", 0.00);
            servicePrice = getIntent().getDoubleExtra("servicePrice", 0.00);
            gstPrice = getIntent().getDoubleExtra("gstPrice", 0.00);
            taskid = getIntent().getIntExtra("taskid",0);
            txtFinalPrice.setText("\u20b9" + String.format("%.2f", reciveAmount));
        }

        etDescription.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                txtCount.setText(String.valueOf(s.toString().length())+" / "+"1500");
                if (s.length() >= 15) {
                    btnContinue.setEnabled(true);
                    btnContinue.setAlpha(0.9f);
                } else {
                    btnContinue.setEnabled(false);
                    btnContinue.setAlpha(0.5f);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);

    }

    @OnClick(R.id.btnContinue)
    public void onViewClicked() {
        Intent intent = new Intent(MakeanOfferActivity1.this, PreviewOfferActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("receiveAmount", reciveAmount);
        intent.putExtra("servicePrice", servicePrice);
        intent.putExtra("gstPrice", gstPrice);
        intent.putExtra("taskid",taskid);
        startActivity(intent);
    }
}
