package com.innasoft.dingudoes.Activities;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.goodiebag.pinview.Pinview;
import com.innasoft.dingudoes.Apis.RetrofitClient;
import com.innasoft.dingudoes.Singleton.AppController;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.VerifyMobileResponse;
import com.innasoft.dingudoes.Response.VerifyOtpResponse;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangeMobilenumberOtpActivity extends AppCompatActivity {

    @BindView(R.id.pinview)
    Pinview pinview;
    @BindView(R.id.txt_resend)
    TextView txtResend;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    String otp, u_id, mobile;
    private static int TIME_OUT = 1500;
    AppController appController;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.otp_activity);

        appController = (AppController) getApplication();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("OTP Verfication");
        ButterKnife.bind(this);

        progressDialog = new ProgressDialog(ChangeMobilenumberOtpActivity.this);
        progressDialog.setMessage("Loading....");

        u_id = getIntent().getStringExtra("user_id");
        mobile = getIntent().getStringExtra("Mobile");

    }

    @OnClick({R.id.pinview, R.id.txt_resend, R.id.btn_submit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.pinview:
                break;
            case R.id.txt_resend:

                Resend(mobile,u_id);

                break;
            case R.id.btn_submit:

                if (appController.isConnection()) {
                    OtpMethod();
                } else {

                    String message = "Sorry! Not connected to internet";
                    int color = Color.RED;

                    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                }

                break;
        }
    }

    private void OtpMethod() {

        otp = pinview.getValue().toString().trim();

        progressDialog.show();
        Call<VerifyOtpResponse> call = RetrofitClient.getInstance().getApi().VerifyOtp(otp, u_id);
        call.enqueue(new Callback<VerifyOtpResponse>() {
            @Override
            public void onResponse(Call<VerifyOtpResponse> call, Response<VerifyOtpResponse> response) {
                if (response.isSuccessful()) ;
                VerifyOtpResponse verifyOtpResponse = response.body();
                if (verifyOtpResponse.isSuccess()) {

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                            Toast.makeText(ChangeMobilenumberOtpActivity.this, verifyOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                           finish();
                        }
                    }, TIME_OUT);


                } else {
                    progressDialog.dismiss();
                    Toast.makeText(ChangeMobilenumberOtpActivity.this, verifyOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<VerifyOtpResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(ChangeMobilenumberOtpActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void Resend(String mobile, String u_id) {

        progressDialog.show();
        Call<VerifyMobileResponse> call1=RetrofitClient.getInstance().getApi().VerifyMobile(mobile,u_id);
        call1.enqueue(new Callback<VerifyMobileResponse>() {
            @Override
            public void onResponse(Call<VerifyMobileResponse> call, Response<VerifyMobileResponse> response) {
                if (response.isSuccessful());
                VerifyMobileResponse verifyMobileResponse=response.body();
                if (verifyMobileResponse.isSuccess()){
                    progressDialog.dismiss();
                    Toast.makeText(ChangeMobilenumberOtpActivity.this, verifyMobileResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
                else {
                    progressDialog.dismiss();
                    Toast.makeText(ChangeMobilenumberOtpActivity.this, verifyMobileResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<VerifyMobileResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(ChangeMobilenumberOtpActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);

    }
}
