package com.innasoft.dingudoes.Activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.Constraints;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.dingudoes.Adapters.RecyclerImagesAdapter;
import com.innasoft.dingudoes.Adapters.TaskCommentsAdapter;
import com.innasoft.dingudoes.Apis.RetrofitClient;
import com.innasoft.dingudoes.Models.CommentsModel;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.CommentsParentResponse;
import com.innasoft.dingudoes.Response.CreateCommentResponse;
import com.innasoft.dingudoes.Response.CreateQuoteResponse;
import com.innasoft.dingudoes.Response.CreateReportTaskResponse;
import com.innasoft.dingudoes.Response.GetProfileResponse;
import com.innasoft.dingudoes.Response.GetTaskCommentResponse;
import com.innasoft.dingudoes.Response.GetTaskDetailsResponse;
import com.innasoft.dingudoes.Singleton.AppController;
import com.innasoft.dingudoes.storage.PrefManager;
import com.skyhope.showmoretextview.ShowMoreTextView;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static com.innasoft.dingudoes.Singleton.AppController.TAG;

public class TaskDetailsActivity extends AppCompatActivity {

    public static final int RESULT_GALLERY = 0;
    ShowMoreTextView text_view_show_more;
    PrefManager prefManager;
    String token;
    int mobile_verify;
    String dateofBirth, address, userId, bankAccount, photo, name, email, about, skill, transportt, language;
    @BindView(R.id.btn_quote)
    Button btnQuote;
    int taskid;
    RecyclerView recycler_list;
    TaskCommentsAdapter taskCommentsAdapter;

    AlertDialog alertDialog;
    AppController appController;
    GetProfileResponse.DataBean dataBean;
    GetProfileResponse.DataBean.UserDetailBean userDetailBean;
    @BindView(R.id.txt_title)
    TextView txtTitle;
    @BindView(R.id.txt_name)
    TextView txtName;
    @BindView(R.id.txt_address)
    TextView txtAddress;
    @BindView(R.id.txt_date)
    TextView txtDate;
    @BindView(R.id.txt_budget)
    TextView txtBudget;
    @BindView(R.id.text_view_show_more)
    ShowMoreTextView textViewShowMore;
    @BindView(R.id.edt_comment)
    EditText edtComment;
    @BindView(R.id.txt_send)
    TextView txtSend;
    @BindView(R.id.recycler_images)
    RecyclerView recyclerImages;
    @BindView(R.id.btn_offer)
    Button btnOffer;
    @BindView(R.id.recycler_list)
    RecyclerView recyclerList;


    private ArrayList permissionsToRequest;
    private ArrayList permissionsRejected = new ArrayList();
    private ArrayList permissions = new ArrayList();

    private final static int ALL_PERMISSIONS_RESULT = 101;

    ArrayList<CommentsParentResponse> commentsParentResponse = new ArrayList<>();
    GridLayoutManager gridLayoutManager;
    GetTaskDetailsResponse.DataBean getTaskDetailsResponseData;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.taskdetails_activity);
        ButterKnife.bind(this);

        appController = (AppController) getApplication();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Task Details");
        recycler_list = findViewById(R.id.recycler_list);

        textViewShowMore.setShowingLine(2);
        textViewShowMore.addShowMoreText("More");
        textViewShowMore.addShowLessText("Less");
        textViewShowMore.setShowMoreColor(Color.RED); // or other color
        textViewShowMore.setShowLessTextColor(Color.RED); // or other color


        prefManager = new PrefManager(TaskDetailsActivity.this);
        HashMap<String, String> profile = prefManager.getUserDetails();
        token = profile.get("Token");
        userId = profile.get("userid");


//        checkPermissionREAD_EXTERNAL_STORAGE(this);
        permissions.add(ACCESS_FINE_LOCATION);
        permissions.add(ACCESS_COARSE_LOCATION);

        taskid = getIntent().getIntExtra("Task_id", 0);
        Log.d(TAG, "onCreate: " + taskid);

        if (appController.isConnection()) {
            getData(token, taskid);
            getComments(token, taskid);
            getUserDetails(userId);

            txtSend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String comment = edtComment.getText().toString();
                    if (comment.equals("")) {
                        Toast.makeText(TaskDetailsActivity.this, "Enter Comments..", Toast.LENGTH_SHORT).show();
                    } else {
                        ProgressDialog progressDialog = new ProgressDialog(TaskDetailsActivity.this);
                        progressDialog.setMessage("Loading.....");
                        progressDialog.show();

                        Call<CreateCommentResponse> call = RetrofitClient.getInstance().getApi().CreateComment(token, null, taskid, comment);
                        call.enqueue(new Callback<CreateCommentResponse>() {
                            @Override
                            public void onResponse(Call<CreateCommentResponse> call, Response<CreateCommentResponse> response) {
                                if (response.isSuccessful()) ;
                                CreateCommentResponse createCommentResponse = response.body();
                                if (createCommentResponse.isSuccess()) {
                                    progressDialog.dismiss();
                                    Toast.makeText(TaskDetailsActivity.this, createCommentResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                    edtComment.setText("");
                                    getComments(token, taskid);
                                } else {
                                    progressDialog.dismiss();
                                    Toast.makeText(TaskDetailsActivity.this, createCommentResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }

                            @Override
                            public void onFailure(Call<CreateCommentResponse> call, Throwable t) {
                                progressDialog.dismiss();
                                Toast.makeText(TaskDetailsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                            }
                        });

                    }
                }
            });

        } else {

            String message = "Sorry! Not connected to internet";
            int color = Color.RED;

            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }


    }

    private void getUserDetails(String userId) {
        Call<GetProfileResponse> call = RetrofitClient.getInstance().getApi().Profile(userId);
        call.enqueue(new Callback<GetProfileResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(Call<GetProfileResponse> call, Response<GetProfileResponse> response) {
                if (response.isSuccessful()) ;
                GetProfileResponse getProfileResponse = response.body();

                if (getProfileResponse.isSuccess()) {

                    dataBean = getProfileResponse.getData();
                    userDetailBean = dataBean.getUserDetail();

                    mobile_verify = dataBean.getMobileVerified();
                    dateofBirth = userDetailBean.getDateOfBirth();
                    address = userDetailBean.getBillingAddress();
                    bankAccount = dataBean.getBeneficaryId();
                    photo = dataBean.getProfileImage();
                    name = dataBean.getFullName();
                    email = dataBean.getEmail();
                    about = dataBean.getUserDetail().getAbout();
                    skill = dataBean.getUserDetail().getSkills();
                    transportt = dataBean.getUserDetail().getTransport();
                    language = dataBean.getUserDetail().getLanguages();
                    dateofBirth = dataBean.getUserDetail().getDateOfBirth();


                }
            }

            @Override
            public void onFailure(Call<GetProfileResponse> call, Throwable t) {

                Toast.makeText(TaskDetailsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void getData(String token, int taskid) {

        ProgressDialog progressDialog = new ProgressDialog(TaskDetailsActivity.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();

        Call<GetTaskDetailsResponse> call = RetrofitClient.getInstance().getApi().GetTeskDetails(token, taskid);
        call.enqueue(new Callback<GetTaskDetailsResponse>() {
            @Override
            public void onResponse(Call<GetTaskDetailsResponse> call, Response<GetTaskDetailsResponse> response) {
                if (response.isSuccessful()) ;
                GetTaskDetailsResponse getTaskDetailsResponse = response.body();
                if (getTaskDetailsResponse.isSuccess() == true) {
                    progressDialog.dismiss();
                    getTaskDetailsResponseData = getTaskDetailsResponse.getData();

                    if (getTaskDetailsResponseData.getStatus().equals("open")) {
                        btnOffer.setVisibility(View.VISIBLE);

                    }

                    textViewShowMore.setText(getTaskDetailsResponseData.getDescription());
                    txtBudget.setText("\u20B9" + getTaskDetailsResponseData.getBudget() + " /-");
                    txtTitle.setText(getTaskDetailsResponseData.getTitle());
                    txtAddress.setText(getTaskDetailsResponseData.getAddress());
                    txtDate.setText(getTaskDetailsResponseData.getFinishDate());

                    String[] images = getTaskDetailsResponseData.getAttachments().toArray(new String[0]);

                    RecyclerImagesAdapter recyclerImagesAdapter = new RecyclerImagesAdapter(images, TaskDetailsActivity.this);
                    gridLayoutManager = new GridLayoutManager(getApplicationContext(), 3);
                    recyclerImages.setLayoutManager(gridLayoutManager);
                    recyclerImages.setNestedScrollingEnabled(false);
                    recyclerImages.setAdapter(recyclerImagesAdapter);


                    for (String value : images) {

                        Log.d(TAG, "image" + value);
                    }


                } else if (getTaskDetailsResponse.isSuccess() == false) {
                    progressDialog.dismiss();
                    Toast.makeText(TaskDetailsActivity.this, getTaskDetailsResponse.getMessage(), Toast.LENGTH_SHORT).show();

                }
            }

            @Override
            public void onFailure(Call<GetTaskDetailsResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(TaskDetailsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void getComments(String token, int taskid) {
        ArrayList<CommentsModel> commentsModels = new ArrayList<>();
        Call<GetTaskCommentResponse> call = RetrofitClient.getInstance().getApi().GetTaskComments(token, taskid);
        call.enqueue(new Callback<GetTaskCommentResponse>() {
            @Override
            public void onResponse(Call<GetTaskCommentResponse> call, Response<GetTaskCommentResponse> response) {
                if (response.isSuccessful()) ;
                GetTaskCommentResponse getTaskCommentResponse = response.body();
                Log.d(TAG, "onResponse: "+getTaskCommentResponse.getData());
                if (getTaskCommentResponse.isSuccess()) {
                    GetTaskCommentResponse.DataBean dataBean = getTaskCommentResponse.getData();

                    List<GetTaskCommentResponse.DataBean.CommentsBean> commentsBeanList = dataBean.getComments();

                    commentsModels.clear();
                    for (GetTaskCommentResponse.DataBean.CommentsBean commentsBean : commentsBeanList) {
                        if (commentsBean.getParentId() == 0) {
                            Log.d(Constraints.TAG, "onResponse: " + commentsBean.getParentId());


                            commentsModels.add(new CommentsModel(commentsBean.getId(), commentsBean.getParentId(), commentsBean.getTaskId(), commentsBean.getCommentText(),
                                    commentsBean.getIsDeleted(), commentsBean.getCreatedBy(), commentsBean.getCreatedAt(), commentsBean.getUpdatedBy(),
                                    commentsBean.getUpdatedAt(), commentsBean.getCreated_by(), commentsBean.getUser().getFull_name(),commentsBean.getUser().getUser_id()));
                        }
                    }

                    taskCommentsAdapter = new TaskCommentsAdapter(commentsModels, TaskDetailsActivity.this);
                    LinearLayoutManager layoutManager = new LinearLayoutManager(TaskDetailsActivity.this);
                    layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                    recycler_list.setLayoutManager(layoutManager);
                    recycler_list.setNestedScrollingEnabled(false);
                    recycler_list.setAdapter(taskCommentsAdapter);
                    taskCommentsAdapter.notifyDataSetChanged();

                }

            }

            @Override
            public void onFailure(Call<GetTaskCommentResponse> call, Throwable t) {
                Toast.makeText(TaskDetailsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.taskdetails, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;

            case R.id.postaTask:

                Intent intent = new Intent(TaskDetailsActivity.this, PostTaskActivity.class);
                intent.putExtra("title", getTaskDetailsResponseData.getTitle());
                intent.putExtra("description", getTaskDetailsResponseData.getDescription());
                intent.putExtra("category", getTaskDetailsResponseData.getCategory());
                intent.putExtra("isRemote", String.valueOf(getTaskDetailsResponseData.getIsRemote()));
                intent.putExtra("address", getTaskDetailsResponseData.getAddress());
                intent.putExtra("budgetType", getTaskDetailsResponseData.getBudgetType());
                intent.putExtra("budget", getTaskDetailsResponseData.getBudget());
                intent.putExtra("taskHours", getTaskDetailsResponseData.getTaskHours());
                intent.putExtra("taskers", String.valueOf(getTaskDetailsResponseData.getTaskers()));
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);


                return true;
            case R.id.share:
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "DinguDoes -");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, "" + "abcdefgi");
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
                return true;
            case R.id.report_task:

                if (appController.isConnection()) {
                    getReport();
                } else {
                    String message = "Sorry! Not connected to internet";
                    int color = Color.RED;

                    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                }


                return true;

        }
        return super.onOptionsItemSelected(item);

    }

    private void getReport() {

        ViewGroup viewGroup = findViewById(android.R.id.content);

        View dialogView = LayoutInflater.from(TaskDetailsActivity.this).inflate(R.layout.my_customdialog, viewGroup, false);

        AlertDialog.Builder builder = new AlertDialog.Builder(TaskDetailsActivity.this);

        builder.setView(dialogView);
        EditText edt_comment = dialogView.findViewById(R.id.edt_comment);

        Button buttonOk = dialogView.findViewById(R.id.buttonOk);
        Button buttonCancel = dialogView.findViewById(R.id.buttonCancel);


        buttonOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String report = edt_comment.getText().toString().trim();
                if (report.equals("")) {
                    Toast.makeText(TaskDetailsActivity.this, "Enter comments..", Toast.LENGTH_SHORT).show();
                } else {

                    String type = "application/json";
                    ProgressDialog progressDialog = new ProgressDialog(TaskDetailsActivity.this);
                    progressDialog.setMessage("Loading...");
                    progressDialog.show();

                    Call<CreateReportTaskResponse> call = RetrofitClient.getInstance().getApi().CreateReportTask(token, type, taskid, report);
                    call.enqueue(new Callback<CreateReportTaskResponse>() {
                        @Override
                        public void onResponse(Call<CreateReportTaskResponse> call, Response<CreateReportTaskResponse> response) {
                            if (response.isSuccessful()) ;
                            CreateReportTaskResponse createReportTaskResponse = response.body();
                            if (createReportTaskResponse.isSuccess()) {
                                progressDialog.dismiss();
                                Toast.makeText(TaskDetailsActivity.this, createReportTaskResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                alertDialog.dismiss();

                            } else {
                                progressDialog.dismiss();
                                Toast.makeText(TaskDetailsActivity.this, createReportTaskResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<CreateReportTaskResponse> call, Throwable t) {
                            progressDialog.dismiss();
                            Toast.makeText(TaskDetailsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    });


                }
            }
        });

        buttonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                alertDialog.dismiss();
            }
        });

        alertDialog = builder.create();
      //  alertDialog.setCancelable(false);
        alertDialog.show();


    }

    @OnClick({R.id.btn_quote,R.id.btn_offer})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_quote:

                break;

            case R.id.btn_offer:
                if (appController.isConnection()) {

                    if (mobile_verify == 0 || dateofBirth == null || address == null || bankAccount == null || photo == null) {

                        // GetAlertDialog();
                        Intent intent = new Intent(TaskDetailsActivity.this, ConfirmDetailsActivity.class);
                        intent.putExtra("Activity", "TaskDetailsActivity");
                        startActivityForResult(intent, 100);

                    } else {

                        Intent intent = new Intent(TaskDetailsActivity.this, MakeAnOfferActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                        intent.putExtra("taskid",taskid);
                        startActivity(intent);
                       // ClickQuote();
                    }
                } else {
                    String message = "Sorry! Not connected to internet";
                    int color = Color.RED;

                    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                }


               // Toast.makeText(this, "ggggg", Toast.LENGTH_SHORT).show();

        }
    }

    private void ClickQuote() {

        ProgressDialog progressDialog = new ProgressDialog(TaskDetailsActivity.this);
        progressDialog.setMessage("Loading....");
        progressDialog.show();

        Log.e(TAG, "onClick:" + token);

        Call<CreateQuoteResponse> createQuoteResponseCall = RetrofitClient.getInstance().getApi().CreateQuote(token, taskid);
        createQuoteResponseCall.enqueue(new Callback<CreateQuoteResponse>() {
            @Override
            public void onResponse(Call<CreateQuoteResponse> call, Response<CreateQuoteResponse> response) {
                if (response.isSuccessful()) ;
                CreateQuoteResponse createQuoteResponse = response.body();
                if (createQuoteResponse.isSuccess()) {
                    progressDialog.dismiss();
                    btnOffer.setText("Waiting");
                    Toast.makeText(getApplicationContext(), createQuoteResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(TaskDetailsActivity.this, createQuoteResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CreateQuoteResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(TaskDetailsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == 100 && data != null) {

            Log.d(TAG, "onActivityResult: " + data.getStringExtra("userId"));
            String userid = data.getStringExtra("userId");
            getUserDetails(userid);

        }


    }


    @OnClick(R.id.btn_offer)
    public void onViewClicked() {
    }
}
