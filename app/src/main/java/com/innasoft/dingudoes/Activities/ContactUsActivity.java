package com.innasoft.dingudoes.Activities;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.webkit.CookieManager;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.innasoft.dingudoes.Apis.RetrofitClient;
import com.innasoft.dingudoes.Singleton.AppController;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.ContactUsResponse;
import com.innasoft.dingudoes.storage.PrefManager;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;

import static android.content.ContentValues.TAG;

public class ContactUsActivity extends AppCompatActivity {
    @BindView(R.id.webView)
    WebView webView;

    WebSettings webSettings;
    String title, url;
    @BindView(R.id.txtAlert)
    TextView txtAlert;
    AppController appController;
    ProgressDialog progressDialog;
    PrefManager prefManager;
    String token,type;

    @SuppressLint("SetJavaScriptEnabled")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contactus_activity);
        ButterKnife.bind(this);

        appController=(AppController) getApplication();

        prefManager = new PrefManager(getApplicationContext());
        HashMap<String, String> profile = prefManager.getUserDetails();
        token = profile.get("Token");

        if (getIntent().getExtras() != null) {
            title = getIntent().getStringExtra("Activityname");
            type = getIntent().getStringExtra("TYPE");
        }

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(title);

        progressDialog=new ProgressDialog(ContactUsActivity.this);
        progressDialog.setMessage("Loading....");
        progressDialog.show();


        webView.setInitialScale(1);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.setScrollbarFadingEnabled(false);
        webView.setWebViewClient(new WebViewClient());
        CookieManager.getInstance().setAcceptCookie(true);

        //  privacyPolicy_web.loadUrl(AppUrls.BASE_URL+AppUrls.PRIVACY_POLICY);

        webView.setWebViewClient(new WebViewClient() {

            public void onPageFinished(WebView view, String url) {

                //   progress_indicator.stop();

            }
        });
        loadDataInWebview();

    }

    private void loadDataInWebview() {
        // adSliderList.clear();
        if (appController.isConnection()) {

            progressDialog.show();
            Call<ContactUsResponse> call = RetrofitClient.getInstance().getApi().ContactUs(token,type);
            call.enqueue(new Callback<ContactUsResponse>() {
                @Override
                public void onResponse(Call<ContactUsResponse> call, retrofit2.Response<ContactUsResponse> response) {
                    if (response.isSuccessful());
                    progressDialog.dismiss();
                    ContactUsResponse contactUsResponse=response.body();
                    if (!contactUsResponse.getData().isEmpty()) {
                        Log.d(TAG, "onResponse: " + contactUsResponse.getData().get(0).getLookupValue());

                        try {
                            assert response.body() != null;
                            String html = contactUsResponse.getData().get(0).getLookupValue();
                            Document document = Jsoup.parse(html);
                            Log.d(TAG, "onResponse: " + document);
                            webView.loadData(String.valueOf(document), "text/html", null);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }else {
                        Toast.makeText(getApplicationContext(), "No data Found !", Toast.LENGTH_SHORT).show();

                    }

                }

                @Override
                public void onFailure(Call<ContactUsResponse> call, Throwable t) {
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();
                }
            });


        } else {
            Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);

    }

}
