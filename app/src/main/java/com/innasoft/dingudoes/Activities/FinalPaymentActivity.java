package com.innasoft.dingudoes.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.gocashfree.cashfreesdk.CFPaymentService;
import com.innasoft.dingudoes.Apis.RetrofitClient;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.CouponResponse;
import com.innasoft.dingudoes.Response.QuoteAcceptResponse;
import com.innasoft.dingudoes.Response.TokenResponse;
import com.innasoft.dingudoes.Response.TransactioSuccessResponse;
import com.innasoft.dingudoes.Singleton.AppController;
import com.innasoft.dingudoes.storage.PrefManager;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_APP_ID;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_CUSTOMER_EMAIL;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_CUSTOMER_NAME;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_CUSTOMER_PHONE;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_ORDER_AMOUNT;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_ORDER_ID;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_ORDER_NOTE;

public class FinalPaymentActivity extends AppCompatActivity {

    TextView txt_amount,txt_camount,txt_couponcode;
    Button btnSubmit;
    String amount,oid,uid,qid,taskid;
    AppController appController;
    PrefManager prefManager;
    String userId,token;
    private static final String TAG = "MyTaskDetailsActivity";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.finalpayment_activity);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Payment Details");
        appController = (AppController) getApplication();

        prefManager = new PrefManager(FinalPaymentActivity.this);
        HashMap<String, String> profile = prefManager.getUserDetails();
        userId = profile.get("userid");
        token = profile.get("Token");
        btnSubmit=findViewById(R.id.btnSubmit);

        txt_couponcode=findViewById(R.id.txt_couponcode);
        txt_camount=findViewById(R.id.txt_camount);
        txt_amount=findViewById(R.id.txt_amount);

        if (getIntent() != null){
            amount=getIntent().getStringExtra("Amount");
            oid=getIntent().getStringExtra("order_id");
            uid=getIntent().getStringExtra("user_id");
            qid=getIntent().getStringExtra("quote_id");
            taskid=getIntent().getStringExtra("TaskId");

            txt_amount.setText(amount);
        }

        txt_camount.setText("");

        txt_couponcode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final AlertDialog dialogBuilder = new AlertDialog.Builder(FinalPaymentActivity.this).create();
                LayoutInflater inflater1 = getLayoutInflater();
                View dialogView = inflater1.inflate(R.layout.couponcode, null);

                RelativeLayout relative_submit = (RelativeLayout) dialogView.findViewById(R.id.relative_submit);
                RelativeLayout relative_cancel = (RelativeLayout) dialogView.findViewById(R.id.relative_cancel);

                EditText edt_amount=dialogView.findViewById(R.id.edt_amount);


                relative_cancel.setOnClickListener(view1 -> {
                    // DO SOMETHINGS
                    dialogBuilder.dismiss();
                });

                relative_submit.setOnClickListener(view12 -> {
                    String coupon=edt_amount.getText().toString().trim();

                    if (coupon.equals("")){
                        Toast.makeText(FinalPaymentActivity.this, "Please Enter coupon code", Toast.LENGTH_SHORT).show();
                    }
                    else {
                        if (appController.isConnection()){

                            ProgressDialog progressDialog = new ProgressDialog(FinalPaymentActivity.this);
                            progressDialog.setMessage("Loading...");
                            progressDialog.show();
                            Call<CouponResponse> call= RetrofitClient.getInstance().getApi().CouponData(userId,amount,coupon);
                            call.enqueue(new Callback<CouponResponse>() {
                                @Override
                                public void onResponse(Call<CouponResponse> call, Response<CouponResponse> response) {
                                    if (response.isSuccessful());
                                    CouponResponse couponResponse=response.body();
                                    if (couponResponse.isSuccess()){
                                      progressDialog.dismiss();
                                      CouponResponse.DataBean dataBean=couponResponse.getData();
                                        dialogBuilder.dismiss();
                                      String discount=String.valueOf(dataBean.getDiscount());
                                        txt_camount.setVisibility(View.VISIBLE);
                                        txt_camount.setText(discount);
                                        txt_couponcode.setVisibility(View.GONE);
                                    }
                                    else {
                                        txt_couponcode.setVisibility(View.VISIBLE);
                                        txt_camount.setVisibility(View.GONE);
                                        progressDialog.dismiss();
                                        dialogBuilder.dismiss();
                                    }
                                }

                                @Override
                                public void onFailure(Call<CouponResponse> call, Throwable t) {
                                    dialogBuilder.dismiss();
                                    progressDialog.dismiss();
                                    Toast.makeText(FinalPaymentActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                                }
                            });
                        }
                        else {
                            Toast.makeText(FinalPaymentActivity.this, "No Internet Connection..", Toast.LENGTH_SHORT).show();
                        }
                    }
                });

                dialogBuilder.setView(dialogView);
                dialogBuilder.show();

            }
        });


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String totalamount=txt_amount.getText().toString()+txt_camount.getText().toString();

                AlertDialog.Builder builder1 = new AlertDialog.Builder(FinalPaymentActivity.this);
//                builder1.setIcon(R.mipmap.ic_launcher_round);
                builder1.setTitle("Are you sure to Accept");
                builder1.setMessage("if you accept the task. it will redirect to payment page.");
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        "Yes",
                        (dialog, id) -> {
                            dialog.cancel();

                            //generate order id
                          //  int random = new Random().nextInt(61000) + 20000;

                            generateToken(oid,totalamount);
                        });

                builder1.setNegativeButton(
                        "No",
                        (dialog, id) -> dialog.cancel());

                AlertDialog alert11 = builder1.create();
                alert11.show();

            }
        });
    }

    private void generateToken(String orderId, String totalamount) {

        Call<TokenResponse> call = RetrofitClient.getInstance().getApi().generateToken(token, orderId, amount);
        call.enqueue(new Callback<TokenResponse>() {
            @Override
            public void onResponse(Call<TokenResponse> call, Response<TokenResponse> response) {
                if (response.isSuccessful()) ;
                TokenResponse tokenResponse = response.body();
                if (tokenResponse.isSuccess()) {

                    //Toast.makeText(MyTaskDetailsActivity.this, tokenResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    payOnline(orderId, tokenResponse.getCftoken(),totalamount);
                }
            }

            @Override
            public void onFailure(Call<TokenResponse> call, Throwable t) {
                Toast.makeText(FinalPaymentActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void payOnline(String orderId, String cftoken, String totalamount) {

        /*
         * token can be generated from your backend by calling cashfree servers. Please
         * check the documentation for details on generating the token.
         * READ THIS TO GENERATE TOKEN: https://bit.ly/2RGV3Pp
         */
        String token = cftoken;


        /*
         * stage allows you to switch between sandboxed and production servers
         * for CashFree Payment Gateway. The possible values are
         *
         * 1. TEST: Use the Test server. You can use this service while integrating
         *      and testing the CashFree PG. No real money will be deducted from the
         *      cards and bank accounts you use this stage. This mode is thus ideal
         *      for use during the development. You can use the cards provided here
         *      while in this stage: https://docs.cashfree.com/docs/resources/#test-data
         *
         * 2. PROD: Once you have completed the testing and integration and successfully
         *      integrated the CashFree PG, use this value for stage variable. This will
         *      enable live transactions
         */
        String stage = "TEST";

        /*
         * appId will be available to you at CashFree Dashboard. This is a unique
         * identifier for your app. Please replace this appId with your appId.
         * Also, as explained below you will need to change your appId to prod
         * credentials before publishing your app.
         */
        String appId = "90232192fbc3a769801f76e63209";
        String order_Id = orderId;
        String orderAmount = totalamount;
        String orderNote = "Test Order";
        String customerName = "suresh";
        String customerPhone = "8919480920";
        String customerEmail = "ksuresh.unique@gmail.com";

        Map<String, String> params = new HashMap<>();

        params.put(PARAM_APP_ID, appId);
        params.put(PARAM_ORDER_ID, order_Id);
        params.put(PARAM_ORDER_AMOUNT, orderAmount);
        params.put(PARAM_ORDER_NOTE, orderNote);
        params.put(PARAM_CUSTOMER_NAME, customerName);
        params.put(PARAM_CUSTOMER_PHONE, customerPhone);
        params.put(PARAM_CUSTOMER_EMAIL, customerEmail);


        for (Map.Entry entry : params.entrySet()) {
            Log.d("CFSKDSample", entry.getKey() + " " + entry.getValue());
        }

        CFPaymentService cfPaymentService = CFPaymentService.getCFPaymentServiceInstance();
        cfPaymentService.setOrientation(this, 0);

        // Use the following method for initiating Payments
        // First color - Toolbar background
        // Second color - Toolbar text and back arrow color
        cfPaymentService.doPayment(FinalPaymentActivity.this, params, token, stage);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //Same request code for all payment APIs.
        Log.d(TAG, "ReqCode : " + CFPaymentService.REQ_CODE);
        Log.d(TAG, "API Response : " + data);
        //Prints all extras. Replace with app logic.
        if (data != null) {
            Bundle bundle = data.getExtras();
            Log.d(TAG, "API Response : " + bundle);
            if (bundle != null)

                for (String key : bundle.keySet()) {
                    String string = " " + key + " => " + bundle.get(key) + ";";
                    Log.d(TAG, "onActivityResult: " + string);
                    if (bundle.getString(key) != null) {
                        Log.d(TAG, key + " : " + bundle.get("txStatus"));


                        if (bundle.get("orderAmount") != null || bundle.get("orderId") != null || bundle.get("referenceId") != null) {

                            String orderAmount = String.valueOf(bundle.get("orderAmount"));
                            String txStatus = String.valueOf(bundle.get("txStatus"));
                            String referenceId = String.valueOf(bundle.get("referenceId"));
                            String txMsg = String.valueOf(bundle.get("txMsg"));
                            String orderId = String.valueOf(bundle.get("orderId"));

                            Log.d(TAG, "onActivityResult1: " + orderAmount);
                            double payAmount = 0.00;
                            try {
                                payAmount = DecimalFormat.getNumberInstance().parse(orderAmount).doubleValue();
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            // save details to server
                            paymentStatus(payAmount, txStatus, referenceId, txMsg, orderId);
                        }

                    }
                }
        }
    }

    // payment status success or failure
    private void paymentStatus(Double orderAmount, String txStatus, String referenceId, String txMsg, String orderId) {
        Call<TransactioSuccessResponse> call = RetrofitClient.getInstance().getApi().orderTransaction(token, Integer.parseInt(userId), Integer.parseInt(uid), Integer.parseInt(taskid), Integer.parseInt(qid), orderAmount, txStatus, referenceId, txMsg, orderId);
        call.enqueue(new Callback<TransactioSuccessResponse>() {
            @Override
            public void onResponse(Call<TransactioSuccessResponse> call, Response<TransactioSuccessResponse> response) {
                if (response.isSuccessful()) ;
                TransactioSuccessResponse quoteAcceptResponse = response.body();
                Log.d(TAG, "onResponse: " + quoteAcceptResponse.getData().getStatus());

                if (quoteAcceptResponse.getData().getStatus().equalsIgnoreCase("FAILED")) {
                    Toast.makeText(FinalPaymentActivity.this, "Your Transaction is Failed!", Toast.LENGTH_SHORT).show();
                } else {
                    ClickAccept();
                }

            }

            @Override
            public void onFailure(Call<TransactioSuccessResponse> call, Throwable t) {

                Toast.makeText(FinalPaymentActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    // click accept button
    private void ClickAccept() {

        ProgressDialog progressDialog = new ProgressDialog(FinalPaymentActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        Call<QuoteAcceptResponse> call = RetrofitClient.getInstance().getApi().AcceptQuote(token, userId, taskid);
        call.enqueue(new Callback<QuoteAcceptResponse>() {
            @Override
            public void onResponse(Call<QuoteAcceptResponse> call, Response<QuoteAcceptResponse> response) {
                if (response.isSuccessful()) ;
                QuoteAcceptResponse quoteAcceptResponse = response.body();
                if (quoteAcceptResponse.isSuccess()) {
                    progressDialog.dismiss();
                    Toast.makeText(FinalPaymentActivity.this, quoteAcceptResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    //btn_accept.setEnabled(false);
                    finish();
                } else {
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<QuoteAcceptResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(FinalPaymentActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }
}
