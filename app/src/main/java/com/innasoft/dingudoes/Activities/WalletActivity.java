package com.innasoft.dingudoes.Activities;

import android.os.Bundle;
import android.view.MenuItem;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.dingudoes.Adapters.OptionsAdapter;
import com.innasoft.dingudoes.Adapters.WalletAdapter;
import com.innasoft.dingudoes.Models.WalletModel;
import com.innasoft.dingudoes.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WalletActivity extends AppCompatActivity {

    @BindView(R.id.txtReward)
    TextView txtReward;
    @BindView(R.id.recyclerRewards)
    RecyclerView recyclerRewards;

    ArrayList<WalletModel> walletModels = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("DD Referal Points");

        txtReward.setText("\u20B9" + "2000");


     //   walletData();


    }

    private void walletData(){

        walletModels.add(new WalletModel("Suresh","10-10-2019 12:30pm"));
        walletModels.add(new WalletModel("Mahesh","10-10-2019 12:30pm"));
        walletModels.add(new WalletModel("Ganesh","10-10-2019 12:30pm"));
        walletModels.add(new WalletModel("Rajesh","10-10-2019 12:30pm"));
        walletModels.add(new WalletModel("Suresh","10-10-2019 12:30pm"));
        walletModels.add(new WalletModel("Mahesh","10-10-2019 12:30pm"));
        walletModels.add(new WalletModel("Ganesh","10-10-2019 12:30pm"));
        walletModels.add(new WalletModel("Rajesh","10-10-2019 12:30pm"));
        walletModels.add(new WalletModel("Mahesh","10-10-2019 12:30pm"));
        walletModels.add(new WalletModel("Ganesh","10-10-2019 12:30pm"));
        walletModels.add(new WalletModel("Rajesh","10-10-2019 12:30pm"));

        WalletAdapter walletAdapter = new WalletAdapter(walletModels,this);
        recyclerRewards.setNestedScrollingEnabled(false);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(WalletActivity.this,RecyclerView.VERTICAL,false);
        recyclerRewards.setLayoutManager(linearLayoutManager);
        recyclerRewards.setAdapter(walletAdapter);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);

    }

}
