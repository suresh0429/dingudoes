package com.innasoft.dingudoes.Activities;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.dingudoes.Adapters.TermsConditionsAdapter;
import com.innasoft.dingudoes.Apis.RetrofitClient;
import com.innasoft.dingudoes.Singleton.AppController;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.TermsConditionsResponse;
import com.innasoft.dingudoes.storage.PrefManager;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TermsConditionsActivity extends AppCompatActivity {
    @BindView(R.id.recycler_terms)
    RecyclerView recyclerTerms;
    TermsConditionsAdapter termsConditionsAdapter;
    PrefManager prefManager;
    String token;
    AppController appController;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.terms_activity);
        ButterKnife.bind(this);
        appController=(AppController) getApplication();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Terms & Conditions");

        prefManager = new PrefManager(getApplicationContext());
        HashMap<String, String> profile = prefManager.getUserDetails();
        token = profile.get("Token");

        if (appController.isConnection()){

            ProgressDialog progressDialog = new ProgressDialog(TermsConditionsActivity.this);
            progressDialog.setMessage("Loading.....");
            progressDialog.show();
            String value="termsAndConditions";

            Call<TermsConditionsResponse> conditionsResponseCall= RetrofitClient.getInstance().getApi().TermsConditions(token,value);
            conditionsResponseCall.enqueue(new Callback<TermsConditionsResponse>() {
                @Override
                public void onResponse(Call<TermsConditionsResponse> call, Response<TermsConditionsResponse> response) {
                    if (response.isSuccessful());
                    TermsConditionsResponse termsConditionsResponse=response.body();
                    if (termsConditionsResponse.isSuccess()){
                        progressDialog.dismiss();

                        List<TermsConditionsResponse.DataBean> dataBeanList=termsConditionsResponse.getData();

                        termsConditionsAdapter=new TermsConditionsAdapter(dataBeanList,TermsConditionsActivity.this);
                        LinearLayoutManager layoutManager = new LinearLayoutManager(TermsConditionsActivity.this);
                        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                        recyclerTerms.setLayoutManager(layoutManager);
                        recyclerTerms.setNestedScrollingEnabled(false);
                        recyclerTerms.setAdapter(termsConditionsAdapter);

                    }
                }

                @Override
                public void onFailure(Call<TermsConditionsResponse> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(TermsConditionsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });

        }
        else {
            String message = "Sorry! Not connected to internet";
            int color = Color.RED;

            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }



    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);

    }
}
