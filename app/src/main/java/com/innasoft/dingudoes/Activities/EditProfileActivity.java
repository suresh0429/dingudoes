package com.innasoft.dingudoes.Activities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.innasoft.dingudoes.Apis.RetrofitClient;
import com.innasoft.dingudoes.Singleton.AppController;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.EditProfileResponse;
import com.innasoft.dingudoes.storage.PrefManager;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EditProfileActivity extends AppCompatActivity {
    private static final String TAG ="EditProfileActivity" ;
    @BindView(R.id.imageview)
    ImageView imageview;
    @BindView(R.id.linear_profile)
    RelativeLayout linearProfile;
    @BindView(R.id.f_name)
    TextInputEditText fName;
    @BindView(R.id.email)
    TextInputEditText email;
    @BindView(R.id.dateofBirth)
    TextInputEditText dateofBirth;
    @BindView(R.id.address)
    TextInputEditText address;
    @BindView(R.id.about)
    TextInputEditText about;
    @BindView(R.id.skills)
    TextInputEditText skills;
    @BindView(R.id.transport)
    TextInputEditText transport;
    @BindView(R.id.languages)
    TextInputEditText languages;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    PrefManager prefManager;
    String token;
    AppController appController;
    private int mYear, mMonth, mDay;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.editprofile_activity);
        ButterKnife.bind(this);

        appController=(AppController) getApplication();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Edit Profile");

        prefManager = new PrefManager(getApplicationContext());
        HashMap<String, String> profile = prefManager.getUserDetails();
        token = profile.get("Token");

        if (getIntent() != null) {

            String name = getIntent().getStringExtra("Fullname");
            String emaill = getIntent().getStringExtra("Email");
            String addres = getIntent().getStringExtra("address");
            String aboutt = getIntent().getStringExtra("About");
            String skill = getIntent().getStringExtra("Skills");
            String transportt = getIntent().getStringExtra("Transport");
            String language = getIntent().getStringExtra("Language");
            String dob = getIntent().getStringExtra("dob");

            fName.setText(name);
            email.setText(emaill);
            address.setText(addres);
            about.setText(aboutt);
            skills.setText(skill);
            transport.setText(transportt);
            languages.setText(language);


            Log.d(TAG, "onCreate: "+dob);

            if (dob != null) {
                try {
                    SimpleDateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
                    SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd");
                    Date date = inputFormat.parse(dob);
                    String formattedDate = outputFormat.format(date);
                    System.out.println(formattedDate);
                    dateofBirth.setText(formattedDate);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }else {
                dateofBirth.setText("");
            }


        }


        dateofBirth.setOnClickListener(view -> {
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            c.setTimeInMillis(System.currentTimeMillis()-1000);

            DatePickerDialog datePickerDialog = new DatePickerDialog(EditProfileActivity.this,
                    (view1, year, monthOfYear, dayOfMonth) -> dateofBirth.setText( year+ "-" + (monthOfYear + 1)+ "-"+dayOfMonth), mYear, mMonth, mDay);
            datePickerDialog.getDatePicker().setMaxDate(System.currentTimeMillis() - 1000);

            datePickerDialog.show();
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);

    }

    @OnClick(R.id.btn_submit)
    public void onViewClicked() {

        if (appController.isConnection()){
            getData();
        }
        else {

            String message = "Sorry! Not connected to internet";
            int color = Color.RED;

            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }


    }

    private void getData() {

        String name=fName.getText().toString().trim();
        String Email=email.getText().toString().trim();
        String dob=dateofBirth.getText().toString().trim();
        String Address=address.getText().toString().trim();
        String About=about.getText().toString().trim();
        String skill=skills.getText().toString().trim();
        String Transport=transport.getText().toString().trim();
        String language=languages.getText().toString().trim();

        if (name.equals("")){
            Toast.makeText(EditProfileActivity.this, "Enter Name..", Toast.LENGTH_SHORT).show();
        }
        else if (Email.equals("")){
            Toast.makeText(EditProfileActivity.this, "Enter Email..", Toast.LENGTH_SHORT).show();
        }
        else if (dob.equals("")){
            Toast.makeText(EditProfileActivity.this, "Enter Date of Birth..", Toast.LENGTH_SHORT).show();
        }
        else if (Address.equals("")){
            Toast.makeText(EditProfileActivity.this, "Enter address..", Toast.LENGTH_SHORT).show();
        }
        else if (About.equals("")){
            Toast.makeText(EditProfileActivity.this, "Enter About..", Toast.LENGTH_SHORT).show();
        }
        else if (skill.equals("")){
            Toast.makeText(EditProfileActivity.this, "Enter Skills..", Toast.LENGTH_SHORT).show();
        }
        else if (Transport.equals("")){
            Toast.makeText(EditProfileActivity.this, "Enter Transportation..", Toast.LENGTH_SHORT).show();
        }
        else if (language.equals("")){
            Toast.makeText(EditProfileActivity.this, "Enter Language..", Toast.LENGTH_SHORT).show();
        }

        else {

            ProgressDialog progressDialog=new ProgressDialog(EditProfileActivity.this);
            progressDialog.setMessage("Loading...");
            progressDialog.show();

            Call<EditProfileResponse> call= RetrofitClient.getInstance().getApi().EditProfile(token,Address,Email,name,skill,About,Transport,language,dob);
            call.enqueue(new Callback<EditProfileResponse>() {
                @Override
                public void onResponse(Call<EditProfileResponse> call, Response<EditProfileResponse> response) {
                    if (response.isSuccessful());
                    EditProfileResponse editProfileResponse=response.body();
                    if (editProfileResponse.isSuccess()){
                        progressDialog.dismiss();
                        Toast.makeText(EditProfileActivity.this, editProfileResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(EditProfileActivity.this, ProfileActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);

                    }
                    else {
                        progressDialog.dismiss();
                        Toast.makeText(EditProfileActivity.this, editProfileResponse.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                }

                @Override
                public void onFailure(Call<EditProfileResponse> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(EditProfileActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });
        }


    }
}
