package com.innasoft.dingudoes.Activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;

import com.crystal.crystalrangeseekbar.interfaces.OnRangeSeekbarChangeListener;
import com.crystal.crystalrangeseekbar.widgets.CrystalRangeSeekbar;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Singleton.AppController;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.ceryle.segmentedbutton.SegmentedButtonGroup;

public class FilterActivity extends AppCompatActivity {
    TextView txt_dstnce;
    SeekBar seekbar_distance;
    SwitchCompat switch_btn;
    String checked;
    Button btn_login;
    EditText edt_loc;
    TextView tvMin, tvMax;
    @BindView(R.id.txt_all)
    TextView txtAll;
    @BindView(R.id.relative_all)
    RelativeLayout relativeAll;
    String todo;
    @BindView(R.id.txt_person)
    TextView txtPerson;
    @BindView(R.id.relative_person)
    RelativeLayout relativePerson;
    @BindView(R.id.txt_remotly)
    TextView txtRemotly;
    @BindView(R.id.relative_remotly)
    RelativeLayout relativeRemotly;
    AppController appController;
    @BindView(R.id.segmentedButtonGroup)
    SegmentedButtonGroup segmentedButtonGroup;

    @SuppressLint("ResourceAsColor")
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filter_activity);
        ButterKnife.bind(this);
        appController = (AppController) getApplication();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Filter");

        seekbar_distance = findViewById(R.id.seekbar_distance);
        txt_dstnce = findViewById(R.id.txt_dstnce);
        switch_btn = findViewById(R.id.switch_btn);
        btn_login = findViewById(R.id.btn_login);
        edt_loc = findViewById(R.id.edt_loc);
        txt_dstnce = findViewById(R.id.txt_dstnce);

        todo = "1";
        segmentedButtonGroup.setOnClickedButtonListener(new SegmentedButtonGroup.OnClickedButtonListener() {
            @Override
            public void onClickedButton(int position) {
                if (position==0){
                    todo = "1";

                }else if (position==1){
                    todo = "2";

                }else if (position==2){
                    todo = "3";

                }
            }
        });



        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (appController.isConnection()) {
                    getData();
                } else {

                    String message = "Sorry! Not connected to internet";
                    int color = Color.RED;

                    Toast.makeText(FilterActivity.this, message, Toast.LENGTH_SHORT).show();
                }


            }
        });

        setRangeSeekbar5();

        seekbar_distance.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                txt_dstnce.setText(" " + i);

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }


            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        switch_btn.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    checked = "1";
                } else {
                    checked = "0";
                }
            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);

    }

    private void setRangeSeekbar5() {

        // get seekbar from view
        final CrystalRangeSeekbar rangeSeekbar = (CrystalRangeSeekbar) findViewById(R.id.rangeSeekbar5);

        // get min and max text view
        tvMin = (TextView) findViewById(R.id.textMin5);
        tvMax = (TextView) findViewById(R.id.textMax5);

        rangeSeekbar.setMaxValue(100);
        rangeSeekbar.setMaxValue(10000);


        // set listener
        rangeSeekbar.setOnRangeSeekbarChangeListener(new OnRangeSeekbarChangeListener() {
            @Override
            public void valueChanged(Number minValue, Number maxValue) {
                tvMin.setText(String.valueOf(minValue));
                tvMax.setText(String.valueOf(maxValue));
            }
        });
    }

    private void getData() {

        String location = edt_loc.getText().toString().trim();
        String minimum = tvMin.getText().toString().trim();
        String maximum = tvMax.getText().toString().trim();
        String distance = txt_dstnce.getText().toString().trim();

        if (location.equals("")) {
            Toast.makeText(FilterActivity.this, "Please Enter Location..", Toast.LENGTH_SHORT).show();
        } else if (distance.equals("")) {
            Toast.makeText(FilterActivity.this, "Please Select Distance..", Toast.LENGTH_SHORT).show();
        } else {

            Intent intent = new Intent();
            // intent.putExtra("keyName", stringToPassBack);
            // setResult(RESULT_OK, intent);


            //Intent intent=new Intent(getApplicationContext(),HomeActivity.class);
            intent.putExtra("Location", location);
            intent.putExtra("Minimum", minimum);
            intent.putExtra("Maximum", maximum);
            intent.putExtra("Distance", distance);
            intent.putExtra("Tobe", todo);
            intent.putExtra("index", 2);
            setResult(100, intent);
            finish();


        }


    }

}
