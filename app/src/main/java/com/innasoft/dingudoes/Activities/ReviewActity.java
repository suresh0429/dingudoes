package com.innasoft.dingudoes.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.hsalf.smilerating.BaseRating;
import com.hsalf.smilerating.SmileRating;
import com.innasoft.dingudoes.Apis.RetrofitClient;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.CreateReviewResponse;
import com.innasoft.dingudoes.Singleton.AppController;
import com.innasoft.dingudoes.storage.PrefManager;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ReviewActity extends AppCompatActivity implements SmileRating.OnSmileySelectionListener, SmileRating.OnRatingSelectedListener {
    private static final String TAG = "review";

    @BindView(R.id.submit_btn)
    Button submitBtn;
    AppController appController;
    PrefManager prefManager;
    String token, userId, Desc;
    String taskid, activity;
    int Rating;
    @BindView(R.id.smile_rating)
    SmileRating smileRating;
    @BindView(R.id.review_Text)
    TextView reviewText;
    @BindView(R.id.edt_desc)
    EditText edtDesc;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.review_alert);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Your Reviews");

        prefManager = new PrefManager(getApplicationContext());
        HashMap<String, String> profile = prefManager.getUserDetails();
        token = profile.get("Token");
        userId = profile.get("userid");

        if (getIntent() != null) {
            taskid = getIntent().getStringExtra("TaskId");
            activity = getIntent().getStringExtra("Activity");
        }

        appController = (AppController) getApplication();
        smileRating.setOnSmileySelectionListener(this);
        smileRating.setOnRatingSelectedListener(this);
        /*Typeface typeface = Typeface.createFromAsset(getAssets(), "MetalMacabre.ttf");
        smileRating.setTypeface(typeface);*/

        if (appController.isConnection()) {

            getData();
        } else {
            String message = "Sorry! Not connected to internet";
            int color = Color.RED;

            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void onSmileySelected(@BaseRating.Smiley int smiley, boolean reselected) {
        switch (smiley) {
            case SmileRating.BAD:
                Log.i(TAG, "Bad");
                reviewText.setText("Bad");
                break;
            case SmileRating.GOOD:
                Log.i(TAG, "Good");
                reviewText.setText("Good");
                break;
            case SmileRating.GREAT:
                Log.i(TAG, "Great");
                reviewText.setText("Great");
                break;
            case SmileRating.OKAY:
                Log.i(TAG, "Okay");
                reviewText.setText("Okay");
                break;
            case SmileRating.TERRIBLE:
                Log.i(TAG, "Terrible");
                reviewText.setText("Terrible");
                break;
            case SmileRating.NONE:
                Log.i(TAG, "None");
                reviewText.setText("None");
                break;
        }
    }

    @Override
    public void onRatingSelected(int level, boolean reselected) {
        Log.i(TAG, "Rated as: " + level + " - " + reselected);
        Rating = level;
    }


    private void getData() {

        /*reviewRatingBar.setOnRatingBarChangeListener((ratingBar, rating, fromUser) -> {

            Log.d(TAG, "onRatingChanged:"+rating);
            Rating=rating;
        });*/

        submitBtn.setOnClickListener(v -> {

            Desc = edtDesc.getText().toString().trim();
            //  Rating=reviewRatingBar.getRating();

            if (Desc.equals("")) {
                Toast.makeText(ReviewActity.this, "Enter Review..", Toast.LENGTH_SHORT).show();
            } else {

                ProgressDialog progressDialog = new ProgressDialog(ReviewActity.this);
                progressDialog.setMessage("Loading....");
                progressDialog.show();

                Call<CreateReviewResponse> createReviewResponseCall = RetrofitClient.getInstance().getApi().CreateReview(token, userId, taskid, Desc, String.valueOf(Rating));
                createReviewResponseCall.enqueue(new Callback<CreateReviewResponse>() {
                    @Override
                    public void onResponse(Call<CreateReviewResponse> call, Response<CreateReviewResponse> response) {
                        if (response.isSuccessful()) ;
                        CreateReviewResponse createReviewResponse = response.body();
                        if (createReviewResponse.isSuccess()) {
                            progressDialog.dismiss();
                            Toast.makeText(ReviewActity.this, createReviewResponse.getMessage(), Toast.LENGTH_SHORT).show();

                            if (activity.equalsIgnoreCase("MyTaskDetailsActivity") || activity.equalsIgnoreCase("PendingReviewsActivity")) {
                                Intent intent = new Intent(ReviewActity.this, HomeActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);
                            }

                        } else {
                            progressDialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<CreateReviewResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(ReviewActity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });

            }

        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:

                if (activity.equalsIgnoreCase("MyTaskDetailsActivity")) {
                    Intent intent = new Intent(ReviewActity.this, HomeActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
                if (activity.equalsIgnoreCase("PendingReviewsActivity")) {
                    finish();
                }
                // onBackPressed();

//                if (activity.equalsIgnoreCase("MyTaskDetailsActivity")) {
//                    Intent intent = new Intent(ReviewActity.this, HomeActivity.class);
//                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
//                    startActivity(intent);
//                }
                // onBackPressed();

                break;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onBackPressed() {

        if (activity.equalsIgnoreCase("MyTaskDetailsActivity")) {
            Intent intent = new Intent(ReviewActity.this, HomeActivity.class);

            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
        }
        else {
            finish();
        }
    }
}
