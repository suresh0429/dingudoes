package com.innasoft.dingudoes.Activities;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.dingudoes.Adapters.NotificationsAdapter;
import com.innasoft.dingudoes.Apis.RetrofitClient;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.GetNotificationsResponse;
import com.innasoft.dingudoes.Singleton.AppController;
import com.innasoft.dingudoes.storage.PrefManager;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationsActivity extends AppCompatActivity {
    @BindView(R.id.recycler_notifications)
    RecyclerView recyclerNotifications;
    AppController appController;
    PrefManager prefManager;
    String userId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notifications_activitgy);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Notifications");

        prefManager = new PrefManager(getApplicationContext());
        HashMap<String, String> profile = prefManager.getUserDetails();
        userId = profile.get("userid");


        appController=(AppController)getApplication();
        if (appController.isConnection()){

            getData(userId);

        }
        else {
            String message = "Sorry! Not connected to internet";
            int color = Color.RED;

            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }

    }

    private void getData(String userId) {

        ProgressDialog progressDialog=new ProgressDialog(NotificationsActivity.this);
        progressDialog.setMessage("Loading....");
        progressDialog.show();

        Call<GetNotificationsResponse> call= RetrofitClient.getInstance().getApi().GetNotifications(userId);
        call.enqueue(new Callback<GetNotificationsResponse>() {
            @Override
            public void onResponse(Call<GetNotificationsResponse> call, Response<GetNotificationsResponse> response) {
                if (response.isSuccessful());
                GetNotificationsResponse getNotificationsResponse=response.body();
                if (getNotificationsResponse.isSuccess()){
                    progressDialog.dismiss();

                    List<GetNotificationsResponse.DataBean> getNotificationsResponseList=getNotificationsResponse.getData();
                    NotificationsAdapter notificationsAdapter=new NotificationsAdapter(getNotificationsResponseList,NotificationsActivity.this);
                    final LinearLayoutManager layoutManager1 = new LinearLayoutManager(NotificationsActivity.this);
                    layoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
                    recyclerNotifications.setLayoutManager(layoutManager1);
                    recyclerNotifications.setNestedScrollingEnabled(false);
                    recyclerNotifications.setAdapter(notificationsAdapter);

                }
                else {
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<GetNotificationsResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(NotificationsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);

    }


}
