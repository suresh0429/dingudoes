package com.innasoft.dingudoes.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.Profile;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.gson.Gson;
import com.innasoft.dingudoes.Apis.RetrofitClient;
import com.innasoft.dingudoes.Response.FCmTokenResponse;
import com.innasoft.dingudoes.Singleton.AppController;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.LoginFailureResponse;
import com.innasoft.dingudoes.Response.LoginResponse;
import com.innasoft.dingudoes.storage.PrefManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {
    private static final String TAG = "LoginActivity";

    FirebaseAuth mAuth;
    GoogleSignInClient mGoogleSignInClient;
    private static final int RC_SIGN_IN = 239;

    CallbackManager callbackManager;

    @BindView(R.id.email)
    TextInputEditText email;
    @BindView(R.id.password)
    TextInputEditText password;
    @BindView(R.id.txt_signup)
    TextView txtSignup;
    @BindView(R.id.txt_forgot)
    TextView txt_forgot;

    @BindView(R.id.btn_login)
    Button btn_login;

    PrefManager session;
    private static int TIME_OUT = 1500;
    @BindView(R.id.fb)
    ImageView fb;
    @BindView(R.id.fblayout)
    LinearLayout fblayout;
    @BindView(R.id.gmail)
    TextView gmail;
    @BindView(R.id.gmaillayout)
    LinearLayout gmaillayout;
    AppController appController;
    String refreshedToken;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        ButterKnife.bind(this);
        email.setText("ganeshchintu800@gmail.com");
        password.setText("123456");


        // Fcm Token
        refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.w("notificationToken",""+ refreshedToken);

        session = new PrefManager(this);
        if (session.isLoggedIn()) {
            startActivity(new Intent(LoginActivity.this, HomeActivity.class));
            finish();
        }

        appController=(AppController) getApplication();

        // Configure Google Sign In
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();

        // Initialize Firebase Auth
        mAuth = FirebaseAuth.getInstance();
        // gmail
        mGoogleSignInClient= GoogleSignIn.getClient(this,gso);

        // fb
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        // App code

                        Log.d(TAG, "onSuccess: "+loginResult.getAccessToken());

                        AccessToken accessToken = loginResult.getAccessToken();
                        Profile profile = Profile.getCurrentProfile();

                        // Facebook Email address
                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(
                                            JSONObject object,
                                            GraphResponse response) {
                                        Log.v("LoginActivity Response ", response.toString());

                                        try {
                                            String name = object.getString("name");
                                            String email = object.getString("email");
                                            String id = object.getString("id");
                                            String profile_ptah = "http://graph.facebook.com/" + id + "/picture?type=large";
                                            Log.v("Email = ", " " + email);
                                            Toast.makeText(getApplicationContext(), "Name " + name, Toast.LENGTH_LONG).show();

                                            // Social Login with Facebook
                                            socialAuth("FACEBOOK", id, name,email,profile_ptah);

                                        } catch (JSONException e) {
                                            e.printStackTrace();
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email,gender, birthday");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        // App code
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                    }
                });
    }

    @OnClick({ R.id.txt_signup, R.id.txt_forgot, R.id.btn_login, R.id.gmaillayout, R.id.fblayout})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.gmaillayout:
                if (appController.isConnection()){
                    signIn();
                }else {
                    Toast.makeText(this, R.string.nointernet, Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.fblayout:
                if (appController.isConnection()){
                    LoginManager.getInstance().logInWithReadPermissions(this, Arrays.asList("public_profile", "user_friends", "email"));

                }else {

                    Toast.makeText(this, R.string.nointernet, Toast.LENGTH_SHORT).show();
                }

                break;
            case R.id.txt_signup:
                Intent intent = new Intent(LoginActivity.this, RegistrationActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;
            case R.id.txt_forgot:
                Intent intent1 = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
                startActivity(intent1);
                overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                break;
            case R.id.btn_login:

                String username = email.getText().toString().trim();
                String passw = password.getText().toString().trim();

                if (appController.isConnection()){
                    ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this);
                    progressDialog.setMessage("Loading.....");
                    progressDialog.show();

                    Call<LoginResponse> call = RetrofitClient.getInstance().getApi().userLogin(username, passw);
                    call.enqueue(new Callback<LoginResponse>() {
                        @Override
                        public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                            if (response.isSuccessful()) {

                                LoginResponse loginResponse = response.body();


                                LoginResponse.DataBean dataBean = loginResponse.getData();
                                session.Login(loginResponse.getToken(), dataBean.getFullName(), dataBean.getEmail(), dataBean.getPhoneNumber(), String.valueOf(dataBean.getUserId()),dataBean.getReferer_code());
                                UpdateFcmToken(loginResponse.getToken());
                                new Handler().postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        progressDialog.dismiss();
                                        Toast.makeText(LoginActivity.this, loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                        // shared animation between two activites
                                        Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                        startActivity(intent);

                                    }
                                }, TIME_OUT);
                            } else {

                                Gson gson = new Gson();

                                LoginFailureResponse errorResponse = gson.fromJson(response.errorBody().charStream(), LoginFailureResponse.class);
                                if (errorResponse.isSuccess()) {
                                    progressDialog.dismiss();
                                    Toast.makeText(LoginActivity.this, errorResponse.getError(), Toast.LENGTH_SHORT).show();
                                } else {
                                    progressDialog.dismiss();
                                    Toast.makeText(LoginActivity.this, errorResponse.getError(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }

                        @Override
                        public void onFailure(Call<LoginResponse> call, Throwable t) {
                            progressDialog.dismiss();
                            Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    });

                }else {

                    Toast.makeText(this, R.string.nointernet, Toast.LENGTH_SHORT).show();
                }


                break;
        }
    }


    private void signIn() {
        Intent signInIntent = mGoogleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
            try {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount acct = task.getResult(ApiException.class);
                Log.d(TAG, "firebaseAuthWithGoogle:" + acct.getId()+"  name : "+acct.getDisplayName()+"  EMAIL : "+acct.getEmail()+
                        "  ID : "+acct.getId()+"  IDTOKEN : "+acct.getIdToken()+"  IMAGE : "+acct.getPhotoUrl());

                // Social Login with Gmail
                socialAuth("GOOGLE",acct.getId() ,acct.getDisplayName(),acct.getEmail(), String.valueOf(acct.getPhotoUrl()));

            } catch (ApiException e) {
                // Google Sign In failed, update UI appropriately
                Log.w(TAG, "Google sign in failed", e);
                // ...
            }
        }
        else {
            callbackManager.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void socialAuth(String provider, String id, String displayName, String email, String profile_ptah) {

        ProgressDialog progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();

        Call<LoginResponse> call = RetrofitClient.getInstance().getApi().userSocialLogin(provider,id,displayName,email);
        call.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                if (response.isSuccessful()) {

                    LoginResponse loginResponse = response.body();

                    LoginResponse.DataBean dataBean = loginResponse.getData();
                    session.Login(loginResponse.getToken(), dataBean.getFullName(), dataBean.getEmail(), dataBean.getPhoneNumber(), String.valueOf(dataBean.getUserId()));
                    UpdateFcmToken(loginResponse.getToken());
                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                            Toast.makeText(LoginActivity.this, loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            // shared animation between two activites
                            Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);

                        }
                    }, TIME_OUT);
                } else {

                    Gson gson = new Gson();

                    LoginFailureResponse errorResponse = gson.fromJson(response.errorBody().charStream(), LoginFailureResponse.class);
                    if (errorResponse.isSuccess()) {
                        progressDialog.dismiss();
                        Toast.makeText(LoginActivity.this, errorResponse.getError(), Toast.LENGTH_SHORT).show();
                    } else {
                        progressDialog.dismiss();
                        Toast.makeText(LoginActivity.this, errorResponse.getError(), Toast.LENGTH_SHORT).show();
                    }
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }


    private void UpdateFcmToken(String token){
        Call<FCmTokenResponse> call = RetrofitClient.getInstance().getApi().updateFcmToken(token, refreshedToken);
        call.enqueue(new Callback<FCmTokenResponse>() {
            @Override
            public void onResponse(Call<FCmTokenResponse> call, Response<FCmTokenResponse> response) {
                if (response.isSuccessful()) {
                    FCmTokenResponse loginResponse = response.body();
                    // Toast.makeText(LoginActivity.this, loginResponse.getMessage(), Toast.LENGTH_SHORT).show();

                    Log.d(TAG, "onResponse:"+loginResponse.getMessage());


                }
            }

            @Override
            public void onFailure(Call<FCmTokenResponse> call, Throwable t) {

                Toast.makeText(LoginActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                Log.d(TAG, "onFailure:"+t.getMessage());

            }
        });
    }
}
