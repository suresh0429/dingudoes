package com.innasoft.dingudoes.Activities;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.dingudoes.Adapters.MakePAymentsRecyclerAdapter;
import com.innasoft.dingudoes.Adapters.ReceivePAymentsRecyclerAdapter;
import com.innasoft.dingudoes.Models.PaymentMethodsModel;
import com.innasoft.dingudoes.Models.ReceivePaymentModel;
import com.innasoft.dingudoes.R;

public class PaymentMethodsActivity extends AppCompatActivity {
    RelativeLayout relative_mpayments,relative_rpayments;
    Drawable drawable, drawable1;
    MakePAymentsRecyclerAdapter paymentMethodsRecyclerAdapter;
    ReceivePAymentsRecyclerAdapter receivePAymentsRecyclerAdapter;
    RecyclerView recycler_pmethods;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.paymentmethods_activity);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Payment Methods");

        recycler_pmethods=findViewById(R.id.recycler_pmethods);

        relative_mpayments=findViewById(R.id.relative_mpayments);
        relative_rpayments=findViewById(R.id.relative_rpayments);

        drawable = getResources().getDrawable(R.drawable.background);
        drawable1 = getResources().getDrawable(R.drawable.background);
        drawable.setColorFilter(Color.parseColor("#7957A5"), PorterDuff.Mode.SRC_ATOP);
        relative_mpayments.setBackground(drawable);
        drawable1.setColorFilter(Color.parseColor("#9F9F9F"), PorterDuff.Mode.SRC_ATOP);
        relative_rpayments.setBackground(drawable1);
        MakePayments();

        relative_rpayments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                drawable1.setColorFilter(Color.parseColor("#7957A5"), PorterDuff.Mode.SRC_ATOP);
                relative_rpayments.setBackground(drawable1);
                drawable.setColorFilter(Color.parseColor("#9F9F9F"), PorterDuff.Mode.SRC_ATOP);
                relative_mpayments.setBackground(drawable);

                ReceivePayments();


            }
        });

        relative_mpayments.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                drawable1.setColorFilter(Color.parseColor("#9F9F9F"), PorterDuff.Mode.SRC_ATOP);
                relative_rpayments.setBackground(drawable1);
                drawable.setColorFilter(Color.parseColor("#7957A5"), PorterDuff.Mode.SRC_ATOP);
                relative_mpayments.setBackground(drawable);

                MakePayments();

            }
        });

    }

    private void ReceivePayments() {
        ReceivePaymentModel[] receivePaymentModels = new ReceivePaymentModel[]{
                new ReceivePaymentModel("123456789098", "Experies 12/2019"),
                new ReceivePaymentModel("098765432121", "Experies 12/2019"),
                new ReceivePaymentModel("123456789098", "Experies 12/2019"),
                new ReceivePaymentModel("098765432121", "Experies 12/2019"),
                new ReceivePaymentModel("098765432121", "Experies 12/2019"),
                new ReceivePaymentModel("098765432121", "Experies 12/2019")
        };

        receivePAymentsRecyclerAdapter = new ReceivePAymentsRecyclerAdapter(receivePaymentModels, PaymentMethodsActivity.this);

        final LinearLayoutManager layoutManager1 = new LinearLayoutManager(PaymentMethodsActivity.this);
        layoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
        recycler_pmethods.setLayoutManager(layoutManager1);
        recycler_pmethods.setNestedScrollingEnabled(false);
        recycler_pmethods.setAdapter(receivePAymentsRecyclerAdapter);


    }

    private void MakePayments() {

        PaymentMethodsModel[] paymentMethodsModels = new PaymentMethodsModel[]{
                new PaymentMethodsModel("098765432121", "Experies 13/2019"),
                new PaymentMethodsModel("098765432121", "Experies 13/2019"),
                new PaymentMethodsModel("123456789098", "Experies 13/2019"),
                new PaymentMethodsModel("098765432121", "Experies 13/2019"),
                new PaymentMethodsModel("098765432121", "Experies 13/2019"),
                new PaymentMethodsModel("098765432121", "Experies 13/2019")
        };

        paymentMethodsRecyclerAdapter = new MakePAymentsRecyclerAdapter(paymentMethodsModels, PaymentMethodsActivity.this);

        final LinearLayoutManager layoutManager1 = new LinearLayoutManager(PaymentMethodsActivity.this);
        layoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
        recycler_pmethods.setLayoutManager(layoutManager1);
        recycler_pmethods.setNestedScrollingEnabled(false);
        recycler_pmethods.setAdapter(paymentMethodsRecyclerAdapter);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);

    }

}
