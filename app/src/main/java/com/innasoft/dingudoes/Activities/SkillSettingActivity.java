package com.innasoft.dingudoes.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.innasoft.dingudoes.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SkillSettingActivity extends AppCompatActivity {
    @BindView(R.id.img_tran)
    ImageView imgTran;
    @BindView(R.id.walk)
    TextView walk;
    @BindView(R.id.relative_trans)
    RelativeLayout relativeTrans;
    @BindView(R.id.img_lang)
    ImageView imgLang;
    @BindView(R.id.telugu)
    TextView telugu;
    @BindView(R.id.relative_language)
    RelativeLayout relativeLanguage;
    @BindView(R.id.img_edu)
    ImageView imgEdu;
    @BindView(R.id.relative_education)
    RelativeLayout relativeEducation;
    @BindView(R.id.img_work)
    ImageView imgWork;
    @BindView(R.id.relative_work)
    RelativeLayout relativeWork;
    @BindView(R.id.relative_specialities)
    RelativeLayout relativeSpecialities;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.skillsetting_activity);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Skill Settings");

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);

    }

    @OnClick({R.id.relative_trans, R.id.relative_language, R.id.relative_education, R.id.relative_work, R.id.relative_specialities})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.relative_trans:
                break;
            case R.id.relative_language:
                break;
            case R.id.relative_education:
                break;
            case R.id.relative_work:
                break;
            case R.id.relative_specialities:
                startActivity(new Intent(SkillSettingActivity.this,SpecialtiesActivity.class));
                break;
        }
    }
}
