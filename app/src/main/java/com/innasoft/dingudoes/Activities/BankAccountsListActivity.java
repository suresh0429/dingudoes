package com.innasoft.dingudoes.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.LinearLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.dingudoes.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BankAccountsListActivity extends AppCompatActivity {
    @BindView(R.id.recycler_bankaccounts)
    RecyclerView recyclerBankaccounts;
    @BindView(R.id.btn_add)
    Button btnAdd;
    @BindView(R.id.btn_add1)
    Button btnAdd1;
    @BindView(R.id.nodatafoundLayout)
    LinearLayout nodatafoundLayout;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.bank_accounts);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Bank Accounts");

        btnAdd.setOnClickListener(v -> {

            Intent intent = new Intent(BankAccountsListActivity.this, AddBankAccountActivity.class);
            startActivity(intent);
        });

        btnAdd1.setOnClickListener(v -> {

            Intent intent = new Intent(BankAccountsListActivity.this, AddBankAccountActivity.class);
            startActivity(intent);
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);

    }

}
