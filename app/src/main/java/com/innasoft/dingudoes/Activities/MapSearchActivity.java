package com.innasoft.dingudoes.Activities;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.innasoft.dingudoes.Apis.RetrofitClient;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.SearchTaskResponse;
import com.innasoft.dingudoes.Singleton.AppController;
import com.innasoft.dingudoes.storage.PrefManager;

import java.util.HashMap;
import java.util.List;
import java.util.StringTokenizer;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.innasoft.dingudoes.Singleton.AppController.TAG;

public class MapSearchActivity extends AppCompatActivity implements OnMapReadyCallback {
    Location currentLocation;
    FusedLocationProviderClient fusedLocationProviderClient;
    private static final int REQUEST_CODE = 101;

    private GoogleMap mMap;
    PrefManager prefManager;
    String token,id,min,max,loc,dist;
    LatLng latLng;
    AppController appController;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.browsetask_fragmrnt);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Browse");

      /*  SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);*/

        appController=(AppController) getApplication();

       // setHasOptionsMenu(true);

        prefManager = new PrefManager(getApplicationContext());
        HashMap<String, String> profile = prefManager.getUserDetails();
        token = profile.get("Token");

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);
        fetchLocation();
    }

    private void fetchLocation() {
        if (ActivityCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_CODE);
            return;
        }
        Task<Location> task = fusedLocationProviderClient.getLastLocation();
        task.addOnSuccessListener(new OnSuccessListener<Location>() {
            @Override
            public void onSuccess(Location location) {
                if (location != null) {
                    currentLocation = location;
                    Toast.makeText(getApplicationContext(), currentLocation.getLatitude() + "" + currentLocation.getLongitude(), Toast.LENGTH_SHORT).show();
                    SupportMapFragment supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
                    assert supportMapFragment != null;
                    supportMapFragment.getMapAsync(MapSearchActivity.this);
                }
            }
        });
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        LatLng latLng = new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude());
        MarkerOptions markerOptions = new MarkerOptions().position(latLng).title("I am here!");
        googleMap.animateCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 5));
        googleMap.addMarker(markerOptions);

        if (appController.isConnection()){
            searchData(token,"1","100","1000","hyderabad","50");

        }
        else {
            String message = "Sorry! Not connected to internet";
            int color = Color.RED;

            Toast.makeText(getApplicationContext(), message, Toast.LENGTH_SHORT).show();
        }


    }

    private void searchData(String token, String id, String min, String max, String loc, String dist){

        ProgressDialog progressDialog=new ProgressDialog(MapSearchActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        Call<SearchTaskResponse> call= RetrofitClient.getInstance().getApi().SearchTask(token, id, min, max, loc,dist);
        call.enqueue(new Callback<SearchTaskResponse>() {
            @Override
            public void onResponse(Call<SearchTaskResponse> call, Response<SearchTaskResponse> response) {
                if (response.isSuccessful());
                SearchTaskResponse searchTaskResponse=response.body();
                //  Log.d(TAG, "onResponse: "+searchTaskResponse.getData());
                if (searchTaskResponse.isSuccess()){
                    progressDialog.dismiss();

                    List<SearchTaskResponse.DataBean> dataBeanList = searchTaskResponse.getData();



                    for (SearchTaskResponse.DataBean dataBean : dataBeanList) {
                        Log.d(TAG, "onResponse: "+dataBean.toString());
                        if (dataBean.getLatitude() != null && dataBean.getLongitude() != null) {

                            try {
                                latLng = new LatLng(Double.parseDouble(dataBean.getLatitude()), Double.parseDouble(dataBean.getLongitude()));
                            } catch (NumberFormatException e) {
                                e.printStackTrace();
                            }

                            int height = 200;
                            int width = 200;
                            BitmapDrawable bitmapdraw=(BitmapDrawable)getResources().getDrawable(R.drawable.dingu);
                            Bitmap b=bitmapdraw.getBitmap();
                            Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);


                           // mMap.addMarker(new MarkerOptions().position(latLng).title(dataBean.getTitle()).snippet(dataBean.getAddress()+"\n"+dataBean.getBudget()+"\n"+dataBean.getId()).icon(BitmapDescriptorFactory.fromResource(R.drawable.dingu)));
                            mMap.addMarker(new MarkerOptions().position(latLng).title(dataBean.getTitle()).snippet(dataBean.getAddress()+"\n"+dataBean.getBudget()+"\n"+dataBean.getId()).icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));
                            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                            // mMap.animateCamera(latLng, 14));

                            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 6));

                            Log.d(TAG, "onResponse: "+dataBean.getLongitude().length());

                            mMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {
                                @Override
                                public View getInfoWindow(Marker marker) {

                                    return null;
                                }

                                @Override
                                public View getInfoContents(Marker marker) {
                                    View v = getLayoutInflater().inflate(R.layout.info_title, null);
                                    v.setLayoutParams(new LinearLayout.LayoutParams(700, 270));

                                    final TextView info1 = (TextView) v.findViewById(R.id.txt_title);
                                    TextView info2 = (TextView) v.findViewById(R.id.txtAddress);
                                    TextView info3 = (TextView) v.findViewById(R.id.txtPrice);

                                    String data = marker.getSnippet();

                                    if (data != null) {
                                    StringTokenizer stringTokenizer = new StringTokenizer(data);
                                    String address = stringTokenizer.nextToken("\n");
                                    String budget = stringTokenizer.nextToken("\n");
                                    String taskId = stringTokenizer.nextToken("\n");

                                    info1.setText(marker.getTitle());
                                    info3.setText("\u20B9" + budget);

                                    info2.setText(address);

                                    mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                                        public void onInfoWindowClick(Marker marker) {
                                            Intent intent = new Intent(getApplicationContext(), TaskDetailsActivity.class);
                                            intent.putExtra("Task_id", Integer.valueOf(taskId));
                                            startActivity(intent);

                                        }
                                    });
                                }

                                    return v;
                                }
                            });
                        }
                    }





                }

            }

            @Override
            public void onFailure(Call<SearchTaskResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });



    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    fetchLocation();
                }
                break;
        }
    }

}
