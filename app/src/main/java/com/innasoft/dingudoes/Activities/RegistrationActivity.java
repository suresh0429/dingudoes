package com.innasoft.dingudoes.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.innasoft.dingudoes.Apis.RetrofitClient;
import com.innasoft.dingudoes.Singleton.AppController;
import com.innasoft.dingudoes.Reciver.ConnectivityReceiver;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.RegisterFailureResponse;
import com.innasoft.dingudoes.Response.RegisterResponse;
import com.innasoft.dingudoes.Response.VerifyMobileResponse;

import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RegistrationActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {
    private static final String TAG = "RegisterActivity";
    FirebaseAuth auth;
    @BindView(R.id.full_name)
    TextInputEditText fullName;
    @BindView(R.id.email)
    TextInputEditText Email;
    @BindView(R.id.mobile_number)
    TextInputEditText mobileNumber;
    @BindView(R.id.address)
    TextInputEditText address;
    @BindView(R.id.password)
    TextInputEditText Password;
    @BindView(R.id.txt_referal)
    TextInputEditText txt_referal;
    @BindView(R.id.post_check)
    CheckBox postCheck;
    @BindView(R.id.cmplt_task)
    TextView cmpltTask;
    @BindView(R.id.complete_check)
    CheckBox completeCheck;
    @BindView(R.id.btn_signup)
    Button btnSignup;
    String p_check, c_check;
    @BindView(R.id.txt_fname)
    TextInputLayout txtFname;
    @BindView(R.id.txt_email)
    TextInputLayout txtEmail;
    @BindView(R.id.txt_mobile)
    TextInputLayout txtMobile;
    @BindView(R.id.txt_address)
    TextInputLayout txtAddress;
    @BindView(R.id.txt_password)
    TextInputLayout txtPassword;
    private static  int TIME_OUT = 1500;
    AppController appController;
    String referal="";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register_activity);
        ButterKnife.bind(this);

        appController=(AppController) getApplication();
        auth = FirebaseAuth.getInstance();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Sign Up");

        firebaseRegister("Suresh","suresh@innasoft.in","123456");

        fullName.addTextChangedListener(new MyTextWatcher(fullName));
        Email.addTextChangedListener(new MyTextWatcher(Email));
        mobileNumber.addTextChangedListener(new MyTextWatcher(mobileNumber));
        Password.addTextChangedListener(new MyTextWatcher(Password));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);

    }

    @OnClick({R.id.btn_signup})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.btn_signup:

                if (appController.isConnection()) {
                    Signup();

                }
                else {

                    String message = "Sorry! Not connected to internet";

                    Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
    private boolean isValidEmail(String email) {
        final String EMAIL_PATTERN = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);

        if (email.isEmpty()) {
            txtEmail.setError("Email is required");
            requestFocus(Email);
            return false;
        } else if (!matcher.matches()) {
            txtEmail.setError("Enter a valid email");
            requestFocus(Email);
            return false;
        } else {
            txtEmail.setErrorEnabled(false);
        }


        return matcher.matches();
    }
    private boolean isValidatePassword(String password) {
        if (password.isEmpty()) {
            txtPassword.setError("Password required");
            requestFocus(Password);
            return false;
        } else if (password.length()< 6 || password.length() > 20) {
            txtPassword.setError("Password Should be 6 to 20 characters");
            requestFocus(Password);
            return false;
        } else {
            txtPassword.setErrorEnabled(false);
        }

        return true;
    }

    private boolean isValidName(String name) {
        Pattern pattern = Pattern.compile("[a-zA-Z ]+");
        Matcher matcher = pattern.matcher(name);

        if (name.isEmpty()) {
            txtFname.setError("name is required");
            requestFocus(fullName);
            return false;
        } else if (!matcher.matches()) {
            txtFname.setError("Enter Alphabets Only");
            requestFocus(fullName);
            return false;
        }
        else if (name.length() < 5 || name.length() > 20) {
            txtFname.setError("Name Should be 5 to 20 characters");
            requestFocus(fullName);
            return false;
        } else {
            txtFname.setErrorEnabled(false);
        }
        return matcher.matches();
    }


    // validate phone
    private boolean isValidPhoneNumber(String mobile) {
        Pattern pattern = Pattern.compile("^[9876]\\d{9}$");
        Matcher matcher = pattern.matcher(mobile);

        if (mobile.isEmpty()) {
            txtMobile.setError("Phone no is required");
            requestFocus(mobileNumber);

            return false;
        } else if (!matcher.matches()) {
            txtMobile.setError("Enter a valid mobile");
            requestFocus(mobileNumber);
            return false;
        } else {
            txtMobile.setErrorEnabled(false);
        }

        return matcher.matches();
    }

    private void requestFocus(View view) {
        if (view.requestFocus()) {
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register connection status listener

//        AppController.getInstance().setConnectivityListener(RegistrationActivity.this);
    }

    private void Signup() {

        String f_name = fullName.getText().toString().trim();
        String Email1 = Email.getText().toString().trim();
        String mobile = mobileNumber.getText().toString().trim();
        String Address = address.getText().toString().trim();
        String password = Password.getText().toString().trim();
        referal = txt_referal.getText().toString().trim();


        if ((!isValidName(f_name))) {
            return;
        }
        if ((!isValidPhoneNumber(mobile))) {
            return;
        }
        if ((!isValidEmail(Email1))) {
            return;
        }
        if ((!isValidatePassword(password))) {
            return;
        }

        p_check = "0";

        postCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {

                    p_check = "1";
                } else {

                    p_check = "0";
                }
            }
        });


        c_check = "0";
        completeCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                if (isChecked) {

                    c_check = "1";
                } else {

                    c_check = "0";
                }
            }
        });


        ProgressDialog progressDialog = new ProgressDialog(RegistrationActivity.this);
        progressDialog.setMessage("Loading....");
        progressDialog.show();

        Call<RegisterResponse> call1 = RetrofitClient.getInstance().getApi().Registration(Email1, f_name, mobile, password, p_check, c_check,referal);
        call1.enqueue(new Callback<RegisterResponse>() {
            @Override
            public void onResponse(Call<RegisterResponse> call, Response<RegisterResponse> response) {
                RegisterResponse registerResponse = response.body();
                if (response.code() == 200) {

                    new Handler().postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            progressDialog.dismiss();
                            RegisterResponse.DataBean dataBean=registerResponse.getData();
                            String u_id=String.valueOf(dataBean.getUserId());

                            Toast.makeText(RegistrationActivity.this, registerResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            VerifyMobile(mobile,u_id);

                           // firebaseRegister(f_name,Email1,password);

                        }
                    }, TIME_OUT);

                }
                else  {

                    progressDialog.dismiss();
                    Toast.makeText(RegistrationActivity.this, "500 response...", Toast.LENGTH_SHORT).show();

                   /* if (response.code() == 500){

                        Gson gson = new Gson();
                        RegisterFailureResponse errorResponse = gson.fromJson(response.errorBody().charStream(), RegisterFailureResponse.class);
                        List<RegisterFailureResponse.ErrorsBean> errorsBeans = errorResponse.getErrors();

                        Toast.makeText(RegistrationActivity.this, errorsBeans.get(0).getMessage(), Toast.LENGTH_SHORT).show();

                       *//*
                        for (int i = 0; i < errorsBeans.size(); i++) {
                            if (errorsBeans.get(i).getType().equals("unique violation")) {
                                Toast.makeText(RegistrationActivity.this, errorsBeans.get(i).getMessage(), Toast.LENGTH_SHORT).show();

                            } else {
                                Toast.makeText(RegistrationActivity.this, errorsBeans.get(i).getMessage(), Toast.LENGTH_SHORT).show();
                            }

                        }*//*

                    }*/

                }
            }

            @Override
            public void onFailure(Call<RegisterResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(RegistrationActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();
                Log.e("check", "onFailure: "+t.getMessage() );

            }
        });
    }

    private void VerifyMobile(String mobile, String userId) {

        Call<VerifyMobileResponse> call1=RetrofitClient.getInstance().getApi().VerifyMobile(mobile,userId);
        call1.enqueue(new Callback<VerifyMobileResponse>() {
            @Override
            public void onResponse(Call<VerifyMobileResponse> call, Response<VerifyMobileResponse> response) {
                if (response.isSuccessful());
                VerifyMobileResponse verifyMobileResponse=response.body();
                if (verifyMobileResponse.isSuccess()){

                    Toast.makeText(RegistrationActivity.this, verifyMobileResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    Intent intent=new Intent(RegistrationActivity.this,OtpActivity.class);
                    intent.putExtra("user_id",userId);
                    intent.putExtra("Mobile",mobile);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                }
                else {

                    Toast.makeText(RegistrationActivity.this, verifyMobileResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<VerifyMobileResponse> call, Throwable t) {
                Toast.makeText(RegistrationActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

        showSnack(isConnected);

    }

    private void showSnack(boolean isConnected) {
        String message;
        int color;
        if (isConnected) {
            message = "Good! Connected to Internet";
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

        } else {
            message = "Sorry! Not connected to internet";
            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();

        }

    }


    private void firebaseRegister(final String username, String email, String password) {

        auth.createUserWithEmailAndPassword( email, password )
                .addOnCompleteListener( new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        task.addOnFailureListener( new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                Log.e( TAG, "onFailure: "+e.getMessage()  );
                            }
                        } );
                        Log.e( TAG, "onComplete: "  );
                        if (task.isSuccessful()) {
                            FirebaseUser firebaseUser = auth.getCurrentUser();
                            String userid = firebaseUser.getUid();


                            DatabaseReference reference = FirebaseDatabase.getInstance().getReference( "users" ).child( userid );

//                            DatabaseReference reference = FirebaseDatabase.getInstance().getReference().child( userid );


                            HashMap<String, String> hashMap = new HashMap<>();
                            hashMap.put( "id", userid );
                            hashMap.put( "username", username );
//                            hashMap.put( "imageURL", "default" );


                            reference.setValue( hashMap ).addOnCompleteListener( new OnCompleteListener<Void>() {

                                @Override
                                public void onComplete(@NonNull Task<Void> task) {


                                    task.addOnFailureListener( new OnFailureListener() {
                                        @Override
                                        public void onFailure(@NonNull Exception e) {
                                            Log.e( TAG, "onFailure: " + e );
                                        }
                                    } );
                                    if (task.isSuccessful()) {

                                       // finish();
                                    }
                                }
                            } );
                        } else {
                            Toast.makeText( RegistrationActivity.this, "You cannot register with this email and password", Toast.LENGTH_SHORT ).show();
                        }

                    }
                } );

    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.full_name:
                    isValidName(fullName.getText().toString().trim());
                    break;
                case R.id.mobile_number:
                    isValidPhoneNumber(mobileNumber.getText().toString().trim());
                    break;
               /* case R.id.etOtp:
                    isValidOtp(etOtp.getText().toString().trim());
                    break;*/
                case R.id.email:
                    isValidEmail(Email.getText().toString().trim());
                    break;
                case R.id.password:
                    isValidatePassword(Password.getText().toString().trim());
                    break;
            }
        }
    }
}
