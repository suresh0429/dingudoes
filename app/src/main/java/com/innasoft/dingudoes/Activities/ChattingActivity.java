package com.innasoft.dingudoes.Activities;

import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.innasoft.dingudoes.Adapters.UserAdapter;
import com.innasoft.dingudoes.Models.Chat;
import com.innasoft.dingudoes.Models.User;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.storage.PrefManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;


public class ChattingActivity extends AppCompatActivity {
    FirebaseAuth auth;
    PrefManager prefManager;
    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    private UserAdapter userAdapter;
    private List<User> mUsers;

    FirebaseUser fuser;
    DatabaseReference reference;
    private List<String> usersList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatting);
        ButterKnife.bind(this);

        prefManager = new PrefManager(getApplicationContext());
        HashMap<String, String> profile = prefManager.getUserDetails();
        String email = profile.get("email");
        String password = profile.get("Token");
        auth = FirebaseAuth.getInstance();
        auth.signInWithEmailAndPassword("ganeshchintu800@gmail.com", "123456")
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            loadgetdata();
                        } else {
                            Toast.makeText(ChattingActivity.this, "Authentication failed", Toast.LENGTH_SHORT).show();
                        }
                    }

                });
    }

    private void loadgetdata() {
        fuser = FirebaseAuth.getInstance().getCurrentUser();

        usersList = new ArrayList<>(  );

        reference = FirebaseDatabase.getInstance().getReference("chats");
        reference.addValueEventListener( new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                usersList.clear();

                for (DataSnapshot snapshot : dataSnapshot.getChildren()){
                    Chat chat = snapshot.getValue(Chat.class);

                    assert chat != null;
                    if (chat.getSender().equals( fuser.getUid() )){
                        usersList.add( chat.getReceiver() );
                    }
                    if (chat.getReceiver().equals( fuser .getUid())){
                        usersList.add(chat.getSender());
                    }
                }
                readChats();
            }


            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        } );
    }

    private void readChats() {
        mUsers = new ArrayList<>(  );
        reference = FirebaseDatabase.getInstance().getReference("users");
        reference.addValueEventListener( new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                mUsers.clear();

                for (DataSnapshot snapshot : dataSnapshot.getChildren()) {
                    User user = snapshot.getValue( User.class );


                    try {
                        for (String id : usersList) {
                            assert user != null;
                            if (user.getId().equals( id )) {
                                if (mUsers.size() != 0) {
                                    for(User user1:mUsers) {
                                        if (!user.getId().equals( user1.getId() )) {
                                            mUsers.add( user );
                                        }
                                    }
                                } else {
                                    mUsers.add( user );
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                userAdapter = new UserAdapter( ChattingActivity.this,mUsers );
                recyclerView.setAdapter( userAdapter );
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        } );




    }
}
