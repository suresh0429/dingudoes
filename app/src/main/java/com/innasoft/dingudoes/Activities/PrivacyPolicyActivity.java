package com.innasoft.dingudoes.Activities;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.dingudoes.Adapters.PrivacyPolicyAdapter;
import com.innasoft.dingudoes.Apis.RetrofitClient;
import com.innasoft.dingudoes.Singleton.AppController;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.PrivacyPolicyResponse;
import com.innasoft.dingudoes.storage.PrefManager;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PrivacyPolicyActivity extends AppCompatActivity {
    @BindView(R.id.recycler_privacy)
    RecyclerView recyclerPrivacy;
    PrivacyPolicyAdapter privacyPolicyAdapter;
    PrefManager prefManager;
    String token;
    AppController appController;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.privacy_activity);
        ButterKnife.bind(this);

        appController=(AppController) getApplication();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Privacy Policy");

        prefManager = new PrefManager(getApplicationContext());
        HashMap<String, String> profile = prefManager.getUserDetails();
        token = profile.get("Token");

        if (appController.isConnection()){
            getData();
        }
        else {
            String message = "Sorry! Not connected to internet";
            int color = Color.RED;

            Toast.makeText(PrivacyPolicyActivity.this, message, Toast.LENGTH_SHORT).show();
        }

    }

    private void getData() {

        ProgressDialog progressDialog = new ProgressDialog(PrivacyPolicyActivity.this);
        progressDialog.setMessage("Loading.....");
        progressDialog.show();

        String value="privacyPolicy";
        Call<PrivacyPolicyResponse> conditionsResponseCall= RetrofitClient.getInstance().getApi().PrivacyPolicy(token,value);
        conditionsResponseCall.enqueue(new Callback<PrivacyPolicyResponse>() {
            @Override
            public void onResponse(Call<PrivacyPolicyResponse> call, Response<PrivacyPolicyResponse> response) {
                if (response.isSuccessful());
                PrivacyPolicyResponse termsConditionsResponse=response.body();
                if (termsConditionsResponse.isSuccess()){
                    progressDialog.dismiss();

                    List<PrivacyPolicyResponse.DataBean> dataBeanList=termsConditionsResponse.getData();

                    privacyPolicyAdapter=new PrivacyPolicyAdapter(dataBeanList,PrivacyPolicyActivity.this);
                    LinearLayoutManager layoutManager = new LinearLayoutManager(PrivacyPolicyActivity.this);
                    layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                    recyclerPrivacy.setLayoutManager(layoutManager);
                    recyclerPrivacy.setNestedScrollingEnabled(false);
                    recyclerPrivacy.setAdapter(privacyPolicyAdapter);

                }
            }

            @Override
            public void onFailure(Call<PrivacyPolicyResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(PrivacyPolicyActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);

    }

}
