package com.innasoft.dingudoes.Activities;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.dingudoes.Adapters.PaymentEarnedAdapter;
import com.innasoft.dingudoes.Adapters.PaymentOutAdapter;
import com.innasoft.dingudoes.Apis.RetrofitClient;
import com.innasoft.dingudoes.Singleton.AppController;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.PaymentPaidHistoryResponse;
import com.innasoft.dingudoes.Response.PaymentRecievedHistoryResponse;
import com.innasoft.dingudoes.storage.PrefManager;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.ceryle.segmentedbutton.SegmentedButtonGroup;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PaymentHistoryActivity extends AppCompatActivity {
    PaymentEarnedAdapter paymentEarnedAdapter;
    PaymentOutAdapter paymentOutAdapter;
    RecyclerView recycler_payments;
    ProgressDialog progressDialog;
    PrefManager prefManager;
    String token;
    AppController appController;
    @BindView(R.id.segmentedButtonGroup)
    SegmentedButtonGroup segmentedButtonGroup;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.paymenthistory_activity);
        ButterKnife.bind(this);
        appController=(AppController) getApplication();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Payment History");

        recycler_payments = findViewById(R.id.recycler_payments);

        prefManager = new PrefManager(getApplicationContext());
        HashMap<String, String> profile = prefManager.getUserDetails();
        token = profile.get("Token");

        progressDialog = new ProgressDialog(PaymentHistoryActivity.this);
        progressDialog.setMessage("Please wait......");

        if (appController.isConnection()){
            EarnedData();
            segmentedButtonGroup.setOnClickedButtonListener(new SegmentedButtonGroup.OnClickedButtonListener() {
                @Override
                public void onClickedButton(int position) {
                    if (position==0){
                        EarnedData();
                    }
                    else if (position==1){
                        PaidData();
                    }
                }
            });
        }
        else {

            String message = "Sorry! Not connected to internet";
            int color = Color.RED;

            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }

    private void EarnedData() {

        progressDialog.show();

        String rec = "earned";
        Call<PaymentRecievedHistoryResponse> call = RetrofitClient.getInstance().getApi().PaymentHistoryRecieved(token, rec);
        call.enqueue(new Callback<PaymentRecievedHistoryResponse>() {
            @Override
            public void onResponse(Call<PaymentRecievedHistoryResponse> call, Response<PaymentRecievedHistoryResponse> response) {
                if (response.isSuccessful()) ;
                PaymentRecievedHistoryResponse paymentRecievedHistoryResponse = response.body();
                if (paymentRecievedHistoryResponse.isSuccess()) {
                    progressDialog.dismiss();

                    // paymentOutAdapter = new PaymentOutAdapter(historyModels, PaymentHistoryActivity.this);
//        final LinearLayoutManager layoutManager1 = new LinearLayoutManager(PaymentHistoryActivity.this);
//        layoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
//        recycler_payments.setLayoutManager(layoutManager1);
//        recycler_payments.setNestedScrollingEnabled(false);
//        recycler_payments.setAdapter(paymentOutAdapter);


                } else {
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<PaymentRecievedHistoryResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(PaymentHistoryActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


    }

    private void PaidData() {

        progressDialog.show();
        String paidd = "paid";
        Call<PaymentPaidHistoryResponse> call = RetrofitClient.getInstance().getApi().PaymentHistoryPaid(token, paidd);
        call.enqueue(new Callback<PaymentPaidHistoryResponse>() {
            @Override
            public void onResponse(Call<PaymentPaidHistoryResponse> call, Response<PaymentPaidHistoryResponse> response) {
                if (response.isSuccessful()) ;
                PaymentPaidHistoryResponse paymentPaidHistoryResponse = response.body();
                if (paymentPaidHistoryResponse.isSuccess()) {
                    progressDialog.dismiss();


                   // paymentOutAdapter = new PaymentOutAdapter(historyModels, PaymentHistoryActivity.this);
//        final LinearLayoutManager layoutManager1 = new LinearLayoutManager(PaymentHistoryActivity.this);
//        layoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
//        recycler_payments.setLayoutManager(layoutManager1);
//        recycler_payments.setNestedScrollingEnabled(false);
//        recycler_payments.setAdapter(paymentOutAdapter);


                } else {
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<PaymentPaidHistoryResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(PaymentHistoryActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);

    }
}
