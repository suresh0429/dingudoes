package com.innasoft.dingudoes.Activities;

import android.graphics.Color;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.innasoft.dingudoes.Singleton.AppController;
import com.innasoft.dingudoes.R;

public class UploadProfilePictureActivity extends AppCompatActivity {
    AppController appController;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.upload_profilepicture);
        appController=(AppController) getApplication();
        if (appController.isConnection()){

            UploadPhoto();

        }
        else {
            String message = "Sorry! Not connected to internet";
            int color = Color.RED;

            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }

    private void UploadPhoto() {


    }


}
