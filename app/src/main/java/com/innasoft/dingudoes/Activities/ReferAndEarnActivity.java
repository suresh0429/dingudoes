package com.innasoft.dingudoes.Activities;

import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.storage.PrefManager;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ReferAndEarnActivity extends AppCompatActivity {


    @BindView(R.id.imgCopyText)
    ImageView imgCopyText;
    @BindView(R.id.btn_invite)
    Button btnInvite;
    @BindView(R.id.txtCode)
    TextView txtCode;
    PrefManager prefManager;
    String rcode;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.refer_activity);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Refer & Earn");

        prefManager = new PrefManager(ReferAndEarnActivity.this);
        HashMap<String, String> profile = prefManager.getUserDetails();
        rcode = profile.get("userid");

        if (rcode == null){

        }
        else {
            txtCode.setText(rcode);
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);

    }

    @OnClick({R.id.imgCopyText, R.id.btn_invite})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgCopyText:
                ClipboardManager cm = (ClipboardManager) ReferAndEarnActivity.this.getSystemService(Context.CLIPBOARD_SERVICE);
                cm.setText(txtCode.getText());
                Toast.makeText(ReferAndEarnActivity.this, "Copied to clipboard", Toast.LENGTH_SHORT).show();
                break;
            case R.id.btn_invite:
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "DinguDoes -");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, "" + "Dingu Does");
                startActivity(Intent.createChooser(sharingIntent, "Share via"));
                break;
        }
    }


}
