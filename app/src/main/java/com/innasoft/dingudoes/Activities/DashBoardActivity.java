package com.innasoft.dingudoes.Activities;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.dingudoes.Adapters.CompletedTaskRecycerAdapter;
import com.innasoft.dingudoes.Adapters.UserReviewAdapter;
import com.innasoft.dingudoes.Apis.RetrofitClient;
import com.innasoft.dingudoes.Models.UserReviewModel;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.CompletedTaskResponse;
import com.innasoft.dingudoes.Response.DashboardResponse;
import com.innasoft.dingudoes.Response.GetUserReviewResponse;
import com.innasoft.dingudoes.Singleton.AppController;
import com.innasoft.dingudoes.storage.PrefManager;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.ceryle.segmentedbutton.SegmentedButtonGroup;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DashBoardActivity extends AppCompatActivity {
    private static final String TAG = "dashboard";
    AppController appController;
    PrefManager prefManager;
    String token, content, userId;
    ProgressDialog progressDialog;
    UserReviewAdapter adapter;
    List<UserReviewModel> userReviewModels;
    @BindView(R.id.segmentedButtonGroup)
    SegmentedButtonGroup segmentedButtonGroup;
    @BindView(R.id.txt_taskeropen)
    TextView txtTaskeropen;
    @BindView(R.id.txt_taskerassigned)
    TextView txtTaskerassigned;
    @BindView(R.id.txt_taskercompleted)
    TextView txtTaskercompleted;
    @BindView(R.id.ratingbar)
    RatingBar ratingbar;
    @BindView(R.id.txt_review)
    TextView txtReview;
    @BindView(R.id.recycler_userreviews)
    RecyclerView recyclerUserreviews;
    CompletedTaskRecycerAdapter completedTaskRecycerAdapter;
//    @BindView(R.id.progressLayout)
//    LinearLayout progressLayout;
    LinearLayout nodataLayout;
    @BindView(R.id.recycler_completed)
    RecyclerView recycler_completed;
    TextView txtNodata;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard_activity);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Dashboard");

        prefManager = new PrefManager(getApplicationContext());
        HashMap<String, String> profile = prefManager.getUserDetails();
        token = profile.get("Token");
        userId = profile.get("userid");

        txtNodata=findViewById(R.id.txt_nodata);
        nodataLayout=findViewById(R.id.nodataLayout);
        appController = (AppController) getApplication();

        if (appController.isConnection()) {

            getData();

        } else {
            String message = "Sorry! Not connected to internet";

            Toast.makeText(DashBoardActivity.this, message, Toast.LENGTH_LONG).show();
        }
    }

    private void getData() {

        progressDialog = new ProgressDialog(DashBoardActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        content = "application/json";

        TaskerData();
        TaskerReviewData();
        completedTask();

        segmentedButtonGroup.setOnClickedButtonListener(position -> {
            if (position == 0) {

                TaskerData();
                TaskerReviewData();
            } else if (position == 1) {

                PosterData();
                PosterReviewData();
            }
        });
    }

    private void TaskerData() {

        progressDialog.show();
        Call<DashboardResponse> call = RetrofitClient.getInstance().getApi().DashBoard(token, content, userId);
        call.enqueue(new Callback<DashboardResponse>() {
            @Override
            public void onResponse(Call<DashboardResponse> call, Response<DashboardResponse> response) {
                if (response.isSuccessful()) ;
                DashboardResponse dashboardResponse = response.body();
                if (dashboardResponse.isSuccess()) {

                    progressDialog.dismiss();

                    DashboardResponse.DataBean dataBean = dashboardResponse.getData();

                    List<DashboardResponse.DataBean.AsTaskerBean> asTaskerBeanList = dataBean.getAsTasker();

                    txtTaskeropen.setText(asTaskerBeanList.get(0).getTaskerOpen());
                    txtTaskerassigned.setText(asTaskerBeanList.get(0).getTaskerAssigned());
                    txtTaskercompleted.setText(asTaskerBeanList.get(0).getTaskerCompleted());

                } else {
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<DashboardResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(DashBoardActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void PosterData() {

        progressDialog.show();

        Call<DashboardResponse> call = RetrofitClient.getInstance().getApi().DashBoard(token, content, userId);
        call.enqueue(new Callback<DashboardResponse>() {
            @Override
            public void onResponse(Call<DashboardResponse> call, Response<DashboardResponse> response) {
                if (response.isSuccessful()) ;
                DashboardResponse dashboardResponse = response.body();
                if (dashboardResponse.isSuccess()) {
                    progressDialog.dismiss();

                    DashboardResponse.DataBean dataBean = dashboardResponse.getData();

                    List<DashboardResponse.DataBean.AsPosterBean> asPosterBeanList = dataBean.getAsPoster();

                    for (int i = 0; i < asPosterBeanList.size(); i++) {

                        txtTaskeropen.setText(asPosterBeanList.get(i).getPosterOpen());
                        txtTaskerassigned.setText(asPosterBeanList.get(i).getPosterAssigned());
                        txtTaskercompleted.setText(asPosterBeanList.get(i).getPosterCompleted());
                    }

                } else {
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<DashboardResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(DashBoardActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


    }

    private void TaskerReviewData() {
        userReviewModels = new ArrayList<UserReviewModel>();
        progressDialog.show();
        Call<GetUserReviewResponse> call = RetrofitClient.getInstance().getApi().GetUserReviews(token);
        call.enqueue(new Callback<GetUserReviewResponse>() {
            @Override
            public void onResponse(Call<GetUserReviewResponse> call, Response<GetUserReviewResponse> response) {
                if (response.isSuccessful()) ;
                GetUserReviewResponse dashboardResponse = response.body();
                if (dashboardResponse.isSuccess()) {

                    progressDialog.dismiss();

                    List<GetUserReviewResponse.DataBean.TaskerBean> asTaskerBeanList = dashboardResponse.getData().getTasker();
//                    Log.d(TAG, "onResponse: " + asTaskerBeanList.get(0).getRating());
                    userReviewModels.clear();
                    for (GetUserReviewResponse.DataBean.TaskerBean taskerBean : asTaskerBeanList) {

                        userReviewModels.add(new UserReviewModel(taskerBean.getId(), taskerBean.getUserId(), taskerBean.getTaskId(), taskerBean.getReviewText(), taskerBean.getRating()));
                    }

                    txtReview.setText(""+asTaskerBeanList.size()+" Reviews");
                    ratingbar.setRating((float) calculateAverage(userReviewModels));

                    adapter = new UserReviewAdapter(userReviewModels, DashBoardActivity.this);
                    final LinearLayoutManager layoutManager = new LinearLayoutManager(DashBoardActivity.this);
                    layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                    recyclerUserreviews.setLayoutManager(layoutManager);
                    recyclerUserreviews.setNestedScrollingEnabled(false);
                    recyclerUserreviews.setAdapter(adapter);

                    adapter.notifyDataSetChanged();

//

                } else {
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<GetUserReviewResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(DashBoardActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void PosterReviewData() {
        userReviewModels = new ArrayList<UserReviewModel>();
        progressDialog.show();

        Call<GetUserReviewResponse> call = RetrofitClient.getInstance().getApi().GetUserReviews(token);
        call.enqueue(new Callback<GetUserReviewResponse>() {
            @Override
            public void onResponse(Call<GetUserReviewResponse> call, Response<GetUserReviewResponse> response) {
                if (response.isSuccessful()) ;
                GetUserReviewResponse dashboardResponse = response.body();
                if (dashboardResponse.isSuccess()) {
                    progressDialog.dismiss();

                    List<GetUserReviewResponse.DataBean.PosterBean> asPosterBeanList = dashboardResponse.getData().getPoster();

                    userReviewModels.clear();
                    for (GetUserReviewResponse.DataBean.PosterBean posterBean : asPosterBeanList) {

                        userReviewModels.add(new UserReviewModel(posterBean.getId(), posterBean.getUserId(), posterBean.getTaskId(), posterBean.getReviewText(), posterBean.getRating()));
                    }
                    txtReview.setText(""+asPosterBeanList.size()+" Reviews");
                    ratingbar.setRating((float) calculateAverage(userReviewModels));

                    adapter = new UserReviewAdapter(userReviewModels, DashBoardActivity.this);
                    final LinearLayoutManager layoutManager = new LinearLayoutManager(DashBoardActivity.this);
                    layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                    recyclerUserreviews.setLayoutManager(layoutManager);
                    recyclerUserreviews.setNestedScrollingEnabled(false);
                    recyclerUserreviews.setAdapter(adapter);

                    adapter.notifyDataSetChanged();
                } else {
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<GetUserReviewResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(DashBoardActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


    }

    private void completedTask() {

      //  progressLayout.setVisibility(View.VISIBLE);
        String comp = "completed";

        Call<CompletedTaskResponse> completedTaskResponseCall = RetrofitClient.getInstance().getApi().CompletedTask(userId, comp);
        completedTaskResponseCall.enqueue(new Callback<CompletedTaskResponse>() {
            @Override
            public void onResponse(Call<CompletedTaskResponse> call, Response<CompletedTaskResponse> response) {
                if (response.isSuccessful()) ;
                CompletedTaskResponse completedTaskResponse = response.body();
                if (completedTaskResponse.isSuccess()) {
                   // progressLayout.setVisibility(View.GONE);
                    recycler_completed.setVisibility(View.VISIBLE);
                    nodataLayout.setVisibility(View.GONE);
                    List<CompletedTaskResponse.DataBean> dataBeanList = completedTaskResponse.getData();

                    // if (!dataBeanList.isEmpty()) {

                    completedTaskRecycerAdapter = new CompletedTaskRecycerAdapter(dataBeanList,DashBoardActivity.this);
                    LinearLayoutManager layoutManager = new LinearLayoutManager(DashBoardActivity.this);
                    layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                    recycler_completed.setLayoutManager(layoutManager);
                    recycler_completed.setNestedScrollingEnabled(false);
                    recycler_completed.setAdapter(completedTaskRecycerAdapter);
                    completedTaskRecycerAdapter.notifyDataSetChanged();


                } else {
                //    progressLayout.setVisibility(View.GONE);
                    recycler_completed.setVisibility(View.GONE);
                    nodataLayout.setVisibility(View.VISIBLE);
                    txtNodata.setText("Completed Tasks Not Found !");
                }
            }

            @Override
            public void onFailure(Call<CompletedTaskResponse> call, Throwable t) {
            //    progressLayout.setVisibility(View.GONE);
                Toast.makeText(DashBoardActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    private double calculateAverage(List <UserReviewModel> marks) {
        Double sum = 0.00;
        if(!marks.isEmpty()) {
            for (UserReviewModel mark : marks) {
                sum += mark.getRating();
            }
            return sum.doubleValue() / marks.size();
        }
        return sum;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);

    }
}
