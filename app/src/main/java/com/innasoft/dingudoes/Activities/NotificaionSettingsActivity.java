package com.innasoft.dingudoes.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.innasoft.dingudoes.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class NotificaionSettingsActivity extends AppCompatActivity {

    @BindView(R.id.img_tran)
    ImageView imgTran;
    @BindView(R.id.email2)
    TextView email2;
    @BindView(R.id.pppush)
    TextView pppush;
    @BindView(R.id.relative_transactional)
    RelativeLayout relativeTransactional;
    @BindView(R.id.img_upp)
    ImageView imgUpp;
    @BindView(R.id.eemail)
    TextView eemail;
    @BindView(R.id.relative_tupdates)
    RelativeLayout relativeTupdates;
    @BindView(R.id.img_reminder)
    ImageView imgReminder;
    @BindView(R.id.email1)
    TextView email1;
    @BindView(R.id.ppush)
    TextView ppush;
    @BindView(R.id.relative_taskreminder)
    RelativeLayout relativeTaskreminder;
    @BindView(R.id.img_alerts)
    ImageView imgAlerts;
    @BindView(R.id.email)
    TextView email;
    @BindView(R.id.relative_ddalerts)
    RelativeLayout relativeDdalerts;
    @BindView(R.id.img_recommnd)
    ImageView imgRecommnd;
    @BindView(R.id.relative_taskrecommnd)
    RelativeLayout relativeTaskrecommnd;
    @BindView(R.id.img_info)
    ImageView imgInfo;
    @BindView(R.id.push)
    TextView push;
    @BindView(R.id.relative_helpful)
    RelativeLayout relativeHelpful;
    @BindView(R.id.img_updates)
    ImageView imgUpdates;
    @BindView(R.id.walk)
    TextView walk;
    @BindView(R.id.relative_updates)
    RelativeLayout relativeUpdates;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.notificationsettings_activity);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Notification Settings");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);

    }


    @OnClick({R.id.relative_transactional, R.id.relative_tupdates, R.id.relative_taskreminder, R.id.relative_ddalerts, R.id.relative_taskrecommnd, R.id.relative_helpful, R.id.relative_updates})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.relative_transactional:
                startActivity(new Intent(NotificaionSettingsActivity.this,TransactionalActivity.class));
                break;
            case R.id.relative_tupdates:
                break;
            case R.id.relative_taskreminder:
                break;
            case R.id.relative_ddalerts:
                break;
            case R.id.relative_taskrecommnd:
                break;
            case R.id.relative_helpful:
                break;
            case R.id.relative_updates:
                break;
        }
    }
}
