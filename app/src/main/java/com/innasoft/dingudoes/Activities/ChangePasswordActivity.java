package com.innasoft.dingudoes.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.innasoft.dingudoes.Apis.RetrofitClient;
import com.innasoft.dingudoes.Singleton.AppController;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.SetPasswordResponse;
import com.innasoft.dingudoes.storage.PrefManager;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangePasswordActivity extends AppCompatActivity {
    @BindView(R.id.newPass)
    TextInputEditText newPass;
    @BindView(R.id.c_newPass)
    TextInputEditText cNewPass;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    String token;
    PrefManager prefManager;
    private static  int TIME_OUT = 1500;
    private boolean checkInternet;
    AppController appController;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.changepassword_activity);
        ButterKnife.bind(this);
        appController = (AppController) getApplication();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Change Password");

        prefManager = new PrefManager(getApplicationContext());
        HashMap<String, String> profile = prefManager.getUserDetails();
        token = profile.get("Token");


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);

    }

    @OnClick(R.id.btn_submit)
    public void onViewClicked() {
       // ChangePassword();

        boolean isConnected = appController.isConnection();

        if (isConnected) {

            ChangePassword();
        }
        else {

            String message = "Sorry! Not connected to internet";
            int color = Color.RED;

            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
            //showSnack(isConnected);
        }


    }

    private void ChangePassword() {

        String new_password=newPass.getText().toString().trim();
        String c_newpassword=cNewPass.getText().toString().trim();


        if (new_password.equals("")) {
            Toast.makeText(ChangePasswordActivity.this, "Please Enter new Password", Toast.LENGTH_SHORT).show();
        }
        else if (!c_newpassword.equals(new_password)){
            Toast.makeText(this, "Confirm Password not Matched", Toast.LENGTH_SHORT).show();
        }

        else {
            ProgressDialog progressDialog=new ProgressDialog(ChangePasswordActivity.this);
            progressDialog.setMessage("Loading....");
            progressDialog.show();

            Call<SetPasswordResponse> call= RetrofitClient.getInstance().getApi().SetPassword(token,new_password);
            call.enqueue(new Callback<SetPasswordResponse>() {
                @Override
                public void onResponse(Call<SetPasswordResponse> call, Response<SetPasswordResponse> response) {
                    if (response.isSuccessful());
                    SetPasswordResponse setPasswordResponse=response.body();
                    if (setPasswordResponse.isSuccess()){

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismiss();
                                Toast.makeText(ChangePasswordActivity.this, setPasswordResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                Intent intent=new Intent(ChangePasswordActivity.this,HomeActivity.class);
                                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                startActivity(intent);

                            }
                        }, TIME_OUT);


                    }
                    else {
                        progressDialog.dismiss();
                        Toast.makeText(ChangePasswordActivity.this, setPasswordResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<SetPasswordResponse> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(ChangePasswordActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });
        }

    }
}
