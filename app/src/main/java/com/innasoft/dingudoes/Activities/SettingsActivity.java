package com.innasoft.dingudoes.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.innasoft.dingudoes.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingsActivity extends AppCompatActivity {


    @BindView(R.id.relative_taskalert)
    RelativeLayout relativeTaskalert;
    @BindView(R.id.relative_notifi)
    RelativeLayout relativeNotifi;
    @BindView(R.id.relative_cpassword)
    RelativeLayout relativeCpassword;
    @BindView(R.id.relative_cmobile)
    RelativeLayout relativeCmobile;
    @BindView(R.id.relative_addbank)
    RelativeLayout relativeAddbank;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Settings");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);

    }


    @OnClick({R.id.relative_taskalert, R.id.relative_notifi, R.id.relative_cpassword, R.id.relative_cmobile,R.id.relative_addbank})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.relative_taskalert:
                startActivity(new Intent(SettingsActivity.this, TaskAlertActivity.class));

                break;
            case R.id.relative_notifi:
                startActivity(new Intent(SettingsActivity.this, NotificaionSettingsActivity.class));

                break;
            case R.id.relative_cpassword:
                startActivity(new Intent(SettingsActivity.this, ChangePasswordActivity.class));
                break;
            case R.id.relative_cmobile:
                startActivity(new Intent(SettingsActivity.this, ChangeMobileNumberActivity.class));
                break;
            case R.id.relative_addbank:
                Intent intent=new Intent(SettingsActivity.this,BankAccountsListActivity.class);
                startActivity(intent);
                break;
        }
    }

}
