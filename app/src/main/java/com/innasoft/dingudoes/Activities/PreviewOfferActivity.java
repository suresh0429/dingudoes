package com.innasoft.dingudoes.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.innasoft.dingudoes.Apis.RetrofitClient;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.CreateQuoteResponse;
import com.innasoft.dingudoes.storage.PrefManager;

import java.util.HashMap;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.innasoft.dingudoes.Singleton.AppController.TAG;

public class PreviewOfferActivity extends AppCompatActivity {
    double reciveAmount, servicePrice, gstPrice;
    PrefManager prefManager;
    String token,userId;
    int taskid;

    @BindView(R.id.txtserviceFee)
    TextView txtserviceFee;
    @BindView(R.id.txtservicePrice)
    TextView txtservicePrice;
    @BindView(R.id.txtGST)
    TextView txtGST;
    @BindView(R.id.txtGSTPrice)
    TextView txtGSTPrice;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.txtFinal)
    TextView txtFinal;
    @BindView(R.id.txtFinalPrice)
    TextView txtFinalPrice;
    @BindView(R.id.layoutOne)
    LinearLayout layoutOne;
    @BindView(R.id.txtTotalQuote)
    TextView txtTotalQuote;
    @BindView(R.id.btnSubmit)
    Button btnSubmit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_preview_offer);
        ButterKnife.bind(this);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Preview offer");

        prefManager = new PrefManager(PreviewOfferActivity.this);
        HashMap<String, String> profile = prefManager.getUserDetails();
        token = profile.get("Token");
        userId = profile.get("userid");

        if (getIntent() != null) {
            reciveAmount = getIntent().getDoubleExtra("receiveAmount", 0.00);
            servicePrice = getIntent().getDoubleExtra("servicePrice", 0.00);
            gstPrice = getIntent().getDoubleExtra("gstPrice", 0.00);
            taskid = getIntent().getIntExtra("taskid",0);

            txtTotalQuote.setText("\u20b9" + String.format("%.2f", reciveAmount + servicePrice + gstPrice));
            txtservicePrice.setText("\u20b9" + String.format("%.2f", servicePrice));
            txtGSTPrice.setText("\u20b9" + String.format("%.2f", gstPrice));
            txtFinalPrice.setText("\u20b9" + String.format("%.2f", reciveAmount));
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);

    }

    @OnClick(R.id.btnSubmit)
    public void onViewClicked() {
        ClickQuote();
    }

    private void ClickQuote() {

        ProgressDialog progressDialog = new ProgressDialog(PreviewOfferActivity.this);
        progressDialog.setMessage("Loading....");
        progressDialog.show();

        Call<CreateQuoteResponse> createQuoteResponseCall = RetrofitClient.getInstance().getApi().CreateQuote(token, taskid);
        createQuoteResponseCall.enqueue(new Callback<CreateQuoteResponse>() {
            @Override
            public void onResponse(Call<CreateQuoteResponse> call, Response<CreateQuoteResponse> response) {
                if (response.isSuccessful()) ;
                CreateQuoteResponse createQuoteResponse = response.body();
                if (createQuoteResponse.isSuccess()) {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), createQuoteResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(PreviewOfferActivity.this,HomeActivity.class);
                    intent.setFlags(intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);

                    startActivity(intent);
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(PreviewOfferActivity.this, createQuoteResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<CreateQuoteResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(PreviewOfferActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }
}
