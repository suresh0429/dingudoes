package com.innasoft.dingudoes.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.innasoft.dingudoes.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MakeAnOfferActivity extends AppCompatActivity {

    private static final String TAG = "MakeAnOfferActivity";
    @BindView(R.id.txtserviceFee)
    TextView txtserviceFee;
    @BindView(R.id.txtservicePrice)
    TextView txtservicePrice;
    @BindView(R.id.txtGST)
    TextView txtGST;
    @BindView(R.id.txtGSTPrice)
    TextView txtGSTPrice;
    @BindView(R.id.view)
    View view;
    @BindView(R.id.txtFinal)
    TextView txtFinal;
    @BindView(R.id.txtFinalPrice)
    TextView txtFinalPrice;
    @BindView(R.id.layoutOne)
    LinearLayout layoutOne;
    @BindView(R.id.btnContinue)
    Button btnContinue;
    @BindView(R.id.etPrice)
    EditText etPrice;

    Double finalPrice,finalPricePercentage,servicePrice,gstPrice;
    int enterAmount,taskid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_make_an_offer);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Make an offer");

        if (getIntent() != null){
            taskid = getIntent().getIntExtra("taskid",0);
        }

        etPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

                if (!s.toString().equalsIgnoreCase("")){

                    enterAmount=Integer.parseInt(s.toString());
                    finalPricePercentage =(enterAmount / 100.00) * 10;
                    servicePrice = (enterAmount / 100.00) * 7.5;
                    gstPrice = (enterAmount / 100.00) * 2.5;
                    finalPrice = enterAmount - finalPricePercentage;

                    txtservicePrice.setText("- "+"\u20b9"+String.format("%.2f",servicePrice));
                    txtGSTPrice.setText("- "+"\u20b9"+String.format("%.2f",gstPrice));
                    txtFinalPrice.setText("\u20b9"+String.format("%.2f",finalPrice));

                    btnContinue.setAlpha(0.9f);
                    btnContinue.setEnabled(true);
                }


                if (s.length() == 0) {
                    //Make the elements invisible
                    txtservicePrice.setText("\u20b9"+String.format("%.2f",0.00));
                    txtGSTPrice.setText("\u20b9"+String.format("%.2f",0.00));
                    txtFinalPrice.setText("\u20b9"+String.format("%.2f",0.00));

                    btnContinue.setAlpha(0.5f);
                    btnContinue.setEnabled(false);

                }

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                return true;

        }
        return super.onOptionsItemSelected(item);

    }

    @OnClick(R.id.btnContinue)
    public void onViewClicked() {
        Intent intent = new Intent(MakeAnOfferActivity.this, MakeanOfferActivity1.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("receiveAmount",finalPrice);
        intent.putExtra("servicePrice",servicePrice);
        intent.putExtra("gstPrice",gstPrice);
        intent.putExtra("taskid",taskid);
        startActivity(intent);
    }
}
