package com.innasoft.dingudoes.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.gocashfree.cashfreesdk.CFPaymentService;
import com.innasoft.dingudoes.Adapters.QuotesListingRecyclerAdapter;
import com.innasoft.dingudoes.Apis.RetrofitClient;
import com.innasoft.dingudoes.Interface.AcceptListner;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.GetProfileResponse;
import com.innasoft.dingudoes.Response.MyTaskDetailsResponse;
import com.innasoft.dingudoes.Response.QuoteAcceptResponse;
import com.innasoft.dingudoes.Response.TokenResponse;
import com.innasoft.dingudoes.Response.TransactioSuccessResponse;
import com.innasoft.dingudoes.Response.UpdatetaskSattusResponse;
import com.innasoft.dingudoes.Singleton.AppController;
import com.innasoft.dingudoes.storage.PrefManager;
import com.skyhope.showmoretextview.ShowMoreTextView;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_APP_ID;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_CUSTOMER_EMAIL;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_CUSTOMER_NAME;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_CUSTOMER_PHONE;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_ORDER_AMOUNT;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_ORDER_ID;
import static com.gocashfree.cashfreesdk.CFPaymentService.PARAM_ORDER_NOTE;

public class
MyTaskDetailsActivity extends AppCompatActivity implements AcceptListner {
    private static final String TAG = "MyTaskDetailsActivity";
    AcceptListner acceptListner;
    ShowMoreTextView text_view_show_more;
    RecyclerView recycler_quotes;
    QuotesListingRecyclerAdapter quotesListingRecyclerAdapter;
    String task_id,activity;
    AppController appController;
    PrefManager prefManager;
    String userId, token, orderId;
    TextView txt_name, txt_budget;
    int mobile_verify, paidToUserId, quoteId;
    String dateofBirth, address, bankAccount, photo, name, email, about, skill, transportt, language, amount;
    GetProfileResponse.DataBean dataBean;
    GetProfileResponse.DataBean.UserDetailBean userDetailBean;
    Button btn_completed;
    @BindView(R.id.linear_recycler)
    LinearLayout linearRecycler;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.mytaskdetails_activity);
        ButterKnife.bind(this);

        appController = (AppController) getApplication();
        acceptListner = (AcceptListner) this;

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("MyTask Details");
        text_view_show_more = findViewById(R.id.text_view_show_more);

        txt_name = findViewById(R.id.txt_title);
        txt_budget = findViewById(R.id.txt_budget);

        btn_completed = findViewById(R.id.btn_completed);

        //text_view_show_more.setShowingChar(numberOfCharacter);
        //number of line you want to short
        text_view_show_more.setShowingLine(3);

        text_view_show_more.addShowMoreText("More");
        text_view_show_more.addShowLessText("Less");
        text_view_show_more.setShowMoreColor(Color.RED); // or other color
        text_view_show_more.setShowLessTextColor(Color.RED); // or other color

        if (getIntent() != null) {
            task_id = getIntent().getStringExtra("TaskId");
            activity=getIntent().getStringExtra("Activity");
        }

        assert activity != null;
        if (activity.equalsIgnoreCase("Ongoing")){
            /*linearRecycler.setVisibility(View.GONE);
            btn_completed.setVisibility(View.GONE);*/

            linearRecycler.setVisibility(View.VISIBLE);
            btn_completed.setVisibility(View.VISIBLE);
        }
        else {
            linearRecycler.setVisibility(View.VISIBLE);
            btn_completed.setVisibility(View.VISIBLE);

           /* linearRecycler.setVisibility(View.VISIBLE);
            btn_completed.setVisibility(View.VISIBLE);*/
        }

        prefManager = new PrefManager(MyTaskDetailsActivity.this);
        HashMap<String, String> profile = prefManager.getUserDetails();
        userId = profile.get("userid");
        token = profile.get("Token");


        recycler_quotes = findViewById(R.id.recycler_quotes);

        if (appController.isConnection()) {
            getData();
            getUserDetails(userId);
        } else {
            String message = "Sorry! Not connected to internet";
            int color = Color.RED;

            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }

    }

    // get Quote data
    private void getData() {

        ProgressDialog progressDialog = new ProgressDialog(MyTaskDetailsActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        Call<MyTaskDetailsResponse> call = RetrofitClient.getInstance().getApi().GetMyTeskDetails(token, task_id);
        call.enqueue(new Callback<MyTaskDetailsResponse>() {
            @Override
            public void onResponse(Call<MyTaskDetailsResponse> call, Response<MyTaskDetailsResponse> response) {
                if (response.isSuccessful()) ;
                MyTaskDetailsResponse myTaskDetailsResponse = response.body();

                assert myTaskDetailsResponse != null;
                if (myTaskDetailsResponse.isSuccess()) {
                    progressDialog.dismiss();
                    MyTaskDetailsResponse.DataBean dataBean = myTaskDetailsResponse.getData();
                    txt_name.setText(dataBean.getTitle());
                    text_view_show_more.setText(dataBean.getDescription());
                    txt_budget.setText("\u20B9" + dataBean.getBudget() + " /-");

                    if (dataBean.getStatus().equals("completed")) {
                        btn_completed.setAlpha(.5f);
                        btn_completed.setEnabled(false);
                    }else if (dataBean.getStatus().equals("open")){
                        btn_completed.setVisibility(View.GONE);
                    }

                    String budget = dataBean.getBudget();
                    List<MyTaskDetailsResponse.DataBean.QuotesBean> quotesBeanList = dataBean.getQuotes();
                    quotesListingRecyclerAdapter = new QuotesListingRecyclerAdapter(quotesBeanList, MyTaskDetailsActivity.this, token, userId, budget, acceptListner);
                    final LinearLayoutManager layoutManager = new LinearLayoutManager(MyTaskDetailsActivity.this);
                    layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                    recycler_quotes.setLayoutManager(layoutManager);
                    recycler_quotes.setNestedScrollingEnabled(false);
                    recycler_quotes.setAdapter(quotesListingRecyclerAdapter);

                    amount = dataBean.getBudget();

                } else {
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<MyTaskDetailsResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(MyTaskDetailsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


        btn_completed.setOnClickListener(v -> {
            updateTaskStatus();
        });

    }

    // get user details
    private void getUserDetails(String userId) {
        Call<GetProfileResponse> call = RetrofitClient.getInstance().getApi().Profile(userId);
        call.enqueue(new Callback<GetProfileResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(Call<GetProfileResponse> call, Response<GetProfileResponse> response) {
                if (response.isSuccessful()) ;
                GetProfileResponse getProfileResponse = response.body();

                if (getProfileResponse.isSuccess()) {

                    dataBean = getProfileResponse.getData();
                    userDetailBean = dataBean.getUserDetail();

                    mobile_verify = dataBean.getMobileVerified();
                    //
                    // dateofBirth = userDetailBean.getDateOfBirth();
                    address = userDetailBean.getBillingAddress();
                    bankAccount = dataBean.getBeneficaryId();
                    photo = dataBean.getProfileImage();
                    name = dataBean.getFullName();
                    email = dataBean.getEmail();
                    about = dataBean.getUserDetail().getAbout();
                    skill = dataBean.getUserDetail().getSkills();
                    transportt = dataBean.getUserDetail().getTransport();
                    language = dataBean.getUserDetail().getLanguages();
                    dateofBirth = dataBean.getUserDetail().getDateOfBirth();

                }
            }

            @Override
            public void onFailure(Call<GetProfileResponse> call, Throwable t) {

                Toast.makeText(MyTaskDetailsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    private void updateTaskStatus() {
        Call<UpdatetaskSattusResponse> call = RetrofitClient.getInstance().getApi().updateTaskStatus(token, "completed", task_id);
        call.enqueue(new Callback<UpdatetaskSattusResponse>() {
            @Override
            public void onResponse(Call<UpdatetaskSattusResponse> call, Response<UpdatetaskSattusResponse> response) {
                if (response.isSuccessful()) {

                    UpdatetaskSattusResponse loginResponse = response.body();
                    // Toast.makeText(MyTaskDetailsActivity.this, loginResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    Log.d(TAG, "onResponse: " + loginResponse.isSuccess());

                    Intent intent = new Intent(MyTaskDetailsActivity.this, ReviewActity.class);
                    intent.putExtra("TaskId", task_id);
                    intent.putExtra("Activity", "MyTaskDetailsActivity");
                    startActivity(intent);
                }
            }

            @Override
            public void onFailure(Call<UpdatetaskSattusResponse> call, Throwable t) {

                Toast.makeText(MyTaskDetailsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onClick(int position, List<MyTaskDetailsResponse.DataBean.QuotesBean> quotesBeanList, Button btn_accept) {


        if (appController.isConnection()) {

            if (mobile_verify == 0 || dateofBirth == null || address == null || bankAccount == null || photo == null) {

                Intent intent = new Intent(MyTaskDetailsActivity.this, ConfirmDetailsActivity.class);
                intent.putExtra("Activity", "MyTaskDetailsActivity");
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
            else {

                AlertDialog.Builder builder1 = new AlertDialog.Builder(MyTaskDetailsActivity.this);
                builder1.setIcon(R.mipmap.ic_launcher_round);
                builder1.setTitle("Are you sure to Accept");
                builder1.setMessage("if you accept the task. it will redirect to payment page.");
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        "Yes",
                        (dialog, id) -> {
                            dialog.cancel();

                            //generate order id
                            int random = new Random().nextInt(61000) + 20000;
                            orderId = "DDR" + quotesBeanList.get(position).getId();

                            generateToken(orderId);
                        });

                builder1.setNegativeButton(
                        "No",
                        (dialog, id) -> dialog.cancel());

                AlertDialog alert11 = builder1.create();
                alert11.show();

                paidToUserId = quotesBeanList.get(position).getUserId();
                quoteId = quotesBeanList.get(position).getId();


            }
        } else {
            String message = "Sorry! Not connected to internet";
            int color = Color.RED;

            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }


    }

    // generate cashfree token
    private void generateToken(String orderId) {

        Call<TokenResponse> call = RetrofitClient.getInstance().getApi().generateToken(token, orderId, amount);
        call.enqueue(new Callback<TokenResponse>() {
            @Override
            public void onResponse(Call<TokenResponse> call, Response<TokenResponse> response) {
                if (response.isSuccessful()) ;
                TokenResponse tokenResponse = response.body();
                if (tokenResponse.isSuccess()) {

                    //Toast.makeText(MyTaskDetailsActivity.this, tokenResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    payOnline(orderId, tokenResponse.getCftoken());
                }
            }

            @Override
            public void onFailure(Call<TokenResponse> call, Throwable t) {
                Toast.makeText(MyTaskDetailsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    // redirect to payment gateway
    private void payOnline(String orderid, String cftoken) {
        /*
         * token can be generated from your backend by calling cashfree servers. Please
         * check the documentation for details on generating the token.
         * READ THIS TO GENERATE TOKEN: https://bit.ly/2RGV3Pp
         */
        String token = cftoken;


        /*
         * stage allows you to switch between sandboxed and production servers
         * for CashFree Payment Gateway. The possible values are
         *
         * 1. TEST: Use the Test server. You can use this service while integrating
         *      and testing the CashFree PG. No real money will be deducted from the
         *      cards and bank accounts you use this stage. This mode is thus ideal
         *      for use during the development. You can use the cards provided here
         *      while in this stage: https://docs.cashfree.com/docs/resources/#test-data
         *
         * 2. PROD: Once you have completed the testing and integration and successfully
         *      integrated the CashFree PG, use this value for stage variable. This will
         *      enable live transactions
         */
        String stage = "TEST";

        /*
         * appId will be available to you at CashFree Dashboard. This is a unique
         * identifier for your app. Please replace this appId with your appId.
         * Also, as explained below you will need to change your appId to prod
         * credentials before publishing your app.
         */
        String appId = "90232192fbc3a769801f76e63209";
        String orderId = orderid;
        String orderAmount = amount;
        String orderNote = "Test Order";
        String customerName = "suresh";
        String customerPhone = "8919480920";
        String customerEmail = "ksuresh.unique@gmail.com";

        Map<String, String> params = new HashMap<>();

        params.put(PARAM_APP_ID, appId);
        params.put(PARAM_ORDER_ID, orderId);
        params.put(PARAM_ORDER_AMOUNT, orderAmount);
        params.put(PARAM_ORDER_NOTE, orderNote);
        params.put(PARAM_CUSTOMER_NAME, customerName);
        params.put(PARAM_CUSTOMER_PHONE, customerPhone);
        params.put(PARAM_CUSTOMER_EMAIL, customerEmail);


        for (Map.Entry entry : params.entrySet()) {
            Log.d("CFSKDSample", entry.getKey() + " " + entry.getValue());
        }

        CFPaymentService cfPaymentService = CFPaymentService.getCFPaymentServiceInstance();
        cfPaymentService.setOrientation(this, 0);

        // Use the following method for initiating Payments
        // First color - Toolbar background
        // Second color - Toolbar text and back arrow color
        cfPaymentService.doPayment(MyTaskDetailsActivity.this, params, token, stage);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //Same request code for all payment APIs.
        Log.d(TAG, "ReqCode : " + CFPaymentService.REQ_CODE);
        Log.d(TAG, "API Response : " + data);
        //Prints all extras. Replace with app logic.
        if (data != null) {
            Bundle bundle = data.getExtras();
            Log.d(TAG, "API Response : " + bundle);
            if (bundle != null)

                for (String key : bundle.keySet()) {
                    String string = " " + key + " => " + bundle.get(key) + ";";
                    Log.d(TAG, "onActivityResult: " + string);
                    if (bundle.getString(key) != null) {
                        Log.d(TAG, key + " : " + bundle.get("txStatus"));


                        if (bundle.get("orderAmount") != null || bundle.get("orderId") != null || bundle.get("referenceId") != null) {

                            String orderAmount = String.valueOf(bundle.get("orderAmount"));
                            String txStatus = String.valueOf(bundle.get("txStatus"));
                            String referenceId = String.valueOf(bundle.get("referenceId"));
                            String txMsg = String.valueOf(bundle.get("txMsg"));
                            String orderId = String.valueOf(bundle.get("orderId"));

                            Log.d(TAG, "onActivityResult1: " + orderAmount);
                            double payAmount = 0.00;
                            try {
                                payAmount = DecimalFormat.getNumberInstance().parse(orderAmount).doubleValue();
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            // save details to server
                            paymentStatus(payAmount, txStatus, referenceId, txMsg, orderId);
                        }

                    }
                }
        }
    }

    // payment status success or failure
    private void paymentStatus(Double orderAmount, String txStatus, String referenceId, String txMsg, String orderId) {
        Call<TransactioSuccessResponse> call = RetrofitClient.getInstance().getApi().orderTransaction(token, Integer.parseInt(userId), paidToUserId, Integer.parseInt(task_id), quoteId, orderAmount, txStatus, referenceId, txMsg, orderId);
        call.enqueue(new Callback<TransactioSuccessResponse>() {
            @Override
            public void onResponse(Call<TransactioSuccessResponse> call, Response<TransactioSuccessResponse> response) {
                if (response.isSuccessful()) ;
                TransactioSuccessResponse quoteAcceptResponse = response.body();
                Log.d(TAG, "onResponse: " + quoteAcceptResponse.getData().getStatus());

                if (quoteAcceptResponse.getData().getStatus().equalsIgnoreCase("FAILED")) {
                    Toast.makeText(MyTaskDetailsActivity.this, "Your Transaction is Failed!", Toast.LENGTH_SHORT).show();
                } else {
                    ClickAccept();
                }

            }

            @Override
            public void onFailure(Call<TransactioSuccessResponse> call, Throwable t) {

                Toast.makeText(MyTaskDetailsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    // click accept button
    private void ClickAccept() {

        ProgressDialog progressDialog = new ProgressDialog(MyTaskDetailsActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        Call<QuoteAcceptResponse> call = RetrofitClient.getInstance().getApi().AcceptQuote(token, userId, task_id);
        call.enqueue(new Callback<QuoteAcceptResponse>() {
            @Override
            public void onResponse(Call<QuoteAcceptResponse> call, Response<QuoteAcceptResponse> response) {
                if (response.isSuccessful()) ;
                QuoteAcceptResponse quoteAcceptResponse = response.body();
                if (quoteAcceptResponse.isSuccess()) {
                    progressDialog.dismiss();
                    Toast.makeText(MyTaskDetailsActivity.this, quoteAcceptResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    //btn_accept.setEnabled(false);
                    finish();
                } else {
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<QuoteAcceptResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(MyTaskDetailsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }
}
