package com.innasoft.dingudoes.Activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.goodiebag.pinview.Pinview;
import com.google.android.material.textfield.TextInputEditText;
import com.innasoft.dingudoes.Apis.RetrofitClient;
import com.innasoft.dingudoes.Singleton.AppController;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.VerifyMobileResponse;
import com.innasoft.dingudoes.Response.VerifyOtpResponse;
import com.innasoft.dingudoes.storage.PrefManager;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VerifyMobileNumberActivity extends AppCompatActivity {

    @BindView(R.id.edt_mobile)
    TextInputEditText edtMobile;
    @BindView(R.id.btn_verify)
    Button btnVerify;
    @BindView(R.id.linear_mobile)
    LinearLayout linearMobile;
    @BindView(R.id.pinview)
    Pinview pinview;
    @BindView(R.id.txt_resend)
    TextView txtResend;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.linear_otp)
    LinearLayout linearOtp;
    AppController appController;
    PrefManager prefManager;
    String token,user_id,otp;
    private static int TIME_OUT = 1500;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.verify_mobile);
        ButterKnife.bind(this);

        appController=(AppController) getApplication();

        if (appController.isConnection()){
            getVerify();
        }



    }

    private void getVerify() {

        prefManager = new PrefManager(getApplicationContext());
        HashMap<String, String> profile = prefManager.getUserDetails();
        token = profile.get("Token");
        user_id=profile.get("userid");


        btnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String mobile=edtMobile.getText().toString().trim();
                if (mobile.equals("")){
                    Toast.makeText(getApplicationContext(), "Enter Mobile Number..", Toast.LENGTH_SHORT).show();
                }
                else {

                    ProgressDialog progressDialog=new ProgressDialog(VerifyMobileNumberActivity.this);
                    progressDialog.setMessage("Loading....");
                    progressDialog.show();

                    Call<VerifyMobileResponse> call1= RetrofitClient.getInstance().getApi().VerifyMobile(mobile,user_id);
                    call1.enqueue(new Callback<VerifyMobileResponse>() {
                        @Override
                        public void onResponse(Call<VerifyMobileResponse> call, Response<VerifyMobileResponse> response) {
                            if (response.isSuccessful());
                            VerifyMobileResponse verifyMobileResponse=response.body();
                            if (verifyMobileResponse.isSuccess()){
                                progressDialog.dismiss();
                                Toast.makeText(VerifyMobileNumberActivity.this, verifyMobileResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                linearMobile.setVisibility(View.GONE);
                                linearOtp.setVisibility(View.VISIBLE);


                            }
                            else {
                                progressDialog.dismiss();
                                Toast.makeText(VerifyMobileNumberActivity.this, verifyMobileResponse.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onFailure(Call<VerifyMobileResponse> call, Throwable t) {
                            progressDialog.dismiss();
                            Toast.makeText(VerifyMobileNumberActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    });

                }
            }
        });


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                otp = pinview.getValue().trim();

                ProgressDialog progressDialog = new ProgressDialog(VerifyMobileNumberActivity.this);
                progressDialog.setMessage("Loading....");
                progressDialog.show();

                Call<VerifyOtpResponse> call = RetrofitClient.getInstance().getApi().VerifyOtp(otp, user_id);
                call.enqueue(new Callback<VerifyOtpResponse>() {
                    @Override
                    public void onResponse(Call<VerifyOtpResponse> call, Response<VerifyOtpResponse> response) {
                        if (response.isSuccessful()){
                        VerifyOtpResponse verifyOtpResponse = response.body();
                            assert verifyOtpResponse != null;
                            if (verifyOtpResponse.isSuccess()) {

                                progressDialog.dismiss();
                                Toast.makeText(VerifyMobileNumberActivity.this, verifyOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                Intent intent = getIntent();
                                setResult(Activity.RESULT_CANCELED,intent);
                                finish();


                        } else {
                            progressDialog.dismiss();
                            Toast.makeText(VerifyMobileNumberActivity.this, verifyOtpResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                        }else {
                            progressDialog.dismiss();
                            Toast.makeText(VerifyMobileNumberActivity.this,"server Error!", Toast.LENGTH_SHORT).show();

                        }
                    }

                    @Override
                    public void onFailure(Call<VerifyOtpResponse> call, Throwable t) {
                        progressDialog.dismiss();
                        Toast.makeText(VerifyMobileNumberActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                    }
                });
            }
        });

    }
}
