package com.innasoft.dingudoes.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.innasoft.dingudoes.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AddMustHavesActivity extends AppCompatActivity {

    @BindView(R.id.ediText)
    EditText ediText;
    @BindView(R.id.txt_add)
    TextView txtAdd;
    @BindView(R.id.list)
    ListView list;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    private ArrayAdapter<String> adapter;
    private ArrayList<String> arrayList;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.addmust_activity);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Add Must Have");

        arrayList = new ArrayList<String>();
        // Adapter: You need three parameters 'the context, id of the layout (it will be where the data is shown),
        // and the array that contains the data
        adapter = new ArrayAdapter<String>(getApplicationContext(), android.R.layout.simple_spinner_item, arrayList);

        // Here, you set the data in your ListView
        list.setAdapter(adapter);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            list.setNestedScrollingEnabled(true);
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);

    }

    @OnClick({R.id.txt_add, R.id.btn_submit})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.txt_add:

                if (!ediText.getText().toString().isEmpty()){
                    arrayList.add(ediText.getText().toString());
                    // next thing you have to do is check if your adapter has changed
                    adapter.notifyDataSetChanged();
                    ediText.setText("");
                }


                break;
            case R.id.btn_submit:

                Intent intent = new Intent();
                intent.putStringArrayListExtra("ARRAYLIST", arrayList);
                setResult(RESULT_OK,intent);

                finish();

                break;
        }
    }



}
