package com.innasoft.dingudoes.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.innasoft.dingudoes.Apis.RetrofitClient;
import com.innasoft.dingudoes.Singleton.AppController;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.ChangeMobilenumberResponse;
import com.innasoft.dingudoes.storage.PrefManager;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChangeMobileNumberActivity extends AppCompatActivity {


    AppController appController;
    @BindView(R.id.edt_mobile)
    TextInputEditText edtMobile;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    PrefManager prefManager;
    String token,user_id;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.changemobile_activity);
        ButterKnife.bind(this);
        appController = (AppController) getApplication();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Change Mobile Number");

        prefManager = new PrefManager(getApplicationContext());
        HashMap<String, String> profile = prefManager.getUserDetails();
        token = profile.get("Token");
        user_id=profile.get("userid");


        if (appController.isConnection()){

            btnSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    getData();
                }
            });
        }
        else {
            String message = "Sorry! Not connected to internet";
            int color = Color.RED;

            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }

    }

    private void getData() {

        String mobile=edtMobile.getText().toString().trim();
        if (mobile.equals("")){
            Toast.makeText(getApplicationContext(), "Enter Mobile Number...", Toast.LENGTH_SHORT).show();
        }
        else {

            ProgressDialog progressDialog=new ProgressDialog(ChangeMobileNumberActivity.this);
            progressDialog.setMessage("Loading....");
            progressDialog.show();

            Call<ChangeMobilenumberResponse> changeMobilenumberResponseCall= RetrofitClient.getInstance().getApi().ChangeMobilenumber(token,user_id,mobile);
            changeMobilenumberResponseCall.enqueue(new Callback<ChangeMobilenumberResponse>() {
                @Override
                public void onResponse(Call<ChangeMobilenumberResponse> call, Response<ChangeMobilenumberResponse> response) {
                    if (response.isSuccessful());
                    ChangeMobilenumberResponse changeMobilenumberResponse=response.body();
                    if (changeMobilenumberResponse.isSuccess()){
                        progressDialog.dismiss();
                        Intent intent=new Intent(ChangeMobileNumberActivity.this,ChangeMobilenumberOtpActivity.class);
                        intent.putExtra("user_id",user_id);
                        intent.putExtra("Mobile",mobile);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        //Toast.makeText(getApplicationContext(), changeMobilenumberResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        finish();
                    }
                    else {
                        progressDialog.dismiss();
                    }
                }

                @Override
                public void onFailure(Call<ChangeMobilenumberResponse> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });


        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);

    }

}
