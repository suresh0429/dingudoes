package com.innasoft.dingudoes.Activities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.innasoft.dingudoes.Apis.RetrofitClient;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.CreateReportTaskResponse;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.innasoft.dingudoes.Apis.RetrofitClient.CONTACT_US_URL;
import static com.innasoft.dingudoes.Apis.RetrofitClient.PRIVACY_POLICY_URL;
import static com.innasoft.dingudoes.Apis.RetrofitClient.TERMS_CONDITIONS_URL;

public class HelpActivity extends AppCompatActivity {
    RelativeLayout relative_terms, relative_privacy;
    @BindView(R.id.relative_support)
    RelativeLayout relativeSupport;
    @BindView(R.id.relative_insurence)
    RelativeLayout relativeInsurence;
    @BindView(R.id.relative_open)
    RelativeLayout relativeOpen;
    @BindView(R.id.relative_contactus)
    RelativeLayout relativeContactus;
    AlertDialog alertDialog;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.help_activity);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Help");

        relative_privacy = findViewById(R.id.relative_privacy);
        relative_terms = findViewById(R.id.relative_terms);

        relative_terms.setOnClickListener(view -> {
            Intent intent=new Intent(HelpActivity.this,ContactUsActivity.class);
            intent.putExtra("TYPE","termsAndConditions");
            intent.putExtra("Activityname","Terms & Conditions");
            startActivity(intent);
        });

        relative_privacy.setOnClickListener(view -> {
            Intent intent=new Intent(HelpActivity.this,ContactUsActivity.class);
            intent.putExtra("TYPE","privacyPolicy");
            intent.putExtra("Activityname","Privacy Policy");
            startActivity(intent);
        });
        relativeContactus.setOnClickListener(v -> {

//                Intent intent=new Intent(HelpActivity.this,ContactUsActivity.class);
//                intent.putExtra("TYPE","contactUs");
//                intent.putExtra("Activityname","Contact Us");
//                startActivity(intent);

            ViewGroup viewGroup = findViewById(android.R.id.content);

            View dialogView = LayoutInflater.from(HelpActivity.this).inflate(R.layout.alert_support, viewGroup, false);

            AlertDialog.Builder builder = new AlertDialog.Builder(HelpActivity.this);

            builder.setView(dialogView);

            LinearLayout linear_chat=dialogView.findViewById(R.id.linear_chat);
            LinearLayout linear_email=dialogView.findViewById(R.id.linear_email);

            Button buttonCancel=dialogView.findViewById(R.id.buttonCancel);

            linear_email.setOnClickListener(v1 -> {

                Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                        "mailto", "abc@gmail.com", null));
                startActivity(Intent.createChooser(emailIntent, null));
                alertDialog.dismiss();



            });

            linear_chat.setOnClickListener(v1 -> {

            });

            buttonCancel.setOnClickListener(v12 ->
                    alertDialog.dismiss());



            alertDialog = builder.create();

            //  alertDialog.setCancelable(false);
            alertDialog.show();


        });

        relativeInsurence.setOnClickListener(v -> {

            Intent intent=new Intent(HelpActivity.this,ContactUsActivity.class);
            intent.putExtra("TYPE","insurance");
            intent.putExtra("Activityname","Insurance");
            startActivity(intent);



        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);

    }

}
