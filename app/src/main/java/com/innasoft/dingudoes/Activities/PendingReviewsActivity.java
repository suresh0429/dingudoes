package com.innasoft.dingudoes.Activities;

import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.dingudoes.Adapters.PendingReviewsAdapter;
import com.innasoft.dingudoes.Apis.RetrofitClient;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.PendingReviewsResponse;
import com.innasoft.dingudoes.Singleton.AppController;
import com.innasoft.dingudoes.storage.PrefManager;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PendingReviewsActivity extends AppCompatActivity {
    @BindView(R.id.recycler_pending)
    RecyclerView recyclerPending;
    AppController appController;
    PrefManager prefManager;
    String token;
    PendingReviewsAdapter pendingReviewsAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pendingreviews_activity);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Pending Reviews");

        prefManager = new PrefManager(getApplicationContext());
        HashMap<String, String> profile = prefManager.getUserDetails();
        token = profile.get("Token");

        appController=(AppController) getApplication();
        if (appController.isConnection()){

            getData(token);

        }
        else {
            String message = "Sorry! Not connected to internet";
            int color = Color.RED;

            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }

    private void getData(String token) {

        ProgressDialog progressDialog=new ProgressDialog(PendingReviewsActivity.this);
        progressDialog.setMessage("Loading....");
        progressDialog.show();

        Call<PendingReviewsResponse> call= RetrofitClient.getInstance().getApi().PendingReviews(token);
        call.enqueue(new Callback<PendingReviewsResponse>() {
            @Override
            public void onResponse(Call<PendingReviewsResponse> call, Response<PendingReviewsResponse> response) {
                if (response.isSuccessful());
                PendingReviewsResponse pendingReviewsResponse=response.body();
                if (pendingReviewsResponse.isSuccess()){
                    progressDialog.dismiss();
                    List<PendingReviewsResponse.DataBean> dataBeanList=pendingReviewsResponse.getData();
                    if (dataBeanList.isEmpty()){
                        Toast.makeText(PendingReviewsActivity.this, "No Data Found....!", Toast.LENGTH_LONG).show();
                    }
                    else {
                        pendingReviewsAdapter=new PendingReviewsAdapter(dataBeanList,PendingReviewsActivity.this);
                        final LinearLayoutManager layoutManager1 = new LinearLayoutManager(PendingReviewsActivity.this);
                        layoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
                        recyclerPending.setLayoutManager(layoutManager1);
                        recyclerPending.setNestedScrollingEnabled(false);
                        recyclerPending.setAdapter(pendingReviewsAdapter);

                    }

                }
                else {
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<PendingReviewsResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(PendingReviewsActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);

    }

}
