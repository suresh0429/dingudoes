package com.innasoft.dingudoes.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.innasoft.dingudoes.Adapters.TaskAlertsAdapter;
import com.innasoft.dingudoes.Apis.RetrofitClient;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.EnableTaskAlertResponse;
import com.innasoft.dingudoes.Response.GetTaskAlertResponse;
import com.innasoft.dingudoes.Singleton.AppController;
import com.innasoft.dingudoes.storage.PrefManager;

import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TaskAlertActivity extends AppCompatActivity {

    @BindView(R.id.turn_on)
    SwitchCompat turnOn;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.relative_addcustomalert)
    RelativeLayout relativeAddcustomalert;
    @BindView(R.id.recycler_taskalerts)
    RecyclerView recyclerTaskalerts;

    AppController appController;
    PrefManager prefManager;
    String token, content;
    TaskAlertsAdapter taskAlertsAdapter;
    ProgressDialog progressDialog;
    static Boolean isTouched = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.taskalert_activity);
        ButterKnife.bind(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Task Alerts");
        prefManager = new PrefManager(getApplicationContext());
        HashMap<String, String> profile = prefManager.getUserDetails();
        token = profile.get("Token");

        appController = (AppController) getApplication();

        if (appController.isConnection()) {
            getData();
        } else {
            String message = "Sorry! Not connected to internet";
            int color = Color.RED;

            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }


    }

    private void getData() {

        content = "application/json";

        SharedPreferences pref = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
        Boolean on=pref.getBoolean("Enable",true);
        turnOn.setChecked(on);
//

        progressDialog = new ProgressDialog(TaskAlertActivity.this);
        progressDialog.setMessage("Loading....");
        progressDialog.show();

        turnOn.setOnCheckedChangeListener((buttonView, isChecked) -> {

                if (isChecked) {
                    Call<EnableTaskAlertResponse> call=RetrofitClient.getInstance().getApi().EnableTaskAlert("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6InN1cmVzaEBpbm5hc29mdC5pbiIsInVzZXJfaWQiOjM0LCJpYXQiOjE1NzMwMjAxMDQsImV4cCI6MTU3MzEwNjUwNH0.7k3UHeekEVmTca7Kl8KuWE9e77LlXyMghwH7g10UtuA",content);
                    call.enqueue(new Callback<EnableTaskAlertResponse>() {
                        @Override
                        public void onResponse(Call<EnableTaskAlertResponse> call, Response<EnableTaskAlertResponse> response) {
                            if (response.isSuccessful());
                            EnableTaskAlertResponse enableTaskAlertResponse=response.body();
                            if (enableTaskAlertResponse.isSuccess()){
                                progressDialog.dismiss();
                                Toast.makeText(TaskAlertActivity.this, enableTaskAlertResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                SharedPreferences pref1 = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                                SharedPreferences.Editor editor = pref1.edit();
                                editor.putBoolean("Enable",true);
                                editor.apply();
                            }
                            else {
                                progressDialog.dismiss();
                            }
                        }

                        @Override
                        public void onFailure(Call<EnableTaskAlertResponse> call, Throwable t) {
                            progressDialog.dismiss();
                            Toast.makeText(TaskAlertActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                        }
                    });
                }
                else {

                    SharedPreferences pref1 = getApplicationContext().getSharedPreferences("MyPref", MODE_PRIVATE);
                    SharedPreferences.Editor editor = pref1.edit();
                    editor.putBoolean("Enable",false);
                    editor.apply();
                }

        });

        relativeAddcustomalert.setOnClickListener(view ->
                startActivity(new Intent(TaskAlertActivity.this, NewTaskAlertActivity.class)));


        Call<GetTaskAlertResponse> call = RetrofitClient.getInstance().getApi().GetTaskAlerts(token, content);
        call.enqueue(new Callback<GetTaskAlertResponse>() {
            @Override
            public void onResponse(Call<GetTaskAlertResponse> call, Response<GetTaskAlertResponse> response) {
                if (response.isSuccessful()) ;
                GetTaskAlertResponse getTaskAlertResponse = response.body();
                if (getTaskAlertResponse.isSuccess()) {
                    progressDialog.dismiss();
                    GetTaskAlertResponse.DataBean dataBean = getTaskAlertResponse.getData();
                    List<GetTaskAlertResponse.DataBean.TasksAlertsBean> tasksAlertsBeanList = dataBean.getTasksAlerts();

                    taskAlertsAdapter = new TaskAlertsAdapter(tasksAlertsBeanList, TaskAlertActivity.this);
                    final LinearLayoutManager layoutManager1 = new LinearLayoutManager(TaskAlertActivity.this);
                    layoutManager1.setOrientation(LinearLayoutManager.VERTICAL);
                    recyclerTaskalerts.setLayoutManager(layoutManager1);
                    recyclerTaskalerts.setNestedScrollingEnabled(false);
                    recyclerTaskalerts.setAdapter(taskAlertsAdapter);

                } else {
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<GetTaskAlertResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(TaskAlertActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);

    }

}
