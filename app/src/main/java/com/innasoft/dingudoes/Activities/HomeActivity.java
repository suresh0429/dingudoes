package com.innasoft.dingudoes.Activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.innasoft.dingudoes.Singleton.AppController;
import com.innasoft.dingudoes.Fragments.HomeFragment;
import com.innasoft.dingudoes.Fragments.NotificationsFragment;
import com.innasoft.dingudoes.Fragments.MyTaskFragment;
import com.innasoft.dingudoes.Fragments.OptionsFragment;
import com.innasoft.dingudoes.Fragments.SearchFragment;
import com.innasoft.dingudoes.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity {

    @BindView(R.id.continer)
    FrameLayout continer;
    @BindView(R.id.navigation)
    BottomNavigationView navigation;
    @BindView(R.id.container)
    RelativeLayout container;
    private Boolean exit = false;
    int Index;
    String location, minimum, maximum, distance, tobe;
    AppController appController;

    @SuppressLint("WrongConstant")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        appController = (AppController) getApplication();

        navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
      //  navigation.setLabelVisibilityMode(LabelVisibilityMode.LABEL_VISIBILITY_LABELED);

        /* getSupportActionBar().setTitle("Post a task");*/


        if (getIntent() != null) {
            Index = getIntent().getIntExtra("index", 0);

            location = getIntent().getStringExtra("Location");
            minimum = getIntent().getStringExtra("Minimum");
            maximum = getIntent().getStringExtra("Maximum");
            distance = getIntent().getStringExtra("Distance");
            tobe = getIntent().getStringExtra("Tobe");


        }

        if (appController.isConnection()) {

            HomeFragment homeFragment = new HomeFragment();
            FragmentManager manager1 = getSupportFragmentManager();
            manager1.beginTransaction().replace(R.id.continer, homeFragment, homeFragment.getTag()).commit();

        } else {
            String message = "Sorry! Not connected to internet";
            int color = Color.RED;

            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }



       /* if (Index == 2){

            getSupportActionBar().setTitle("Search");
            getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
            getSupportActionBar().setCustomView(R.layout.abs_layout);
            getSupportActionBar().show();

            Bundle bundle=new Bundle();
            bundle.putString("Location",location);
            bundle.putString("Minimum",minimum);
            bundle.putString("Maximum",maximum);
            bundle.putString("Distance",distance);
            bundle.putString("Tobe",tobe);
            SearchFragment notificationFragment = new SearchFragment();
            notificationFragment.setArguments(bundle);
            FragmentManager manager3 = getSupportFragmentManager();
            manager3.beginTransaction().replace(R.id.continer, notificationFragment, notificationFragment.getTag()).commit();
            navigation.setSelectedItemId(R.id.navigation_search);
        }
        else {*/

        // }


    }

    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @SuppressLint("WrongConstant")
        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.navigation_home:
                    HomeFragment homeFragment = new HomeFragment();
                    FragmentManager manager1 = getSupportFragmentManager();
                    manager1.beginTransaction().replace(R.id.continer, homeFragment, homeFragment.getTag()).commit();
                    return true;
                case R.id.navigation_tasks:
                    MyTaskFragment myTaskFragment1 = new MyTaskFragment();
                    FragmentManager manager2 = getSupportFragmentManager();
                    manager2.beginTransaction().replace(R.id.continer, myTaskFragment1, myTaskFragment1.getTag()).commit();
                    return true;
                case R.id.navigation_search:
                    Bundle bundle = new Bundle();
                    bundle.putString("Location", "hyderabad");
                    bundle.putString("Minimum", "10");
                    bundle.putString("Maximum", "100");
                    bundle.putString("Distance", "50");
                    bundle.putString("Tobe", "1");
                    SearchFragment notificationFragment = new SearchFragment();
                    notificationFragment.setArguments(bundle);
                    FragmentManager manager3 = getSupportFragmentManager();
                    manager3.beginTransaction().replace(R.id.continer, notificationFragment, notificationFragment.getTag()).commit();

                    return true;

                case R.id.navigation_notifications:

                    NotificationsFragment messageFragment = new NotificationsFragment();
                    FragmentManager fragmentManager = getSupportFragmentManager();
                    fragmentManager.beginTransaction().replace(R.id.continer, messageFragment, messageFragment.getTag()).commit();
                    return true;

                case R.id.navigation_options:

                    OptionsFragment optionsFragment = new OptionsFragment();
                    FragmentManager manager5 = getSupportFragmentManager();
                    manager5.beginTransaction().replace(R.id.continer, optionsFragment, optionsFragment.getTag()).commit();
                    return true;
            }
            return false;
        }
    };

    @Override
    public void onBackPressed() {
        // super.onBackPressed();
        if (exit) {
            Intent intent = new Intent(Intent.ACTION_MAIN);
            intent.addCategory(Intent.CATEGORY_HOME);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            moveTaskToBack(true);
        } else {
            Toast.makeText(getApplicationContext(), "Press Back again to Exit.", Toast.LENGTH_SHORT).show();
            exit = true;
        }
    }


}
