package com.innasoft.dingudoes.Activities;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SwitchCompat;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.innasoft.dingudoes.Apis.RetrofitClient;
import com.innasoft.dingudoes.Db.SqliteDatabase;
import com.innasoft.dingudoes.Models.TaskDraftModel;
import com.innasoft.dingudoes.Singleton.AppController;
import com.innasoft.dingudoes.Gallery;
import com.innasoft.dingudoes.Utilities.GalleryUriToPath;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.PosATaskResponse;
import com.innasoft.dingudoes.storage.PrefManager;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.Manifest.permission.ACCESS_COARSE_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static com.innasoft.dingudoes.Utilities.utility.getTime;

public class PostTaskActivity extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private static final int AUTOCOMPLETE_REQUEST_CODE = 200;
    Date dateObj ;
    Location mLocation;
    GoogleApiClient mGoogleApiClient;
    private static final int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;

    @BindView(R.id.relative_addmust)
    RelativeLayout relativeAddmust;
    @BindView(R.id.rec_car_images)
    RecyclerView recCarImages;
    @BindView(R.id.switch_btn)
    SwitchCompat switchBtn;
    @BindView(R.id.txt_date)
    TextView txtDate;
    @BindView(R.id.relative_date)
    RelativeLayout relativeDate;
    @BindView(R.id.checkbox)
    CheckBox checkbox;
    @BindView(R.id.txt_time)
    TextView txtTime;
    @BindView(R.id.relative_time)
    RelativeLayout relativeTime;
    @BindView(R.id.text)
    TextView text;
    @BindView(R.id.relative_total)
    RelativeLayout relativeTotal;
    @BindView(R.id.text1)
    TextView text1;
    @BindView(R.id.relative_hourly)
    RelativeLayout relativeHourly;
    @BindView(R.id.txt)
    TextView txt;
    @BindView(R.id.relative_budget)
    RelativeLayout relativeBudget;
    @BindView(R.id.edt_hours)
    EditText edtHours;
    @BindView(R.id.edt_hbudget)
    EditText edtHbudget;
    @BindView(R.id.linear)
    LinearLayout linear;
    @BindView(R.id.decreas)
    ImageView decreas;
    @BindView(R.id.show_value)
    TextView showValue;
    @BindView(R.id.increas)
    ImageView increas;
    @BindView(R.id.linear_cart)
    LinearLayout linearCart;
    @BindView(R.id.progressbar_add_increase)
    ProgressBar progressbarAddIncrease;
    @BindView(R.id.Relative_increase)
    RelativeLayout RelativeIncrease;
    @BindView(R.id.image)
    ImageView image;
    @BindView(R.id.attach)
    ImageView attach;
    @BindView(R.id.txt_count)
    TextView txtCount;
    @BindView(R.id.relative_image)
    RelativeLayout relativeImage;
    @BindView(R.id.btn_post)
    Button btnPost;
    @BindView(R.id.edt_title)
    EditText edtTitle;
    @BindView(R.id.edt_desc)
    EditText edtDesc;
    @BindView(R.id.edt_location)
    EditText edtLocation;
    @BindView(R.id.edt_budget)
    EditText edtBudget;

    DateFormat output = new SimpleDateFormat("yyyy/MM/dd", Locale.US);
    DateFormat inputFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
    private int mYear, mMonth, mDay, mHour, mMinute;

    private LocationRequest mLocationRequest;
    private long UPDATE_INTERVAL = 15000;  /* 15 secs */
    private long FASTEST_INTERVAL = 5000; /* 5 secs */

    private ArrayList permissionsToRequest;
    private ArrayList permissionsRejected = new ArrayList();
    private ArrayList permissions = new ArrayList();

    private final static int ALL_PERMISSIONS_RESULT = 101;

    Double latitude, longitude;

    private static final String TAG = "post";
    Drawable drawable, drawable1;


    MultipartBody.Part[] body;

    int CAMERA_CAPTURE = 1;
    int OPEN_MEDIA_PICKER = 2;
    int PICK_IMAGE = 3;
    int PICK_FILE_REQUEST = 7;
    String pickedDocPath = null;
    ArrayList<File> document_file = new ArrayList<>();

    public static List<String> selectedvideoImgList = new ArrayList<>();
    String multi_image_path = "empty";
    List<MultipartBody.Part> images_array = new ArrayList<>();
    public static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 123;
    String is_remote ="";
    String budget_type, must_haves="", datetxt, dateString;
    int count = 1;
    Calendar myCalendar;
    String category;
    PrefManager prefManager;
    String token;
    ArrayList<String> arraylist;
    ArrayList<TaskDraftModel> draftModelArrayList = new ArrayList<>();

    String budget;

    AppController appController;
    private SqliteDatabase mDatabase;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.posttask_activity);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Post a Task");

        // places sdk
        String apiKey = getString(R.string.googleapikey);

        /**
         * Initialize Places. For simplicity, the API key is hard-coded. In a production
         * environment we recommend using a secure mechanism to manage API keys.
         */
        if (!Places.isInitialized()) {
            Places.initialize(getApplicationContext(), apiKey);
        }

        PlacesClient placesClient = Places.createClient(this);

        appController = (AppController) getApplication();
        prefManager = new PrefManager(getApplicationContext());
        HashMap<String, String> profile = prefManager.getUserDetails();
        token = profile.get("Token");

        if (getIntent() != null) {
            String title = getIntent().getStringExtra("title");
            String description = getIntent().getStringExtra("description");
            category = getIntent().getStringExtra("category");
            is_remote = getIntent().getStringExtra("isRemote");
            String address = getIntent().getStringExtra("address");
            String budgetType = getIntent().getStringExtra("budgetType");
            String budget = getIntent().getStringExtra("budget");
            String taskHours = getIntent().getStringExtra("taskHours");
            String taskers = getIntent().getStringExtra("taskers");
            arraylist = getIntent().getStringArrayListExtra("ARRAYLIST");

            if (is_remote != null || budgetType != null || taskers != null) {
                edtTitle.setText(title);
                edtDesc.setText(description);
                edtLocation.setText(address);


                if (is_remote.equalsIgnoreCase("1")) {
                    switchBtn.setChecked(true);
                }

                if (budgetType.equalsIgnoreCase("Total")) {
                    edtBudget.setText(budget);
                } else {
                    edtHbudget.setText(budget);
                    edtHours.setText(taskHours);
                }

                showValue.setText(taskers);

            }

        }

        checkPermissionREAD_EXTERNAL_STORAGE(this);
        permissions.add(ACCESS_FINE_LOCATION);
        permissions.add(ACCESS_COARSE_LOCATION);

        permissionsToRequest = findUnAskedPermissions(permissions);
        //get the permissions we have asked for before but are not granted..
        //we will store this in a global list to access later.


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {


            if (permissionsToRequest.size() > 0)
                requestPermissions((String[]) permissionsToRequest.toArray(new String[permissionsToRequest.size()]), ALL_PERMISSIONS_RESULT);
        }


        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();


        relativeTime.setOnClickListener(v -> {
            // Get Current Time
            final Calendar c = Calendar.getInstance();
            mHour = c.get(Calendar.HOUR_OF_DAY);
            mMinute = c.get(Calendar.MINUTE);

            // Launch Time Picker Dialog
            TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                    (view, hourOfDay, minute) -> {
                        String time = getTime(hourOfDay, minute);
                        txtTime.setText(time);
                    }, mHour, mMinute, false);
            timePickerDialog.show();
        });


        relativeDate.setOnClickListener(v -> {

            // Get Current Date
            final Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog datePickerDialog = new DatePickerDialog(this,
                    (view, year, monthOfYear, dayOfMonth) -> txtDate.setText(dayOfMonth + "-" + (monthOfYear + 1) + "-" + year), mYear, mMonth, mDay);
            datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
            datePickerDialog.show();

        });


        is_remote = "0";
        switchBtn.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) {
                is_remote = "1";
            } else {
                is_remote = "0";
            }
        });

        relativeAddmust.setOnClickListener(view -> {
            Intent intent = new Intent(PostTaskActivity.this, AddMustHavesActivity.class);
            startActivityForResult(intent, 100);
            // startActivity(new Intent(PostTaskActivity.this, AddMustHavesActivity.class));
        });

        checkbox.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {

                // Get Current Time
                final Calendar c = Calendar.getInstance();
                mHour = c.get(Calendar.HOUR_OF_DAY);
                mMinute = c.get(Calendar.MINUTE);

                // Launch Time Picker Dialog
                TimePickerDialog timePickerDialog = new TimePickerDialog(PostTaskActivity.this,
                        (view, hourOfDay, minute) -> {
                            relativeTime.setVisibility(View.VISIBLE);
                            String time = getTime(hourOfDay, minute);
                            txtTime.setText(time);
                        }, mHour, mMinute, false);
                timePickerDialog.show();

            } else {
                relativeTime.setVisibility(View.GONE);
            }
        });

        increas.setOnClickListener(v -> {
            count++;
            showValue.setText("" + count);
        });

        decreas.setOnClickListener(v -> {

            if (count > 1) {
                count--;
                showValue.setText("" + count);
            }

        });

        budget_type = "Total";
        drawable = getResources().getDrawable(R.drawable.background);
        drawable1 = getResources().getDrawable(R.drawable.background);
        drawable.setColorFilter(Color.parseColor("#7957A5"), PorterDuff.Mode.SRC_ATOP);
        relativeTotal.setBackground(drawable);
        drawable1.setColorFilter(Color.parseColor("#9F9F9F"), PorterDuff.Mode.SRC_ATOP);
        relativeHourly.setBackground(drawable1);

        relativeHourly.setOnClickListener(view -> {
            budget_type = "Hourly";
            drawable1.setColorFilter(Color.parseColor("#7957A5"), PorterDuff.Mode.SRC_ATOP);
            relativeHourly.setBackground(drawable1);
            drawable.setColorFilter(Color.parseColor("#9F9F9F"), PorterDuff.Mode.SRC_ATOP);
            relativeTotal.setBackground(drawable);
            relativeBudget.setVisibility(View.GONE);
            linear.setVisibility(View.VISIBLE);

        });

        relativeTotal.setOnClickListener(view -> {
            budget_type = "Total";
            drawable1.setColorFilter(Color.parseColor("#9F9F9F"), PorterDuff.Mode.SRC_ATOP);
            relativeHourly.setBackground(drawable1);
            drawable.setColorFilter(Color.parseColor("#7957A5"), PorterDuff.Mode.SRC_ATOP);
            relativeTotal.setBackground(drawable);
            linear.setVisibility(View.GONE);
            relativeBudget.setVisibility(View.VISIBLE);

        });

//        edtLocation.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                onSearchCalled();
//            }
//        });

        attach.setOnClickListener(view -> {
            Intent intent = new Intent(PostTaskActivity.this, Gallery.class);
            // Set the title
            intent.putExtra("title", "Select media");
            // Mode 1 for both images and videos selection, 2 for images only and 3 for videos!
            intent.putExtra("mode", 2);
            //intent.putExtra("maxSelection", 3); // Optional
            startActivityForResult(intent, OPEN_MEDIA_PICKER);

           /* Intent intent = new Intent();
            intent.setType("image/*");
            intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
            intent.setAction(Intent.ACTION_GET_CONTENT);
            startActivityForResult(Intent.createChooser(intent,"Select Picture"), 30000);*/
        });



        btnPost.setOnClickListener(v -> {

            if (appController.isConnection()) {


                String title = edtTitle.getText().toString().trim();
                String desc = edtDesc.getText().toString().trim();
                String location = edtLocation.getText().toString().trim();

                datetxt = txtDate.getText().toString().trim();

                String showvalue = showValue.getText().toString().trim();

                String hours = edtHours.getText().toString().trim();
                String time = txtTime.getText().toString().trim();

                if (budget_type.equalsIgnoreCase("Total")) {
                    budget = edtBudget.getText().toString().trim();
                } else {
                    budget = edtHbudget.getText().toString().trim();
                }


                if (title.equals("")) {
                    Toast.makeText(PostTaskActivity.this, "Enter Title..", Toast.LENGTH_SHORT).show();
                } else if (desc.equals("")) {
                    Toast.makeText(PostTaskActivity.this, "Enter Description..", Toast.LENGTH_SHORT).show();
                } else if (location.equals("")) {
                    Toast.makeText(PostTaskActivity.this, "Enter Location..", Toast.LENGTH_SHORT).show();
                } else if (budget.equalsIgnoreCase("")) {

                    Toast.makeText(PostTaskActivity.this, "Enter Budget..", Toast.LENGTH_SHORT).show();
                } else if (datetxt.equals("")) {
                    Toast.makeText(PostTaskActivity.this, "Enter Date..", Toast.LENGTH_SHORT).show();

                } /*else if (must_haves == null) {
                   // Toast.makeText(PostTaskActivity.this, "Select Must haves", Toast.LENGTH_SHORT).show();

                } */
                else {

                    getData(title, desc, location, budget, showvalue, category, dateFormat(datetxt), budget_type, is_remote, token, must_haves, hours, time);
                }

            } else {
                String message = "Sorry! Not connected to internet";
                int color = Color.RED;

                Toast.makeText(PostTaskActivity.this, message, Toast.LENGTH_SHORT).show();
            }
        });

    }


    public void onSearchCalled() {
        // Set the fields to specify which types of place data to return.
        List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);
        // Start the autocomplete intent.
        Intent intent = new Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields).setCountry("IN") //INDIA
                .build(this);
        startActivityForResult(intent, AUTOCOMPLETE_REQUEST_CODE);
    }
    private void draftsData() {
        mDatabase = new SqliteDatabase(this);

        String title = edtTitle.getText().toString().trim();
        String desc = edtDesc.getText().toString().trim();
        String location = edtLocation.getText().toString().trim();
        datetxt = txtDate.getText().toString().trim();


        String showvalue = showValue.getText().toString().trim();

        String hours = edtHours.getText().toString().trim();
        String time = txtTime.getText().toString().trim();

        if (budget_type.equalsIgnoreCase("Total")) {
            budget = edtBudget.getText().toString().trim();

        } else {
            budget = edtHbudget.getText().toString().trim();

        }
        TaskDraftModel newContact = new TaskDraftModel(title, desc, location,  dateFormat(datetxt), must_haves, hours, time,budget,category,is_remote,budget_type,showvalue, latitude, longitude);
        mDatabase.addContacts(newContact);

    }


    private void getData(String title, String desc, String location, String budget, String show_value, String category, String date, String budget_type, String is_remote, String token, String must_haves, String hours, String time) {

        RequestBody Title = RequestBody.create(MediaType.parse("text/plain"), title);
        RequestBody Desc = RequestBody.create(MediaType.parse("text/plain"), desc);
        RequestBody Location = RequestBody.create(MediaType.parse("text/plain"), location);
        RequestBody Budget = RequestBody.create(MediaType.parse("text/plain"), budget);
        RequestBody Show_value = RequestBody.create(MediaType.parse("text/plain"), show_value);
        RequestBody Category = RequestBody.create(MediaType.parse("text/plain"), category);
        RequestBody Date = RequestBody.create(MediaType.parse("text/plain"), date);
        RequestBody BudgetType = RequestBody.create(MediaType.parse("text/plain"), budget_type);
        RequestBody ISRemote = RequestBody.create(MediaType.parse("text/plain"), is_remote);
        RequestBody list = RequestBody.create(MediaType.parse("text/plain"), must_haves);
        RequestBody Hours = RequestBody.create(MediaType.parse("text/plain"), hours);
        RequestBody Time = RequestBody.create(MediaType.parse("text/plain"), time);
        RequestBody latitude1 = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(latitude));
        RequestBody longitude1 = RequestBody.create(MediaType.parse("text/plain"), String.valueOf(longitude));


        ProgressDialog progressDialog = new ProgressDialog(PostTaskActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.show();

        // Call<PosATaskResponse> call= RetrofitClient.getInstance().getApi().PostTask(token,requestBody);
        Call<PosATaskResponse> call = RetrofitClient.getInstance().getApi().PostTask(token, images_array, Title, Desc, Category, ISRemote, Location, Date, BudgetType, Budget, Show_value, list, Hours, Time, latitude1, longitude1);
        call.enqueue(new Callback<PosATaskResponse>() {
            @Override
            public void onResponse(Call<PosATaskResponse> call, Response<PosATaskResponse> response) {
                if (response.isSuccessful()) ;
                PosATaskResponse posATaskResponse = response.body();
               // Log.d(TAG, "onResponse: " + posATaskResponse.getMessage() + "____" + posATaskResponse.getData().getBudget());
                if (posATaskResponse.isSuccess()) {
                    progressDialog.dismiss();
                    Toast.makeText(PostTaskActivity.this, posATaskResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(PostTaskActivity.this, CongratulationsActivity.class);
                    startActivity(intent);
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(PostTaskActivity.this, posATaskResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<PosATaskResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(PostTaskActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                //configureCameraIdle();
                Place place = Autocomplete.getPlaceFromIntent(data);
                Log.i(TAG, "Place: " + place.getName() + ", " + place.getId() + ", " + place.getAddress());
                //  Toast.makeText(EditAddressActivity.this, "ID: " + place.getId() + "address:" + place.getAddress() + "Name:" + place.getName() + " latlong: " + place.getLatLng(), Toast.LENGTH_LONG).show();
                // String address = place.getAddress();
                String stringlat = place.getLatLng().toString();
                CharSequence name = place.getName();
                final CharSequence address = place.getAddress();

                if (name.toString().contains("°")) {
                    name = "";
                }
                stringlat = stringlat.substring(stringlat.indexOf("(") + 1);
                stringlat = stringlat.substring(0, stringlat.indexOf(")"));
                String latValue = stringlat.split(",")[0];
                latitude = Double.valueOf(latValue);
                String lngValue = stringlat.split(",")[1];
                longitude = Double.valueOf(lngValue);

                Double lat = Double.valueOf(latitude);
                Double lng = Double.valueOf(longitude);
                String fulladd = address.toString();
                String addr[] = fulladd.split(",", 2);

                try {
                  Geocoder  geocoder = new Geocoder(PostTaskActivity.this);
                   List<Address> addresses = geocoder.getFromLocation(lat, lng, 1);

                    String area = addresses.get(0).getSubLocality();
                   // area_edt.setText(area);


                    if (addr[1] != null && area != null && addr[1].contains(area)) {
                        addr[1] = addr[1].replace(area, " ");
                    }
                    edtLocation.setText(area);


                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        }
        if (requestCode == OPEN_MEDIA_PICKER && resultCode == RESULT_OK && data != null) {

            ArrayList<String> selectionResult = data.getStringArrayListExtra("result");
            if (selectionResult.size() > 0) {
                for (int index = 0; index < selectionResult.size(); index++) {

                    selectedvideoImgList.add(selectionResult.get(index));

                    multi_image_path = selectionResult.get(index);
                    String PickedImgPath = selectionResult.get(index);
                    System.out.println("path" + PickedImgPath);

                    File file = new File(selectionResult.get(index));
                    RequestBody surveyBody = RequestBody.create(MediaType.parse("multipart/form-data"), file);
                    // surveyImagesParts[index] = MultipartBody.Part.createFormData("attachments", file.getName(), surveyBody);
                    images_array.add(MultipartBody.Part.createFormData("attachments", file.getName(), surveyBody));
                }


                relativeImage.setVisibility(View.VISIBLE);
                txtCount.setText(String.valueOf(selectedvideoImgList.size()));
            }
        } else if (requestCode == CAMERA_CAPTURE) {
            if (resultCode == RESULT_OK) {
                onCaptureImageResult(data);
            }
        }
        if (requestCode == PICK_IMAGE) {
            if (resultCode == RESULT_OK) {

                Uri picUri = data.getData();
                String[] filePath = {MediaStore.Images.Media.DATA};
                Cursor c = getApplicationContext().getContentResolver().query(picUri, filePath, null, null, null);
                c.moveToFirst();
                int columnIndex = c.getColumnIndex(filePath[0]);

                //  PickedImgPath = GalleryUriToPath.getPath(getApplicationContext(), picUri);

                try {
                    Bitmap bm = BitmapFactory.decodeStream(getApplicationContext().getContentResolver().openInputStream(picUri));
//                    pick_img_part.setImageBitmap(bm);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }

                c.close();
            }
        } else if (requestCode == PICK_FILE_REQUEST && resultCode == RESULT_OK) {
            Uri filePath = data.getData();
//                pickedDocPath = String.valueOf(filePath);
            pickedDocPath = GalleryUriToPath.getPath(this, filePath);
            document_file.add(new File(pickedDocPath));
            Log.e("path ", pickedDocPath);
            //   Toast.makeText(this, "document has  selected", Toast.LENGTH_SHORT).show();
            String filename = pickedDocPath.substring(pickedDocPath.lastIndexOf("/") + 1);
            //  text_doc_name.setText(filename);
            // text_doc_name.setText("document has  selected");

        }

        if (requestCode == 100 && data != null) {
            ArrayList<String> arrayList = data.getStringArrayListExtra("ARRAYLIST");
//            String[] array = Objects.requireNonNull(data).getStringArrayExtra("ARRAYLIST");
            must_haves = arrayList.toString();
            Log.d(TAG, "onActivityResult: " + must_haves);
        }

    }


    private void onCaptureImageResult(Intent data) {
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File destination = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");
        FileOutputStream fo;
        try {
            destination.createNewFile();
            fo = new FileOutputStream(destination);
            fo.write(bytes.toByteArray());
            fo.close();

            Log.e("Camera Path", destination.getAbsolutePath());
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public boolean checkPermissionREAD_EXTERNAL_STORAGE(
            final Context context) {
        int currentAPIVersion = Build.VERSION.SDK_INT;
        if (currentAPIVersion >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(context,
                    Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        (Activity) context,
                        Manifest.permission.READ_EXTERNAL_STORAGE)) {
                    showDialog("External storage", context, Manifest.permission.READ_EXTERNAL_STORAGE);

                } else {
                    ActivityCompat
                            .requestPermissions(
                                    (Activity) context,
                                    new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                    MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                }
                return false;
            } else {
                return true;
            }

        } else {
            return true;
        }
    }

    private void showDialog(String external_storage, Context context, String readExternalStorage) {

        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
        alertBuilder.setCancelable(true);
        alertBuilder.setTitle("Permission necessary");
        alertBuilder.setMessage(external_storage + " permission is necessary");
        alertBuilder.setPositiveButton(android.R.string.yes,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ActivityCompat.requestPermissions((Activity) context,
                                new String[]{readExternalStorage},
                                MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE);
                    }
                });
        AlertDialog alert = alertBuilder.create();
        alert.show();
    }

    private ArrayList findUnAskedPermissions(ArrayList wanted) {
        ArrayList result = new ArrayList();

        for (Object perm : wanted) {
            if (!hasPermission(String.valueOf(perm))) {
                result.add(perm);
            }
        }

        return result;
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (!checkPlayServices()) {
            Toast.makeText(PostTaskActivity.this, "Please install Google Play services.", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {


        if (ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);


        if (mLocation != null) {
            // edtLocation.setText();

            latitude = mLocation.getLatitude();
            longitude = mLocation.getLongitude();
            Log.d(TAG, "onConnected: " + "Latitude : " + mLocation.getLatitude() + " , Longitude : " + mLocation.getLongitude());
            // latLng.setText("Latitude : "+mLocation.getLatitude()+" , Longitude : "+mLocation.getLongitude());

            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());

            try {
                addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                String address = addresses.get(0).getSubLocality(); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String country = addresses.get(0).getCountryName();
                String postalCode = addresses.get(0).getPostalCode();
                String knownName = addresses.get(0).getFeatureName();
                Log.d(TAG, "onConnected: " + address);
                edtLocation.setText(address);
            } catch (IOException e) {
                e.printStackTrace();
            }

            // Only if available else return NULL

        }

        startLocationUpdates();

    }

    @Override
    public void onConnectionSuspended(int i) {

    }


    @Override
    public void onLocationChanged(Location location) {

        if (location != null)
            // edtLocation.setText();

            latitude = mLocation.getLatitude();
        longitude = mLocation.getLongitude();
        Log.d(TAG, "onConnected: " + "Latitude : " + mLocation.getLatitude() + " , Longitude : " + mLocation.getLongitude());

        //  latLng.setText("Latitude : "+location.getLatitude()+" , Longitude : "+location.getLongitude());
        Geocoder geocoder;
        List<Address> addresses;
        geocoder = new Geocoder(this, Locale.getDefault());

        try {
            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = addresses.get(0).getSubLocality(); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();
            Log.d(TAG, "onConnected: " + addresses);
            edtLocation.setText(address);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }


    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (apiAvailability.isUserResolvableError(resultCode)) {
                apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST)
                        .show();
            } else
                finish();

            return false;
        }
        return true;
    }

    protected void startLocationUpdates() {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        mLocationRequest.setInterval(UPDATE_INTERVAL);
        mLocationRequest.setFastestInterval(FASTEST_INTERVAL);
        if (ActivityCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getApplicationContext(), "Enable Permissions", Toast.LENGTH_LONG).show();
        }

        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, (LocationListener) this);


    }

    private boolean hasPermission(Object permission) {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission((String) permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    private boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }


    @TargetApi(Build.VERSION_CODES.M)
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        switch (requestCode) {

            case ALL_PERMISSIONS_RESULT:
                for (Object perms : permissionsToRequest) {
                    if (!hasPermission(perms)) {
                        permissionsRejected.add(perms);
                    }
                }

                if (permissionsRejected.size() > 0) {


                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        if (shouldShowRequestPermissionRationale((String) permissionsRejected.get(0))) {
                            showMessageOKCancel("These permissions are mandatory for the application. Please allow access.",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                requestPermissions((String[]) permissionsRejected.toArray(new String[permissionsRejected.size()]), ALL_PERMISSIONS_RESULT);
                                            }
                                        }
                                    });
                            return;
                        }
                    }

                }

                break;
        }

    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(PostTaskActivity.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopLocationUpdates();
    }


    public void stopLocationUpdates() {
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi
                    .removeLocationUpdates(mGoogleApiClient, (LocationListener) this);
            mGoogleApiClient.disconnect();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }


    private String dateFormat(String convertdate){
        String input_date =convertdate;
        SimpleDateFormat format = new SimpleDateFormat("dd-MM-yyyy");
        Date date = null;
        try {
            date = format.parse(input_date );
            System.out.println(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        format = new SimpleDateFormat("yyyy/MM/dd");

       // System.out.println("Date :" +format.format(date));

        return input_date;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                draftsData();
                break;
        }
        return super.onOptionsItemSelected(item);

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        draftsData();
    }
}
