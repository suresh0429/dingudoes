package com.innasoft.dingudoes.Activities;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.innasoft.dingudoes.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CongratulationsActivity extends AppCompatActivity {
    @BindView(R.id.btn_done)
    Button btnDone;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.congratulations_activity);
        ButterKnife.bind(this);

        btnDone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(CongratulationsActivity.this,HomeActivity.class);
                startActivity(intent);
            }
        });

    }
}
