package com.innasoft.dingudoes.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.appbar.CollapsingToolbarLayout;
import com.innasoft.dingudoes.Adapters.UserReviewAdapter;
import com.innasoft.dingudoes.Apis.RetrofitClient;
import com.innasoft.dingudoes.Models.UserReviewModel;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.DashboardResponse;
import com.innasoft.dingudoes.Response.GetProfileResponse;
import com.innasoft.dingudoes.Response.GetUserReviewResponse;
import com.innasoft.dingudoes.Singleton.AppController;
import com.innasoft.dingudoes.storage.PrefManager;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.StringJoiner;

import butterknife.BindView;
import butterknife.ButterKnife;
import co.ceryle.segmentedbutton.SegmentedButtonGroup;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.facebook.FacebookSdk.getApplicationContext;
import static com.innasoft.dingudoes.Utilities.utility.capitalize;

public class ProfileActivity extends AppCompatActivity {
    @BindView(R.id.image)
    CircleImageView image;
    @BindView(R.id.txt_name)
    TextView txtName;
    @BindView(R.id.txt_l_online)
    TextView txtLOnline;
    @BindView(R.id.btn_quote)
    Button btnQuote;
    @BindView(R.id.txt_about)
    TextView txtAbout;
    @BindView(R.id.txt_skills)
    TextView txtSkills;
    @BindView(R.id.txt_languages)
    TextView txtLanguages;
    @BindView(R.id.txt_transportation)
    TextView txtTransportation;
    @BindView(R.id.txt_address)
    TextView txtAddress;
    @BindView(R.id.segmentedButtonGroup)
    SegmentedButtonGroup segmentedButtonGroup;
    @BindView(R.id.txt_taskeropen)
    TextView txtTaskeropen;
    @BindView(R.id.txt_taskerassigned)
    TextView txtTaskerassigned;
    @BindView(R.id.txt_taskercompleted)
    TextView txtTaskercompleted;
    @BindView(R.id.ratingbar)
    RatingBar ratingbar;
    @BindView(R.id.txt_review)
    TextView txtReview;
    @BindView(R.id.txtBenifecryId)
    TextView txtBenifecryId;
    @BindView(R.id.verifiedIdDate)
    TextView verifiedIdDate;
    @BindView(R.id.txtMobile)
    TextView txtMobile;
    @BindView(R.id.img_facebook)
    ImageView imgFacebook;
    @BindView(R.id.img_f_click)
    ImageView imgFClick;
    @BindView(R.id.img_gmail)
    ImageView imgGmail;
    @BindView(R.id.img_g_click)
    ImageView imgGClick;
    @BindView(R.id.img_linkedin)
    ImageView imgLinkedin;
    @BindView(R.id.progress_bar)
    ProgressBar progressBar;
    @BindView(R.id.txtPercentage)
    TextView txtPercentage;
    private Menu menu;
    PrefManager prefManager;
    String name, email, userId;
    boolean fromTask;
    AppController appController;
    GetProfileResponse.DataBean dataBean;
    GetProfileResponse.DataBean.UserDetailBean userDetailBean;
    String joined_Skills;
    String token, content;
    ProgressDialog progressDialog;
    UserReviewAdapter adapter;
    List<UserReviewModel> userReviewModels;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile_scrolling);
        ButterKnife.bind(this);
        prefManager = new PrefManager(getApplicationContext());
        appController = (AppController) getApplication();

        final Toolbar mToolbar = (Toolbar) findViewById(R.id.toolbar);

        mToolbar.setTitle(" ");
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        CollapsingToolbarLayout collapsingToolbarLayout = findViewById(R.id.toolbar_layout);
        collapsingToolbarLayout.setTitleEnabled(true);

        //collapsingToolbarLayout.setTitle(" ");

        collapsingToolbarLayout.setTitle("");
        collapsingToolbarLayout.setExpandedTitleColor(getResources().getColor(R.color.black)); // transperent color = #00000000 collapsingToolbarLayout.setCollapsedTitleTextColor(Color.rgb(0, 0, 0)); //Color of your title

        AppBarLayout mAppBarLayout = (AppBarLayout) findViewById(R.id.app_bar);
        mAppBarLayout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            boolean isShow = false;
            int scrollRange = -1;

            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (scrollRange == -1) {
                    scrollRange = appBarLayout.getTotalScrollRange();
                }
                if (scrollRange + verticalOffset == 0) {
                    collapsingToolbarLayout.setTitle(name);
                    isShow = true;
                } else if (isShow) {
                    collapsingToolbarLayout.setTitle(" ");//careful there should a space between double quote otherwise it wont work isShow = false;
                }
            }
        });

        if (getIntent() != null){
            userId = getIntent().getStringExtra("user_id");
            token = getIntent().getStringExtra("token");
            email = getIntent().getStringExtra("email");
            fromTask = getIntent().getBooleanExtra("fromTask",false);
        }



        if (appController.isConnection()) {
            getData();
        } else {

        }

    }

    private void getData() {
        content = "application/json";

        progressDialog = new ProgressDialog(ProfileActivity.this);
        progressDialog.setMessage("Loading....");
        progressDialog.show();
        TaskerData();
        TaskerReviewData();

        if (fromTask){
            btnQuote.setVisibility(View.VISIBLE);
        }else {
            btnQuote.setVisibility(View.GONE);
        }

        segmentedButtonGroup.setOnClickedButtonListener(position -> {
            if (position == 0) {

                TaskerData();
                TaskerReviewData();
            } else if (position == 1) {

                PosterData();
                PosterReviewData();
            }
        });

        Call<GetProfileResponse> call = RetrofitClient.getInstance().getApi().Profile(userId);
        call.enqueue(new Callback<GetProfileResponse>() {
            @RequiresApi(api = Build.VERSION_CODES.O)
            @Override
            public void onResponse(Call<GetProfileResponse> call, Response<GetProfileResponse> response) {
                if (response.isSuccessful()) ;
                GetProfileResponse getProfileResponse = response.body();

                if (getProfileResponse.isSuccess()) {

                    progressDialog.dismiss();

                    dataBean = getProfileResponse.getData();
                    userDetailBean = dataBean.getUserDetail();

                    name = dataBean.getFullName();

                    txtName.setText(name);
                    //txtEmail.setText(dataBean.getEmail());
                    txtAddress.setText(userDetailBean.getBillingAddress());
                    txtAbout.setText(userDetailBean.getAbout());

                    if (userDetailBean.getLicense() != null) {
                        txtBenifecryId.setText(userDetailBean.getLicense());
                    }
                    if (dataBean.getUpdatedAt() != null) {
                        verifiedIdDate.setText(dataBean.getUpdatedAt());
                    }
                    if (dataBean.getMobile() != null) {
                        txtMobile.setText(dataBean.getMobile());
                    }

                    if (getProfileResponse.getProfileScore()==0){
                        progressBar.setProgress(0);
                        txtPercentage.setText("0%");
                    }else if (getProfileResponse.getProfileScore()==1){
                        progressBar.setProgress(20);
                        txtPercentage.setText("20%");
                    }else if (getProfileResponse.getProfileScore()==2){
                        progressBar.setProgress(40);
                        txtPercentage.setText("40%");
                    }else if (getProfileResponse.getProfileScore()==3){
                        progressBar.setProgress(60);
                        txtPercentage.setText("60%");
                    }else if (getProfileResponse.getProfileScore()==4){
                        progressBar.setProgress(80);
                        txtPercentage.setText("80%");
                    }else if (getProfileResponse.getProfileScore()==5){
                        progressBar.setProgress(100);
                        txtPercentage.setText("100%");
                    }


                    Picasso.get().load(RetrofitClient.BASE_URL + dataBean.getProfileImage()).error(R.mipmap.ic_launcher_round).into(image);

                    String skill = userDetailBean.getSkills();
                    txtSkills.setText(skill);
//                    if (skill != null) {
//                        String[] temp = skill.replaceAll("['\\[\\]']", "").split(",");
//                        StringJoiner sj = new StringJoiner(",");
//                        StringBuilder builder = new StringBuilder();
//                        for (String details : temp) {
//                            builder.append("* " + details + "\n");
//                            sj.add(details);
//                        }
//                        joined_Skills = sj.toString();
//                        Log.d("Joined", "onResponse: " + joined_Skills);
//                        txtSkills.setText(capitalize(builder.toString().trim()));
//                    }

                    String trans = userDetailBean.getTransport();
                    if (trans != null) {
                        String[] value = trans.replaceAll("[\\[\\]]", "").split(",");
                        StringBuilder builder1 = new StringBuilder();
                        for (String details : value) {
                            builder1.append("* " + details + "\n");
                        }
                        txtTransportation.setText(capitalize(builder1.toString().trim()));
                    }

                    String lang = userDetailBean.getLanguages();
                    if (lang != null) {
                        String[] vv = lang.replaceAll("[\\[\\]]", "").split(",");
                        StringBuilder builder2 = new StringBuilder();
                        for (String details : vv) {
                            builder2.append("* " + details + "\n");
                        }

                        txtLanguages.setText(capitalize(builder2.toString().trim()));
                    }
                    String mobile = String.valueOf(dataBean.getIsVerified());
                    prefManager.profile(mobile, userDetailBean.getDateOfBirth(), userDetailBean.getBillingAddress());

                } else {
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<GetProfileResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(ProfileActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


    }

    private void TaskerData() {

        progressDialog.show();
        Call<DashboardResponse> call = RetrofitClient.getInstance().getApi().DashBoard(token, content, userId);
        call.enqueue(new Callback<DashboardResponse>() {
            @Override
            public void onResponse(Call<DashboardResponse> call, Response<DashboardResponse> response) {
                if (response.isSuccessful()) ;
                DashboardResponse dashboardResponse = response.body();
                if (dashboardResponse.isSuccess()) {

                    progressDialog.dismiss();

                    DashboardResponse.DataBean dataBean = dashboardResponse.getData();

                    List<DashboardResponse.DataBean.AsTaskerBean> asTaskerBeanList = dataBean.getAsTasker();

                    txtTaskeropen.setText(asTaskerBeanList.get(0).getTaskerOpen());
                    txtTaskerassigned.setText(asTaskerBeanList.get(0).getTaskerAssigned());
                    txtTaskercompleted.setText(asTaskerBeanList.get(0).getTaskerCompleted());

                } else {
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<DashboardResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(ProfileActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void PosterData() {

        progressDialog.show();

        Call<DashboardResponse> call = RetrofitClient.getInstance().getApi().DashBoard(token, content, userId);
        call.enqueue(new Callback<DashboardResponse>() {
            @Override
            public void onResponse(Call<DashboardResponse> call, Response<DashboardResponse> response) {
                if (response.isSuccessful()) ;
                DashboardResponse dashboardResponse = response.body();
                if (dashboardResponse.isSuccess()) {
                    progressDialog.dismiss();

                    DashboardResponse.DataBean dataBean = dashboardResponse.getData();

                    List<DashboardResponse.DataBean.AsPosterBean> asPosterBeanList = dataBean.getAsPoster();

                    for (int i = 0; i < asPosterBeanList.size(); i++) {

                        txtTaskeropen.setText(asPosterBeanList.get(i).getPosterOpen());
                        txtTaskerassigned.setText(asPosterBeanList.get(i).getPosterAssigned());
                        txtTaskercompleted.setText(asPosterBeanList.get(i).getPosterCompleted());
                    }

                } else {
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<DashboardResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(ProfileActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


    }

    private void TaskerReviewData() {
        userReviewModels = new ArrayList<UserReviewModel>();
        progressDialog.show();
        Call<GetUserReviewResponse> call = RetrofitClient.getInstance().getApi().GetUserReviews(token);
        call.enqueue(new Callback<GetUserReviewResponse>() {
            @Override
            public void onResponse(Call<GetUserReviewResponse> call, Response<GetUserReviewResponse> response) {
                if (response.isSuccessful()) ;
                GetUserReviewResponse dashboardResponse = response.body();
                if (dashboardResponse.isSuccess()) {

                    progressDialog.dismiss();

                    List<GetUserReviewResponse.DataBean.TaskerBean> asTaskerBeanList = dashboardResponse.getData().getTasker();
//                    Log.d(TAG, "onResponse: " + asTaskerBeanList.get(0).getRating());
                    userReviewModels.clear();
                    for (GetUserReviewResponse.DataBean.TaskerBean taskerBean : asTaskerBeanList) {

                        userReviewModels.add(new UserReviewModel(taskerBean.getId(), taskerBean.getUserId(), taskerBean.getTaskId(), taskerBean.getReviewText(), taskerBean.getRating()));
                    }

                    txtReview.setText("" + asTaskerBeanList.size() + " Reviews");
                    ratingbar.setRating((float) calculateAverage(userReviewModels));

//                    adapter = new UserReviewAdapter(userReviewModels, ProfileActivity.this);
//                    final LinearLayoutManager layoutManager = new LinearLayoutManager(ProfileActivity.this);
//                    layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
//                    recyclerUserreviews.setLayoutManager(layoutManager);
//                    recyclerUserreviews.setNestedScrollingEnabled(false);
//                    recyclerUserreviews.setAdapter(adapter);
//
//                    adapter.notifyDataSetChanged();

//

                } else {
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<GetUserReviewResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(ProfileActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });

    }

    private void PosterReviewData() {
        userReviewModels = new ArrayList<UserReviewModel>();
        progressDialog.show();

        Call<GetUserReviewResponse> call = RetrofitClient.getInstance().getApi().GetUserReviews(token);
        call.enqueue(new Callback<GetUserReviewResponse>() {
            @Override
            public void onResponse(Call<GetUserReviewResponse> call, Response<GetUserReviewResponse> response) {
                if (response.isSuccessful()) ;
                GetUserReviewResponse dashboardResponse = response.body();
                if (dashboardResponse.isSuccess()) {
                    progressDialog.dismiss();

                    List<GetUserReviewResponse.DataBean.PosterBean> asPosterBeanList = dashboardResponse.getData().getPoster();

                    userReviewModels.clear();
                    for (GetUserReviewResponse.DataBean.PosterBean posterBean : asPosterBeanList) {

                        userReviewModels.add(new UserReviewModel(posterBean.getId(), posterBean.getUserId(), posterBean.getTaskId(), posterBean.getReviewText(), posterBean.getRating()));
                    }
                    txtReview.setText("" + asPosterBeanList.size() + " Reviews");
                    ratingbar.setRating((float) calculateAverage(userReviewModels));

//                    adapter = new UserReviewAdapter(userReviewModels, ProfileActivity.this);
//                    final LinearLayoutManager layoutManager = new LinearLayoutManager(ProfileActivity.this);
//                    layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
//                    recyclerUserreviews.setLayoutManager(layoutManager);
//                    recyclerUserreviews.setNestedScrollingEnabled(false);
//                    recyclerUserreviews.setAdapter(adapter);
//
//                    adapter.notifyDataSetChanged();
                } else {
                    progressDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<GetUserReviewResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(ProfileActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


    }

    private double calculateAverage(List<UserReviewModel> marks) {
        Double sum = 0.00;
        if (!marks.isEmpty()) {
            for (UserReviewModel mark : marks) {
                sum += mark.getRating();
            }
            return sum.doubleValue() / marks.size();
        }
        return sum;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        this.menu = menu;
        getMenuInflater().inflate(R.menu.menu_share, menu);
        MenuItem itemEdit = menu.findItem(R.id.edit);
        if (fromTask){
            itemEdit.setVisible(false);
        }else {
            itemEdit.setVisible(true);
        }


        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
            case R.id.share:
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "DinguDoes -");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, "" + "");
                startActivity(Intent.createChooser(sharingIntent, "Share via"));

                return true;
            case R.id.edit:
                Intent intent = new Intent(ProfileActivity.this, EditProfileActivity.class);
                intent.putExtra("Fullname", dataBean.getFullName());
                intent.putExtra("Email", dataBean.getEmail());
                intent.putExtra("address", userDetailBean.getBillingAddress());
                intent.putExtra("About", userDetailBean.getAbout());
                intent.putExtra("Skills", joined_Skills);
                intent.putExtra("Transport", userDetailBean.getTransport());
                intent.putExtra("Language", userDetailBean.getLanguages());
                intent.putExtra("dob", userDetailBean.getDateOfBirth());
                startActivity(intent);
        }
        return super.onOptionsItemSelected(item);

    }


}
