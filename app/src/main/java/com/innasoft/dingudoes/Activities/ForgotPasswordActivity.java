package com.innasoft.dingudoes.Activities;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.innasoft.dingudoes.Apis.RetrofitClient;
import com.innasoft.dingudoes.Singleton.AppController;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.ForgotPasswordResponse;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ForgotPasswordActivity extends AppCompatActivity {

    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.Email)
    TextInputEditText Email;
    AppController appController;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.forgotpassword_activity);
        ButterKnife.bind(this);

        appController=(AppController) getApplication();

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Forgot Password");

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);

    }

    @OnClick(R.id.btn_submit)
    public void onViewClicked() {

        if (appController.isConnection()){

            String email=Email.getText().toString().trim();

            ProgressDialog progressDialog=new ProgressDialog(ForgotPasswordActivity.this);
            progressDialog.setMessage("Loading....");
            progressDialog.show();

            Call<ForgotPasswordResponse> call= RetrofitClient.getInstance().getApi().ForgotPassword(email);
            call.enqueue(new Callback<ForgotPasswordResponse>() {
                @Override
                public void onResponse(Call<ForgotPasswordResponse> call, Response<ForgotPasswordResponse> response) {
                    if (response.isSuccessful());
                    ForgotPasswordResponse forgotPasswordResponse=response.body();
                    if (forgotPasswordResponse.isSuccess()){
                        progressDialog.dismiss();
                        Toast.makeText(ForgotPasswordActivity.this, forgotPasswordResponse.getMessage(), Toast.LENGTH_SHORT).show();
                        Intent intent=new Intent(ForgotPasswordActivity.this,ForgotPasswordOtpActivity.class);
                        intent.putExtra("Email",email);
                        startActivity(intent);
                    }

                }

                @Override
                public void onFailure(Call<ForgotPasswordResponse> call, Throwable t) {
                    progressDialog.dismiss();
                    Toast.makeText(ForgotPasswordActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

                }
            });


        }
        else {
            String message = "Sorry! Not connected to internet";
            int color = Color.RED;

            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }

    }
}
