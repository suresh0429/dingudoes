package com.innasoft.dingudoes.Activities;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.textfield.TextInputEditText;
import com.innasoft.dingudoes.Apis.RetrofitClient;
import com.innasoft.dingudoes.Singleton.AppController;
import com.innasoft.dingudoes.R;
import com.innasoft.dingudoes.Response.AddBankAccountResponse;
import com.innasoft.dingudoes.Response.GeneratePayoutTokenResponse;
import com.innasoft.dingudoes.storage.PrefManager;

import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class
AddBankAccountActivity extends AppCompatActivity {
    AppController appController;
    PrefManager prefManager;
    String token, payout_token, content, Email, Address;
    @BindView(R.id.ifsc)
    TextInputEditText ifsc;
    @BindView(R.id.a_number)
    TextInputEditText aNumber;
    @BindView(R.id.c_anumber)
    TextInputEditText cAnumber;
    @BindView(R.id.A_name)
    TextInputEditText AName;
    @BindView(R.id.mobilenumber)
    TextInputEditText mobilenumber;
    @BindView(R.id.city)
    TextInputEditText city;
    @BindView(R.id.state)
    TextInputEditText state;
    @BindView(R.id.pincode)
    TextInputEditText pincode;
    @BindView(R.id.btn_submit)
    Button btnSubmit;
    @BindView(R.id.address)
    TextInputEditText address;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.add_bankaccount);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Add Bank Account");

        prefManager = new PrefManager(getApplicationContext());
        HashMap<String, String> profile = prefManager.getUserDetails();
        token = profile.get("Token");
        Email = profile.get("email");
        Address = profile.get("address");


        appController = (AppController) getApplication();
        if (appController.isConnection()) {

            AddBankAccount();

        } else {
            String message = "Sorry! Not connected to internet";
            int color = Color.RED;

            Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }

    }

    private void AddBankAccount() {

        content = "application/json";

        GenerateToken(token, content);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String if_code = ifsc.getText().toString().trim();
                String acc_no = aNumber.getText().toString().trim();
                String a_name = AName.getText().toString().trim();
                String mobile = mobilenumber.getText().toString().trim();
                String Address = address.getText().toString().trim();

                String City = city.getText().toString().trim();
                String State = state.getText().toString().trim();
                String Pincode = pincode.getText().toString().trim();


                if (if_code.equals("")) {
                    Toast.makeText(AddBankAccountActivity.this, "Enter IFSC code..", Toast.LENGTH_SHORT).show();
                } else if (acc_no.equals("")) {
                    Toast.makeText(AddBankAccountActivity.this, "Enter Account Number..", Toast.LENGTH_SHORT).show();

                } else if (a_name.equals("")) {
                    Toast.makeText(AddBankAccountActivity.this, "Enter Account Holder Name..", Toast.LENGTH_SHORT).show();

                } else if (mobile.equals("")) {
                    Toast.makeText(AddBankAccountActivity.this, "Enter Mobile Number..", Toast.LENGTH_SHORT).show();

                }else if (Address.equals("")){
                    Toast.makeText(AddBankAccountActivity.this, "Enter Address..", Toast.LENGTH_SHORT).show();

                }
                else if (City.equals("")) {
                    Toast.makeText(AddBankAccountActivity.this, "Enter City..", Toast.LENGTH_SHORT).show();

                } else if (State.equals("")) {
                    Toast.makeText(AddBankAccountActivity.this, "Enter State..", Toast.LENGTH_SHORT).show();

                } else if (Pincode.equals("")) {
                    Toast.makeText(AddBankAccountActivity.this, "Enter Pincode..", Toast.LENGTH_SHORT).show();

                } else {

                    AddBankAccountDetails(if_code, acc_no, a_name, mobile,Address, City, State, Pincode);
                }

            }
        });

    }

    // generate payout token
    private void GenerateToken(String token, String content) {

        Call<GeneratePayoutTokenResponse> call = RetrofitClient.getInstance().getApi().GenerateToken(token, content);
        call.enqueue(new Callback<GeneratePayoutTokenResponse>() {
            @Override
            public void onResponse(Call<GeneratePayoutTokenResponse> call, Response<GeneratePayoutTokenResponse> response) {
                if (response.isSuccessful()) ;
                GeneratePayoutTokenResponse generatePayoutTokenResponse = response.body();
                if (generatePayoutTokenResponse.isSuccess()) {

                    GeneratePayoutTokenResponse.DataBean dataBean = generatePayoutTokenResponse.getData();

                    payout_token = dataBean.getToken();
                }
            }

            @Override
            public void onFailure(Call<GeneratePayoutTokenResponse> call, Throwable t) {

            }
        });
    }


    private void AddBankAccountDetails(String if_code, String acc_no, String a_name, String mobile,String address, String city, String state, String pincode) {

        ProgressDialog progressDialog = new ProgressDialog(AddBankAccountActivity.this);
        progressDialog.setMessage("Loading....");
        progressDialog.show();

        Call<AddBankAccountResponse> call = RetrofitClient.getInstance().getApi().AddBankAccount(token,payout_token, a_name, Email,
                mobile, acc_no, if_code, address, city, state, pincode);
        call.enqueue(new Callback<AddBankAccountResponse>() {
            @Override
            public void onResponse(Call<AddBankAccountResponse> call, Response<AddBankAccountResponse> response) {
                if (response.isSuccessful()) ;
                AddBankAccountResponse addBankAccountResponse = response.body();
                if (addBankAccountResponse.isSuccess()) {
                    progressDialog.dismiss();
                    Toast.makeText(AddBankAccountActivity.this, addBankAccountResponse.getMessage(), Toast.LENGTH_SHORT).show();
                    Intent intent = getIntent();
                    setResult(Activity.RESULT_CANCELED, intent);
                    finish();
                } else {
                    progressDialog.dismiss();
                    Toast.makeText(AddBankAccountActivity.this, addBankAccountResponse.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onFailure(Call<AddBankAccountResponse> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(AddBankAccountActivity.this, t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case android.R.id.home:
                onBackPressed();
                break;
        }
        return super.onOptionsItemSelected(item);

    }

}
