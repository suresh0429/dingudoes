package com.innasoft.dingudoes;

import android.content.Context;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


import androidx.recyclerview.widget.RecyclerView;

import com.squareup.picasso.Picasso;

import java.util.List;


public class GalleryAdapter extends RecyclerView.Adapter<GalleryAdapter.ViewHolder> {
    private List<String> pressModels;
    Context context;
    int adapter_position;

    public GalleryAdapter(Context context, List<String> pressModels) {
        this.context = context;
        this.pressModels = pressModels;
    }

//    public void notifyAdapter(List<Uri> pressModels) {
//        this.pressModels = pressModels;
//        notifyDataSetChanged();
//    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = (LayoutInflater.from(parent.getContext())).inflate(R.layout.galleryitem, parent, false);
        ViewHolder holder = new ViewHolder(view);


        return holder;
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
       String menuModel = pressModels.get(position);
       String txt_path = pressModels.get(position);
    //   holder.id_text_name.setText(pressModels.get(position));
        String file_name = txt_path.substring(txt_path.lastIndexOf("/") + 1);
        holder.id_text_name.setText(file_name);
//        Glide.with(context)
//                .load(menuModel)
//                .into(holder.img);

        Picasso.get().load(menuModel).into(holder.img);
        final int getAdapter_position  = holder.getAdapterPosition();
        holder.img_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              remove_item(getAdapter_position);
            }
        });
    }

    private void remove_item(int getAdapter_position)
    {
        pressModels.remove(getAdapter_position);
        notifyItemRemoved(getAdapter_position);
        notifyItemRangeChanged(getAdapter_position,pressModels.size());
        notifyDataSetChanged();

    }

    @Override
    public int getItemCount() {
        return pressModels.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView img,img_cancel;
        TextView id_text_name;

        public ViewHolder(View itemView) {
            super(itemView);
            img = (ImageView) itemView.findViewById(R.id.img);
            id_text_name = itemView.findViewById(R.id.id_text_name);
            img_cancel = itemView.findViewById(R.id.img_cancel);
        }
    }
}


