package com.innasoft.dingudoes.Models;

public class OngoingModel {
    private String txt_date,txt_area,txt_time,txt_amount;

    public OngoingModel(String txt_date, String txt_area, String txt_time, String txt_amount) {
        this.txt_date = txt_date;
        this.txt_area = txt_area;
        this.txt_time = txt_time;
        this.txt_amount = txt_amount;
    }

    public String getTxt_date() {
        return txt_date;
    }

    public void setTxt_date(String txt_date) {
        this.txt_date = txt_date;
    }

    public String getTxt_area() {
        return txt_area;
    }

    public void setTxt_area(String txt_area) {
        this.txt_area = txt_area;
    }

    public String getTxt_time() {
        return txt_time;
    }

    public void setTxt_time(String txt_time) {
        this.txt_time = txt_time;
    }

    public String getTxt_amount() {
        return txt_amount;
    }

    public void setTxt_amount(String txt_amount) {
        this.txt_amount = txt_amount;
    }
}
