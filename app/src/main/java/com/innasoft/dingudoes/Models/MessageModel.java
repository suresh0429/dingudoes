package com.innasoft.dingudoes.Models;

public class MessageModel {
    private String txt_name,txt_area,txt_desc;

    public MessageModel(String txt_name, String txt_area, String txt_desc) {
        this.txt_name = txt_name;
        this.txt_area = txt_area;
        this.txt_desc = txt_desc;
    }

    public String getTxt_name() {
        return txt_name;
    }

    public void setTxt_name(String txt_name) {
        this.txt_name = txt_name;
    }

    public String getTxt_area() {
        return txt_area;
    }

    public void setTxt_area(String txt_area) {
        this.txt_area = txt_area;
    }

    public String getTxt_desc() {
        return txt_desc;
    }

    public void setTxt_desc(String txt_desc) {
        this.txt_desc = txt_desc;
    }
}
