package com.innasoft.dingudoes.Models;

public class UserReviewModel {
    private int id;
    private int userId;
    private int taskId;
    private String reviewText;
    private double rating;

    public UserReviewModel(int id, int userId, int taskId, String reviewText, double rating) {
        this.id = id;
        this.userId = userId;
        this.taskId = taskId;
        this.reviewText = reviewText;
        this.rating = rating;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getTaskId() {
        return taskId;
    }

    public void setTaskId(int taskId) {
        this.taskId = taskId;
    }

    public String getReviewText() {
        return reviewText;
    }

    public void setReviewText(String reviewText) {
        this.reviewText = reviewText;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }
}
