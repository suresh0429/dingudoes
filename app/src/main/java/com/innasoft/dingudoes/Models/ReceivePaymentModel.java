package com.innasoft.dingudoes.Models;

public class ReceivePaymentModel {

    String ac_no,exp_date;

    public String getAc_no() {
        return ac_no;
    }

    public void setAc_no(String ac_no) {
        this.ac_no = ac_no;
    }

    public String getExp_date() {
        return exp_date;
    }

    public void setExp_date(String exp_date) {
        this.exp_date = exp_date;
    }

    public ReceivePaymentModel(String ac_no, String exp_date) {
        this.ac_no = ac_no;
        this.exp_date = exp_date;
    }
}
