package com.innasoft.dingudoes.Models;

public class HistoryModel {
    String payment,amount;


    public HistoryModel(String payment, String amount) {
        this.payment = payment;
        this.amount = amount;
    }

    public String getPayment() {
        return payment;
    }

    public void setPayment(String payment) {
        this.payment = payment;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
