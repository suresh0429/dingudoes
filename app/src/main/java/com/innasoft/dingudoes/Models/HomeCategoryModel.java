package com.innasoft.dingudoes.Models;

public class HomeCategoryModel
{
   //private String cate_id;
    private String categ_name;
    private int categ_id;

    public HomeCategoryModel(String categ_name, int categ_id) {
        this.categ_name = categ_name;
        this.categ_id = categ_id;
    }

    public String getCateg_name() {
        return categ_name;
    }

    public void setCateg_name(String categ_name) {
        this.categ_name = categ_name;
    }

    public int getCateg_id() {
        return categ_id;
    }

    public void setCateg_id(int categ_id) {
        this.categ_id = categ_id;
    }
}
