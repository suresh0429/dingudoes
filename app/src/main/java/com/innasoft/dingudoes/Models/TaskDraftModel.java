package com.innasoft.dingudoes.Models;

public class TaskDraftModel {
    int id;
    String title,desc,location,dateString,must_haves,hours,time,price,category,isRemote,budgetType,taskers;
    Double latitude,longitude;

    public TaskDraftModel(int id, String title, String desc, String location, String dateString, String must_haves, String hours, String time, String price, String category, String isRemote, String budgetType, String taskers, Double latitude, Double longitude) {
        this.id = id;
        this.title = title;
        this.desc = desc;
        this.location = location;
        this.dateString = dateString;
        this.must_haves = must_haves;
        this.hours = hours;
        this.time = time;
        this.price = price;
        this.category = category;
        this.isRemote = isRemote;
        this.budgetType = budgetType;
        this.taskers = taskers;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public TaskDraftModel(String title, String desc, String location, String dateString, String must_haves, String hours, String time, String price, String category, String isRemote, String budgetType, String taskers, Double latitude, Double longitude) {
        this.title = title;
        this.desc = desc;
        this.location = location;
        this.dateString = dateString;
        this.must_haves = must_haves;
        this.hours = hours;
        this.time = time;
        this.price = price;
        this.category = category;
        this.isRemote = isRemote;
        this.budgetType = budgetType;
        this.taskers = taskers;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getDateString() {
        return dateString;
    }

    public void setDateString(String dateString) {
        this.dateString = dateString;
    }

    public String getMust_haves() {
        return must_haves;
    }

    public void setMust_haves(String must_haves) {
        this.must_haves = must_haves;
    }

    public String getHours() {
        return hours;
    }

    public void setHours(String hours) {
        this.hours = hours;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getIsRemote() {
        return isRemote;
    }

    public void setIsRemote(String isRemote) {
        this.isRemote = isRemote;
    }

    public String getBudgetType() {
        return budgetType;
    }

    public void setBudgetType(String budgetType) {
        this.budgetType = budgetType;
    }

    public String getTaskers() {
        return taskers;
    }

    public void setTaskers(String taskers) {
        this.taskers = taskers;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
}
