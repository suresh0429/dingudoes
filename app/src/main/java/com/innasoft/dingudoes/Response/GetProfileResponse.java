package com.innasoft.dingudoes.Response;

import com.google.gson.annotations.SerializedName;

public class GetProfileResponse {


    /**
     * success : true
     * data : {"user_id":1,"full_name":"Vinod Kumar Ravuri","email":"rvinodbs234@gmail.com","mobile":"8125588780","address":null,"post_task":0,"complete_task":0,"is_active":0,"is_verified":0,"mobile_verified":1,"user_type":2,"passwordResetToken":1467,"resetRequestedDateTime":"2019-10-29T09:02:00.000Z","mobileToken":5351,"tokenDateTime":"2019-10-24T19:24:56.000Z","facebook_key":null,"gmail_key":null,"created_by":null,"updated_by":1,"createdAt":"2019-08-20T13:00:39.000Z","updatedAt":"2019-12-17T19:56:40.000Z","profileImage":"uploads/1572426919000-6 pyments-methods.jpg","beneficaryId":"VIRA257080","fcmToken":"dp34o5pGrW0:APA91bFC8TE4N3WDvydQaxeMSIqHVvr7gIUxeiBUyE69HjL6Dv5DgxF7AVqKm0cIQlUM6knLJKGN0h156FAgij_WxWYkteUZaYHyVcJScQv7aJO4h-TaQzeDJ08DUCO-e6Su2e9yk-xM","userDetail":{"id":1,"userId":1,"billingAddress":"Madhapur, Hyderabad","skills":"Web Development","about":"Hello World","transport":"online","languages":"Hindi,English,Telugu","dateOfBirth":null,"aadharNumber":"335678901234","license":"3316VYRK2019","facebookId":"","mailNotification":true,"smsNotification":true,"pushNotification":false,"newUserDiscountApplied":false,"createdBy":null,"createdAt":"2019-09-06T17:20:56.000Z","updatedBy":null,"updatedAt":"2019-12-17T19:56:40.000Z","user_id":1}}
     * profileScore : 4
     */

    @SerializedName("success")
    private boolean success;
    @SerializedName("data")
    private DataBean data;
    @SerializedName("profileScore")
    private int profileScore;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public int getProfileScore() {
        return profileScore;
    }

    public void setProfileScore(int profileScore) {
        this.profileScore = profileScore;
    }

    public static class DataBean {
        /**
         * user_id : 1
         * full_name : Vinod Kumar Ravuri
         * email : rvinodbs234@gmail.com
         * mobile : 8125588780
         * address : null
         * post_task : 0
         * complete_task : 0
         * is_active : 0
         * is_verified : 0
         * mobile_verified : 1
         * user_type : 2
         * passwordResetToken : 1467
         * resetRequestedDateTime : 2019-10-29T09:02:00.000Z
         * mobileToken : 5351
         * tokenDateTime : 2019-10-24T19:24:56.000Z
         * facebook_key : null
         * gmail_key : null
         * created_by : null
         * updated_by : 1
         * createdAt : 2019-08-20T13:00:39.000Z
         * updatedAt : 2019-12-17T19:56:40.000Z
         * profileImage : uploads/1572426919000-6 pyments-methods.jpg
         * beneficaryId : VIRA257080
         * fcmToken : dp34o5pGrW0:APA91bFC8TE4N3WDvydQaxeMSIqHVvr7gIUxeiBUyE69HjL6Dv5DgxF7AVqKm0cIQlUM6knLJKGN0h156FAgij_WxWYkteUZaYHyVcJScQv7aJO4h-TaQzeDJ08DUCO-e6Su2e9yk-xM
         * userDetail : {"id":1,"userId":1,"billingAddress":"Madhapur, Hyderabad","skills":"Web Development","about":"Hello World","transport":"online","languages":"Hindi,English,Telugu","dateOfBirth":null,"aadharNumber":"335678901234","license":"3316VYRK2019","facebookId":"","mailNotification":true,"smsNotification":true,"pushNotification":false,"newUserDiscountApplied":false,"createdBy":null,"createdAt":"2019-09-06T17:20:56.000Z","updatedBy":null,"updatedAt":"2019-12-17T19:56:40.000Z","user_id":1}
         */

        @SerializedName("user_id")
        private int userId;
        @SerializedName("full_name")
        private String fullName;
        @SerializedName("email")
        private String email;
        @SerializedName("mobile")
        private String mobile;
        @SerializedName("address")
        private Object address;
        @SerializedName("post_task")
        private int postTask;
        @SerializedName("complete_task")
        private int completeTask;
        @SerializedName("is_active")
        private int isActive;
        @SerializedName("is_verified")
        private int isVerified;
        @SerializedName("mobile_verified")
        private int mobileVerified;
        @SerializedName("user_type")
        private int userType;
        @SerializedName("passwordResetToken")
        private int passwordResetToken;
        @SerializedName("resetRequestedDateTime")
        private String resetRequestedDateTime;
        @SerializedName("mobileToken")
        private int mobileToken;
        @SerializedName("tokenDateTime")
        private String tokenDateTime;
        @SerializedName("facebook_key")
        private Object facebookKey;
        @SerializedName("gmail_key")
        private Object gmailKey;
        @SerializedName("created_by")
        private Object createdBy;
        @SerializedName("updated_by")
        private int updatedBy;
        @SerializedName("createdAt")
        private String createdAt;
        @SerializedName("updatedAt")
        private String updatedAt;
        @SerializedName("profileImage")
        private String profileImage;
        @SerializedName("beneficaryId")
        private String beneficaryId;
        @SerializedName("fcmToken")
        private String fcmToken;
        @SerializedName("userDetail")
        private UserDetailBean userDetail;

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public String getFullName() {
            return fullName;
        }

        public void setFullName(String fullName) {
            this.fullName = fullName;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public Object getAddress() {
            return address;
        }

        public void setAddress(Object address) {
            this.address = address;
        }

        public int getPostTask() {
            return postTask;
        }

        public void setPostTask(int postTask) {
            this.postTask = postTask;
        }

        public int getCompleteTask() {
            return completeTask;
        }

        public void setCompleteTask(int completeTask) {
            this.completeTask = completeTask;
        }

        public int getIsActive() {
            return isActive;
        }

        public void setIsActive(int isActive) {
            this.isActive = isActive;
        }

        public int getIsVerified() {
            return isVerified;
        }

        public void setIsVerified(int isVerified) {
            this.isVerified = isVerified;
        }

        public int getMobileVerified() {
            return mobileVerified;
        }

        public void setMobileVerified(int mobileVerified) {
            this.mobileVerified = mobileVerified;
        }

        public int getUserType() {
            return userType;
        }

        public void setUserType(int userType) {
            this.userType = userType;
        }

        public int getPasswordResetToken() {
            return passwordResetToken;
        }

        public void setPasswordResetToken(int passwordResetToken) {
            this.passwordResetToken = passwordResetToken;
        }

        public String getResetRequestedDateTime() {
            return resetRequestedDateTime;
        }

        public void setResetRequestedDateTime(String resetRequestedDateTime) {
            this.resetRequestedDateTime = resetRequestedDateTime;
        }

        public int getMobileToken() {
            return mobileToken;
        }

        public void setMobileToken(int mobileToken) {
            this.mobileToken = mobileToken;
        }

        public String getTokenDateTime() {
            return tokenDateTime;
        }

        public void setTokenDateTime(String tokenDateTime) {
            this.tokenDateTime = tokenDateTime;
        }

        public Object getFacebookKey() {
            return facebookKey;
        }

        public void setFacebookKey(Object facebookKey) {
            this.facebookKey = facebookKey;
        }

        public Object getGmailKey() {
            return gmailKey;
        }

        public void setGmailKey(Object gmailKey) {
            this.gmailKey = gmailKey;
        }

        public Object getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(Object createdBy) {
            this.createdBy = createdBy;
        }

        public int getUpdatedBy() {
            return updatedBy;
        }

        public void setUpdatedBy(int updatedBy) {
            this.updatedBy = updatedBy;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public String getProfileImage() {
            return profileImage;
        }

        public void setProfileImage(String profileImage) {
            this.profileImage = profileImage;
        }

        public String getBeneficaryId() {
            return beneficaryId;
        }

        public void setBeneficaryId(String beneficaryId) {
            this.beneficaryId = beneficaryId;
        }

        public String getFcmToken() {
            return fcmToken;
        }

        public void setFcmToken(String fcmToken) {
            this.fcmToken = fcmToken;
        }

        public UserDetailBean getUserDetail() {
            return userDetail;
        }

        public void setUserDetail(UserDetailBean userDetail) {
            this.userDetail = userDetail;
        }

        public static class UserDetailBean {
            /**
             * id : 1
             * userId : 1
             * billingAddress : Madhapur, Hyderabad
             * skills : Web Development
             * about : Hello World
             * transport : online
             * languages : Hindi,English,Telugu
             * dateOfBirth : null
             * aadharNumber : 335678901234
             * license : 3316VYRK2019
             * facebookId :
             * mailNotification : true
             * smsNotification : true
             * pushNotification : false
             * newUserDiscountApplied : false
             * createdBy : null
             * createdAt : 2019-09-06T17:20:56.000Z
             * updatedBy : null
             * updatedAt : 2019-12-17T19:56:40.000Z
             * user_id : 1
             */

            @SerializedName("id")
            private int id;
            @SerializedName("userId")
            private int userId;
            @SerializedName("billingAddress")
            private String billingAddress;
            @SerializedName("skills")
            private String skills;
            @SerializedName("about")
            private String about;
            @SerializedName("transport")
            private String transport;
            @SerializedName("languages")
            private String languages;
            @SerializedName("dateOfBirth")
            private String dateOfBirth;
            @SerializedName("aadharNumber")
            private String aadharNumber;
            @SerializedName("license")
            private String license;
            @SerializedName("facebookId")
            private String facebookId;
            @SerializedName("mailNotification")
            private boolean mailNotification;
            @SerializedName("smsNotification")
            private boolean smsNotification;
            @SerializedName("pushNotification")
            private boolean pushNotification;
            @SerializedName("newUserDiscountApplied")
            private boolean newUserDiscountApplied;
            @SerializedName("createdBy")
            private Object createdBy;
            @SerializedName("createdAt")
            private String createdAt;
            @SerializedName("updatedBy")
            private Object updatedBy;
            @SerializedName("updatedAt")
            private String updatedAt;


            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getUserId() {
                return userId;
            }

            public void setUserId(int userId) {
                this.userId = userId;
            }

            public String getBillingAddress() {
                return billingAddress;
            }

            public void setBillingAddress(String billingAddress) {
                this.billingAddress = billingAddress;
            }

            public String getSkills() {
                return skills;
            }

            public void setSkills(String skills) {
                this.skills = skills;
            }

            public String getAbout() {
                return about;
            }

            public void setAbout(String about) {
                this.about = about;
            }

            public String getTransport() {
                return transport;
            }

            public void setTransport(String transport) {
                this.transport = transport;
            }

            public String getLanguages() {
                return languages;
            }

            public void setLanguages(String languages) {
                this.languages = languages;
            }

            public String getDateOfBirth() {
                return dateOfBirth;
            }

            public void setDateOfBirth(String dateOfBirth) {
                this.dateOfBirth = dateOfBirth;
            }

            public String getAadharNumber() {
                return aadharNumber;
            }

            public void setAadharNumber(String aadharNumber) {
                this.aadharNumber = aadharNumber;
            }

            public String getLicense() {
                return license;
            }

            public void setLicense(String license) {
                this.license = license;
            }

            public String getFacebookId() {
                return facebookId;
            }

            public void setFacebookId(String facebookId) {
                this.facebookId = facebookId;
            }

            public boolean isMailNotification() {
                return mailNotification;
            }

            public void setMailNotification(boolean mailNotification) {
                this.mailNotification = mailNotification;
            }

            public boolean isSmsNotification() {
                return smsNotification;
            }

            public void setSmsNotification(boolean smsNotification) {
                this.smsNotification = smsNotification;
            }

            public boolean isPushNotification() {
                return pushNotification;
            }

            public void setPushNotification(boolean pushNotification) {
                this.pushNotification = pushNotification;
            }

            public boolean isNewUserDiscountApplied() {
                return newUserDiscountApplied;
            }

            public void setNewUserDiscountApplied(boolean newUserDiscountApplied) {
                this.newUserDiscountApplied = newUserDiscountApplied;
            }

            public Object getCreatedBy() {
                return createdBy;
            }

            public void setCreatedBy(Object createdBy) {
                this.createdBy = createdBy;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public Object getUpdatedBy() {
                return updatedBy;
            }

            public void setUpdatedBy(Object updatedBy) {
                this.updatedBy = updatedBy;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }


        }
    }
}
