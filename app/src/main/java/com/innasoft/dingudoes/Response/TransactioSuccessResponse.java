package com.innasoft.dingudoes.Response;

public class TransactioSuccessResponse {


    /**
     * success : true
     * data : {"createdAt":"2019-11-01T09:09:37.679Z","updatedAt":{"val":"CURRENT_TIMESTAMP"},"id":8,"paidBy":"1","paidTo":"2","taskId":"19","quoteId":"1","amount":"100","transactionNumber":"12222","transactionComments":"fgfkfkf","orderId":"order_001","status":"success","isDeleted":0}
     */

    private boolean success;
    private DataBean data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * createdAt : 2019-11-01T09:09:37.679Z
         * updatedAt : {"val":"CURRENT_TIMESTAMP"}
         * id : 8
         * paidBy : 1
         * paidTo : 2
         * taskId : 19
         * quoteId : 1
         * amount : 100
         * transactionNumber : 12222
         * transactionComments : fgfkfkf
         * orderId : order_001
         * status : success
         * isDeleted : 0
         */

        private String createdAt;
        private UpdatedAtBean updatedAt;
        private int id;
        private String paidBy;
        private String paidTo;
        private String taskId;
        private String quoteId;
        private String amount;
        private String transactionNumber;
        private String transactionComments;
        private String orderId;
        private String status;
        private int isDeleted;

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public UpdatedAtBean getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(UpdatedAtBean updatedAt) {
            this.updatedAt = updatedAt;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getPaidBy() {
            return paidBy;
        }

        public void setPaidBy(String paidBy) {
            this.paidBy = paidBy;
        }

        public String getPaidTo() {
            return paidTo;
        }

        public void setPaidTo(String paidTo) {
            this.paidTo = paidTo;
        }

        public String getTaskId() {
            return taskId;
        }

        public void setTaskId(String taskId) {
            this.taskId = taskId;
        }

        public String getQuoteId() {
            return quoteId;
        }

        public void setQuoteId(String quoteId) {
            this.quoteId = quoteId;
        }

        public String getAmount() {
            return amount;
        }

        public void setAmount(String amount) {
            this.amount = amount;
        }

        public String getTransactionNumber() {
            return transactionNumber;
        }

        public void setTransactionNumber(String transactionNumber) {
            this.transactionNumber = transactionNumber;
        }

        public String getTransactionComments() {
            return transactionComments;
        }

        public void setTransactionComments(String transactionComments) {
            this.transactionComments = transactionComments;
        }

        public String getOrderId() {
            return orderId;
        }

        public void setOrderId(String orderId) {
            this.orderId = orderId;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public int getIsDeleted() {
            return isDeleted;
        }

        public void setIsDeleted(int isDeleted) {
            this.isDeleted = isDeleted;
        }

        public static class UpdatedAtBean {
            /**
             * val : CURRENT_TIMESTAMP
             */

            private String val;

            public String getVal() {
                return val;
            }

            public void setVal(String val) {
                this.val = val;
            }
        }
    }
}
