package com.innasoft.dingudoes.Response;

import java.util.List;

public class PendingReviewsResponse {


    /**
     * success : true
     * data : [{"id":3,"title":"test","description":"testing the validation","category":"Delivery","mustHaves":null,"isRemote":null,"address":"Madhapur","finishDate":null,"finishTime":null,"budgetType":null,"budget":"100","taskHours":null,"taskers":2,"latitude":"17.4486","longitude":"78.3908","status":"completed","isDeleted":0,"createdBy":1,"createdAt":"2019-08-26T03:43:57.000Z","updatedBy":null,"updatedAt":"2019-08-26T03:43:57.000Z","reviews":[{"id":2,"userId":1,"taskId":3,"reviewText":"testing","rating":3.5,"isDeleted":0,"createdBy":null,"createdAt":"2019-09-06T00:59:03.000Z","updatedBy":null,"updatedAt":"2019-09-06T00:59:03.000Z","task_id":3}]}]
     */

    private boolean success;
    private List<DataBean> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 3
         * title : test
         * description : testing the validation
         * category : Delivery
         * mustHaves : null
         * isRemote : null
         * address : Madhapur
         * finishDate : null
         * finishTime : null
         * budgetType : null
         * budget : 100
         * taskHours : null
         * taskers : 2
         * latitude : 17.4486
         * longitude : 78.3908
         * status : completed
         * isDeleted : 0
         * createdBy : 1
         * createdAt : 2019-08-26T03:43:57.000Z
         * updatedBy : null
         * updatedAt : 2019-08-26T03:43:57.000Z
         * reviews : [{"id":2,"userId":1,"taskId":3,"reviewText":"testing","rating":3.5,"isDeleted":0,"createdBy":null,"createdAt":"2019-09-06T00:59:03.000Z","updatedBy":null,"updatedAt":"2019-09-06T00:59:03.000Z","task_id":3}]
         */

        private int id;
        private String title;
        private String description;
        private String category;
        private String mustHaves;
        private String isRemote;
        private String address;
        private String finishDate;
        private String finishTime;
        private String budgetType;
        private String budget;
        private String taskHours;
        private int taskers;
        private String latitude;
        private String longitude;
        private String status;
        private int isDeleted;
        private int createdBy;
        private String createdAt;
        private String updatedBy;
        private String updatedAt;
        private List<ReviewsBean> reviews;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getMustHaves() {
            return mustHaves;
        }

        public void setMustHaves(String mustHaves) {
            this.mustHaves = mustHaves;
        }

        public String getIsRemote() {
            return isRemote;
        }

        public void setIsRemote(String isRemote) {
            this.isRemote = isRemote;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getFinishDate() {
            return finishDate;
        }

        public void setFinishDate(String finishDate) {
            this.finishDate = finishDate;
        }

        public String getFinishTime() {
            return finishTime;
        }

        public void setFinishTime(String finishTime) {
            this.finishTime = finishTime;
        }

        public String getBudgetType() {
            return budgetType;
        }

        public void setBudgetType(String budgetType) {
            this.budgetType = budgetType;
        }

        public String getBudget() {
            return budget;
        }

        public void setBudget(String budget) {
            this.budget = budget;
        }

        public String getTaskHours() {
            return taskHours;
        }

        public void setTaskHours(String taskHours) {
            this.taskHours = taskHours;
        }

        public int getTaskers() {
            return taskers;
        }

        public void setTaskers(int taskers) {
            this.taskers = taskers;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public int getIsDeleted() {
            return isDeleted;
        }

        public void setIsDeleted(int isDeleted) {
            this.isDeleted = isDeleted;
        }

        public int getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(int createdBy) {
            this.createdBy = createdBy;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedBy() {
            return updatedBy;
        }

        public void setUpdatedBy(String updatedBy) {
            this.updatedBy = updatedBy;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public List<ReviewsBean> getReviews() {
            return reviews;
        }

        public void setReviews(List<ReviewsBean> reviews) {
            this.reviews = reviews;
        }

        public static class ReviewsBean {
            /**
             * id : 2
             * userId : 1
             * taskId : 3
             * reviewText : testing
             * rating : 3.5
             * isDeleted : 0
             * createdBy : null
             * createdAt : 2019-09-06T00:59:03.000Z
             * updatedBy : null
             * updatedAt : 2019-09-06T00:59:03.000Z
             * task_id : 3
             */

            private int id;
            private int userId;
            private int taskId;
            private String reviewText;
            private double rating;
            private int isDeleted;
            private Object createdBy;
            private String createdAt;
            private Object updatedBy;
            private String updatedAt;
            private int task_id;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getUserId() {
                return userId;
            }

            public void setUserId(int userId) {
                this.userId = userId;
            }

            public int getTaskId() {
                return taskId;
            }

            public void setTaskId(int taskId) {
                this.taskId = taskId;
            }

            public String getReviewText() {
                return reviewText;
            }

            public void setReviewText(String reviewText) {
                this.reviewText = reviewText;
            }

            public double getRating() {
                return rating;
            }

            public void setRating(double rating) {
                this.rating = rating;
            }

            public int getIsDeleted() {
                return isDeleted;
            }

            public void setIsDeleted(int isDeleted) {
                this.isDeleted = isDeleted;
            }

            public Object getCreatedBy() {
                return createdBy;
            }

            public void setCreatedBy(Object createdBy) {
                this.createdBy = createdBy;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public Object getUpdatedBy() {
                return updatedBy;
            }

            public void setUpdatedBy(Object updatedBy) {
                this.updatedBy = updatedBy;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public int getTask_id() {
                return task_id;
            }

            public void setTask_id(int task_id) {
                this.task_id = task_id;
            }
        }
    }
}
