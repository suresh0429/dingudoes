package com.innasoft.dingudoes.Response;

import java.util.List;

public class PaymentPaidHistoryResponse {


    /**
     * success : true
     * data : []
     */

    private boolean success;
    private List<?> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<?> getData() {
        return data;
    }

    public void setData(List<?> data) {
        this.data = data;
    }
}
