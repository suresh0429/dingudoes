package com.innasoft.dingudoes.Response;

public class VerifyMobileResponse {


    /**
     * success : true
     * message : Mobile Number Verified Successfully
     */

    private boolean success;
    private String message;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
