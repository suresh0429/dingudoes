package com.innasoft.dingudoes.Response;

import java.util.List;

public class ContactUsResponse {


    /**
     * success : true
     * data : [{"lookupId":3,"lookupKey":"contactUs","lookupValue":"<!DOCTYPE html>\n<html>\n<meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n<link rel=\"stylesheet\" href=\"https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css\">\n\t<section class=\"page-in page-in2\">\n      \t<div class=\"container\">\n\t      \t<div class=\"row\">\n\t\t        <div class=\"col-md-12\">\n\t\t          \t<div class=\"container-mid\">\n\t\t            \t<h2>Contact us<\/h2> \n\t\t          \t<\/div>\n\t\t          \t<div class=\"inner-in\">\n\t\t          \t\t<p>\n\t\t          \t\t\tThe Company respects your privacy and values the trust you place in it. Set out below is the Company\u2019s \u2018Privacy Policy\u2019 which details the manner in which information relating to you is collected, used and disclosed.\n\t\t          \t\t<\/p>\n\t\t\t\t\t\t<p>\n\t\t          \t\t\tCustomer are advised to read and understand our Privacy Policy carefully, as by accessing the website/app you agree to be bound by the terms and conditions of the Privacy Policy and consent to the collection, storage and use of information relating to you as provided herein.\n\t\t          \t\t<\/p>\n\t\t\t\t\t\t<p>\n\t\t          \t\t\tIf you do not agree with the terms and conditions of our Privacy Policy, including in relation to the manner of collection or use of your information, please do not use or access the website/app.\n\t\t          \t\t<\/p>\n\t\t\t\t\t\t<p>\n\t\t          \t\t\tOur Privacy Policy is incorporated into the Terms and Conditions of Use of the website/app, and is subject to change from time to time without notice. It is strongly recommended that you periodically review our Privacy Policy as posted on the App/Web.\n\t\t          \t\t<\/p>\n\t\t\t\t\t\t<p>\n\t\t          \t\t\tShould you have any clarifications regarding this Privacy Policy, please do not hesitate to contact us at <a href=\"mailto:support@dingu.in\">support@dingu.in<\/a>\n\t\t          \t\t<\/p>\t\t          \t\n\n\t\t          \t<\/div>\n\t          \t<\/div>\n\t      \t<\/div>\n      \t<\/div>  \t\n  \t<\/section>\n  \t<style type=\"text/css\">\n  \t\t.page-in {\n\t\t    margin: 0;\n\t\t    padding: 0 20px;\n\t\t}\n\t\t\n\t\tp{\n\t\ttext-align:justify;\n\t\t}\n  \t<\/style>\n    <\/body>\n<\/html>","createdAt":"2019-10-25T05:57:52.000Z","updatedBy":1,"updatedAt":"2019-10-25T05:57:52.000Z"}]
     */

    private boolean success;
    private List<DataBean> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * lookupId : 3
         * lookupKey : contactUs
         * lookupValue : <!DOCTYPE html>
         <html>
         <meta name="viewport" content="width=device-width, initial-scale=1.0">
         <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
         <section class="page-in page-in2">
         <div class="container">
         <div class="row">
         <div class="col-md-12">
         <div class="container-mid">
         <h2>Contact us</h2>
         </div>
         <div class="inner-in">
         <p>
         The Company respects your privacy and values the trust you place in it. Set out below is the Company’s ‘Privacy Policy’ which details the manner in which information relating to you is collected, used and disclosed.
         </p>
         <p>
         Customer are advised to read and understand our Privacy Policy carefully, as by accessing the website/app you agree to be bound by the terms and conditions of the Privacy Policy and consent to the collection, storage and use of information relating to you as provided herein.
         </p>
         <p>
         If you do not agree with the terms and conditions of our Privacy Policy, including in relation to the manner of collection or use of your information, please do not use or access the website/app.
         </p>
         <p>
         Our Privacy Policy is incorporated into the Terms and Conditions of Use of the website/app, and is subject to change from time to time without notice. It is strongly recommended that you periodically review our Privacy Policy as posted on the App/Web.
         </p>
         <p>
         Should you have any clarifications regarding this Privacy Policy, please do not hesitate to contact us at <a href="mailto:support@dingu.in">support@dingu.in</a>
         </p>

         </div>
         </div>
         </div>
         </div>
         </section>
         <style type="text/css">
         .page-in {
         margin: 0;
         padding: 0 20px;
         }

         p{
         text-align:justify;
         }
         </style>
         </body>
         </html>
         * createdAt : 2019-10-25T05:57:52.000Z
         * updatedBy : 1
         * updatedAt : 2019-10-25T05:57:52.000Z
         */

        private int lookupId;
        private String lookupKey;
        private String lookupValue;
        private String createdAt;
        private int updatedBy;
        private String updatedAt;

        public int getLookupId() {
            return lookupId;
        }

        public void setLookupId(int lookupId) {
            this.lookupId = lookupId;
        }

        public String getLookupKey() {
            return lookupKey;
        }

        public void setLookupKey(String lookupKey) {
            this.lookupKey = lookupKey;
        }

        public String getLookupValue() {
            return lookupValue;
        }

        public void setLookupValue(String lookupValue) {
            this.lookupValue = lookupValue;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public int getUpdatedBy() {
            return updatedBy;
        }

        public void setUpdatedBy(int updatedBy) {
            this.updatedBy = updatedBy;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }
    }
}
