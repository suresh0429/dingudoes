package com.innasoft.dingudoes.Response;

import java.util.List;

public class GetUserReviewResponse {


    /**
     * success : true
     * data : {"tasker":[{"id":2,"userId":1,"taskId":3,"reviewText":"testing","rating":3.5,"isDeleted":0,"createdBy":null,"createdAt":"2019-09-06T00:59:03.000Z","updatedBy":null,"updatedAt":"2019-09-06T00:59:03.000Z","task_id":3,"task":{"id":3,"title":"test","description":"testing the validation","category":"Delivery","mustHaves":null,"isRemote":null,"address":"Madhapur","finishDate":null,"finishTime":null,"budgetType":null,"budget":"100","taskHours":null,"taskers":2,"latitude":"17.4486","longitude":"78.3908","status":"completed","isDeleted":0,"createdBy":1,"createdAt":"2019-08-26T03:43:57.000Z","updatedBy":null,"updatedAt":"2019-08-26T03:43:57.000Z"}},{"id":4,"userId":1,"taskId":20,"reviewText":"testing 2","rating":5,"isDeleted":0,"createdBy":1,"createdAt":"2019-10-12T08:33:55.000Z","updatedBy":1,"updatedAt":"2019-10-12T08:33:54.000Z","task_id":20,"task":{"id":20,"title":"testing dev fin","description":"testing file upload","category":"delivery","mustHaves":"['Must have 1','Must have 2','Must have 3']","isRemote":1,"address":"Madhapur","finishDate":"2019-08-22","finishTime":"6","budgetType":"Total","budget":"100","taskHours":"4","taskers":2,"latitude":"17.4486","longitude":"78.3908","status":"assigned","isDeleted":0,"createdBy":1,"createdAt":"2019-10-12T08:28:21.000Z","updatedBy":1,"updatedAt":"2019-10-24T18:53:07.000Z"}},{"id":5,"userId":1,"taskId":20,"reviewText":"testing","rating":3.5,"isDeleted":0,"createdBy":1,"createdAt":"2019-10-12T08:34:08.000Z","updatedBy":1,"updatedAt":"2019-10-12T08:34:08.000Z","task_id":20,"task":{"id":20,"title":"testing dev fin","description":"testing file upload","category":"delivery","mustHaves":"['Must have 1','Must have 2','Must have 3']","isRemote":1,"address":"Madhapur","finishDate":"2019-08-22","finishTime":"6","budgetType":"Total","budget":"100","taskHours":"4","taskers":2,"latitude":"17.4486","longitude":"78.3908","status":"assigned","isDeleted":0,"createdBy":1,"createdAt":"2019-10-12T08:28:21.000Z","updatedBy":1,"updatedAt":"2019-10-24T18:53:07.000Z"}}],"poster":[{"id":1,"userId":1,"taskId":1,"reviewText":"testing","rating":3.5,"isDeleted":0,"createdBy":null,"createdAt":"2019-09-01T09:50:36.000Z","updatedBy":null,"updatedAt":"2019-09-01T09:50:36.000Z","task_id":1,"task":{"id":1,"title":"test","description":"","category":null,"mustHaves":null,"isRemote":null,"address":"Madhapur","finishDate":null,"finishTime":null,"budgetType":null,"budget":"100","taskHours":null,"taskers":2,"latitude":"","longitude":"","status":"assigned","isDeleted":1,"createdBy":3,"createdAt":"2019-08-22T04:17:55.000Z","updatedBy":null,"updatedAt":"2019-11-01T11:55:37.000Z"}}]}
     */

    private boolean success;
    private DataBean data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        private List<TaskerBean> tasker;
        private List<PosterBean> poster;

        public List<TaskerBean> getTasker() {
            return tasker;
        }

        public void setTasker(List<TaskerBean> tasker) {
            this.tasker = tasker;
        }

        public List<PosterBean> getPoster() {
            return poster;
        }

        public void setPoster(List<PosterBean> poster) {
            this.poster = poster;
        }

        public static class TaskerBean {
            /**
             * id : 2
             * userId : 1
             * taskId : 3
             * reviewText : testing
             * rating : 3.5
             * isDeleted : 0
             * createdBy : null
             * createdAt : 2019-09-06T00:59:03.000Z
             * updatedBy : null
             * updatedAt : 2019-09-06T00:59:03.000Z
             * task_id : 3
             * task : {"id":3,"title":"test","description":"testing the validation","category":"Delivery","mustHaves":null,"isRemote":null,"address":"Madhapur","finishDate":null,"finishTime":null,"budgetType":null,"budget":"100","taskHours":null,"taskers":2,"latitude":"17.4486","longitude":"78.3908","status":"completed","isDeleted":0,"createdBy":1,"createdAt":"2019-08-26T03:43:57.000Z","updatedBy":null,"updatedAt":"2019-08-26T03:43:57.000Z"}
             */

            private int id;
            private int userId;
            private int taskId;
            private String reviewText;
            private double rating;
            private int isDeleted;
            private Object createdBy;
            private String createdAt;
            private Object updatedBy;
            private String updatedAt;
            private int task_id;
            private TaskBean task;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getUserId() {
                return userId;
            }

            public void setUserId(int userId) {
                this.userId = userId;
            }

            public int getTaskId() {
                return taskId;
            }

            public void setTaskId(int taskId) {
                this.taskId = taskId;
            }

            public String getReviewText() {
                return reviewText;
            }

            public void setReviewText(String reviewText) {
                this.reviewText = reviewText;
            }

            public double getRating() {
                return rating;
            }

            public void setRating(double rating) {
                this.rating = rating;
            }

            public int getIsDeleted() {
                return isDeleted;
            }

            public void setIsDeleted(int isDeleted) {
                this.isDeleted = isDeleted;
            }

            public Object getCreatedBy() {
                return createdBy;
            }

            public void setCreatedBy(Object createdBy) {
                this.createdBy = createdBy;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public Object getUpdatedBy() {
                return updatedBy;
            }

            public void setUpdatedBy(Object updatedBy) {
                this.updatedBy = updatedBy;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public int getTask_id() {
                return task_id;
            }

            public void setTask_id(int task_id) {
                this.task_id = task_id;
            }

            public TaskBean getTask() {
                return task;
            }

            public void setTask(TaskBean task) {
                this.task = task;
            }

            public static class TaskBean {
                /**
                 * id : 3
                 * title : test
                 * description : testing the validation
                 * category : Delivery
                 * mustHaves : null
                 * isRemote : null
                 * address : Madhapur
                 * finishDate : null
                 * finishTime : null
                 * budgetType : null
                 * budget : 100
                 * taskHours : null
                 * taskers : 2
                 * latitude : 17.4486
                 * longitude : 78.3908
                 * status : completed
                 * isDeleted : 0
                 * createdBy : 1
                 * createdAt : 2019-08-26T03:43:57.000Z
                 * updatedBy : null
                 * updatedAt : 2019-08-26T03:43:57.000Z
                 */

                private int id;
                private String title;
                private String description;
                private String category;
                private Object mustHaves;
                private Object isRemote;
                private String address;
                private Object finishDate;
                private Object finishTime;
                private Object budgetType;
                private String budget;
                private Object taskHours;
                private int taskers;
                private String latitude;
                private String longitude;
                private String status;
                private int isDeleted;
                private int createdBy;
                private String createdAt;
                private Object updatedBy;
                private String updatedAt;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public String getDescription() {
                    return description;
                }

                public void setDescription(String description) {
                    this.description = description;
                }

                public String getCategory() {
                    return category;
                }

                public void setCategory(String category) {
                    this.category = category;
                }

                public Object getMustHaves() {
                    return mustHaves;
                }

                public void setMustHaves(Object mustHaves) {
                    this.mustHaves = mustHaves;
                }

                public Object getIsRemote() {
                    return isRemote;
                }

                public void setIsRemote(Object isRemote) {
                    this.isRemote = isRemote;
                }

                public String getAddress() {
                    return address;
                }

                public void setAddress(String address) {
                    this.address = address;
                }

                public Object getFinishDate() {
                    return finishDate;
                }

                public void setFinishDate(Object finishDate) {
                    this.finishDate = finishDate;
                }

                public Object getFinishTime() {
                    return finishTime;
                }

                public void setFinishTime(Object finishTime) {
                    this.finishTime = finishTime;
                }

                public Object getBudgetType() {
                    return budgetType;
                }

                public void setBudgetType(Object budgetType) {
                    this.budgetType = budgetType;
                }

                public String getBudget() {
                    return budget;
                }

                public void setBudget(String budget) {
                    this.budget = budget;
                }

                public Object getTaskHours() {
                    return taskHours;
                }

                public void setTaskHours(Object taskHours) {
                    this.taskHours = taskHours;
                }

                public int getTaskers() {
                    return taskers;
                }

                public void setTaskers(int taskers) {
                    this.taskers = taskers;
                }

                public String getLatitude() {
                    return latitude;
                }

                public void setLatitude(String latitude) {
                    this.latitude = latitude;
                }

                public String getLongitude() {
                    return longitude;
                }

                public void setLongitude(String longitude) {
                    this.longitude = longitude;
                }

                public String getStatus() {
                    return status;
                }

                public void setStatus(String status) {
                    this.status = status;
                }

                public int getIsDeleted() {
                    return isDeleted;
                }

                public void setIsDeleted(int isDeleted) {
                    this.isDeleted = isDeleted;
                }

                public int getCreatedBy() {
                    return createdBy;
                }

                public void setCreatedBy(int createdBy) {
                    this.createdBy = createdBy;
                }

                public String getCreatedAt() {
                    return createdAt;
                }

                public void setCreatedAt(String createdAt) {
                    this.createdAt = createdAt;
                }

                public Object getUpdatedBy() {
                    return updatedBy;
                }

                public void setUpdatedBy(Object updatedBy) {
                    this.updatedBy = updatedBy;
                }

                public String getUpdatedAt() {
                    return updatedAt;
                }

                public void setUpdatedAt(String updatedAt) {
                    this.updatedAt = updatedAt;
                }
            }
        }

        public static class PosterBean {
            /**
             * id : 1
             * userId : 1
             * taskId : 1
             * reviewText : testing
             * rating : 3.5
             * isDeleted : 0
             * createdBy : null
             * createdAt : 2019-09-01T09:50:36.000Z
             * updatedBy : null
             * updatedAt : 2019-09-01T09:50:36.000Z
             * task_id : 1
             * task : {"id":1,"title":"test","description":"","category":null,"mustHaves":null,"isRemote":null,"address":"Madhapur","finishDate":null,"finishTime":null,"budgetType":null,"budget":"100","taskHours":null,"taskers":2,"latitude":"","longitude":"","status":"assigned","isDeleted":1,"createdBy":3,"createdAt":"2019-08-22T04:17:55.000Z","updatedBy":null,"updatedAt":"2019-11-01T11:55:37.000Z"}
             */

            private int id;
            private int userId;
            private int taskId;
            private String reviewText;
            private double rating;
            private int isDeleted;
            private Object createdBy;
            private String createdAt;
            private Object updatedBy;
            private String updatedAt;
            private int task_id;
            private TaskBeanX task;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getUserId() {
                return userId;
            }

            public void setUserId(int userId) {
                this.userId = userId;
            }

            public int getTaskId() {
                return taskId;
            }

            public void setTaskId(int taskId) {
                this.taskId = taskId;
            }

            public String getReviewText() {
                return reviewText;
            }

            public void setReviewText(String reviewText) {
                this.reviewText = reviewText;
            }

            public double getRating() {
                return rating;
            }

            public void setRating(double rating) {
                this.rating = rating;
            }

            public int getIsDeleted() {
                return isDeleted;
            }

            public void setIsDeleted(int isDeleted) {
                this.isDeleted = isDeleted;
            }

            public Object getCreatedBy() {
                return createdBy;
            }

            public void setCreatedBy(Object createdBy) {
                this.createdBy = createdBy;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public Object getUpdatedBy() {
                return updatedBy;
            }

            public void setUpdatedBy(Object updatedBy) {
                this.updatedBy = updatedBy;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public int getTask_id() {
                return task_id;
            }

            public void setTask_id(int task_id) {
                this.task_id = task_id;
            }

            public TaskBeanX getTask() {
                return task;
            }

            public void setTask(TaskBeanX task) {
                this.task = task;
            }

            public static class TaskBeanX {
                /**
                 * id : 1
                 * title : test
                 * description :
                 * category : null
                 * mustHaves : null
                 * isRemote : null
                 * address : Madhapur
                 * finishDate : null
                 * finishTime : null
                 * budgetType : null
                 * budget : 100
                 * taskHours : null
                 * taskers : 2
                 * latitude :
                 * longitude :
                 * status : assigned
                 * isDeleted : 1
                 * createdBy : 3
                 * createdAt : 2019-08-22T04:17:55.000Z
                 * updatedBy : null
                 * updatedAt : 2019-11-01T11:55:37.000Z
                 */

                private int id;
                private String title;
                private String description;
                private Object category;
                private Object mustHaves;
                private Object isRemote;
                private String address;
                private Object finishDate;
                private Object finishTime;
                private Object budgetType;
                private String budget;
                private Object taskHours;
                private int taskers;
                private String latitude;
                private String longitude;
                private String status;
                private int isDeleted;
                private int createdBy;
                private String createdAt;
                private Object updatedBy;
                private String updatedAt;

                public int getId() {
                    return id;
                }

                public void setId(int id) {
                    this.id = id;
                }

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public String getDescription() {
                    return description;
                }

                public void setDescription(String description) {
                    this.description = description;
                }

                public Object getCategory() {
                    return category;
                }

                public void setCategory(Object category) {
                    this.category = category;
                }

                public Object getMustHaves() {
                    return mustHaves;
                }

                public void setMustHaves(Object mustHaves) {
                    this.mustHaves = mustHaves;
                }

                public Object getIsRemote() {
                    return isRemote;
                }

                public void setIsRemote(Object isRemote) {
                    this.isRemote = isRemote;
                }

                public String getAddress() {
                    return address;
                }

                public void setAddress(String address) {
                    this.address = address;
                }

                public Object getFinishDate() {
                    return finishDate;
                }

                public void setFinishDate(Object finishDate) {
                    this.finishDate = finishDate;
                }

                public Object getFinishTime() {
                    return finishTime;
                }

                public void setFinishTime(Object finishTime) {
                    this.finishTime = finishTime;
                }

                public Object getBudgetType() {
                    return budgetType;
                }

                public void setBudgetType(Object budgetType) {
                    this.budgetType = budgetType;
                }

                public String getBudget() {
                    return budget;
                }

                public void setBudget(String budget) {
                    this.budget = budget;
                }

                public Object getTaskHours() {
                    return taskHours;
                }

                public void setTaskHours(Object taskHours) {
                    this.taskHours = taskHours;
                }

                public int getTaskers() {
                    return taskers;
                }

                public void setTaskers(int taskers) {
                    this.taskers = taskers;
                }

                public String getLatitude() {
                    return latitude;
                }

                public void setLatitude(String latitude) {
                    this.latitude = latitude;
                }

                public String getLongitude() {
                    return longitude;
                }

                public void setLongitude(String longitude) {
                    this.longitude = longitude;
                }

                public String getStatus() {
                    return status;
                }

                public void setStatus(String status) {
                    this.status = status;
                }

                public int getIsDeleted() {
                    return isDeleted;
                }

                public void setIsDeleted(int isDeleted) {
                    this.isDeleted = isDeleted;
                }

                public int getCreatedBy() {
                    return createdBy;
                }

                public void setCreatedBy(int createdBy) {
                    this.createdBy = createdBy;
                }

                public String getCreatedAt() {
                    return createdAt;
                }

                public void setCreatedAt(String createdAt) {
                    this.createdAt = createdAt;
                }

                public Object getUpdatedBy() {
                    return updatedBy;
                }

                public void setUpdatedBy(Object updatedBy) {
                    this.updatedBy = updatedBy;
                }

                public String getUpdatedAt() {
                    return updatedAt;
                }

                public void setUpdatedAt(String updatedAt) {
                    this.updatedAt = updatedAt;
                }
            }
        }
    }
}
