package com.innasoft.dingudoes.Response;

import java.util.List;

public class DashboardResponse {


    /**
     * success : true
     * data : {"asPoster":[{"posterOpen":"41","posterAssigned":"4","posterCompleted":"0"}],"asTasker":[{"taskerOpen":"11","taskerAssigned":"23","taskerCompleted":"0"}]}
     */

    private boolean success;
    private DataBean data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        private List<AsPosterBean> asPoster;
        private List<AsTaskerBean> asTasker;

        public List<AsPosterBean> getAsPoster() {
            return asPoster;
        }

        public void setAsPoster(List<AsPosterBean> asPoster) {
            this.asPoster = asPoster;
        }

        public List<AsTaskerBean> getAsTasker() {
            return asTasker;
        }

        public void setAsTasker(List<AsTaskerBean> asTasker) {
            this.asTasker = asTasker;
        }

        public static class AsPosterBean {
            /**
             * posterOpen : 41
             * posterAssigned : 4
             * posterCompleted : 0
             */

            private String posterOpen;
            private String posterAssigned;
            private String posterCompleted;

            public String getPosterOpen() {
                return posterOpen;
            }

            public void setPosterOpen(String posterOpen) {
                this.posterOpen = posterOpen;
            }

            public String getPosterAssigned() {
                return posterAssigned;
            }

            public void setPosterAssigned(String posterAssigned) {
                this.posterAssigned = posterAssigned;
            }

            public String getPosterCompleted() {
                return posterCompleted;
            }

            public void setPosterCompleted(String posterCompleted) {
                this.posterCompleted = posterCompleted;
            }
        }

        public static class AsTaskerBean {
            /**
             * taskerOpen : 11
             * taskerAssigned : 23
             * taskerCompleted : 0
             */

            private String taskerOpen;
            private String taskerAssigned;
            private String taskerCompleted;

            public String getTaskerOpen() {
                return taskerOpen;
            }

            public void setTaskerOpen(String taskerOpen) {
                this.taskerOpen = taskerOpen;
            }

            public String getTaskerAssigned() {
                return taskerAssigned;
            }

            public void setTaskerAssigned(String taskerAssigned) {
                this.taskerAssigned = taskerAssigned;
            }

            public String getTaskerCompleted() {
                return taskerCompleted;
            }

            public void setTaskerCompleted(String taskerCompleted) {
                this.taskerCompleted = taskerCompleted;
            }
        }
    }
}
