package com.innasoft.dingudoes.Response;

public class GeneratePayoutTokenResponse {


    /**
     * status : SUCCESS
     * subCode : 200
     * message : Token generated
     * data : {"token":"ab9JCVXpkI6ICc5RnIsICN4MzUIJiOicGbhJye.abQfigEVVF0XJBVQUV1TZFEUiojIiV3ciwSO4kjN5QjM3UTM6ICdhlmIskDO1cTO0IzN1EjOiAHelJCLikDMy4yMx4CN1EjL1MjI6ICcpJCLlNHbhZmOis2Ylh2QlJXd0Fmbnl2ciwSO3QjM6ICZJRnb192YjFmIsISRRFlNFpFSQh0NFFDVKx0MyATOGNkI6ICZJRnbllGbjJye.abTH1Mkts4iY_aqFYQpp_FgAX5T9x6SNZflQGZTLNaNakzY1oH68GZ_85JZatqPEy-","expiry":1572497589}
     * success : true
     */

    private String status;
    private String subCode;
    private String message;
    private DataBean data;
    private boolean success;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getSubCode() {
        return subCode;
    }

    public void setSubCode(String subCode) {
        this.subCode = subCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public static class DataBean {
        /**
         * token : ab9JCVXpkI6ICc5RnIsICN4MzUIJiOicGbhJye.abQfigEVVF0XJBVQUV1TZFEUiojIiV3ciwSO4kjN5QjM3UTM6ICdhlmIskDO1cTO0IzN1EjOiAHelJCLikDMy4yMx4CN1EjL1MjI6ICcpJCLlNHbhZmOis2Ylh2QlJXd0Fmbnl2ciwSO3QjM6ICZJRnb192YjFmIsISRRFlNFpFSQh0NFFDVKx0MyATOGNkI6ICZJRnbllGbjJye.abTH1Mkts4iY_aqFYQpp_FgAX5T9x6SNZflQGZTLNaNakzY1oH68GZ_85JZatqPEy-
         * expiry : 1572497589
         */

        private String token;
        private int expiry;

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public int getExpiry() {
            return expiry;
        }

        public void setExpiry(int expiry) {
            this.expiry = expiry;
        }
    }
}
