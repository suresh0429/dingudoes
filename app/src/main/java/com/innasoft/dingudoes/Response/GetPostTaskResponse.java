package com.innasoft.dingudoes.Response;

import java.util.List;

public class GetPostTaskResponse {


    /**
     * success : true
     * data : [{"id":1,"title":"test","description":"","category":null,"mustHaves":null,"isRemote":null,"address":"Madhapur","finishDate":null,"finishTime":null,"budgetType":null,"budget":"100","taskHours":null,"taskers":2,"latitude":"","longitude":"","status":"assigned","isDeleted":1,"createdBy":3,"createdAt":"2019-08-22T04:17:55.000Z","updatedBy":null,"updatedAt":"2019-10-24T18:55:26.000Z","quotes":[{"id":1,"userId":2,"taskId":1,"isAccepted":1,"isDeleted":0,"createdBy":null,"createdAt":"2019-09-01T09:38:23.000Z","updatedBy":1,"updatedAt":"2019-10-24T18:55:26.000Z","task_id":1},{"id":2,"userId":1,"taskId":1,"isAccepted":1,"isDeleted":0,"createdBy":null,"createdAt":"2019-10-05T18:36:55.000Z","updatedBy":1,"updatedAt":"2019-10-24T18:55:26.000Z","task_id":1}]},{"id":22,"title":"testing dev fin","description":"testing file upload","category":"delivery","mustHaves":"['Must have 1','Must have 2','Must have 3']","isRemote":1,"address":"Madhapur","finishDate":"2019-08-22","finishTime":"6","budgetType":"Total","budget":"100","taskHours":"4","taskers":2,"latitude":"17.4486","longitude":"78.3908","status":"open","isDeleted":1,"createdBy":3,"createdAt":"2019-10-14T07:11:52.000Z","updatedBy":3,"updatedAt":"2019-10-14T07:11:52.000Z","quotes":[]},{"id":26,"title":"testing","description":"testing file upload","category":"delivery","mustHaves":"['Must have 1','Must have 2','Must have 3']","isRemote":1,"address":"Madhapur","finishDate":"2019-08-22","finishTime":"6","budgetType":"Total","budget":"100","taskHours":"4","taskers":2,"latitude":"17.4486","longitude":"78.3908","status":"open","isDeleted":0,"createdBy":3,"createdAt":"2019-10-18T04:24:07.000Z","updatedBy":3,"updatedAt":"2019-10-18T04:24:07.000Z","quotes":[]},{"id":27,"title":"testing","description":"testing file upload","category":"delivery","mustHaves":"['Must have 1','Must have 2','Must have 3']","isRemote":1,"address":"Madhapur","finishDate":"2019-08-22","finishTime":"0","budgetType":"Total","budget":"100","taskHours":"4","taskers":2,"latitude":null,"longitude":null,"status":"open","isDeleted":0,"createdBy":3,"createdAt":"2019-10-18T04:34:11.000Z","updatedBy":3,"updatedAt":"2019-10-18T04:34:11.000Z","quotes":[]},{"id":28,"title":"testing","description":"testing file upload","category":"delivery","mustHaves":"['Must have 1','Must have 2','Must have 3']","isRemote":1,"address":"Madhapur","finishDate":"2019-08-22","finishTime":"0","budgetType":"Total","budget":"100","taskHours":"0","taskers":2,"latitude":null,"longitude":null,"status":"open","isDeleted":0,"createdBy":3,"createdAt":"2019-10-18T05:31:10.000Z","updatedBy":3,"updatedAt":"2019-10-18T05:31:10.000Z","quotes":[]},{"id":29,"title":"testing","description":"testing file upload","category":"delivery","mustHaves":null,"isRemote":1,"address":"Madhapur","finishDate":"2019-08-22","finishTime":"0","budgetType":"Total","budget":"100","taskHours":"0","taskers":2,"latitude":null,"longitude":null,"status":"open","isDeleted":0,"createdBy":3,"createdAt":"2019-10-18T05:36:27.000Z","updatedBy":3,"updatedAt":"2019-10-18T05:36:27.000Z","quotes":[]},{"id":30,"title":"testing","description":"testing file upload","category":"delivery","mustHaves":"['Must have 1','Must have 2','Must have 3']","isRemote":1,"address":"Madhapur","finishDate":"2019-08-22","finishTime":"0","budgetType":"Total","budget":"100","taskHours":"0","taskers":2,"latitude":null,"longitude":null,"status":"open","isDeleted":0,"createdBy":3,"createdAt":"2019-10-18T09:18:03.000Z","updatedBy":3,"updatedAt":"2019-10-18T09:18:03.000Z","quotes":[]},{"id":31,"title":"testing","description":"testing file upload","category":"delivery","mustHaves":"[must1,must2]","isRemote":1,"address":"Madhapur","finishDate":"2019-08-22","finishTime":"0","budgetType":"Total","budget":"100","taskHours":"0","taskers":2,"latitude":null,"longitude":null,"status":"open","isDeleted":0,"createdBy":3,"createdAt":"2019-10-18T09:19:14.000Z","updatedBy":3,"updatedAt":"2019-10-18T09:19:14.000Z","quotes":[]},{"id":32,"title":"testing","description":"testing file upload","category":"delivery","mustHaves":"[must1,must2]","isRemote":1,"address":"Madhapur","finishDate":"2019-08-22","finishTime":"0","budgetType":"Total","budget":"100","taskHours":"0","taskers":2,"latitude":null,"longitude":null,"status":"open","isDeleted":0,"createdBy":3,"createdAt":"2019-10-18T09:20:01.000Z","updatedBy":3,"updatedAt":"2019-10-18T09:20:01.000Z","quotes":[]},{"id":33,"title":"testing","description":"testing file upload","category":"delivery","mustHaves":"[must1,must2]","isRemote":1,"address":"Madhapur","finishDate":"2019-08-22","finishTime":"0","budgetType":"Total","budget":"100","taskHours":"0","taskers":2,"latitude":null,"longitude":null,"status":"open","isDeleted":0,"createdBy":3,"createdAt":"2019-10-18T09:26:22.000Z","updatedBy":3,"updatedAt":"2019-10-18T09:26:22.000Z","quotes":[]},{"id":34,"title":"testing","description":"testing file upload","category":"delivery","mustHaves":"[must1,must2]","isRemote":1,"address":"Madhapur","finishDate":"2019-08-22","finishTime":"0","budgetType":"Total","budget":"100","taskHours":"0","taskers":2,"latitude":null,"longitude":null,"status":"open","isDeleted":0,"createdBy":3,"createdAt":"2019-10-18T09:26:41.000Z","updatedBy":3,"updatedAt":"2019-10-18T09:26:41.000Z","quotes":[]},{"id":35,"title":"testing1","description":"testing file upload1","category":"delivery1","mustHaves":"[must1,must2]","isRemote":1,"address":"Madhapur","finishDate":"2019-08-22","finishTime":"0","budgetType":"Total","budget":"100","taskHours":"0","taskers":2,"latitude":null,"longitude":null,"status":"open","isDeleted":0,"createdBy":3,"createdAt":"2019-10-18T09:27:02.000Z","updatedBy":3,"updatedAt":"2019-10-18T09:27:02.000Z","quotes":[]},{"id":36,"title":"task","description":"description","category":"Painting","mustHaves":"[one, two]","isRemote":1,"address":"Hyderabad","finishDate":"2019-10-18","finishTime":"0","budgetType":"Total","budget":"50","taskHours":"0","taskers":1,"latitude":null,"longitude":null,"status":"open","isDeleted":0,"createdBy":3,"createdAt":"2019-10-18T09:33:50.000Z","updatedBy":3,"updatedAt":"2019-10-18T09:33:50.000Z","quotes":[]},{"id":37,"title":"testing","description":"testing file upload","category":"delivery","mustHaves":"['Must have 1','Must have 2','Must have 3']","isRemote":1,"address":"Madhapur","finishDate":"2019-08-22","finishTime":"6","budgetType":"Total","budget":"100","taskHours":"4","taskers":2,"latitude":"18.4386","longitude":"79.1288","status":"open","isDeleted":0,"createdBy":3,"createdAt":"2019-10-21T10:40:59.000Z","updatedBy":3,"updatedAt":"2019-10-21T10:40:59.000Z","quotes":[]},{"id":38,"title":"Create Resume","description":"testing file upload","category":"delivery","mustHaves":"['Must have 1','Must have 2','Must have 3']","isRemote":1,"address":"Madhapur","finishDate":"2019-08-22","finishTime":"0","budgetType":"Total","budget":"100","taskHours":"0","taskers":2,"latitude":null,"longitude":null,"status":"open","isDeleted":0,"createdBy":3,"createdAt":"2019-10-21T12:26:57.000Z","updatedBy":3,"updatedAt":"2019-10-21T12:26:57.000Z","quotes":[]},{"id":39,"title":"Create Resume","description":"testing file upload","category":"delivery","mustHaves":"['Must have 1','Must have 2','Must have 3']","isRemote":1,"address":"Madhapur","finishDate":"2019-08-22","finishTime":"6","budgetType":"Total","budget":"100","taskHours":"4","taskers":2,"latitude":"17.4486","longitude":"78.3908","status":"open","isDeleted":0,"createdBy":3,"createdAt":"2019-10-21T12:27:27.000Z","updatedBy":3,"updatedAt":"2019-10-21T12:27:27.000Z","quotes":[]},{"id":40,"title":"tak","description":"description","category":"Painting","mustHaves":"[one, tw]","isRemote":0,"address":"Madhapur","finishDate":"0000-00-00","finishTime":"13","budgetType":"Total","budget":"50","taskHours":"0","taskers":1,"latitude":"17.4493767","longitude":"78.386902","status":"open","isDeleted":0,"createdBy":3,"createdAt":"2019-10-22T07:31:59.000Z","updatedBy":3,"updatedAt":"2019-10-22T07:31:59.000Z","quotes":[]},{"id":41,"title":"task painting","description":"description","category":"Painting","mustHaves":"[one, two]","isRemote":1,"address":"Madhapur","finishDate":"0000-00-00","finishTime":"15","budgetType":"Total","budget":"50","taskHours":"0","taskers":8,"latitude":"17.4493783","longitude":"78.3869092","status":"open","isDeleted":0,"createdBy":3,"createdAt":"2019-10-22T07:37:48.000Z","updatedBy":3,"updatedAt":"2019-10-22T07:37:48.000Z","quotes":[]},{"id":42,"title":"ganesh","description":"testing","category":"delivery","mustHaves":"['Must have 1','Must have 2','Must have 3']","isRemote":1,"address":"Madhapur","finishDate":"2019-08-22","finishTime":"6","budgetType":"Total","budget":"100","taskHours":"4","taskers":2,"latitude":"17.4486","longitude":"78.3908","status":"open","isDeleted":0,"createdBy":3,"createdAt":"2019-10-22T07:38:41.000Z","updatedBy":3,"updatedAt":"2019-10-22T07:38:41.000Z","quotes":[]},{"id":43,"title":"painting","description":"description","category":"Painting","mustHaves":"[one, two]","isRemote":0,"address":"Madhapur","finishDate":"0000-00-00","finishTime":"4","budgetType":"Total","budget":"50","taskHours":"0","taskers":2,"latitude":"17.4493794","longitude":"78.3869067","status":"open","isDeleted":0,"createdBy":3,"createdAt":"2019-10-22T07:43:04.000Z","updatedBy":3,"updatedAt":"2019-10-22T07:43:04.000Z","quotes":[]},{"id":44,"title":"sample task","description":"description","category":"Painting","mustHaves":"[one, two]","isRemote":0,"address":"Madhapur","finishDate":"0000-00-00","finishTime":"13","budgetType":"Total","budget":"50","taskHours":"0","taskers":1,"latitude":"17.4493788","longitude":"78.3869055","status":"open","isDeleted":0,"createdBy":3,"createdAt":"2019-10-22T07:48:28.000Z","updatedBy":3,"updatedAt":"2019-10-22T07:48:28.000Z","quotes":[]},{"id":45,"title":"carpenting","description":"asdfghjkl","category":"Gardening","mustHaves":"[two, one]","isRemote":0,"address":"madhapur","finishDate":"0000-00-00","finishTime":"13","budgetType":"Total","budget":"50","taskHours":"0","taskers":4,"latitude":"null","longitude":"null","status":"open","isDeleted":0,"createdBy":3,"createdAt":"2019-10-22T08:02:20.000Z","updatedBy":3,"updatedAt":"2019-10-22T08:02:20.000Z","quotes":[]},{"id":46,"title":"ganesh","description":"testing","category":"delivery","mustHaves":"['Must have 1','Must have 2','Must have 3']","isRemote":1,"address":"Madhapur","finishDate":"2019-08-22","finishTime":"6","budgetType":"Total","budget":"50","taskHours":"4","taskers":2,"latitude":"17.4486","longitude":"78.3908","status":"open","isDeleted":0,"createdBy":3,"createdAt":"2019-10-23T11:48:37.000Z","updatedBy":3,"updatedAt":"2019-10-23T11:48:37.000Z","quotes":[]},{"id":47,"title":"ganesh","description":"testing","category":"delivery","mustHaves":"['Must have 1','Must have 2','Must have 3']","isRemote":1,"address":"Madhapur","finishDate":"2019-08-22","finishTime":"6","budgetType":"Total","budget":"50","taskHours":"4","taskers":2,"latitude":"17.4486","longitude":"78.3908","status":"open","isDeleted":0,"createdBy":3,"createdAt":"2019-10-23T11:49:13.000Z","updatedBy":3,"updatedAt":"2019-10-23T11:49:13.000Z","quotes":[]},{"id":48,"title":"ganesh","description":"testing","category":"delivery","mustHaves":"['Must have 1','Must have 2','Must have 3']","isRemote":1,"address":"Madhapur","finishDate":"2019-08-22","finishTime":"6","budgetType":"Total","budget":"50","taskHours":"4","taskers":2,"latitude":"17.4486","longitude":"78.3908","status":"open","isDeleted":0,"createdBy":3,"createdAt":"2019-10-23T11:51:38.000Z","updatedBy":3,"updatedAt":"2019-10-23T11:51:38.000Z","quotes":[]},{"id":49,"title":"ganesh","description":"testing","category":"delivery","mustHaves":"['Must have 1','Must have 2','Must have 3']","isRemote":1,"address":"Madhapur","finishDate":"2019-08-22","finishTime":"6","budgetType":"Total","budget":"50","taskHours":"4","taskers":2,"latitude":"17.4486","longitude":"78.3908","status":"open","isDeleted":0,"createdBy":3,"createdAt":"2019-10-23T11:51:43.000Z","updatedBy":3,"updatedAt":"2019-10-23T11:51:43.000Z","quotes":[]},{"id":50,"title":"ganesh","description":"testing","category":"delivery","mustHaves":"['Must have 1','Must have 2','Must have 3']","isRemote":1,"address":"Madhapur","finishDate":"2019-08-22","finishTime":"6","budgetType":"Total","budget":"50","taskHours":"4","taskers":2,"latitude":"17.4486","longitude":"78.3908","status":"open","isDeleted":0,"createdBy":3,"createdAt":"2019-10-24T04:42:08.000Z","updatedBy":3,"updatedAt":"2019-10-24T04:42:08.000Z","quotes":[]},{"id":51,"title":"ganesh","description":"testing","category":"delivery","mustHaves":"['Must have 1','Must have 2','Must have 3']","isRemote":1,"address":"Madhapur","finishDate":"2019-08-22","finishTime":"6","budgetType":"Total","budget":"50","taskHours":"4","taskers":2,"latitude":"17.4486","longitude":"78.3908","status":"open","isDeleted":0,"createdBy":3,"createdAt":"2019-10-24T04:42:43.000Z","updatedBy":3,"updatedAt":"2019-10-24T04:42:43.000Z","quotes":[]},{"id":52,"title":"ganesh","description":"testing","category":"delivery","mustHaves":"['Must have 1','Must have 2','Must have 3']","isRemote":1,"address":"Madhapur","finishDate":"2019-08-22","finishTime":"6","budgetType":"Total","budget":"50","taskHours":"4","taskers":2,"latitude":"17.4486","longitude":"78.3908","status":"open","isDeleted":0,"createdBy":3,"createdAt":"2019-10-24T04:42:53.000Z","updatedBy":3,"updatedAt":"2019-10-24T04:42:53.000Z","quotes":[]},{"id":53,"title":null,"description":null,"category":null,"mustHaves":null,"isRemote":0,"address":null,"finishDate":"0000-00-00","finishTime":"0","budgetType":null,"budget":null,"taskHours":"0","taskers":null,"latitude":null,"longitude":null,"status":"open","isDeleted":0,"createdBy":3,"createdAt":"2019-10-24T04:43:10.000Z","updatedBy":3,"updatedAt":"2019-10-24T04:43:10.000Z","quotes":[]},{"id":54,"title":null,"description":null,"category":null,"mustHaves":null,"isRemote":0,"address":null,"finishDate":"0000-00-00","finishTime":"0","budgetType":null,"budget":null,"taskHours":"0","taskers":null,"latitude":null,"longitude":null,"status":"open","isDeleted":0,"createdBy":3,"createdAt":"2019-10-24T04:48:23.000Z","updatedBy":3,"updatedAt":"2019-10-24T04:48:23.000Z","quotes":[]},{"id":55,"title":"washing","description":"hii","category":"Painting","mustHaves":"[one ]","isRemote":0,"address":"Madhapur","finishDate":"0000-00-00","finishTime":"11","budgetType":"Total","budget":"50","taskHours":"0","taskers":2,"latitude":"17.4493675","longitude":"78.3868896","status":"open","isDeleted":0,"createdBy":3,"createdAt":"2019-10-24T06:03:37.000Z","updatedBy":3,"updatedAt":"2019-10-24T06:03:37.000Z","quotes":[]},{"id":56,"title":"task","description":"description","category":"Painting","mustHaves":"[one]","isRemote":0,"address":"Madhapur","finishDate":"0000-00-00","finishTime":"11","budgetType":"Total","budget":"50","taskHours":"0","taskers":1,"latitude":"17.4493774","longitude":"78.3869023","status":"open","isDeleted":0,"createdBy":3,"createdAt":"2019-10-24T06:25:55.000Z","updatedBy":3,"updatedAt":"2019-10-24T06:25:55.000Z","quotes":[]},{"id":57,"title":"tadk","description":"description","category":"Painting","mustHaves":"[one]","isRemote":0,"address":"Madhapur","finishDate":"0000-00-00","finishTime":"0","budgetType":"Total","budget":"50","taskHours":"0","taskers":1,"latitude":"17.4493793","longitude":"78.3869013","status":"open","isDeleted":0,"createdBy":3,"createdAt":"2019-10-24T06:28:31.000Z","updatedBy":3,"updatedAt":"2019-10-24T06:28:31.000Z","quotes":[]},{"id":58,"title":"task suresh","description":"description","category":"Painting","mustHaves":"[one, two]","isRemote":0,"address":"Madhapur","finishDate":"0000-00-00","finishTime":"5","budgetType":"Total","budget":"50","taskHours":"0","taskers":2,"latitude":"17.4493769","longitude":"78.3869022","status":"open","isDeleted":0,"createdBy":3,"createdAt":"2019-10-24T06:40:14.000Z","updatedBy":3,"updatedAt":"2019-10-24T06:40:14.000Z","quotes":[]},{"id":59,"title":"testing","description":"testing file upload","category":"delivery","mustHaves":"['Must have 1','Must have 2','Must have 3']","isRemote":1,"address":"Madhapur","finishDate":"2019-08-22","finishTime":"6","budgetType":"Total","budget":"100","taskHours":"4","taskers":2,"latitude":"17.4486","longitude":"78.3908","status":"open","isDeleted":0,"createdBy":3,"createdAt":"2019-10-24T06:46:02.000Z","updatedBy":3,"updatedAt":"2019-10-24T06:46:02.000Z","quotes":[]},{"id":60,"title":"testing","description":"testing file upload","category":"delivery","mustHaves":"['Must have 1','Must have 2','Must have 3']","isRemote":1,"address":"Madhapur","finishDate":"2019-08-22","finishTime":"6","budgetType":"Total","budget":"100","taskHours":"4","taskers":2,"latitude":"17.4486","longitude":"78.3908","status":"open","isDeleted":0,"createdBy":3,"createdAt":"2019-10-24T06:46:44.000Z","updatedBy":3,"updatedAt":"2019-10-24T06:46:44.000Z","quotes":[]},{"id":61,"title":"task","description":"description","category":"Painting","mustHaves":"[one, one]","isRemote":0,"address":"Madhapur","finishDate":"0000-00-00","finishTime":"0","budgetType":"Total","budget":"50","taskHours":"0","taskers":1,"latitude":"17.4493812","longitude":"78.3869059","status":"open","isDeleted":0,"createdBy":3,"createdAt":"2019-10-24T06:53:03.000Z","updatedBy":3,"updatedAt":"2019-10-24T06:53:03.000Z","quotes":[]},{"id":62,"title":"suresh de","description":"description","category":"Painting","mustHaves":"[one]","isRemote":0,"address":"Madhapur","finishDate":"0000-00-00","finishTime":"12","budgetType":"Total","budget":"50","taskHours":"0","taskers":1,"latitude":"17.4493776","longitude":"78.386905","status":"open","isDeleted":0,"createdBy":3,"createdAt":"2019-10-24T06:56:32.000Z","updatedBy":3,"updatedAt":"2019-10-24T06:56:32.000Z","quotes":[]},{"id":63,"title":"task details","description":"ones that cjcjcjvivkvkvkbk","category":"Painting","mustHaves":"[ovicuci, jcjvivivi]","isRemote":0,"address":"Madhapur","finishDate":"0000-00-00","finishTime":"12","budgetType":"Total","budget":"5000","taskHours":"0","taskers":1,"latitude":"17.4493782","longitude":"78.3869036","status":"open","isDeleted":0,"createdBy":3,"createdAt":"2019-10-24T06:58:16.000Z","updatedBy":3,"updatedAt":"2019-10-24T06:58:16.000Z","quotes":[]},{"id":64,"title":"painting task","description":"painting description","category":"Painting","mustHaves":"[must have, must have2]","isRemote":0,"address":"Madhapur","finishDate":"0000-00-00","finishTime":"0","budgetType":"Total","budget":"500","taskHours":"0","taskers":2,"latitude":"17.449413","longitude":"78.3869382","status":"open","isDeleted":0,"createdBy":3,"createdAt":"2019-10-25T05:22:54.000Z","updatedBy":3,"updatedAt":"2019-10-25T05:22:54.000Z","quotes":[]}]
     */

    private boolean success;
    private List<DataBean> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 1
         * title : test
         * description :
         * category : null
         * mustHaves : null
         * isRemote : null
         * address : Madhapur
         * finishDate : null
         * finishTime : null
         * budgetType : null
         * budget : 100
         * taskHours : null
         * taskers : 2
         * latitude :
         * longitude :
         * status : assigned
         * isDeleted : 1
         * createdBy : 3
         * createdAt : 2019-08-22T04:17:55.000Z
         * updatedBy : null
         * updatedAt : 2019-10-24T18:55:26.000Z
         * quotes : [{"id":1,"userId":2,"taskId":1,"isAccepted":1,"isDeleted":0,"createdBy":null,"createdAt":"2019-09-01T09:38:23.000Z","updatedBy":1,"updatedAt":"2019-10-24T18:55:26.000Z","task_id":1},{"id":2,"userId":1,"taskId":1,"isAccepted":1,"isDeleted":0,"createdBy":null,"createdAt":"2019-10-05T18:36:55.000Z","updatedBy":1,"updatedAt":"2019-10-24T18:55:26.000Z","task_id":1}]
         */

        private int id;
        private String title;
        private String description;
        private String category;
        private String mustHaves;
        private String isRemote;
        private String address;
        private String finishDate;
        private String finishTime;
        private String budgetType;
        private String budget;
        private String taskHours;
        private int taskers;
        private String latitude;
        private String longitude;
        private String status;
        private int isDeleted;
        private int createdBy;
        private String createdAt;
        private String updatedBy;
        private String updatedAt;
        private List<QuotesBean> quotes;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getMustHaves() {
            return mustHaves;
        }

        public void setMustHaves(String mustHaves) {
            this.mustHaves = mustHaves;
        }

        public String getIsRemote() {
            return isRemote;
        }

        public void setIsRemote(String isRemote) {
            this.isRemote = isRemote;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getFinishDate() {
            return finishDate;
        }

        public void setFinishDate(String finishDate) {
            this.finishDate = finishDate;
        }

        public String getFinishTime() {
            return finishTime;
        }

        public void setFinishTime(String finishTime) {
            this.finishTime = finishTime;
        }

        public String getBudgetType() {
            return budgetType;
        }

        public void setBudgetType(String budgetType) {
            this.budgetType = budgetType;
        }

        public String getBudget() {
            return budget;
        }

        public void setBudget(String budget) {
            this.budget = budget;
        }

        public String getTaskHours() {
            return taskHours;
        }

        public void setTaskHours(String taskHours) {
            this.taskHours = taskHours;
        }

        public int getTaskers() {
            return taskers;
        }

        public void setTaskers(int taskers) {
            this.taskers = taskers;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public int getIsDeleted() {
            return isDeleted;
        }

        public void setIsDeleted(int isDeleted) {
            this.isDeleted = isDeleted;
        }

        public int getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(int createdBy) {
            this.createdBy = createdBy;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public String getUpdatedBy() {
            return updatedBy;
        }

        public void setUpdatedBy(String updatedBy) {
            this.updatedBy = updatedBy;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public List<QuotesBean> getQuotes() {
            return quotes;
        }

        public void setQuotes(List<QuotesBean> quotes) {
            this.quotes = quotes;
        }

        public static class QuotesBean {
            /**
             * id : 1
             * userId : 2
             * taskId : 1
             * isAccepted : 1
             * isDeleted : 0
             * createdBy : null
             * createdAt : 2019-09-01T09:38:23.000Z
             * updatedBy : 1
             * updatedAt : 2019-10-24T18:55:26.000Z
             * task_id : 1
             */

            private int id;
            private int userId;
            private int taskId;
            private int isAccepted;
            private int isDeleted;
            private String createdBy;
            private String createdAt;
            private int updatedBy;
            private String updatedAt;
            private int task_id;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getUserId() {
                return userId;
            }

            public void setUserId(int userId) {
                this.userId = userId;
            }

            public int getTaskId() {
                return taskId;
            }

            public void setTaskId(int taskId) {
                this.taskId = taskId;
            }

            public int getIsAccepted() {
                return isAccepted;
            }

            public void setIsAccepted(int isAccepted) {
                this.isAccepted = isAccepted;
            }

            public int getIsDeleted() {
                return isDeleted;
            }

            public void setIsDeleted(int isDeleted) {
                this.isDeleted = isDeleted;
            }

            public String getCreatedBy() {
                return createdBy;
            }

            public void setCreatedBy(String createdBy) {
                this.createdBy = createdBy;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public int getUpdatedBy() {
                return updatedBy;
            }

            public void setUpdatedBy(int updatedBy) {
                this.updatedBy = updatedBy;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public int getTask_id() {
                return task_id;
            }

            public void setTask_id(int task_id) {
                this.task_id = task_id;
            }
        }
    }
}
