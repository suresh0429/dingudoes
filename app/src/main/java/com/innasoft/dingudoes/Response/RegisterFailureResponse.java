package com.innasoft.dingudoes.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class RegisterFailureResponse {


    /**
     * name : SequelizeUniqueConstraintError
     * errors : [{"message":"unique_mobile must be unique","type":"unique violation","path":"unique_mobile","value":"1234567894","origin":"DB","instance":{"is_active":0,"is_verified":0,"user_id":null,"email":"ganeshchintu123455@gmail.com","full_name":"ganesh","mobile":1234567894,"password":"$2y$10$2Wkvmu78MQf4pri37EwM8uRPrR1.rjWPM12YuTIPLcDS.NqxPO60a","post_task":0,"complete_task":0,"user_type":2,"updatedAt":"2019-09-23T11:04:26.237Z","createdAt":"2019-09-23T11:04:26.237Z"},"validatorKey":"not_unique","validatorName":null,"validatorArgs":[]}]
     * fields : {"unique_mobile":"1234567894"}
     * parent : {"code":"ER_DUP_ENTRY","errno":1062,"sqlState":"23000","sqlMessage":"Duplicate entry '1234567894' for key 'unique_mobile'","sql":"INSERT INTO `users` (`user_id`,`full_name`,`email`,`mobile`,`password`,`post_task`,`complete_task`,`is_active`,`is_verified`,`user_type`,`created_date`,`updated_date`) VALUES (DEFAULT,?,?,?,?,?,?,?,?,?,?,?);"}
     * original : {"code":"ER_DUP_ENTRY","errno":1062,"sqlState":"23000","sqlMessage":"Duplicate entry '1234567894' for key 'unique_mobile'","sql":"INSERT INTO `users` (`user_id`,`full_name`,`email`,`mobile`,`password`,`post_task`,`complete_task`,`is_active`,`is_verified`,`user_type`,`created_date`,`updated_date`) VALUES (DEFAULT,?,?,?,?,?,?,?,?,?,?,?);"}
     * sql : INSERT INTO `users` (`user_id`,`full_name`,`email`,`mobile`,`password`,`post_task`,`complete_task`,`is_active`,`is_verified`,`user_type`,`created_date`,`updated_date`) VALUES (DEFAULT,?,?,?,?,?,?,?,?,?,?,?);
     */

    @SerializedName("name")
    private String name;
    @SerializedName("fields")
    private FieldsBean fields;
    @SerializedName("parent")
    private ParentBean parent;
    @SerializedName("original")
    private OriginalBean original;
    @SerializedName("sql")
    private String sql;
    @SerializedName("errors")
    private List<ErrorsBean> errors;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public FieldsBean getFields() {
        return fields;
    }

    public void setFields(FieldsBean fields) {
        this.fields = fields;
    }

    public ParentBean getParent() {
        return parent;
    }

    public void setParent(ParentBean parent) {
        this.parent = parent;
    }

    public OriginalBean getOriginal() {
        return original;
    }

    public void setOriginal(OriginalBean original) {
        this.original = original;
    }

    public String getSql() {
        return sql;
    }

    public void setSql(String sql) {
        this.sql = sql;
    }

    public List<ErrorsBean> getErrors() {
        return errors;
    }

    public void setErrors(List<ErrorsBean> errors) {
        this.errors = errors;
    }

    public static class FieldsBean {
        /**
         * unique_mobile : 1234567894
         */

        @SerializedName("unique_mobile")
        private String uniqueMobile;

        public String getUniqueMobile() {
            return uniqueMobile;
        }

        public void setUniqueMobile(String uniqueMobile) {
            this.uniqueMobile = uniqueMobile;
        }
    }

    public static class ParentBean {
        /**
         * code : ER_DUP_ENTRY
         * errno : 1062
         * sqlState : 23000
         * sqlMessage : Duplicate entry '1234567894' for key 'unique_mobile'
         * sql : INSERT INTO `users` (`user_id`,`full_name`,`email`,`mobile`,`password`,`post_task`,`complete_task`,`is_active`,`is_verified`,`user_type`,`created_date`,`updated_date`) VALUES (DEFAULT,?,?,?,?,?,?,?,?,?,?,?);
         */

        @SerializedName("code")
        private String code;
        @SerializedName("errno")
        private int errno;
        @SerializedName("sqlState")
        private String sqlState;
        @SerializedName("sqlMessage")
        private String sqlMessage;
        @SerializedName("sql")
        private String sql;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public int getErrno() {
            return errno;
        }

        public void setErrno(int errno) {
            this.errno = errno;
        }

        public String getSqlState() {
            return sqlState;
        }

        public void setSqlState(String sqlState) {
            this.sqlState = sqlState;
        }

        public String getSqlMessage() {
            return sqlMessage;
        }

        public void setSqlMessage(String sqlMessage) {
            this.sqlMessage = sqlMessage;
        }

        public String getSql() {
            return sql;
        }

        public void setSql(String sql) {
            this.sql = sql;
        }
    }

    public static class OriginalBean {
        /**
         * code : ER_DUP_ENTRY
         * errno : 1062
         * sqlState : 23000
         * sqlMessage : Duplicate entry '1234567894' for key 'unique_mobile'
         * sql : INSERT INTO `users` (`user_id`,`full_name`,`email`,`mobile`,`password`,`post_task`,`complete_task`,`is_active`,`is_verified`,`user_type`,`created_date`,`updated_date`) VALUES (DEFAULT,?,?,?,?,?,?,?,?,?,?,?);
         */

        @SerializedName("code")
        private String code;
        @SerializedName("errno")
        private int errno;
        @SerializedName("sqlState")
        private String sqlState;
        @SerializedName("sqlMessage")
        private String sqlMessage;
        @SerializedName("sql")
        private String sql;

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public int getErrno() {
            return errno;
        }

        public void setErrno(int errno) {
            this.errno = errno;
        }

        public String getSqlState() {
            return sqlState;
        }

        public void setSqlState(String sqlState) {
            this.sqlState = sqlState;
        }

        public String getSqlMessage() {
            return sqlMessage;
        }

        public void setSqlMessage(String sqlMessage) {
            this.sqlMessage = sqlMessage;
        }

        public String getSql() {
            return sql;
        }

        public void setSql(String sql) {
            this.sql = sql;
        }
    }

    public static class ErrorsBean {
        /**
         * message : unique_mobile must be unique
         * type : unique violation
         * path : unique_mobile
         * value : 1234567894
         * origin : DB
         * instance : {"is_active":0,"is_verified":0,"user_id":null,"email":"ganeshchintu123455@gmail.com","full_name":"ganesh","mobile":1234567894,"password":"$2y$10$2Wkvmu78MQf4pri37EwM8uRPrR1.rjWPM12YuTIPLcDS.NqxPO60a","post_task":0,"complete_task":0,"user_type":2,"updatedAt":"2019-09-23T11:04:26.237Z","createdAt":"2019-09-23T11:04:26.237Z"}
         * validatorKey : not_unique
         * validatorName : null
         * validatorArgs : []
         */

        @SerializedName("message")
        private String message;
        @SerializedName("type")
        private String type;
        @SerializedName("path")
        private String path;
        @SerializedName("value")
        private String value;
        @SerializedName("origin")
        private String origin;
        @SerializedName("instance")
        private InstanceBean instance;
        @SerializedName("validatorKey")
        private String validatorKey;
        @SerializedName("validatorName")
        private Object validatorName;
        @SerializedName("validatorArgs")
        private List<?> validatorArgs;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getPath() {
            return path;
        }

        public void setPath(String path) {
            this.path = path;
        }

        public String getValue() {
            return value;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getOrigin() {
            return origin;
        }

        public void setOrigin(String origin) {
            this.origin = origin;
        }

        public InstanceBean getInstance() {
            return instance;
        }

        public void setInstance(InstanceBean instance) {
            this.instance = instance;
        }

        public String getValidatorKey() {
            return validatorKey;
        }

        public void setValidatorKey(String validatorKey) {
            this.validatorKey = validatorKey;
        }

        public Object getValidatorName() {
            return validatorName;
        }

        public void setValidatorName(Object validatorName) {
            this.validatorName = validatorName;
        }

        public List<?> getValidatorArgs() {
            return validatorArgs;
        }

        public void setValidatorArgs(List<?> validatorArgs) {
            this.validatorArgs = validatorArgs;
        }

        public static class InstanceBean {
            /**
             * is_active : 0
             * is_verified : 0
             * user_id : null
             * email : ganeshchintu123455@gmail.com
             * full_name : ganesh
             * mobile : 1234567894
             * password : $2y$10$2Wkvmu78MQf4pri37EwM8uRPrR1.rjWPM12YuTIPLcDS.NqxPO60a
             * post_task : 0
             * complete_task : 0
             * user_type : 2
             * updatedAt : 2019-09-23T11:04:26.237Z
             * createdAt : 2019-09-23T11:04:26.237Z
             */

            @SerializedName("is_active")
            private int isActive;
            @SerializedName("is_verified")
            private int isVerified;
            @SerializedName("user_id")
            private Object userId;
            @SerializedName("email")
            private String email;
            @SerializedName("full_name")
            private String fullName;
            @SerializedName("password")
            private String password;
            @SerializedName("post_task")
            private int postTask;
            @SerializedName("complete_task")
            private int completeTask;
            @SerializedName("user_type")
            private int userType;
            @SerializedName("updatedAt")
            private String updatedAt;
            @SerializedName("createdAt")
            private String createdAt;

            public int getIsActive() {
                return isActive;
            }

            public void setIsActive(int isActive) {
                this.isActive = isActive;
            }

            public int getIsVerified() {
                return isVerified;
            }

            public void setIsVerified(int isVerified) {
                this.isVerified = isVerified;
            }

            public Object getUserId() {
                return userId;
            }

            public void setUserId(Object userId) {
                this.userId = userId;
            }

            public String getEmail() {
                return email;
            }

            public void setEmail(String email) {
                this.email = email;
            }

            public String getFullName() {
                return fullName;
            }

            public void setFullName(String fullName) {
                this.fullName = fullName;
            }

            public String getPassword() {
                return password;
            }

            public void setPassword(String password) {
                this.password = password;
            }

            public int getPostTask() {
                return postTask;
            }

            public void setPostTask(int postTask) {
                this.postTask = postTask;
            }

            public int getCompleteTask() {
                return completeTask;
            }

            public void setCompleteTask(int completeTask) {
                this.completeTask = completeTask;
            }

            public int getUserType() {
                return userType;
            }

            public void setUserType(int userType) {
                this.userType = userType;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }
        }
    }
}
