package com.innasoft.dingudoes.Response;

import java.util.List;

public class HomeCategoriesResponse {


    /**
     * success : true
     * data : [{"name":"Painting","imageUrl":"/resources/painting.png"},{"name":"Cleaning","imageUrl":"/resources/cleaning.png"},{"name":"Gardening","imageUrl":"/resources/gardening.png"},{"name":"Computer","imageUrl":"/resources/painting.png"},{"name":"Photography","imageUrl":"/resources/painting.png"}]
     */

    private boolean success;
    private List<DataBean> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * name : Painting
         * imageUrl : /resources/painting.png
         */

        private String name;
        private String imageUrl;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImageUrl() {
            return imageUrl;
        }

        public void setImageUrl(String imageUrl) {
            this.imageUrl = imageUrl;
        }
    }
}
