package com.innasoft.dingudoes.Response;

public class TokenResponse {


    /**
     * status : OK
     * message : Token generated
     * cftoken : bk9JCN4MzUIJiOicGbhJCLiQ1VKJiOiAXe0Jye.pAQfiMGZxEmYyQWM4EmYkVjI6ICdsF2cfJCL4EDM2kDM1cTNxojIwhXZiwiIS5USiojI5NmblJnc1NkclRmcvJCLiAjMiojI05Wdv1WQyVGZy9mIsIyMxITMwADMyVGZy9EIiojIklkclRmcvJye.3DXunwmK6kYMfDzYPPNuKGSEz9GfjwhaGLePtnnuqD9_w0yLM3DqPIyNfpg-gQ1j1d
     * success : true
     */

    private String status;
    private String message;
    private String cftoken;
    private boolean success;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getCftoken() {
        return cftoken;
    }

    public void setCftoken(String cftoken) {
        this.cftoken = cftoken;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
}
