package com.innasoft.dingudoes.Response;

import java.util.List;

public class FCmTokenResponse {


    /**
     * success : true
     * message : Fcm token Updated Successfully
     * data : [1]
     */

    private boolean success;
    private String message;
    private List<Integer> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Integer> getData() {
        return data;
    }

    public void setData(List<Integer> data) {
        this.data = data;
    }
}
