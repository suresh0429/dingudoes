package com.innasoft.dingudoes.Response;

import com.google.gson.annotations.SerializedName;

public class PosATaskResponse {


    /**
     * success : true
     * message : Task created successfully
     * data : {"createdAt":"2019-11-29T07:12:46.730Z","updatedAt":{"val":"CURRENT_TIMESTAMP"},"id":71,"title":"testing","description":"testing file upload","category":"delivery","mustHaves":"['Must have 1','Must have 2','Must have 3']","isRemote":1,"address":"Madhapur","finishDate":"2019-08-22","finishTime":6,"budgetType":"Total","budget":"100","taskHours":4,"taskers":"2","latitude":"17.4486","longitude":"78.3908","status":"open","isDeleted":0,"createdBy":3,"updatedBy":3}
     */

    @SerializedName("success")
    private boolean success;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private DataBean data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * createdAt : 2019-11-29T07:12:46.730Z
         * updatedAt : {"val":"CURRENT_TIMESTAMP"}
         * id : 71
         * title : testing
         * description : testing file upload
         * category : delivery
         * mustHaves : ['Must have 1','Must have 2','Must have 3']
         * isRemote : 1
         * address : Madhapur
         * finishDate : 2019-08-22
         * finishTime : 6
         * budgetType : Total
         * budget : 100
         * taskHours : 4
         * taskers : 2
         * latitude : 17.4486
         * longitude : 78.3908
         * status : open
         * isDeleted : 0
         * createdBy : 3
         * updatedBy : 3
         */

        @SerializedName("createdAt")
        private String createdAt;
        @SerializedName("updatedAt")
        private UpdatedAtBean updatedAt;
        @SerializedName("id")
        private int id;
        @SerializedName("title")
        private String title;
        @SerializedName("description")
        private String description;
        @SerializedName("category")
        private String category;
        @SerializedName("mustHaves")
        private String mustHaves;
        @SerializedName("isRemote")
        private int isRemote;
        @SerializedName("address")
        private String address;
        @SerializedName("finishDate")
        private String finishDate;
        @SerializedName("finishTime")
        private int finishTime;
        @SerializedName("budgetType")
        private String budgetType;
        @SerializedName("budget")
        private String budget;
        @SerializedName("taskHours")
        private int taskHours;
        @SerializedName("taskers")
        private String taskers;
        @SerializedName("latitude")
        private String latitude;
        @SerializedName("longitude")
        private String longitude;
        @SerializedName("status")
        private String status;
        @SerializedName("isDeleted")
        private int isDeleted;
        @SerializedName("createdBy")
        private int createdBy;
        @SerializedName("updatedBy")
        private int updatedBy;

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public UpdatedAtBean getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(UpdatedAtBean updatedAt) {
            this.updatedAt = updatedAt;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getMustHaves() {
            return mustHaves;
        }

        public void setMustHaves(String mustHaves) {
            this.mustHaves = mustHaves;
        }

        public int getIsRemote() {
            return isRemote;
        }

        public void setIsRemote(int isRemote) {
            this.isRemote = isRemote;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getFinishDate() {
            return finishDate;
        }

        public void setFinishDate(String finishDate) {
            this.finishDate = finishDate;
        }

        public int getFinishTime() {
            return finishTime;
        }

        public void setFinishTime(int finishTime) {
            this.finishTime = finishTime;
        }

        public String getBudgetType() {
            return budgetType;
        }

        public void setBudgetType(String budgetType) {
            this.budgetType = budgetType;
        }

        public String getBudget() {
            return budget;
        }

        public void setBudget(String budget) {
            this.budget = budget;
        }

        public int getTaskHours() {
            return taskHours;
        }

        public void setTaskHours(int taskHours) {
            this.taskHours = taskHours;
        }

        public String getTaskers() {
            return taskers;
        }

        public void setTaskers(String taskers) {
            this.taskers = taskers;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public int getIsDeleted() {
            return isDeleted;
        }

        public void setIsDeleted(int isDeleted) {
            this.isDeleted = isDeleted;
        }

        public int getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(int createdBy) {
            this.createdBy = createdBy;
        }

        public int getUpdatedBy() {
            return updatedBy;
        }

        public void setUpdatedBy(int updatedBy) {
            this.updatedBy = updatedBy;
        }

        public static class UpdatedAtBean {
            /**
             * val : CURRENT_TIMESTAMP
             */

            @SerializedName("val")
            private String val;

            public String getVal() {
                return val;
            }

            public void setVal(String val) {
                this.val = val;
            }
        }
    }
}
