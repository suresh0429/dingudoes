package com.innasoft.dingudoes.Response;

public class ChangeMobilenumberResponse {


    /**
     * success : true
     * message : OTP has Sent Successfully
     */

    private boolean success;
    private String message;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
