package com.innasoft.dingudoes.Response;

public class CreateTaskAlertsResponse {


    /**
     * success : true
     * message : Task Alert created successfully
     * data : {"createdAt":"2019-11-05T07:22:13.681Z","updatedAt":{"val":"CURRENT_TIMESTAMP"},"id":8,"userId":3,"isRemote":"1","keywords":"testing","location":"Madhapur","distance":"10","isDeleted":0,"createdBy":3,"updatedBy":3}
     */

    private boolean success;
    private String message;
    private DataBean data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * createdAt : 2019-11-05T07:22:13.681Z
         * updatedAt : {"val":"CURRENT_TIMESTAMP"}
         * id : 8
         * userId : 3
         * isRemote : 1
         * keywords : testing
         * location : Madhapur
         * distance : 10
         * isDeleted : 0
         * createdBy : 3
         * updatedBy : 3
         */

        private String createdAt;
        private UpdatedAtBean updatedAt;
        private int id;
        private int userId;
        private String isRemote;
        private String keywords;
        private String location;
        private String distance;
        private int isDeleted;
        private int createdBy;
        private int updatedBy;

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public UpdatedAtBean getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(UpdatedAtBean updatedAt) {
            this.updatedAt = updatedAt;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public String getIsRemote() {
            return isRemote;
        }

        public void setIsRemote(String isRemote) {
            this.isRemote = isRemote;
        }

        public String getKeywords() {
            return keywords;
        }

        public void setKeywords(String keywords) {
            this.keywords = keywords;
        }

        public String getLocation() {
            return location;
        }

        public void setLocation(String location) {
            this.location = location;
        }

        public String getDistance() {
            return distance;
        }

        public void setDistance(String distance) {
            this.distance = distance;
        }

        public int getIsDeleted() {
            return isDeleted;
        }

        public void setIsDeleted(int isDeleted) {
            this.isDeleted = isDeleted;
        }

        public int getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(int createdBy) {
            this.createdBy = createdBy;
        }

        public int getUpdatedBy() {
            return updatedBy;
        }

        public void setUpdatedBy(int updatedBy) {
            this.updatedBy = updatedBy;
        }

        public static class UpdatedAtBean {
            /**
             * val : CURRENT_TIMESTAMP
             */

            private String val;

            public String getVal() {
                return val;
            }

            public void setVal(String val) {
                this.val = val;
            }
        }
    }
}
