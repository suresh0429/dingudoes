package com.innasoft.dingudoes.Response;

import java.util.List;

public class UpdatetaskSattusResponse {


    /**
     * success : true
     * data : [1]
     */

    private boolean success;
    private List<Integer> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<Integer> getData() {
        return data;
    }

    public void setData(List<Integer> data) {
        this.data = data;
    }
}
