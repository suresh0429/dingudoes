package com.innasoft.dingudoes.Response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class SetPasswordResponse {


    /**
     * success : true
     * message : Password Updated Successfully
     * data : [1]
     */

    @SerializedName("success")
    private boolean success;
    @SerializedName("message")
    private String message;
    @SerializedName("data")
    private List<Integer> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Integer> getData() {
        return data;
    }

    public void setData(List<Integer> data) {
        this.data = data;
    }
}
