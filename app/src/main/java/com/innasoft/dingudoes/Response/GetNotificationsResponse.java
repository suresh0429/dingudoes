package com.innasoft.dingudoes.Response;

import java.util.List;

public class GetNotificationsResponse {


    /**
     * success : true
     * data : [{"id":1,"type":"task","refId":1,"userId":"1","createdBy":1,"createdAt":"2019-11-11T00:00:00.000Z","updatedBy":1,"updatedAt":"2019-11-11T00:00:00.000Z","ref_id":1,"task":{"id":1,"title":"test","description":"","category":null,"mustHaves":null,"isRemote":null,"address":"Madhapur","finishDate":null,"finishTime":null,"budgetType":null,"budget":"100","taskHours":null,"taskers":2,"latitude":"","longitude":"","status":"completed","isDeleted":1,"createdBy":3,"createdAt":"2019-08-22T04:17:55.000Z","updatedBy":null,"updatedAt":"2019-11-06T12:17:35.000Z"}}]
     */

    private boolean success;
    private List<DataBean> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 1
         * type : task
         * refId : 1
         * userId : 1
         * createdBy : 1
         * createdAt : 2019-11-11T00:00:00.000Z
         * updatedBy : 1
         * updatedAt : 2019-11-11T00:00:00.000Z
         * ref_id : 1
         * task : {"id":1,"title":"test","description":"","category":null,"mustHaves":null,"isRemote":null,"address":"Madhapur","finishDate":null,"finishTime":null,"budgetType":null,"budget":"100","taskHours":null,"taskers":2,"latitude":"","longitude":"","status":"completed","isDeleted":1,"createdBy":3,"createdAt":"2019-08-22T04:17:55.000Z","updatedBy":null,"updatedAt":"2019-11-06T12:17:35.000Z"}
         */

        private int id;
        private String type;
        private int refId;
        private String userId;
        private int createdBy;
        private String createdAt;
        private int updatedBy;
        private String updatedAt;
        private int ref_id;
        private TaskBean task;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public int getRefId() {
            return refId;
        }

        public void setRefId(int refId) {
            this.refId = refId;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public int getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(int createdBy) {
            this.createdBy = createdBy;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public int getUpdatedBy() {
            return updatedBy;
        }

        public void setUpdatedBy(int updatedBy) {
            this.updatedBy = updatedBy;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public int getRef_id() {
            return ref_id;
        }

        public void setRef_id(int ref_id) {
            this.ref_id = ref_id;
        }

        public TaskBean getTask() {
            return task;
        }

        public void setTask(TaskBean task) {
            this.task = task;
        }

        public static class TaskBean {
            /**
             * id : 1
             * title : test
             * description :
             * category : null
             * mustHaves : null
             * isRemote : null
             * address : Madhapur
             * finishDate : null
             * finishTime : null
             * budgetType : null
             * budget : 100
             * taskHours : null
             * taskers : 2
             * latitude :
             * longitude :
             * status : completed
             * isDeleted : 1
             * createdBy : 3
             * createdAt : 2019-08-22T04:17:55.000Z
             * updatedBy : null
             * updatedAt : 2019-11-06T12:17:35.000Z
             */

            private int id;
            private String title;
            private String description;
            private Object category;
            private Object mustHaves;
            private Object isRemote;
            private String address;
            private Object finishDate;
            private Object finishTime;
            private Object budgetType;
            private String budget;
            private Object taskHours;
            private int taskers;
            private String latitude;
            private String longitude;
            private String status;
            private int isDeleted;
            private int createdBy;
            private String createdAt;
            private Object updatedBy;
            private String updatedAt;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public String getTitle() {
                return title;
            }

            public void setTitle(String title) {
                this.title = title;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            public Object getCategory() {
                return category;
            }

            public void setCategory(Object category) {
                this.category = category;
            }

            public Object getMustHaves() {
                return mustHaves;
            }

            public void setMustHaves(Object mustHaves) {
                this.mustHaves = mustHaves;
            }

            public Object getIsRemote() {
                return isRemote;
            }

            public void setIsRemote(Object isRemote) {
                this.isRemote = isRemote;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public Object getFinishDate() {
                return finishDate;
            }

            public void setFinishDate(Object finishDate) {
                this.finishDate = finishDate;
            }

            public Object getFinishTime() {
                return finishTime;
            }

            public void setFinishTime(Object finishTime) {
                this.finishTime = finishTime;
            }

            public Object getBudgetType() {
                return budgetType;
            }

            public void setBudgetType(Object budgetType) {
                this.budgetType = budgetType;
            }

            public String getBudget() {
                return budget;
            }

            public void setBudget(String budget) {
                this.budget = budget;
            }

            public Object getTaskHours() {
                return taskHours;
            }

            public void setTaskHours(Object taskHours) {
                this.taskHours = taskHours;
            }

            public int getTaskers() {
                return taskers;
            }

            public void setTaskers(int taskers) {
                this.taskers = taskers;
            }

            public String getLatitude() {
                return latitude;
            }

            public void setLatitude(String latitude) {
                this.latitude = latitude;
            }

            public String getLongitude() {
                return longitude;
            }

            public void setLongitude(String longitude) {
                this.longitude = longitude;
            }

            public String getStatus() {
                return status;
            }

            public void setStatus(String status) {
                this.status = status;
            }

            public int getIsDeleted() {
                return isDeleted;
            }

            public void setIsDeleted(int isDeleted) {
                this.isDeleted = isDeleted;
            }

            public int getCreatedBy() {
                return createdBy;
            }

            public void setCreatedBy(int createdBy) {
                this.createdBy = createdBy;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public Object getUpdatedBy() {
                return updatedBy;
            }

            public void setUpdatedBy(Object updatedBy) {
                this.updatedBy = updatedBy;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }
        }
    }
}
