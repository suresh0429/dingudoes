package com.innasoft.dingudoes.Response;

public class OptionsModel
{
   //private String cate_id;
    private String categ_name;
    private int categ_image;

    public OptionsModel(String categ_name, int categ_image) {
        //this.cate_id = cate_id;
        this.categ_name = categ_name;
        this.categ_image = categ_image;
    }

   /* public String getCate_id() {
        return cate_id;
    }

    public void setCate_id(String cate_id) {
        this.cate_id = cate_id;
    }*/

    public String getCateg_name() {
        return categ_name;
    }

    public void setCateg_name(String categ_name) {
        this.categ_name = categ_name;
    }

    public int getCateg_image() {
        return categ_image;
    }

    public void setCateg_image(int categ_image) {
        this.categ_image = categ_image;
    }
}
