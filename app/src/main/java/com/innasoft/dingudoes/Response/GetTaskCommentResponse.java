package com.innasoft.dingudoes.Response;

import java.util.List;

public class GetTaskCommentResponse {


    /**
     * success : true
     * message :
     * data : {"comments":[{"id":4,"parentId":0,"taskId":20,"commentText":"testing 2","isDeleted":0,"createdBy":1,"createdAt":"2019-10-12T08:32:54.000Z","updatedBy":1,"updatedAt":"2019-10-12T08:32:53.000Z","created_by":1,"user":{"user_id":1,"full_name":"Vinod Kumar Ravuri"}},{"id":16,"parentId":null,"taskId":20,"commentText":"q","isDeleted":0,"createdBy":3,"createdAt":"2019-10-24T09:11:40.000Z","updatedBy":3,"updatedAt":"2019-10-24T09:11:40.000Z","created_by":3,"user":{"user_id":3,"full_name":"ganesh chintu"}},{"id":48,"parentId":null,"taskId":20,"commentText":"bb","isDeleted":0,"createdBy":3,"createdAt":"2019-11-01T11:04:27.000Z","updatedBy":3,"updatedAt":"2019-11-01T11:04:27.000Z","created_by":3,"user":{"user_id":3,"full_name":"ganesh chintu"}},{"id":65,"parentId":null,"taskId":20,"commentText":"Ghhfvb","isDeleted":0,"createdBy":3,"createdAt":"2019-11-08T08:23:56.000Z","updatedBy":3,"updatedAt":"2019-11-08T08:23:56.000Z","created_by":3,"user":{"user_id":3,"full_name":"ganesh chintu"}},{"id":66,"parentId":null,"taskId":20,"commentText":"Hello","isDeleted":0,"createdBy":3,"createdAt":"2019-11-08T08:24:14.000Z","updatedBy":3,"updatedAt":"2019-11-08T08:24:14.000Z","created_by":3,"user":{"user_id":3,"full_name":"ganesh chintu"}}]}
     */

    private boolean success;
    private String message;
    private DataBean data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        private List<CommentsBean> comments;

        public List<CommentsBean> getComments() {
            return comments;
        }

        public void setComments(List<CommentsBean> comments) {
            this.comments = comments;
        }

        public static class CommentsBean {
            /**
             * id : 4
             * parentId : 0
             * taskId : 20
             * commentText : testing 2
             * isDeleted : 0
             * createdBy : 1
             * createdAt : 2019-10-12T08:32:54.000Z
             * updatedBy : 1
             * updatedAt : 2019-10-12T08:32:53.000Z
             * created_by : 1
             * user : {"user_id":1,"full_name":"Vinod Kumar Ravuri"}
             */

            private int id;
            private int parentId;
            private int taskId;
            private String commentText;
            private int isDeleted;
            private int createdBy;
            private String createdAt;
            private int updatedBy;
            private String updatedAt;
            private int created_by;
            private UserBean user;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getParentId() {
                return parentId;
            }

            public void setParentId(int parentId) {
                this.parentId = parentId;
            }

            public int getTaskId() {
                return taskId;
            }

            public void setTaskId(int taskId) {
                this.taskId = taskId;
            }

            public String getCommentText() {
                return commentText;
            }

            public void setCommentText(String commentText) {
                this.commentText = commentText;
            }

            public int getIsDeleted() {
                return isDeleted;
            }

            public void setIsDeleted(int isDeleted) {
                this.isDeleted = isDeleted;
            }

            public int getCreatedBy() {
                return createdBy;
            }

            public void setCreatedBy(int createdBy) {
                this.createdBy = createdBy;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public int getUpdatedBy() {
                return updatedBy;
            }

            public void setUpdatedBy(int updatedBy) {
                this.updatedBy = updatedBy;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public int getCreated_by() {
                return created_by;
            }

            public void setCreated_by(int created_by) {
                this.created_by = created_by;
            }

            public UserBean getUser() {
                return user;
            }

            public void setUser(UserBean user) {
                this.user = user;
            }

            public static class UserBean {
                /**
                 * user_id : 1
                 * full_name : Vinod Kumar Ravuri
                 */

                private int user_id;
                private String full_name;

                public int getUser_id() {
                    return user_id;
                }

                public void setUser_id(int user_id) {
                    this.user_id = user_id;
                }

                public String getFull_name() {
                    return full_name;
                }

                public void setFull_name(String full_name) {
                    this.full_name = full_name;
                }
            }
        }
    }
}
