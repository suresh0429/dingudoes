package com.innasoft.dingudoes.Response;

import java.util.List;

public class GetTaskDetailsResponse {


    /**
     * success : true
     * message :
     * data : {"id":19,"title":"Dev Test 1","description":"Dev testing 1","category":"delivery","mustHaves":"['Must have 1','Must have 2','Must have 3']","isRemote":1,"address":"Madhapur","finishDate":"2019-08-22","finishTime":"6","budgetType":"Total","budget":"100","taskHours":"4","taskers":2,"latitude":"17.4486","longitude":"78.3908","status":"assigned","isDeleted":1,"createdBy":1,"createdAt":"2019-10-12T05:35:53.000Z","updatedBy":1,"updatedAt":"2019-10-25T09:24:54.000Z","quotes":[{"id":6,"userId":3,"taskId":19,"isAccepted":1,"isDeleted":0,"createdBy":3,"createdAt":"2019-10-16T04:56:59.000Z","updatedBy":3,"updatedAt":"2019-10-25T09:24:54.000Z","user_id":3,"task_id":19},{"id":7,"userId":3,"taskId":19,"isAccepted":1,"isDeleted":0,"createdBy":3,"createdAt":"2019-10-17T09:38:19.000Z","updatedBy":3,"updatedAt":"2019-10-25T09:24:54.000Z","user_id":3,"task_id":19},{"id":10,"userId":3,"taskId":19,"isAccepted":1,"isDeleted":0,"createdBy":3,"createdAt":"2019-10-17T10:06:50.000Z","updatedBy":3,"updatedAt":"2019-10-25T09:24:54.000Z","user_id":3,"task_id":19},{"id":11,"userId":3,"taskId":19,"isAccepted":1,"isDeleted":0,"createdBy":3,"createdAt":"2019-10-17T10:08:46.000Z","updatedBy":3,"updatedAt":"2019-10-25T09:24:54.000Z","user_id":3,"task_id":19},{"id":13,"userId":3,"taskId":19,"isAccepted":1,"isDeleted":0,"createdBy":3,"createdAt":"2019-10-17T12:19:55.000Z","updatedBy":3,"updatedAt":"2019-10-25T09:24:54.000Z","user_id":3,"task_id":19},{"id":14,"userId":3,"taskId":19,"isAccepted":1,"isDeleted":0,"createdBy":3,"createdAt":"2019-10-17T12:22:02.000Z","updatedBy":3,"updatedAt":"2019-10-25T09:24:54.000Z","user_id":3,"task_id":19},{"id":15,"userId":3,"taskId":19,"isAccepted":1,"isDeleted":0,"createdBy":3,"createdAt":"2019-10-18T03:42:46.000Z","updatedBy":3,"updatedAt":"2019-10-25T09:24:54.000Z","user_id":3,"task_id":19},{"id":16,"userId":3,"taskId":19,"isAccepted":1,"isDeleted":0,"createdBy":3,"createdAt":"2019-10-18T04:50:36.000Z","updatedBy":3,"updatedAt":"2019-10-25T09:24:54.000Z","user_id":3,"task_id":19},{"id":17,"userId":3,"taskId":19,"isAccepted":1,"isDeleted":0,"createdBy":3,"createdAt":"2019-10-18T06:15:02.000Z","updatedBy":3,"updatedAt":"2019-10-25T09:24:54.000Z","user_id":3,"task_id":19},{"id":18,"userId":3,"taskId":19,"isAccepted":1,"isDeleted":0,"createdBy":3,"createdAt":"2019-10-18T09:40:00.000Z","updatedBy":3,"updatedAt":"2019-10-25T09:24:54.000Z","user_id":3,"task_id":19},{"id":19,"userId":3,"taskId":19,"isAccepted":1,"isDeleted":0,"createdBy":3,"createdAt":"2019-10-18T09:41:19.000Z","updatedBy":3,"updatedAt":"2019-10-25T09:24:54.000Z","user_id":3,"task_id":19},{"id":21,"userId":3,"taskId":19,"isAccepted":1,"isDeleted":0,"createdBy":3,"createdAt":"2019-10-18T09:48:53.000Z","updatedBy":3,"updatedAt":"2019-10-25T09:24:54.000Z","user_id":3,"task_id":19},{"id":22,"userId":3,"taskId":19,"isAccepted":1,"isDeleted":0,"createdBy":3,"createdAt":"2019-10-18T10:14:27.000Z","updatedBy":3,"updatedAt":"2019-10-25T09:24:54.000Z","user_id":3,"task_id":19},{"id":23,"userId":3,"taskId":19,"isAccepted":1,"isDeleted":0,"createdBy":3,"createdAt":"2019-10-18T10:15:20.000Z","updatedBy":3,"updatedAt":"2019-10-25T09:24:54.000Z","user_id":3,"task_id":19},{"id":24,"userId":3,"taskId":19,"isAccepted":1,"isDeleted":0,"createdBy":3,"createdAt":"2019-10-18T10:15:39.000Z","updatedBy":3,"updatedAt":"2019-10-25T09:24:54.000Z","user_id":3,"task_id":19},{"id":25,"userId":3,"taskId":19,"isAccepted":1,"isDeleted":0,"createdBy":3,"createdAt":"2019-10-22T11:38:33.000Z","updatedBy":3,"updatedAt":"2019-10-25T09:24:54.000Z","user_id":3,"task_id":19},{"id":26,"userId":3,"taskId":19,"isAccepted":1,"isDeleted":0,"createdBy":3,"createdAt":"2019-10-24T05:27:47.000Z","updatedBy":3,"updatedAt":"2019-10-25T09:24:54.000Z","user_id":3,"task_id":19},{"id":27,"userId":3,"taskId":19,"isAccepted":1,"isDeleted":0,"createdBy":3,"createdAt":"2019-10-24T05:53:54.000Z","updatedBy":3,"updatedAt":"2019-10-25T09:24:54.000Z","user_id":3,"task_id":19},{"id":28,"userId":3,"taskId":19,"isAccepted":1,"isDeleted":0,"createdBy":3,"createdAt":"2019-10-24T05:54:10.000Z","updatedBy":3,"updatedAt":"2019-10-25T09:24:54.000Z","user_id":3,"task_id":19}],"attachments":["uploads\\1570858552881-download (1).png","uploads\\1570858552883-download.png","uploads\\1570858552884-download (2).jpg"]}
     */

    private boolean success;
    private String message;
    private DataBean data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 19
         * title : Dev Test 1
         * description : Dev testing 1
         * category : delivery
         * mustHaves : ['Must have 1','Must have 2','Must have 3']
         * isRemote : 1
         * address : Madhapur
         * finishDate : 2019-08-22
         * finishTime : 6
         * budgetType : Total
         * budget : 100
         * taskHours : 4
         * taskers : 2
         * latitude : 17.4486
         * longitude : 78.3908
         * status : assigned
         * isDeleted : 1
         * createdBy : 1
         * createdAt : 2019-10-12T05:35:53.000Z
         * updatedBy : 1
         * updatedAt : 2019-10-25T09:24:54.000Z
         * quotes : [{"id":6,"userId":3,"taskId":19,"isAccepted":1,"isDeleted":0,"createdBy":3,"createdAt":"2019-10-16T04:56:59.000Z","updatedBy":3,"updatedAt":"2019-10-25T09:24:54.000Z","user_id":3,"task_id":19},{"id":7,"userId":3,"taskId":19,"isAccepted":1,"isDeleted":0,"createdBy":3,"createdAt":"2019-10-17T09:38:19.000Z","updatedBy":3,"updatedAt":"2019-10-25T09:24:54.000Z","user_id":3,"task_id":19},{"id":10,"userId":3,"taskId":19,"isAccepted":1,"isDeleted":0,"createdBy":3,"createdAt":"2019-10-17T10:06:50.000Z","updatedBy":3,"updatedAt":"2019-10-25T09:24:54.000Z","user_id":3,"task_id":19},{"id":11,"userId":3,"taskId":19,"isAccepted":1,"isDeleted":0,"createdBy":3,"createdAt":"2019-10-17T10:08:46.000Z","updatedBy":3,"updatedAt":"2019-10-25T09:24:54.000Z","user_id":3,"task_id":19},{"id":13,"userId":3,"taskId":19,"isAccepted":1,"isDeleted":0,"createdBy":3,"createdAt":"2019-10-17T12:19:55.000Z","updatedBy":3,"updatedAt":"2019-10-25T09:24:54.000Z","user_id":3,"task_id":19},{"id":14,"userId":3,"taskId":19,"isAccepted":1,"isDeleted":0,"createdBy":3,"createdAt":"2019-10-17T12:22:02.000Z","updatedBy":3,"updatedAt":"2019-10-25T09:24:54.000Z","user_id":3,"task_id":19},{"id":15,"userId":3,"taskId":19,"isAccepted":1,"isDeleted":0,"createdBy":3,"createdAt":"2019-10-18T03:42:46.000Z","updatedBy":3,"updatedAt":"2019-10-25T09:24:54.000Z","user_id":3,"task_id":19},{"id":16,"userId":3,"taskId":19,"isAccepted":1,"isDeleted":0,"createdBy":3,"createdAt":"2019-10-18T04:50:36.000Z","updatedBy":3,"updatedAt":"2019-10-25T09:24:54.000Z","user_id":3,"task_id":19},{"id":17,"userId":3,"taskId":19,"isAccepted":1,"isDeleted":0,"createdBy":3,"createdAt":"2019-10-18T06:15:02.000Z","updatedBy":3,"updatedAt":"2019-10-25T09:24:54.000Z","user_id":3,"task_id":19},{"id":18,"userId":3,"taskId":19,"isAccepted":1,"isDeleted":0,"createdBy":3,"createdAt":"2019-10-18T09:40:00.000Z","updatedBy":3,"updatedAt":"2019-10-25T09:24:54.000Z","user_id":3,"task_id":19},{"id":19,"userId":3,"taskId":19,"isAccepted":1,"isDeleted":0,"createdBy":3,"createdAt":"2019-10-18T09:41:19.000Z","updatedBy":3,"updatedAt":"2019-10-25T09:24:54.000Z","user_id":3,"task_id":19},{"id":21,"userId":3,"taskId":19,"isAccepted":1,"isDeleted":0,"createdBy":3,"createdAt":"2019-10-18T09:48:53.000Z","updatedBy":3,"updatedAt":"2019-10-25T09:24:54.000Z","user_id":3,"task_id":19},{"id":22,"userId":3,"taskId":19,"isAccepted":1,"isDeleted":0,"createdBy":3,"createdAt":"2019-10-18T10:14:27.000Z","updatedBy":3,"updatedAt":"2019-10-25T09:24:54.000Z","user_id":3,"task_id":19},{"id":23,"userId":3,"taskId":19,"isAccepted":1,"isDeleted":0,"createdBy":3,"createdAt":"2019-10-18T10:15:20.000Z","updatedBy":3,"updatedAt":"2019-10-25T09:24:54.000Z","user_id":3,"task_id":19},{"id":24,"userId":3,"taskId":19,"isAccepted":1,"isDeleted":0,"createdBy":3,"createdAt":"2019-10-18T10:15:39.000Z","updatedBy":3,"updatedAt":"2019-10-25T09:24:54.000Z","user_id":3,"task_id":19},{"id":25,"userId":3,"taskId":19,"isAccepted":1,"isDeleted":0,"createdBy":3,"createdAt":"2019-10-22T11:38:33.000Z","updatedBy":3,"updatedAt":"2019-10-25T09:24:54.000Z","user_id":3,"task_id":19},{"id":26,"userId":3,"taskId":19,"isAccepted":1,"isDeleted":0,"createdBy":3,"createdAt":"2019-10-24T05:27:47.000Z","updatedBy":3,"updatedAt":"2019-10-25T09:24:54.000Z","user_id":3,"task_id":19},{"id":27,"userId":3,"taskId":19,"isAccepted":1,"isDeleted":0,"createdBy":3,"createdAt":"2019-10-24T05:53:54.000Z","updatedBy":3,"updatedAt":"2019-10-25T09:24:54.000Z","user_id":3,"task_id":19},{"id":28,"userId":3,"taskId":19,"isAccepted":1,"isDeleted":0,"createdBy":3,"createdAt":"2019-10-24T05:54:10.000Z","updatedBy":3,"updatedAt":"2019-10-25T09:24:54.000Z","user_id":3,"task_id":19}]
         * attachments : ["uploads\\1570858552881-download (1).png","uploads\\1570858552883-download.png","uploads\\1570858552884-download (2).jpg"]
         */

        private int id;
        private String title;
        private String description;
        private String category;
        private String mustHaves;
        private int isRemote;
        private String address;
        private String finishDate;
        private String finishTime;
        private String budgetType;
        private String budget;
        private String taskHours;
        private int taskers;
        private String latitude;
        private String longitude;
        private String status;
        private int isDeleted;
        private int createdBy;
        private String createdAt;
        private int updatedBy;
        private String updatedAt;
        private List<QuotesBean> quotes;
        private List<String> attachments;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getMustHaves() {
            return mustHaves;
        }

        public void setMustHaves(String mustHaves) {
            this.mustHaves = mustHaves;
        }

        public int getIsRemote() {
            return isRemote;
        }

        public void setIsRemote(int isRemote) {
            this.isRemote = isRemote;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getFinishDate() {
            return finishDate;
        }

        public void setFinishDate(String finishDate) {
            this.finishDate = finishDate;
        }

        public String getFinishTime() {
            return finishTime;
        }

        public void setFinishTime(String finishTime) {
            this.finishTime = finishTime;
        }

        public String getBudgetType() {
            return budgetType;
        }

        public void setBudgetType(String budgetType) {
            this.budgetType = budgetType;
        }

        public String getBudget() {
            return budget;
        }

        public void setBudget(String budget) {
            this.budget = budget;
        }

        public String getTaskHours() {
            return taskHours;
        }

        public void setTaskHours(String taskHours) {
            this.taskHours = taskHours;
        }

        public int getTaskers() {
            return taskers;
        }

        public void setTaskers(int taskers) {
            this.taskers = taskers;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public int getIsDeleted() {
            return isDeleted;
        }

        public void setIsDeleted(int isDeleted) {
            this.isDeleted = isDeleted;
        }

        public int getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(int createdBy) {
            this.createdBy = createdBy;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public int getUpdatedBy() {
            return updatedBy;
        }

        public void setUpdatedBy(int updatedBy) {
            this.updatedBy = updatedBy;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public List<QuotesBean> getQuotes() {
            return quotes;
        }

        public void setQuotes(List<QuotesBean> quotes) {
            this.quotes = quotes;
        }

        public List<String> getAttachments() {
            return attachments;
        }

        public void setAttachments(List<String> attachments) {
            this.attachments = attachments;
        }

        public static class QuotesBean {
            /**
             * id : 6
             * userId : 3
             * taskId : 19
             * isAccepted : 1
             * isDeleted : 0
             * createdBy : 3
             * createdAt : 2019-10-16T04:56:59.000Z
             * updatedBy : 3
             * updatedAt : 2019-10-25T09:24:54.000Z
             * user_id : 3
             * task_id : 19
             */

            private int id;
            private int userId;
            private int taskId;
            private int isAccepted;
            private int isDeleted;
            private int createdBy;
            private String createdAt;
            private int updatedBy;
            private String updatedAt;
            private int user_id;
            private int task_id;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getUserId() {
                return userId;
            }

            public void setUserId(int userId) {
                this.userId = userId;
            }

            public int getTaskId() {
                return taskId;
            }

            public void setTaskId(int taskId) {
                this.taskId = taskId;
            }

            public int getIsAccepted() {
                return isAccepted;
            }

            public void setIsAccepted(int isAccepted) {
                this.isAccepted = isAccepted;
            }

            public int getIsDeleted() {
                return isDeleted;
            }

            public void setIsDeleted(int isDeleted) {
                this.isDeleted = isDeleted;
            }

            public int getCreatedBy() {
                return createdBy;
            }

            public void setCreatedBy(int createdBy) {
                this.createdBy = createdBy;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public int getUpdatedBy() {
                return updatedBy;
            }

            public void setUpdatedBy(int updatedBy) {
                this.updatedBy = updatedBy;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public int getUser_id() {
                return user_id;
            }

            public void setUser_id(int user_id) {
                this.user_id = user_id;
            }

            public int getTask_id() {
                return task_id;
            }

            public void setTask_id(int task_id) {
                this.task_id = task_id;
            }
        }
    }
}
