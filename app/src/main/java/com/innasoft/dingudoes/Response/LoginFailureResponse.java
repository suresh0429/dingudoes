package com.innasoft.dingudoes.Response;

import com.google.gson.annotations.SerializedName;

public class LoginFailureResponse {


    /**
     * success : false
     * error : Incorrect Username or password
     * data : null
     */

    @SerializedName("success")
    private boolean success;
    @SerializedName("error")
    private String error;
    @SerializedName("data")
    private Object data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
