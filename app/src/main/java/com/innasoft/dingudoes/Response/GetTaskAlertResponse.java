package com.innasoft.dingudoes.Response;

import java.util.List;

public class GetTaskAlertResponse {


    /**
     * success : true
     * message :
     * data : {"tasksAlerts":[{"id":4,"userId":3,"isRemote":1,"keywords":"testing","location":"Madhapur","distance":"10","isDeleted":0,"createdBy":null,"createdAt":"2019-10-17T06:42:39.000Z","updatedBy":null,"updatedAt":"2019-10-17T06:42:39.000Z"}]}
     */

    private boolean success;
    private String message;
    private DataBean data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        private List<TasksAlertsBean> tasksAlerts;

        public List<TasksAlertsBean> getTasksAlerts() {
            return tasksAlerts;
        }

        public void setTasksAlerts(List<TasksAlertsBean> tasksAlerts) {
            this.tasksAlerts = tasksAlerts;
        }

        public static class TasksAlertsBean {
            /**
             * id : 4
             * userId : 3
             * isRemote : 1
             * keywords : testing
             * location : Madhapur
             * distance : 10
             * isDeleted : 0
             * createdBy : null
             * createdAt : 2019-10-17T06:42:39.000Z
             * updatedBy : null
             * updatedAt : 2019-10-17T06:42:39.000Z
             */

            private int id;
            private int userId;
            private int isRemote;
            private String keywords;
            private String location;
            private String distance;
            private int isDeleted;
            private Object createdBy;
            private String createdAt;
            private Object updatedBy;
            private String updatedAt;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getUserId() {
                return userId;
            }

            public void setUserId(int userId) {
                this.userId = userId;
            }

            public int getIsRemote() {
                return isRemote;
            }

            public void setIsRemote(int isRemote) {
                this.isRemote = isRemote;
            }

            public String getKeywords() {
                return keywords;
            }

            public void setKeywords(String keywords) {
                this.keywords = keywords;
            }

            public String getLocation() {
                return location;
            }

            public void setLocation(String location) {
                this.location = location;
            }

            public String getDistance() {
                return distance;
            }

            public void setDistance(String distance) {
                this.distance = distance;
            }

            public int getIsDeleted() {
                return isDeleted;
            }

            public void setIsDeleted(int isDeleted) {
                this.isDeleted = isDeleted;
            }

            public Object getCreatedBy() {
                return createdBy;
            }

            public void setCreatedBy(Object createdBy) {
                this.createdBy = createdBy;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public Object getUpdatedBy() {
                return updatedBy;
            }

            public void setUpdatedBy(Object updatedBy) {
                this.updatedBy = updatedBy;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }
        }
    }
}
