package com.innasoft.dingudoes.Response;

public class RegisterResponse {


    /**
     * success : true
     * message : user Created Successfully
     * data : {"createdAt":"2019-10-30T12:57:45.135Z","updatedAt":{"val":"CURRENT_TIMESTAMP"},"id":28,"userId":38}
     */

    private boolean success;
    private String message;
    private DataBean data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * createdAt : 2019-10-30T12:57:45.135Z
         * updatedAt : {"val":"CURRENT_TIMESTAMP"}
         * id : 28
         * userId : 38
         */

        private String createdAt;
        private UpdatedAtBean updatedAt;
        private int id;
        private int userId;

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public UpdatedAtBean getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(UpdatedAtBean updatedAt) {
            this.updatedAt = updatedAt;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public static class UpdatedAtBean {
            /**
             * val : CURRENT_TIMESTAMP
             */

            private String val;

            public String getVal() {
                return val;
            }

            public void setVal(String val) {
                this.val = val;
            }
        }
    }
}
