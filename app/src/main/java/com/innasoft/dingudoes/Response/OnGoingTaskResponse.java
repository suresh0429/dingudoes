package com.innasoft.dingudoes.Response;

import java.util.List;

public class OnGoingTaskResponse {


    /**
     * success : true
     * data : [{"id":11,"title":null,"description":null,"category":null,"mustHaves":null,"isRemote":null,"address":null,"finishDate":null,"finishTime":null,"budgetType":null,"budget":null,"taskHours":null,"taskers":null,"latitude":null,"longitude":null,"status":"assigned","isDeleted":0,"createdBy":null,"createdAt":"2019-10-09T04:07:07.000Z","updatedBy":null,"updatedAt":"2019-10-09T04:07:07.000Z","quote":{"id":3,"userId":1,"taskId":11,"isAccepted":1,"isDeleted":0,"createdBy":null,"createdAt":"2019-10-09T04:08:28.000Z","updatedBy":null,"updatedAt":"2019-10-09T04:11:42.000Z","task_id":11}}]
     */

    private boolean success;
    private List<DataBean> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 11
         * title : null
         * description : null
         * category : null
         * mustHaves : null
         * isRemote : null
         * address : null
         * finishDate : null
         * finishTime : null
         * budgetType : null
         * budget : null
         * taskHours : null
         * taskers : null
         * latitude : null
         * longitude : null
         * status : assigned
         * isDeleted : 0
         * createdBy : null
         * createdAt : 2019-10-09T04:07:07.000Z
         * updatedBy : null
         * updatedAt : 2019-10-09T04:07:07.000Z
         * quote : {"id":3,"userId":1,"taskId":11,"isAccepted":1,"isDeleted":0,"createdBy":null,"createdAt":"2019-10-09T04:08:28.000Z","updatedBy":null,"updatedAt":"2019-10-09T04:11:42.000Z","task_id":11}
         */

        private int id;
        private String title;
        private String description;
        private String category;
        private String mustHaves;
        private String isRemote;
        private String address;
        private String finishDate;
        private String finishTime;
        private String budgetType;
        private String budget;
        private String taskHours;
        private String taskers;
        private String latitude;
        private String longitude;
        private String status;
        private int isDeleted;
        private String createdBy;
        private String createdAt;
        private Object updatedBy;
        private String updatedAt;
        private QuoteBean quote;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getMustHaves() {
            return mustHaves;
        }

        public void setMustHaves(String mustHaves) {
            this.mustHaves = mustHaves;
        }

        public String getIsRemote() {
            return isRemote;
        }

        public void setIsRemote(String isRemote) {
            this.isRemote = isRemote;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getFinishDate() {
            return finishDate;
        }

        public void setFinishDate(String finishDate) {
            this.finishDate = finishDate;
        }

        public String getFinishTime() {
            return finishTime;
        }

        public void setFinishTime(String finishTime) {
            this.finishTime = finishTime;
        }

        public String getBudgetType() {
            return budgetType;
        }

        public void setBudgetType(String budgetType) {
            this.budgetType = budgetType;
        }

        public String getBudget() {
            return budget;
        }

        public void setBudget(String budget) {
            this.budget = budget;
        }

        public String getTaskHours() {
            return taskHours;
        }

        public void setTaskHours(String taskHours) {
            this.taskHours = taskHours;
        }

        public String getTaskers() {
            return taskers;
        }

        public void setTaskers(String taskers) {
            this.taskers = taskers;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public int getIsDeleted() {
            return isDeleted;
        }

        public void setIsDeleted(int isDeleted) {
            this.isDeleted = isDeleted;
        }

        public String getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(String createdBy) {
            this.createdBy = createdBy;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public Object getUpdatedBy() {
            return updatedBy;
        }

        public void setUpdatedBy(Object updatedBy) {
            this.updatedBy = updatedBy;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }

        public QuoteBean getQuote() {
            return quote;
        }

        public void setQuote(QuoteBean quote) {
            this.quote = quote;
        }

        public static class QuoteBean {
            /**
             * id : 3
             * userId : 1
             * taskId : 11
             * isAccepted : 1
             * isDeleted : 0
             * createdBy : null
             * createdAt : 2019-10-09T04:08:28.000Z
             * updatedBy : null
             * updatedAt : 2019-10-09T04:11:42.000Z
             * task_id : 11
             */

            private int id;
            private int userId;
            private int taskId;
            private int isAccepted;
            private int isDeleted;
            private Object createdBy;
            private String createdAt;
            private Object updatedBy;
            private String updatedAt;
            private int task_id;

            public int getId() {
                return id;
            }

            public void setId(int id) {
                this.id = id;
            }

            public int getUserId() {
                return userId;
            }

            public void setUserId(int userId) {
                this.userId = userId;
            }

            public int getTaskId() {
                return taskId;
            }

            public void setTaskId(int taskId) {
                this.taskId = taskId;
            }

            public int getIsAccepted() {
                return isAccepted;
            }

            public void setIsAccepted(int isAccepted) {
                this.isAccepted = isAccepted;
            }

            public int getIsDeleted() {
                return isDeleted;
            }

            public void setIsDeleted(int isDeleted) {
                this.isDeleted = isDeleted;
            }

            public Object getCreatedBy() {
                return createdBy;
            }

            public void setCreatedBy(Object createdBy) {
                this.createdBy = createdBy;
            }

            public String getCreatedAt() {
                return createdAt;
            }

            public void setCreatedAt(String createdAt) {
                this.createdAt = createdAt;
            }

            public Object getUpdatedBy() {
                return updatedBy;
            }

            public void setUpdatedBy(Object updatedBy) {
                this.updatedBy = updatedBy;
            }

            public String getUpdatedAt() {
                return updatedAt;
            }

            public void setUpdatedAt(String updatedAt) {
                this.updatedAt = updatedAt;
            }

            public int getTask_id() {
                return task_id;
            }

            public void setTask_id(int task_id) {
                this.task_id = task_id;
            }
        }
    }
}
