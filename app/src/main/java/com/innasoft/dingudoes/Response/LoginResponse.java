package com.innasoft.dingudoes.Response;

public class LoginResponse {


    /**
     * success : true
     * message : Authentication successful!
     * token : eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VybmFtZSI6ImdhbmVzaGNoaW50dTgwMEBnbWFpbC5jb20iLCJ1c2VyX2lkIjozLCJpYXQiOjE1NzExMTY2NjYsImV4cCI6MTU3MTIwMzA2Nn0.jnlp9sqRjmq032kO-APeoK1qjZ6iizcBx5Tg_gbj_dI
     * data : {"email":"ganeshchintu800@gmail.com","phoneNumber":"8801200610","fullName":"ganesh","userId":3}
     */

    private boolean success;
    private String message;
    private String token;
    private DataBean data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * email : ganeshchintu800@gmail.com
         * phoneNumber : 8801200610
         * fullName : ganesh
         * userId : 3
         */

        private String email;
        private String phoneNumber;
        private String fullName;
        private int userId;
        private String referer_code;

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPhoneNumber() {
            return phoneNumber;
        }

        public void setPhoneNumber(String phoneNumber) {
            this.phoneNumber = phoneNumber;
        }

        public String getFullName() {
            return fullName;
        }

        public void setFullName(String fullName) {
            this.fullName = fullName;
        }

        public int getUserId() {
            return userId;
        }

        public void setUserId(int userId) {
            this.userId = userId;
        }

        public String getReferer_code() {
            return referer_code;
        }

        public void setReferer_code(String referer_code) {
            this.referer_code = referer_code;
        }
    }
}
