package com.innasoft.dingudoes.Response;

import java.util.List;

public class TermsConditionsResponse {


    /**
     * success : true
     * data : [{"lookupId":1,"lookupKey":"termsAndConditions","lookupValue":"Lorem Ipsum Dummy Contectbt","createdAt":null,"updatedBy":1,"updatedAt":"2019-10-22T01:59:00.000Z"}]
     */

    private boolean success;
    private List<DataBean> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * lookupId : 1
         * lookupKey : termsAndConditions
         * lookupValue : Lorem Ipsum Dummy Contectbt
         * createdAt : null
         * updatedBy : 1
         * updatedAt : 2019-10-22T01:59:00.000Z
         */

        private int lookupId;
        private String lookupKey;
        private String lookupValue;
        private Object createdAt;
        private int updatedBy;
        private String updatedAt;

        public int getLookupId() {
            return lookupId;
        }

        public void setLookupId(int lookupId) {
            this.lookupId = lookupId;
        }

        public String getLookupKey() {
            return lookupKey;
        }

        public void setLookupKey(String lookupKey) {
            this.lookupKey = lookupKey;
        }

        public String getLookupValue() {
            return lookupValue;
        }

        public void setLookupValue(String lookupValue) {
            this.lookupValue = lookupValue;
        }

        public Object getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(Object createdAt) {
            this.createdAt = createdAt;
        }

        public int getUpdatedBy() {
            return updatedBy;
        }

        public void setUpdatedBy(int updatedBy) {
            this.updatedBy = updatedBy;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }
    }
}
