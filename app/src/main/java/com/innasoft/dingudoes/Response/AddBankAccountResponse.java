package com.innasoft.dingudoes.Response;

public class AddBankAccountResponse {


    /**
     * success : true
     * message : Beneficiary added successfully
     */

    private boolean success;
    private String message;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
