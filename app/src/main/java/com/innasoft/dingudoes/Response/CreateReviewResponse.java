package com.innasoft.dingudoes.Response;

public class CreateReviewResponse {


    /**
     * success : true
     * message : Review created successfully
     * data : {"createdAt":"2019-11-05T11:20:29.097Z","updatedAt":{"val":"CURRENT_TIMESTAMP"},"id":6,"userId":"3","taskId":"3","reviewText":"testing","rating":"3.5","isDeleted":0,"createdBy":34,"updatedBy":34}
     */

    private boolean success;
    private String message;
    private DataBean data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * createdAt : 2019-11-05T11:20:29.097Z
         * updatedAt : {"val":"CURRENT_TIMESTAMP"}
         * id : 6
         * userId : 3
         * taskId : 3
         * reviewText : testing
         * rating : 3.5
         * isDeleted : 0
         * createdBy : 34
         * updatedBy : 34
         */

        private String createdAt;
        private UpdatedAtBean updatedAt;
        private int id;
        private String userId;
        private String taskId;
        private String reviewText;
        private String rating;
        private int isDeleted;
        private int createdBy;
        private int updatedBy;

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public UpdatedAtBean getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(UpdatedAtBean updatedAt) {
            this.updatedAt = updatedAt;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getUserId() {
            return userId;
        }

        public void setUserId(String userId) {
            this.userId = userId;
        }

        public String getTaskId() {
            return taskId;
        }

        public void setTaskId(String taskId) {
            this.taskId = taskId;
        }

        public String getReviewText() {
            return reviewText;
        }

        public void setReviewText(String reviewText) {
            this.reviewText = reviewText;
        }

        public String getRating() {
            return rating;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }

        public int getIsDeleted() {
            return isDeleted;
        }

        public void setIsDeleted(int isDeleted) {
            this.isDeleted = isDeleted;
        }

        public int getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(int createdBy) {
            this.createdBy = createdBy;
        }

        public int getUpdatedBy() {
            return updatedBy;
        }

        public void setUpdatedBy(int updatedBy) {
            this.updatedBy = updatedBy;
        }

        public static class UpdatedAtBean {
            /**
             * val : CURRENT_TIMESTAMP
             */

            private String val;

            public String getVal() {
                return val;
            }

            public void setVal(String val) {
                this.val = val;
            }
        }
    }
}
