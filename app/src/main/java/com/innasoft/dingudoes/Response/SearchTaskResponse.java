package com.innasoft.dingudoes.Response;

import java.io.Serializable;
import java.util.List;

public class SearchTaskResponse {


    /**
     * success : true
     * data : [{"id":19,"title":"Dev Test 1","description":"Dev testing 1","category":"delivery","mustHaves":"['Must have 1','Must have 2','Must have 3']","isRemote":1,"address":"Madhapur","finishDate":"2019-08-22","finishTime":"6","budgetType":"Total","budget":"100","taskHours":"4","taskers":2,"latitude":"17.4486","longitude":"78.3908","status":"open","isDeleted":0,"createdBy":1,"createdAt":"2019-10-12T05:35:53.000Z","updatedBy":1,"updatedAt":"2019-10-12T05:35:53.000Z"},{"id":20,"title":"testing dev fin","description":"testing file upload","category":"delivery","mustHaves":"['Must have 1','Must have 2','Must have 3']","isRemote":1,"address":"Madhapur","finishDate":"2019-08-22","finishTime":"6","budgetType":"Total","budget":"100","taskHours":"4","taskers":2,"latitude":"17.4486","longitude":"78.3908","status":"open","isDeleted":0,"createdBy":1,"createdAt":"2019-10-12T08:28:21.000Z","updatedBy":1,"updatedAt":"2019-10-12T08:28:21.000Z"},{"id":21,"title":"testing dev fin","description":"testing file upload","category":"delivery","mustHaves":"['Must have 1','Must have 2','Must have 3']","isRemote":1,"address":"Madhapur","finishDate":"2019-08-22","finishTime":"6","budgetType":"Total","budget":"100","taskHours":"4","taskers":2,"latitude":"17.4486","longitude":"78.3908","status":"open","isDeleted":0,"createdBy":1,"createdAt":"2019-10-14T07:09:34.000Z","updatedBy":1,"updatedAt":"2019-10-14T07:09:34.000Z"},{"id":22,"title":"testing dev fin","description":"testing file upload","category":"delivery","mustHaves":"['Must have 1','Must have 2','Must have 3']","isRemote":1,"address":"Madhapur","finishDate":"2019-08-22","finishTime":"6","budgetType":"Total","budget":"100","taskHours":"4","taskers":2,"latitude":"17.4486","longitude":"78.3908","status":"open","isDeleted":0,"createdBy":3,"createdAt":"2019-10-14T07:11:52.000Z","updatedBy":3,"updatedAt":"2019-10-14T07:11:52.000Z"}]
     */

    private boolean success;
    private List<DataBean> data;

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 19
         * title : Dev Test 1
         * description : Dev testing 1
         * category : delivery
         * mustHaves : ['Must have 1','Must have 2','Must have 3']
         * isRemote : 1
         * address : Madhapur
         * finishDate : 2019-08-22
         * finishTime : 6
         * budgetType : Total
         * budget : 100
         * taskHours : 4
         * taskers : 2
         * latitude : 17.4486
         * longitude : 78.3908
         * status : open
         * isDeleted : 0
         * createdBy : 1
         * createdAt : 2019-10-12T05:35:53.000Z
         * updatedBy : 1
         * updatedAt : 2019-10-12T05:35:53.000Z
         */

        private int id;
        private String title;
        private String description;
        private String category;
        private String mustHaves;
        private int isRemote;
        private String address;
        private String finishDate;
        private String finishTime;
        private String budgetType;
        private String budget;
        private String taskHours;
        private int taskers;
        private String latitude;
        private String longitude;
        private String status;
        private int isDeleted;
        private int createdBy;
        private String createdAt;
        private int updatedBy;
        private String updatedAt;

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getCategory() {
            return category;
        }

        public void setCategory(String category) {
            this.category = category;
        }

        public String getMustHaves() {
            return mustHaves;
        }

        public void setMustHaves(String mustHaves) {
            this.mustHaves = mustHaves;
        }

        public int getIsRemote() {
            return isRemote;
        }

        public void setIsRemote(int isRemote) {
            this.isRemote = isRemote;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getFinishDate() {
            return finishDate;
        }

        public void setFinishDate(String finishDate) {
            this.finishDate = finishDate;
        }

        public String getFinishTime() {
            return finishTime;
        }

        public void setFinishTime(String finishTime) {
            this.finishTime = finishTime;
        }

        public String getBudgetType() {
            return budgetType;
        }

        public void setBudgetType(String budgetType) {
            this.budgetType = budgetType;
        }

        public String getBudget() {
            return budget;
        }

        public void setBudget(String budget) {
            this.budget = budget;
        }

        public String getTaskHours() {
            return taskHours;
        }

        public void setTaskHours(String taskHours) {
            this.taskHours = taskHours;
        }

        public int getTaskers() {
            return taskers;
        }

        public void setTaskers(int taskers) {
            this.taskers = taskers;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public int getIsDeleted() {
            return isDeleted;
        }

        public void setIsDeleted(int isDeleted) {
            this.isDeleted = isDeleted;
        }

        public int getCreatedBy() {
            return createdBy;
        }

        public void setCreatedBy(int createdBy) {
            this.createdBy = createdBy;
        }

        public String getCreatedAt() {
            return createdAt;
        }

        public void setCreatedAt(String createdAt) {
            this.createdAt = createdAt;
        }

        public int getUpdatedBy() {
            return updatedBy;
        }

        public void setUpdatedBy(int updatedBy) {
            this.updatedBy = updatedBy;
        }

        public String getUpdatedAt() {
            return updatedAt;
        }

        public void setUpdatedAt(String updatedAt) {
            this.updatedAt = updatedAt;
        }
    }
}
